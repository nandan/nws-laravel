<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('index');
});


// AuthController

Route::post('/auth/login', array('as' => 'auth_login', 'uses' => 'AuthController@run_login'));

Route::post('/auth/modal_login', array('as' => 'auth_modal_login', 'uses' => 'AuthController@run_modal_login'));

Route::get('/auth/logout', array('as' => 'auth_logout', 'uses' => 'AuthController@run_logout'));

Route::post('/auth/register', array('as' => 'auth_register', 'uses' => 'AuthController@run_register'));

Route::post('/auth/modal_register', array('as' => 'auth_modal_register', 'uses' => 'AuthController@run_modal_register'));

Route::post('/auth/modal_register_login', array('as' => 'auth_modal_register_login', 'uses' => 'AuthController@run_modal_register_login'));

Route::post('/auth/express_login', array('as' => 'run_express_login', 'uses' => 'AuthController@run_express_login'));


// Search
// Search Result

Route::get('/search/result', array('as' => 'show_search_result', 'uses' => 'SearchController@show_search_result'));

Route::post('/search/booking_modal', array('as' => 'search_booking_modal', 'uses' => 'SearchController@run_search_modal'));


// Booking
// Create Booking
Route::post('/booking/new', array('as' => 'create_booking', 'uses' => 'BookingsController@run_store_booking'));


//ControlCenterController

// General

Route::get('/controlcenter', array('as' => 'controlcenter', 'uses' => 'ControlCenterController@show_controlcenter'));


//EXPRESS CALENDAR

Route::get('/express/calendar', array('as' => 'show_express_calendar', 'uses' => 'ControlCenterController@show_express_calendar'));

Route::post('/express/init_express_calendar', array('as' => 'init_express_calendar', 'uses' => 'ControlCenterController@init_express_calendar'));

Route::post('/express/longpolling', array('as' => 'longpolling_express_calendar', 'uses' => 'ControlCenterController@longpolling_express_calendar'));

Route::get('/express/test', array('as' => 'express_test', 'uses' => 'ControlCenterController@express_test'));

Route::post('/express/booking_details', array('as' => 'express_booking_details', 'uses' => 'ControlCenterController@express_booking_details'));

// List your Workspace

Route::get('/listworkspace', array('as' => 'listworkspace', 'uses' => 'ControlCenterController@show_list_your_workspace'));

Route::post('/run_list_your_workspace', array('as' => 'run_list_your_workspace', 'uses' => 'ControlCenterController@run_list_your_workspace'));


// Customer

Route::get('/controlcenter/bookings', array('as' => 'controlcenter_bookings', 'uses' => 'ControlCenterController@show_bookings'));

Route::get('/controlcenter/settings', array('as' => 'controlcenter_settings', 'uses' => 'ControlCenterController@show_settings'));

Route::post('/controlcenter/run_personal_information', array('as' => 'run_personal_information', 'uses' => 'ControlCenterController@run_personal_information'));

Route::post('/controlcenter/run_profile_picture', array('as' => 'run_profile_picture', 'uses' => 'ControlCenterController@run_profile_picture'));

Route::post('/controlcenter/run_delete_profile_picture', array('as' => 'run_delete_profile_picture', 'uses' => 'ControlCenterController@run_delete_profile_picture'));

Route::post('/controlcenter/run_localization', array('as' => 'run_localization', 'uses' => 'ControlCenterController@run_localization'));

Route::post('/controlcenter/show_company_calendar_ajax', array('as' => 'show_company_calendar_ajax', 'uses' => 'ControlCenterController@show_company_calendar_ajax'));

Route::post('/controlcenter/example_calendar_events', array('as' => 'example_calendar_events', 'uses' => 'ControlCenterController@example_calendar_events'));




/* Company */
// Sidebar Links
Route::get('/controlcenter/company_bookings', array('as' => 'controlcenter_company_bookings', 'uses' => 'ControlCenterController@show_company_bookings'));

Route::get('/controlcenter/company_venues', array('as' => 'controlcenter_company_venues', 'uses' => 'ControlCenterController@show_company_venues'));

Route::get('/controlcenter/company_financials', array('as' => 'controlcenter_company_financials', 'uses' => 'ControlCenterController@show_company_financials'));

Route::get('/controlcenter/company_settings', array('as' => 'controlcenter_company_settings', 'uses' => 'ControlCenterController@show_company_settings'));
// Real action links
Route::get('/controlcenter/edit_company/{id}', array('as' => 'controlcenter_edit_company', 'uses' => 'ControlCenterController@show_edit_company'));

Route::post('/controlcenter/update_company/{id}', array('as' => 'controlcenter_update_company', 'uses' => 'ControlCenterController@run_update_company'));

/* Venue */
// Sidebar Links
Route::get('/controlcenter/venue_bookings/{id}', array('as' => 'controlcenter_venue_bookings', 'uses' => 'ControlCenterController@show_venue_bookings'));

Route::get('/controlcenter/venue_calendar', array('as' => 'controlcenter_venue_calendar', 'uses' => 'ControlCenterController@show_venue_calendar'));

Route::get('/controlcenter/venue_rooms/{id}', array('as' => 'controlcenter_venue_rooms', 'uses' => 'ControlCenterController@show_venue_rooms'));

Route::get('/controlcenter/venue_financials', array('as' => 'controlcenter_venue_financials', 'uses' => 'ControlCenterController@show_venue_financials'));

Route::get('/controlcenter/venue_settings/{id}', array('as' => 'controlcenter_venue_settings', 'uses' => 'ControlCenterController@show_venue_settings'));
// Real action links
Route::get('/controlcenter/create_venue', array('as' => 'controlcenter_create_venue', 'uses' => 'ControlCenterController@show_create_venue'));

Route::post('/controlcenter/store_venue', array('as' => 'controlcenter_store_venue', 'uses' => 'ControlCenterController@run_store_venue'));

Route::get('/controlcenter/edit_venue/{id}', array('as' => 'controlcenter_edit_venue', 'uses' => 'ControlCenterController@show_edit_venue'));

Route::get('/controlcenter/edit_venue_calendar/{id}', array('as' => 'controlcenter_edit_venue_calendar', 'uses' => 'ControlCenterController@show_edit_venue_calendar'));

Route::get('/controlcenter/edit_venue_exception_rules/{id}', array('as' => 'controlcenter_edit_venue_exception_rules', 'uses' => 'ControlCenterController@show_edit_venue_exception_rules'));

Route::get('/controlcenter/edit_venue_user_management/{id}', array('as' => 'controlcenter_edit_venue_user_management', 'uses' => 'ControlCenterController@show_edit_venue_user_management'));

Route::post('/controlcenter/update_venue/{id}', array('as' => 'controlcenter_update_venue', 'uses' => 'ControlCenterController@run_update_venue'));

Route::post('/controlcenter/update_venue_calendar/{id}', array('as' => 'controlcenter_update_venue_calendar', 'uses' => 'ControlCenterController@run_update_venue_calendar'));

// This adds new exception rules to venues
Route::post('/controlcenter/update_venue_exception_rules/{id}', array('as' => 'controlcenter_update_venue_exception_rules', 'uses' => 'ControlCenterController@run_update_venue_exception_rules'));

// Single events on Venue Exception Rules page
// View
Route::get('/controlcenter/view_venue_single_exception_rule/{id}', array('as' => 'controlcenter_view_venue_single_exception_rule', 'uses' => 'ControlCenterController@show_view_venue_single_exception_rule'));
// Edit
Route::get('/controlcenter/edit_venue_single_exception_rule/{id}', array('as' => 'controlcenter_edit_venue_single_exception_rule', 'uses' => 'ControlCenterController@show_edit_venue_single_exception_rule'));
// Update
Route::post('/controlcenter/update_venue_single_exception_rule/{id}', array('as' => 'controlcenter_update_venue_single_exception_rule', 'uses' => 'ControlCenterController@run_update_venue_single_exception_rule'));
// Remove
Route::post('/controlcenter/remove_venue_single_exception_rule/{id}', array('as' => 'controlcenter_remove_venue_single_exception_rule', 'uses' => 'ControlCenterController@run_remove_venue_single_exception_rule'));

// Single Venue Details Page
Route::get('/venues/show_venue/{id}', array('as' => 'show_venue_details', 'uses' => 'VenuesController@show_venue'));

/* Rooms */
// Sidebar Links
Route::get('/controlcenter/room_bookings/{id}', array('as' => 'controlcenter_room_bookings', 'uses' => 'ControlCenterController@show_room_bookings'));

Route::get('/controlcenter/room_financials', array('as' => 'controlcenter_room_financials', 'uses' => 'ControlCenterController@show_room_financials'));

Route::get('/controlcenter/room_settings/{id}', array('as' => 'controlcenter_room_settings', 'uses' => 'ControlCenterController@show_room_settings'));
//Real action links
Route::get('/controlcenter/create_room/{id}', array('as' => 'controlcenter_create_room', 'uses' => 'ControlCenterController@show_create_room'));

Route::post('/controlcenter/store_room/{id}', array('as' => 'controlcenter_store_room', 'uses' => 'ControlCenterController@run_store_room'));

Route::get('/controlcenter/edit_room/{id}', array('as' => 'controlcenter_edit_room', 'uses' => 'ControlCenterController@show_edit_room'));

Route::post('/controlcenter/update_room/{id}', array('as' => 'controlcenter_update_room', 'uses' => 'ControlCenterController@run_update_room'));

// Single Room Details Page
Route::get('/rooms/show_room/{id}', array('as' => 'show_room_details', 'uses' => 'RoomsController@show_room'));

// WIZARD for new business center owners to add company, venue and offices
// Show Create Wizard
Route::get('/wizard/create', array('as' => 'create_wizard', 'uses' => 'ControlCenterController@show_create_wizard'));


// Submit Create Wizard
Route::post('/wizard/store', array('as' => 'store_wizard', 'uses' => 'ControlCenterController@run_store_wizard'));


/* Reservations */
// Approval from Workspace Owner
Route::get('/reservation/approve/{code}', array('as' => 'approve_reservation', 'uses' => 'BookingsController@run_approve_booking'));
// Rejection from Workspace Owner
Route::get('/reservation/reject/{code}', array('as' => 'reject_reservation', 'uses' => 'BookingsController@run_reject_booking'));
// Show Reservation details
Route::get('/reservation/{code}', array('as' => 'show_reservation', 'uses' => 'BookingsController@show_booking_details'));


// Caldav Controller

Route::get('/caldav/installed', array('as' => 'caldav_installed', 'uses' => 'CaldavController@installed'));

Route::any('/caldav/server', array('as' => 'caldav_server', 'uses' => 'CaldavController@server'));

Route::any('/caldav/server/principals/{any}', array('as' => 'caldav_server2', 'uses' => 'CaldavController@server'));
Route::any('/caldav/server/principals/{any}/{all}', array('as' => 'caldav_server3', 'uses' => 'CaldavController@server'));

Route::any('/caldav/server/calendars/{any}', array('as' => 'caldav_server4', 'uses' => 'CaldavController@server'));
Route::any('/caldav/server/calendars/{any}/{all}', array('as' => 'caldav_server5', 'uses' => 'CaldavController@server'));

Route::any('/caldav/server/calendars', array('as' => 'caldav_server6', 'uses' => 'CaldavController@server'));
Route::any('/caldav/server/principals', array('as' => 'caldav_server7', 'uses' => 'CaldavController@server'));



Route::get('/caldav/hash', array('as' => 'caldav_hash', 'uses' => 'CaldavController@hash'));
