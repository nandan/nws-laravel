<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_prices', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('room_id');
			$table->string('price_original');
			$table->string('price_period');
			$table->string('tax_flat');
			$table->string('tax_percentage');
			$table->string('price_retail');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_prices');
	}

}
