<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVenueExceptionRuleEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venue_exception_rule_events', function(Blueprint $table) {
			$table->increments('id');
			$table->datetime('dtstart');
			$table->datetime('dtend');
			$table->datetime('until');
			$table->string('title');
			$table->string('freq');
			$table->integer('interval');
			$table->integer('count');
			$table->string('wkst');
			$table->string('byday');
			$table->string('bymonth');
			$table->string('bymonthday');
			$table->string('byyearday');
			$table->integer('timezone_id');
			$table->integer('venue_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venue_exception_rule_events');
	}

}
