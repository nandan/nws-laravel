<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomOpeningHoursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_opening_hours', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('room_id');
			$table->integer('day_index');
			$table->integer('start_time');
			$table->integer('end_time');
			$table->string('timezone');
			$table->string('closed');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_opening_hours');
	}

}
