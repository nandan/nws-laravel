<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rooms', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('venue_id');
			$table->string('room_name');
			$table->integer('room_types_id');
			$table->text('description');
			$table->integer('stars');
			$table->integer('amenities_id');
			$table->string('max_capacity');
			$table->string('preparation_time');
			$table->string('pre_booking_period');
			$table->string('buffer_time');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rooms');
	}

}
