<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerBookingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_bookings', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->integer('room_id');
			$table->integer('venue_id');
			$table->datetime('start_time');
			$table->datetime('end_time');
			$table->string('title');
			$table->string('transaction_id');
			$table->integer('status_id');
			$table->text('comment');
			$table->text('request_amenities');
			$table->integer('currency_id');
			$table->string('price_orig');
			$table->string('price_tax');
			$table->string('price_final');
			$table->string('timezone');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_bookings');
	}

}
