<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddStripeToCustomerDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('customer_details', function(Blueprint $table) {
			$table->string('stripe_customer_id');
			$table->string('last_CC');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('customer_details', function(Blueprint $table) {
			$table->dropColumn('stripe_customer_id');
			$table->dropColumn('last_CC');
		});
	}

}
