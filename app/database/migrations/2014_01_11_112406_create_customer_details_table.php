<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer_details', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('company');
			$table->string('gender');
			$table->string('title');
			$table->integer('country_id')->nullable();
			$table->integer('state_id')->nullable();
			$table->integer('city_id')->nullable();
			$table->string('phone_office');
			$table->string('phone_mobile');
			$table->integer('photo_id')->nullable();
			$table->string('used_credit_card')->default('0');
			$table->string('braintree_customer_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer_details');
	}

}
