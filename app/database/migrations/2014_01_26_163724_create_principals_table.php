<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePrincipalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('principals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('uri');
			$table->string('email');
			$table->string('displayname');
			$table->string('vcardurl');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('principals');
	}

}
