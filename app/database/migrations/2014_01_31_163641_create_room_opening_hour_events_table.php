<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoomOpeningHourEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('room_opening_hour_events', function(Blueprint $table) {
			$table->increments('id');
			$table->datetime('dtstart');
			$table->datetime('dtend');
			$table->datetime('until');
			$table->string('title');
			$table->string('freq');
			$table->integer('interval');
			$table->integer('count');
			$table->string('wkst');
			$table->string('byday');
			$table->string('bymonth');
			$table->string('bymonthday');
			$table->string('byyearday');
			$table->integer('timezone_id');
			$table->integer('venue_id');
			$table->integer('room_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('room_opening_hour_events');
	}

}
