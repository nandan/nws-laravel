<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVenuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venues', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('company_id');
			$table->string('venue_name');
			$table->text('description');
			$table->string('email');
			$table->integer('country_id');
			$table->integer('state_id');
			$table->integer('city_id');
			$table->string('postal_code');
			$table->string('street_name');
			$table->string('street_number');
			$table->string('floor');
			$table->string('contact_person');
			$table->string('phone_office');
			$table->string('fax_office');
			$table->string('website_url');
			$table->string('timezone');
			$table->integer('logo_img_id');
			$table->integer('cover_img_id');
			$table->string('slug_url');
			$table->string('facebook_url');
			$table->string('twitter_url');
			$table->string('linkedin_url');
			$table->string('googleplus_url');
			$table->string('lat');
			$table->string('lng');
			$table->integer('stars');
			$table->text('arrival_info');
			$table->string('approval_status');
			$table->string('opening_hours');
			$table->string('username');
			$table->string('digesta1');
			$table->integer('calendar_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venues');
	}

}
