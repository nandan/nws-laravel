<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarEventsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendar_events', function(Blueprint $table) {
			$table->increments('id');
			$table->datetime('dtstart');
			$table->datetime('dtend');
			$table->datetime('until');
			$table->string('title');
			$table->string('freq');
			$table->integer('interval');
			$table->integer('count');
			$table->string('wkst');
			$table->string('byday');
			$table->string('bymonthday');
			$table->string('byyearday');
			$table->integer('venue_id');
			$table->integer('room_id');
			$table->integer('timezone_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendar_events');
	}

}
