<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAmenitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('amenities', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('room_id');
			$table->string('public_wifi')->default('-1');
			$table->string('secure_wifi')->default('-1');
			$table->string('wired_internet')->default('-1');
			$table->string('flatscreen')->default('-1');
			$table->string('video_conferencing')->default('-1');
			$table->string('flipboard')->default('-1');
			$table->string('whiteboard')->default('-1');
			$table->string('projector')->default('-1');
			$table->string('print_scan_copy')->default('-1');
			$table->string('shared_kitchen')->default('-1');
			$table->string('catering')->default('-1');
			$table->string('concierge_service')->default('-1');
			$table->string('filtered_water')->default('-1');
			$table->string('phone_room')->default('-1');
			$table->string('pet_friendly')->default('-1');
			$table->string('handicap_accessible')->default('-1');
			$table->string('conference_phone')->default('-1');
			$table->string('outdoor_space')->default('-1');
			$table->string('on-site_restaurant')->default('-1');
			$table->string('coffee_tea')->default('-1');
			$table->string('notary_service')->default('-1');
			$table->string('shower_facility')->default('-1');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('amenities');
	}

}
