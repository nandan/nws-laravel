<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendars', function(Blueprint $table) {
			$table->increments('id');
			$table->string(' principaluri')->nullable();
			$table->string('displayname')->nullable();
			$table->string('uri')->nullable();
			$table->integer('ctag')->default('0')->unsigned();
			$table->text('description')->nullable();
			$table->integer('calendarorder')->default('0')->unsigned();
			$table->string('calendarcolor')->nullable();
			$table->text('timezone')->nullable();
			$table->string('components')->nullable();
			$table->smallInteger('transparent')->default('0');
			$table->room_id('integer');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendars');
	}

}
