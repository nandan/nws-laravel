<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCalendarobjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendarobjects', function(Blueprint $table) {
			$table->increments('id');
			$table->binary('calendardata')->nullable();
			$table->string('uri')->nullable();
			$table->integer('calendarid');
			$table->integer('lastmodified')->nullable();
			$table->string('etag')->nullable();
			$table->integer('size')->unsigned();
			$table->string('componenttype')->nullable();
			$table->integer('firstoccurence')->nullable();
			$table->integer('lastoccurence')->nullable();
			$table->integer('our_object');
			$table->string('synced')->default('0');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('calendarobjects');
	}

}
