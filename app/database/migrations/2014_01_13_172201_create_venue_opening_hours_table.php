<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVenueOpeningHoursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('venue_opening_hours', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('venue_id');
			$table->integer('day_index');
			$table->integer('start_time');
			$table->integer('end_time');
			$table->string('timezone');
			$table->string('closed');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('venue_opening_hours');
	}

}
