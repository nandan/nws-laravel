<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('company_name');
			$table->string('country');
			$table->string('state');
			$table->string('city');
			$table->string('postal_code');
			$table->string('street_name');
			$table->string('floor');
			$table->string('contact_person');
			$table->string('phone_office');
			$table->string('fax_office');
			$table->string('email_office');
			$table->string('website_url');
			$table->integer('logo_img_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('companies');
	}

}
