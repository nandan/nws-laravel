<?php

class Room_typesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		 DB::table('room_types')->truncate();

		$room_types = array(
			'room_type_name' => 'office',
			'created_at' => new DateTime,
			'updated_at' => new DateTime,
		);

		DB::table('room_types')->insert($room_types);


		$room_types = array(
			'room_type_name' => 'meetingroom',
			'created_at' => new DateTime,
			'updated_at' => new DateTime,
		);

		DB::table('room_types')->insert($room_types);

		$room_types = array(
			'room_type_name' => 'desk',
			'created_at' => new DateTime,
			'updated_at' => new DateTime,
		);

		DB::table('room_types')->insert($room_types);

		$room_types = array(
			'room_type_name' => 'trainingroom',
			'created_at' => new DateTime,
			'updated_at' => new DateTime,
		);

		DB::table('room_types')->insert($room_types);


		$room_types = array(
			'room_type_name' => 'hotellobby',
			'created_at' => new DateTime,
			'updated_at' => new DateTime,
		);

		DB::table('room_types')->insert($room_types);








	}

}
