<?php

class Venue_exception_rule_eventsTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('venue_exception_rule_events')->truncate();

		$venue_exception_rule_events = array(
			array('dtstart' => '2014-02-26 00:00:00', 'dtend' => '2014-02-28 00:00:00', 'title' => 'Vacation', 'freq' => 'daily',
				'byday' => '', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '35'),
			array('dtstart' => '2014-03-10 13:00:00', 'dtend' => '2014-03-12 15:00:00', 'title' => 'Repairing', 'freq' => 'daily',
				'byday' => '', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '35'),
			array('dtstart' => '2014-02-18 10:00:00', 'dtend' => '2014-03-18 12:00:00', 'title' => 'Putzplan', 'freq' => 'weekly',
				'byday' => 'TU', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '35'),
			array('dtstart' => '2014-02-10 16:00:00', 'dtend' => '2014-06-10 18:00:00', 'title' => 'Inspection', 'freq' => 'monthly',
				'byday' => '1FR', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '35'),
			array('dtstart' => '2014-02-24 00:00:00', 'dtend' => '2014-02-25 00:00:00', 'title' => 'Off days', 'freq' => 'daily',
				'byday' => '', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '36'),
			array('dtstart' => '2014-02-26 16:00:00', 'dtend' => '2014-03-26 18:00:00', 'title' => 'Cleaning', 'freq' => 'weekly',
				'byday' => 'TH', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '36'),
			array('dtstart' => '2014-02-15 12:00:00', 'dtend' => '2014-10-16 14:00:00', 'title' => 'Employee Meetings', 'freq' => 'monthly',
				'byday' => '2TU', 'bymonthday' => '', 'timezone_id' => '136', 'venue_id' => '36'),
			// array('dtstart' => '', 'dtend' => '', 'title' => '', 'freq' => '',
			// 	'byday' => '', 'bymonthday' => '', 'timezone_id' => '', 'venue_id' => ''),
		);

		// Uncomment the below to run the seeder
		DB::table('venue_exception_rule_events')->insert($venue_exception_rule_events);
	}

}
