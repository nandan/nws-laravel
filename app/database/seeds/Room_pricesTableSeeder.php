<?php

class Room_pricesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('room_prices')->truncate();

		$room_prices = array(
			array(
				'room_id' => '29', 'price_original' => '30', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '35'
			),
			array(
				'room_id' => '29', 'price_original' => '200', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '215'
			),
			array(
				'room_id' => '30', 'price_original' => '32', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '38'
			),
			array(
				'room_id' => '30', 'price_original' => '220', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '230'
			),
			array(
				'room_id' => '31', 'price_original' => '31', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '36'
			),
			array(
				'room_id' => '31', 'price_original' => '210', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '220'
			),
			array(
				'room_id' => '32', 'price_original' => '35', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '40'
			),
			array(
				'room_id' => '32', 'price_original' => '240', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '250'
			),
			array(
				'room_id' => '33', 'price_original' => '39', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '45'
			),
			array(
				'room_id' => '33', 'price_original' => '300', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '320'
			),
			array(
				'room_id' => '34', 'price_original' => '42', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '48'
			),
			array(
				'room_id' => '34', 'price_original' => '320', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '340'
			),
			array(
				'room_id' => '35', 'price_original' => '49', 'price_period' => '0', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '60'
			),
			array(
				'room_id' => '35', 'price_original' => '350', 'price_period' => '1', 
				'tax_flat' => '10', 'tax_percentage' => '20', 'price_retail' => '380'
			)
		);

		// Uncomment the below to run the seeder
		DB::table('room_prices')->insert($room_prices);
	}

}
