<?php

class DatabaseSeeder extends Seeder {

    public function run()
    {
        $this->call('CompaniesTableSeeder');

        $this->command->info('companies table seeded!');
    }

}

class CompaniesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('companies')->truncate();

		$companies = array(
			array('user_id' => '1',
			'company_name' => 'Dussmann Office', 'country' => 'Germany', 'State' => 'Berlin', 'City' => 'Berlin',
			'postal_code' => '10117', 'street_name' => 'Friedrichstrasse 90', 'contact_person' => 'Jens Mueller', 
			'phone_office' => '+49 30 2025 - 30 00', 'fax_office' => '+49 30 2025 - 33 33', 'email_office' => 'info@dussmann-office.com',
			 'website_url' => 'www.dussmann-office.com'),
			array('user_id' => '2',
			'company_name' => 'Excellent Business Center', 'country' => 'Germany', 'State' => 'NRW', 'City' => 'Cologne',
			'postal_code' => '50678', 'street_name' => 'Im Zollhafen 24', 'contact_person' => 'Hans Geber', 
			'phone_office' => '+49 221 650 78 - 0', 'fax_office' => '+49 221 650 78 - 450', 'email_office' => 'presse@excellent-bc.de',
			 'website_url' => 'http://www.excellent-bc.de'),
		);

		// Uncomment the below to run the seeder
		DB::table('companies')->insert($companies);
	}

}
