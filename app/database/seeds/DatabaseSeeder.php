<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		// $this->call('UserTableSeeder');
		$this->call('VenuesTableSeeder');
		$this->call('RoomsTableSeeder');
		$this->call('Room_typesTableSeeder');
		$this->call('AmenitiesTableSeeder');
		$this->call('CompaniesTableSeeder');
		$this->call('Room_pricesTableSeeder');
		$this->call('Venue_exception_rule_eventsTableSeeder');
	}

}