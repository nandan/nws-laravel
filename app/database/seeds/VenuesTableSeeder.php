<?php

class VenuesTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('venues')->truncate();

		$venues = array(
			array(
				'company_id' => '63', 'venue_name' => 'Dussmann Office Europa Center', 'description' => 'Der Dussmann Office-Standort Europa-Center-Berlin liegt mitten im pulsierenden Herzen der City-West, am Schnittpunkt zwischen Kurfürstendamm und Tauentzienstraße. Die exklusive Adresse macht diesen Standort besonders interessant für unseren Büro-Service. Das Messegelände am Funkturm ist nur drei Kilometer entfernt. Alle Büros bieten einen phantastischen Blick auf den belebten Breitscheidplatz und die berühmte Kaiser-Wilhelm-Gedächtniskirche.', 'email' => 'dussmann-officebln.europacenter@dussmann.de', 
				'country_id' => '42', 'state_id' => '46', 'city_id' => '46', 'postal_code' => '10789',
				'street_name' => 'Europa-Center', 'street_number' => '', 'floor' => '',
				'contact_person' => 'Tina Cupalolo', 'phone_office' => '+49 30 254 93 - 0', 'fax_office' => '+49 30 254 93 - 299', 'website_url' => 'http://www.dussmann-office.com/de/standorte/berlin-europa-center/',
				'timezone' => '136', 'lat' => '52.504658', 'lng' => '13.336998'
			),
			array(
				'company_id' => '63', 'venue_name' => 'Dussmann Office Berlin - Friedrichstrasse', 'description' => 'Im Dussmann Office in der Berliner Friedrichstraße residieren Sie im Dussmann-Haus in direkter Nachbarschaft zum Regierungsviertel, zur Straße Unter den Linden und zum Brandenburger Tor. In den unteren Etagen des Dussmann-Hauses befindet sich Dussmann das KulturKaufhaus. Hier können Sie von der CD über Bücher bis hin zur Software einkaufen und das vielfältige Veranstaltungsprogramm nutzen.', 'email' => 'dussmann-officeberlinf@dussmann.de', 
				'country_id' => '42', 'state_id' => '46', 'city_id' => '46', 'postal_code' => '10117',
				'street_name' => 'Friedrichstraße', 'street_number' => '90', 'floor' => '',
				'contact_person' => 'Margit Krätzig', 'phone_office' => '+49 30 2025 - 30 00', 'fax_office' => '+49 30 2025 - 33 33', 'website_url' => 'http://www.dussmann-office.com/de/standorte/berlin-friedrichstrasse/',
				'timezone' => '136', 'lat' => '52.518299', 'lng' => '13.388490'
			),
			array(
				'company_id' => '64', 'venue_name' => 'Excellent Business Center Berlin Hbf', 'description' => 'Das Excellent Business Center Berlin Hauptbahnhof befindet sich in der 8. Etage der lichtdurchfluteten und filigranen Bügelbauten, die den Berliner Hauptbahnhof überspannen. Genießen Sie die äußerst geschmackvolle Einrichtung sowie den Blick über die Berliner Skyline. Die zahlreichen Restaurants und Cafés der im Bahnhof gelegenen Shopping Mall sowie die unmittelbare Nähe zum Spreebogen versüßen den Freizeitwert. Aber darüber hinaus ist dies auch ein Standort mit internationalem Renommee wo sich nur einen Steinwurf entfernt das Regierungsviertel mit dem Bundeskanzleramt, Abgeordnetenhäusern und dem Reichstag befinden. - See more at: http://www.excellent-bc.de/de/berlin-hauptbahnhof.php#sthash.oijRREWN.dpuf', 'email' => 'berlin-hbf@excellent-bc.de', 
				'country_id' => '42', 'state_id' => '46', 'city_id' => '46', 'postal_code' => '10557',
				'street_name' => 'Europaplatz', 'street_number' => '2', 'floor' => '',
				'contact_person' => '', 'phone_office' => '+49 30 408192-0', 'fax_office' => '+49 30 408192-450', 'website_url' => 'http://www.excellent-bc.de/de/berlin-hauptbahnhof.php',
				'timezone' => '136', 'lat' => '52.525214', 'lng' => '13.368219'
			),
			array(
				'company_id' => '64', 'venue_name' => 'Excellent Business Center Berlin Unter den Linden', 'description' => 'Das Excellent Business Center Berlin am „Unter den Linden“ befindet sich an einer der prominentesten Adressen Deutschlands. Hier kreuzt der berühmten Boulevard „Unter den Linden“ und die ebenfalls bekannte „Friedrichstraße“. Schon seit über 100 Jahren schlägt hier das Herz der Metropole Berlin. Das repräsentative wie auch moderne Business Center steht für die gelungene Verbindung von Funktionalität, Service und Komfort.', 'email' => 'unterdenlinden@excellent-bc.de', 
				'country_id' => '42', 'state_id' => '46', 'city_id' => '46', 'postal_code' => '10117',
				'street_name' => 'Unter den Linden', 'street_number' => '16', 'floor' => '',
				'contact_person' => '', 'phone_office' => '+49 30 408173-0', 'fax_office' => '+49 30 408173-450', 'website_url' => 'http://www.excellent-bc.de/de/berlin-unter-den-linden.php',
				'timezone' => '136', 'lat' => '52.517132', 'lng' => '13.389215'
			),
			array(
				'company_id' => '64', 'venue_name' => 'Excellent Business Center Berlin Friedrichstraße', 'description' => 'Das Excellent Business Center Berlin Friedrichstraße zählt hinsichtlich seiner Lage, Repräsentativität und Funktionalität zu den ersten Mietadressen Berlins. Direkt an der Straßenecke zum Prachtboulevard "Unter den Linden" gelegen, befinden sich in unmittelbarer Nähe zahlreiche Restaurants, Einkaufsmöglichkeiten, Banken und Hotels. Genießen Sie hier den besonderen Komfort einer Premium-Adresse, die auch in Sachen Design und hochwertiger Einrichtung überzeugt.', 'email' => 'berlin@excellent-bc.de', 
				'country_id' => '42', 'state_id' => '46', 'city_id' => '46', 'postal_code' => '10117',
				'street_name' => 'Friedrichstraße', 'street_number' => '88', 'floor' => '',
				'contact_person' => '', 'phone_office' => '+49 30 408173-0', 'fax_office' => '+49 30 408173-450', 'website_url' => 'http://www.excellent-bc.de/de/berlin-unter-den-linden.php',
				'timezone' => '136', 'lat' => '52.517502', 'lng' => '13.388816'
			),
			// array(
			// 	'company_id' => '63', 'venue_name' => '', 'description' => '', 'email' => '', 
			// 	'country_id' => '', 'state_id' => '', 'city_id' => '', 'postal_code' => '',
			// 	'street_name' => '', 'street_number' => '', 'floor' => '',
			// 	'contact_person' => '', 'phone_office' => '', 'fax_office' => '', 'website_url' => '',
			// 	'timezone' => '136', 'lat' => '', 'lng' => ''
			// ),
		);

		
		DB::table('venues')->insert($venues);
	}

}
