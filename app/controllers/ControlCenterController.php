<?php
/*
* HANDLING CONTROLCENTER
*/

// Helper
use Carbon\Carbon;

class ControlCenterController extends BaseController {

    /*
    * Show Control Center. For Customer, Company and Venue User
    * Has to direct to sub functions. 
    */
    function show_controlcenter(){
        try{
            //check if logged in..
            if ( ! Sentry::check())
            {
                // User is not logged in
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return Redirect::action('ControlCenterController@show_bookings');
                } elseif ($group_id == "2") {
                    return Redirect::action('ControlCenterController@show_company_bookings');
                } elseif ($group_id == "3") {
                    $user = Sentry::getUser();
                    $origin_venue = DB::table('users_venues')->where('user_id', $user->id)->first();

                    return Redirect::action('ControlCenterController@show_venue_bookings', array('id' => $origin_venue->venue_id));
                }   
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * List your Workspace
    *
    */

    function show_list_your_workspace(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is logged in
                return View::make('controlcenter.list_your_workspace');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');

                if($group_id == "1"){
                    // Normal Customer, trying to upgrade
                    return View::make('controlcenter.list_your_workspace');
                } elseif ($group_id == "2") {
                    // MUST BE Notified that he can only create new venue/rooms
                    return Redirect::action('ControlCenterController@show_create_venue');
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Check if the user gets redirect to the wizard or gets an ajax response + login window that he is not logged in
    */

    function run_list_your_workspace(){
        try{
            //check if logged in
            //redirect to Wizard
            if(!Sentry::check()){
                return Response::json(array('status' => 'Error', 'message' => 'Please login first'));
            }else{
                return Response::json(array('status' => 'OK', 'message' => 'Redirect to Wizard'));
            }
        } catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }




	/**
     *  Wizard for news users to add company, venue and office 
     * 
     */

    /**
     * Show the wizard form for creating a new company, venue and offices.
     */

    function show_create_wizard()
    {
    	try {
            //check if logged in..
            if ( ! Sentry::check())
            {
                // User is not logged in
                return Redirect::to('/');
            }
            else
            {
                // User is logged in
                //get session key
                $group_id = Session::get('group_id_key');

                if($group_id == "1"){
                    return View::make('wizard.show');
                } elseif ($group_id == "2") {
                    return View::make('wizard.show');
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            Log::error($e);
            print_r($e->getMessage());
        }
    }


    /**
     * Store a newly created resources for a new company, venue and offices.
     */

    function run_store_wizard()
    {
        try {

            // Check if logged in..
            if (!Sentry::check())
            {
                // User is not logged in
                return Redirect::to('/');
            }
            else
            {
                // User is logged in
                //get session key
                $group_id = Session::get('group_id_key');

                if(($group_id == "1") || ($group_id == "2")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    /* Add Company */
                    // Company Name
                    $company_name = Input::get('company_name');
                    // Company Address
                    $company_street = Input::get('company_street');
                    $company_floor = Input::get('company_floor');
                    $company_city = Input::get('company_city');
                    $company_state = Input::get('company_state');
                    $company_zip = Input::get('company_zip');
                    $company_country = Input::get('company_country');
                    // Company Contacts
                    $company_contact_name = Input::get('company_contact_name');
                    $company_telephone = Input::get('company_telephone');
                    $company_fax = Input::get('company_fax');
                    $company_email = Input::get('company_email');
                    $company_website = Input::get('company_website');



                    /* Add venue */
                    // Venue Name
                    $venue_name = Input::get('venue_name');
                    $venue_description = Input::get('venue_description');
                    // Venue Address
                    $venue_street = Input::get('venue_street');
                    $venue_floor = Input::get('venue_floor');
                    $venue_city = Input::get('venue_city');
                    $venue_state = Input::get('venue_state');
                    $venue_zip = Input::get('venue_zip');
                    $venue_country = Input::get('venue_country');
                    $venue_lat = Input::get('venue_lat');
                    $venue_lng = Input::get('venue_lng');
                    // Venue Contacts
                    $venue_contact_name = Input::get('venue_contact_name');
                    $venue_telephone = Input::get('venue_telephone');
                    $venue_fax = Input::get('venue_fax');
                    $venue_email = Input::get('venue_email');
                    $venue_website = Input::get('venue_website');
                    // Venue Time
                    $venue_timezone = Input::get('venue_timeZoneId');
                    $venue_timezonename = Input::get('venue_timeZoneName');
                    $venue_startmonday = Input::get('venue_startmonday');
                    $venue_endmonday = Input::get('venue_endmonday');
                    $venue_closedmonday = Input::get('venue_closedmonday');
                    $venue_starttuesday = Input::get('venue_starttuesday');
                    $venue_endtuesday = Input::get('venue_endtuesday');
                    $venue_closedtuesday = Input::get('venue_closedtuesday');
                    $venue_startwednesday = Input::get('venue_startwednesday');
                    $venue_endwednesday = Input::get('venue_endwednesday');
                    $venue_closedwednesday = Input::get('venue_closedwednesday');
                    $venue_startthursday = Input::get('venue_startthursday');
                    $venue_endthursday = Input::get('venue_endthursday');
                    $venue_closedthursday = Input::get('venue_closedthursday');
                    $venue_startfriday = Input::get('venue_startfriday');
                    $venue_endfriday = Input::get('venue_endfriday');
                    $venue_closedfriday = Input::get('venue_closedfriday');
                    $venue_startsaturday = Input::get('venue_startsaturday');
                    $venue_endsaturday = Input::get('venue_endsaturday');
                    $venue_closedsaturday = Input::get('venue_closedsaturday');
                    $venue_startsunday = Input::get('venue_startsunday');
                    $venue_endsunday = Input::get('venue_endsunday');
                    $venue_closedsunday = Input::get('venue_closedsunday');


                    /* Add venue */
                    // Office Name
                    $office_name = Input::get('office_name');
                    $office_description = Input::get('office_description');
                    // Office Details
                    $office_types = Input::get('office_types');
                    $office_capacity = Input::get('office_capacity');
                    $office_amenities_public_wifi = Input::get('office_amenities_public_wifi');
                    $office_amenities_secure_wifi = Input::get('office_amenities_secure_wifi');
                    $office_amenities_wired_internet = Input::get('office_amenities_wired_internet');
                    $office_amenities_flatscreen = Input::get('office_amenities_flatscreen');
                    $office_amenities_video_conferencing = Input::get('office_amenities_video_conferencing');
                    $office_amenities_projector = Input::get('office_amenities_projector');
                    $office_amenities_whiteboard = Input::get('office_amenities_whiteboard');
                    $office_amenities_flipboard = Input::get('office_amenities_flipboard');
                    $office_amenities_phone_room = Input::get('office_amenities_phone_room');
                    $office_amenities_conference_phone = Input::get('office_amenities_conference_phone');
                    $office_amenities_print_scan_copy = Input::get('office_amenities_print_scan_copy');
                    $office_amenities_handicap_accessible = Input::get('office_amenities_handicap_accessible');
                    $office_amenities_coffee_tea = Input::get('office_amenities_coffee_tea');
                    $office_amenities_filtered_water = Input::get('office_amenities_filtered_water');
                    $office_amenities_on_site_restaurant = Input::get('office_amenities_on_site_restaurant');
                    $office_amenities_catering = Input::get('office_amenities_catering');
                    $office_amenities_concierge_service = Input::get('office_amenities_concierge_service');
                    $office_amenities_notary_service = Input::get('office_amenities_notary_service');
                    $office_amenities_shared_kitchen = Input::get('office_amenities_shared_kitchen');
                    $office_amenities_shower_facility = Input::get('office_amenities_shower_facility');
                    $office_amenities_pet_friendly = Input::get('office_amenities_pet_friendly');
                    $office_amenities_outdoor_space = Input::get('office_amenities_outdoor_space');
                    // Pricing
                    $office_currencies = Input::get('office_currencies');
                    $office_price_hourly = Input::get('office_price_hourly');
                    $office_flattax_hourly = Input::get('office_flattax_hourly');
                    $office_percentagetax_hourly = Input::get('office_percentagetax_hourly');
                    $office_price_daily = Input::get('office_price_daily');
                    $office_flattax_daily = Input::get('office_flattax_daily');
                    $office_percentagetax_daily = Input::get('office_percentagetax_daily');
                    $office_price_monthly = Input::get('office_price_monthly');
                    $office_flattax_monthly = Input::get('office_flattax_monthly');
                    $office_percentagetax_monthly = Input::get('office_percentagetax_monthly');
                    // Time Information
                    $office_startmonday = Input::get('office_startmonday');
                    $office_endmonday = Input::get('office_endmonday');
                    $office_closedmonday = Input::get('office_closedmonday');
                    $office_starttuesday = Input::get('office_starttuesday');
                    $office_endtuesday = Input::get('office_endtuesday');
                    $office_closedtuesday = Input::get('office_closedtuesday');
                    $office_startwednesday = Input::get('office_startwednesday');
                    $office_endwednesday = Input::get('office_endwednesday');
                    $office_closedwednesday = Input::get('office_closedwednesday');
                    $office_startthursday = Input::get('office_startthursday');
                    $office_endthursday = Input::get('office_endthursday');
                    $office_closedthursday = Input::get('office_closedthursday');
                    $office_startfriday = Input::get('office_startfriday');
                    $office_endfriday = Input::get('office_endfriday');
                    $office_closedfriday = Input::get('office_closedfriday');
                    $office_startsaturday = Input::get('office_startsaturday');
                    $office_endsaturday = Input::get('office_endsaturday');
                    $office_closedsaturday = Input::get('office_closedsaturday');
                    $office_startsunday = Input::get('office_startsunday');
                    $office_endsunday = Input::get('office_endsunday');
                    $office_closedsunday = Input::get('office_closedsunday');

                    // make time variables matching for time format in DB. For Venue
                    $venue_startmonday = ''.$venue_startmonday.':00:00';
                    $venue_endmonday = ''.$venue_endmonday.':00:00';
                    $venue_starttuesday = ''.$venue_starttuesday.':00:00';
                    $venue_endtuesday = ''.$venue_endtuesday.':00:00';
                    $venue_startwednesday = ''.$venue_startwednesday.':00:00';
                    $venue_endwednesday = ''.$venue_endwednesday.':00:00';
                    $venue_startthursday = ''.$venue_startthursday.':00:00';
                    $venue_endthursday = ''.$venue_endthursday.':00:00';
                    $venue_startfriday = ''.$venue_startfriday.':00:00';
                    $venue_endfriday = ''.$venue_endfriday.':00:00';
                    $venue_startsaturday = ''.$venue_startsaturday.':00:00';
                    $venue_endsaturday = ''.$venue_endsaturday.':00:00';
                    $venue_startsunday = ''.$venue_startsunday.':00:00';
                    $venue_endsunday = ''.$venue_endsunday.':00:00';

                    // make time variables matching for time format in DB. For Room
                    $office_startmonday = ''.$office_startmonday.':00:00';
                    $office_endmonday = ''.$office_endmonday.':00:00';
                    $office_starttuesday = ''.$office_starttuesday.':00:00';
                    $office_endtuesday = ''.$office_endtuesday.':00:00';
                    $office_startwednesday = ''.$office_startwednesday.':00:00';
                    $office_endwednesday = ''.$office_endwednesday.':00:00';
                    $office_startthursday = ''.$office_startthursday.':00:00';
                    $office_endthursday = ''.$office_endthursday.':00:00';
                    $office_startfriday = ''.$office_startfriday.':00:00';
                    $office_endfriday = ''.$office_endfriday.':00:00';
                    $office_startsaturday = ''.$office_startsaturday.':00:00';
                    $office_endsaturday = ''.$office_endsaturday.':00:00';
                    $office_startsunday = ''.$office_startsunday.':00:00';
                    $office_endsunday = ''.$office_endsunday.':00:00';

                    /* Inserting in the table */
                     $user = Sentry::getUser();

                    // Add Company in the table
                    // Insert in the table
                    $company_id = DB::table('companies')->insertGetId(
                        array('user_id' => $user->id, 'company_name' => $company_name, 'country' => $company_country, 'state' => $company_state,
                        'city' => $company_city, 'postal_code' => $company_zip, 'street_name' => $company_street, 'floor' => $company_floor ,
                        'contact_person' => $company_contact_name ,'phone_office' => $company_telephone,
                        'fax_office' => $company_fax, 'email_office' => $company_email, 'website_url' => $company_website)
                    );


                    // Add Venue in the table
                    // Create/associate entry for country, state and city
                    // Country
                    $existing_country = DB::table('countries')->where('country_name', $venue_country)->first();
                    if (is_null($existing_country))
                    {
                        $venue_country_id = DB::table('countries')->insertGetId(
                            array('country_name' => $venue_country)
                        );
                    } else {
                        $venue_country_id = $existing_country->id;
                    }
                    // State
                    $existing_state = DB::table('states')->where('state_name', $venue_state)->
                        where('country_id', $venue_country_id)->first();
                    if (is_null($existing_state))
                    {
                        $venue_country_id = DB::table('states')->insertGetId(
                            array('state_name' => $venue_state, 'country_id' => $venue_country_id)
                        );
                    } else {
                        $venue_state_id = $existing_state->id;
                    }
                    // City
                    $existing_city = DB::table('cities')->where('city_name', $venue_city)->
                        where('country_id', $venue_country_id)->where('state_id', $venue_state_id)->first();
                    if (is_null($existing_city))
                    {
                        $venue_city_id = DB::table('cities')->insertGetId(
                            array('city_name' => $venue_city, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id)
                        );
                    } else {
                        $venue_city_id = $existing_city->id;
                    }

                    // Check for real Timezone ID in "timezones" table
                    $find_timezoneid = DB::table('timezones')->where('zone_name', $venue_timezone)->first();
                    if (is_null($find_timezoneid))
                    {
                        $venue_timezoneid = '-1';
                    }
                    else
                    {
                        $venue_timezoneid = $find_timezoneid->id;
                    }

                    // Insert in the table
                    $venue_id = DB::table('venues')->insertGetId(
                        array('company_id' => $company_id, 'venue_name' => $venue_name, 'description' => $venue_description, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id,
                        'city_id' => $venue_city_id, 'postal_code' => $venue_zip, 'street_name' => $venue_street, 'floor' => $venue_floor, 'contact_person' => $venue_contact_name, 'phone_office' => $venue_telephone,
                        'fax_office' => $venue_fax, 'email' => $venue_email, 'website_url' => $venue_website, 'timezone' => $venue_timezoneid, 'lat' => $venue_lat, 'lng' => $venue_lng)
                    );
                    // Insert opening hours in the table
                    DB::table('venue_opening_hours')->insert(array(
                        array('venue_id' => $venue_id, 'day_index' => '0', 'start_time' => $venue_startsunday, 'end_time' => $venue_endsunday, 'closed' => $venue_closedsunday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '1', 'start_time' => $venue_startmonday, 'end_time' => $venue_endmonday, 'closed' => $venue_closedmonday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '2', 'start_time' => $venue_starttuesday, 'end_time' => $venue_endtuesday, 'closed' => $venue_closedtuesday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '3', 'start_time' => $venue_startwednesday, 'end_time' => $venue_endwednesday, 'closed' => $venue_closedwednesday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '4', 'start_time' => $venue_startthursday, 'end_time' => $venue_endthursday, 'closed' => $venue_closedthursday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '5', 'start_time' => $venue_startfriday, 'end_time' => $venue_endfriday, 'closed' => $venue_closedfriday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '6', 'start_time' => $venue_startsaturday, 'end_time' => $venue_endsaturday, 'closed' => $venue_closedsaturday, 'timezone' => $venue_timezoneid)
                    ));


                    // Add Office in the table
                    // Calculate room prices
                    // Hourly Price
                    if ($office_price_hourly == "") {
                        $office_price_hourly = 0;
                        $office_flattax_hourly = 0;
                        $office_percentagetax_hourly = 0;
                        $office_retailprice_hourly = 0;
                    } else {
                        // Hourly Retail Price
                        if ($office_percentagetax_hourly != "")
                        {
                            $office_retailprice_hourly = $office_price_hourly + ($office_price_hourly * (intval($office_percentagetax_hourly)/100));
                            if ($office_flattax_hourly != "") {
                                $office_retailprice_hourly = $office_retailprice_hourly + $office_flattax_hourly;
                            }
                        }
                    }

                    // Daily Price
                    if ($office_price_daily == "") {
                        $office_price_daily = 0;
                        $office_flattax_daily = 0;
                        $office_percentagetax_daily = 0;
                        $office_retailprice_daily = 0;
                    } else {
                        // Daily Retail Price
                        if ($office_percentagetax_daily != "")
                        {
                            $office_retailprice_daily = $office_price_daily + ($office_price_daily * (intval($office_percentagetax_daily)/100));
                            if ($office_flattax_daily != "") {
                                $office_retailprice_daily = $office_retailprice_daily + $office_flattax_daily;
                            }
                        }
                    }

                    // Monthly price
                    if ($office_price_monthly == "") {
                        $office_price_monthly = 0;
                        $office_flattax_monthly = 0;
                        $office_percentagetax_monthly = 0;
                        $office_retailprice_monthly = 0;
                    } else {
                        // monthly Retail Price
                        if ($office_percentagetax_monthly != "")
                        {
                            $office_retailprice_monthly = $office_price_monthly + ($office_price_monthly * (intval($office_percentagetax_monthly)/100));
                            if ($office_flattax_monthly != "") {
                                $office_retailprice_monthly = $office_retailprice_monthly + $office_flattax_monthly;
                            }
                        }
                    }

                    // Insert in the table
                    $room_id = DB::table('rooms')->insertGetId(
                        array('venue_id' => $venue_id, 'room_name' => $office_name, 'description' => $office_description, 'room_types_id' => $office_types, 'currency' => $office_currencies,
                        'max_capacity' => $office_capacity)
                    );
                    // Insert prices in the table
                    DB::table('room_prices')->insert(array(
                        array('room_id' => $room_id, 'price_original' => $office_price_hourly, 'price_period' => '0', 'tax_flat' => $office_flattax_hourly,
                        'tax_percentage' => $office_percentagetax_hourly, 'price_retail' => $office_retailprice_hourly),
                        array('room_id' => $room_id, 'price_original' => $office_price_daily, 'price_period' => '1', 'tax_flat' => $office_flattax_daily,
                        'tax_percentage' => $office_percentagetax_daily, 'price_retail' => $office_retailprice_daily),
                        array('room_id' => $room_id, 'price_original' => $office_price_monthly, 'price_period' => '2', 'tax_flat' => $office_flattax_monthly,
                        'tax_percentage' => $office_percentagetax_monthly, 'price_retail' => $office_retailprice_monthly)
                    ));
                    // Insert opening hours in the table
                     DB::table('room_opening_hours')->insert(array(
                        array('room_id' => $room_id, 'day_index' => '0', 'start_time' => $office_startsunday, 'end_time' => $office_endsunday, 'closed' => $office_closedsunday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '1', 'start_time' => $office_startmonday, 'end_time' => $office_endmonday, 'closed' => $office_closedmonday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '2', 'start_time' => $office_starttuesday, 'end_time' => $office_endtuesday, 'closed' => $office_closedtuesday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '3', 'start_time' => $office_startwednesday, 'end_time' => $office_endwednesday, 'closed' => $office_closedwednesday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '4', 'start_time' => $office_startthursday, 'end_time' => $office_endthursday, 'closed' => $office_closedthursday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '5', 'start_time' => $office_startfriday, 'end_time' => $office_endfriday, 'closed' => $office_closedfriday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '6', 'start_time' => $office_startsaturday, 'end_time' => $office_endsaturday, 'closed' => $office_closedsaturday, 'timezone' => $venue_timezoneid)
                    ));
                    // Insert amenities in the table
                    DB::table('amenities')->insert(
                        array('room_id' => $room_id, 'public_wifi' => $office_amenities_public_wifi, 'secure_wifi' => $office_amenities_secure_wifi,
                        'wired_internet' => $office_amenities_wired_internet, 'flatscreen' => $office_amenities_flatscreen, 'video_conferencing' => $office_amenities_video_conferencing,
                        'flipboard' => $office_amenities_flipboard, 'whiteboard' => $office_amenities_whiteboard, 'projector' => $office_amenities_projector,
                        'print_scan_copy' => $office_amenities_print_scan_copy, 'shared_kitchen' => $office_amenities_shared_kitchen, 'catering' => $office_amenities_catering,
                        'concierge_service' => $office_amenities_concierge_service, 'filtered_water' => $office_amenities_filtered_water, 'phone_room' => $office_amenities_phone_room,
                        'pet_friendly' => $office_amenities_pet_friendly, 'handicap_accessible' => $office_amenities_handicap_accessible, 'conference_phone' => $office_amenities_conference_phone,
                        'outdoor_space' => $office_amenities_outdoor_space, 'on_site_restaurant' => $office_amenities_on_site_restaurant, 'coffee_tea' => $office_amenities_coffee_tea,
                        'notary_service' => $office_amenities_notary_service, 'shower_facility' => $office_amenities_shower_facility)
                    );

                    if ($group_id == "1")
                    {
                        // Change UserGroups Table
                        DB::table('users_groups')->where('user_id',$user->id)->update(array('group_id' => '2'));

                        //Set Session Key to Group 2 = Company
                        Session::put('group_id_key', '2');
                    }

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            Log::error($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }


    /**
     *  Control Center sidebar-orientation pages for different user roles 
     * 
     */

    /**  For Customer **/

    /*
    * Show Bookings. For Customer
    */
    
    function show_bookings(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                // User is logged in
                return View::make('controlcenter.customer_bookings');
            }
        } catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Settings. For Customer
    */
    function show_settings(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                // User is logged in
                $user = Sentry::getUser();

                $personal_information = DB::table('users')->join('customer_details', 'users.id', '=','customer_details.user_id')
                    ->select('users.email','users.first_name', 'users.last_name', 'users.currency', 'users.language', 'customer_details.title', 
                        'customer_details.phone_office', 'customer_details.phone_mobile', 'customer_details.company', 'customer_details.photo_id')
                    ->where('users.id', $user->id)->get();

                // Pictures Schema will be changed. MUST BE ADJUSTED SOON!
                if ($personal_information[0]->photo_id == '0')
                    return View::make('controlcenter.customer_settings', array('personal_information' => $personal_information));
                else
                {
                    $personal_picture = DB::table('pictures')->select('pictures.url')->where('pictures.id', $personal_information[0]->photo_id)->first();
                    return View::make('controlcenter.customer_settings', array('personal_information' => $personal_information, 'personal_picture' => $personal_picture));
                }

            }
        } catch(Exception $e){
            //Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Save Edit Personal Information. For Customer
    */
    function run_personal_information(){
        try{
            // Check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                // User is logged in
                $title = Input::get('title');
                $first_name = Input::get('first_name');
                $last_name = Input::get('last_name');
                $company = Input::get('company');
                $telephone_mobile = Input::get('telephone_mobile');
                $telephone_office = Input::get('telephone_office');

                $user = Sentry::getUser();

                //Users Table
                DB::table('users')->where('id',$user->id)
                    ->update(array('first_name' => $first_name, 'last_name' => $last_name, 'updated_at' => new DateTime));
                
                //Customer Details Table

                DB::table('customer_details')->where('user_id', $user->id)
                    ->update(array('title' => $title, 'company' => $company, 'phone_office' => $telephone_office, 
                        'phone_mobile' => $telephone_mobile, 'updated_at' => new DateTime));
                
                //Response OK
                return Response::json(array('status' => 'OK', 'message' => 'Personal Information saved'));
            }
        } catch(Exception $e){
            Log::error($e);
            return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
        }
    }

    /*
    * Save Upload new Profile Picture. For Customer.
    */

    function run_profile_picture(){

        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                if (!Input::hasFile('file')) return "No Photo Selected";

                $file = Input::file('file');

                $destinationPath = 'uploads/'.str_random(8);
                $filename = $file->getClientOriginalName();
                //$extension =$file->getClientOriginalExtension(); 
                $upload_success = Input::file('file')->move($destinationPath, $filename);
                $full_path = $destinationPath . '/' . $filename;
                $url_path = 'http://localhost:8000/'. $full_path;
                //Create Thumbnail

                if($upload_success){
                    //Resize
                    $image = Image::make($full_path)->resize(200, 200);
                    $image->save($full_path);
                    //DELETE OLD PICTURE

                    //Insert to Pictures Table
                    $new_photo_id =DB::table('pictures')->insertGetId(
                        array('file_path' => $full_path, 'url' => $url_path, 'user_id' => '1','created_at' => new DateTime, 'updated_at' => new DateTime));

                    // Update Customer Details Table
                    $user = Sentry::getUser();

                    DB::table('customer_details')->where('user_id',$user->id)->update(array('photo_id' => $new_photo_id, 'updated_at' => new DateTime));    
                    // Response OK

                    // Return Response::json(array('status' => 'OK', 'message' => 'Personal Information saved', 'image_url' =>$url_path));
                    return Redirect::to('/controlcenter/settings');
                } 
                else
                {
                    // Return Response::json(array('status' => 'Error', 'message' => 'Upload Error'));
                    return "Upload Error";
                }
            }
        } catch(Exception $e){
            //return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
          //  print_r($e);
            return "Exception Error";
        }
    }

    /*
    * Delete Profile Picture. For Customer
    */

    function run_delete_profile_picture(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                // User logged in
                $user = Sentry::getUser();
                // Customer Details Table 
                DB::table('customer_details')->where('user_id', $user->id)->update(array('photo_id' => '0',  'updated_at' => new DateTime));

                return Response::json(array('status' => 'OK', 'message' => 'Profile Picture Deleted'));
            }
        }catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => 'Error Deleting your Profile Picture'));

        }


    }

    /*
    *  Localization Settings Save. For Customer.
    */

    function run_localization(){
        try{
            // Check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                // User logged in
                $currency = Input::get('currency');
                $language = Input::get('language');

                // Customer Details Table 
                $user = Sentry::getUser();

                DB::table('users')->where('id', $user->id)->update(array('currency' => $currency, 'language' => $language));

                return Response::json(array('status' => 'OK', 'message' => 'Localization Settings updated'));
            }
        } catch(Exception $e){

            //print_r($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }


    /**  For Company **/

    /*
    * Show Bookings. For Company
    */
    function show_company_bookings(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    $user = Sentry::getUser();
                    $company = DB::table('companies')->where('user_id', $user->id)->first();

                    return View::make('controlcenter.company_bookings', array('company' => $company));
                } elseif ($group_id == "3") {
                    $user = Sentry::getUser();
                    $origin_venue = DB::table('users_venues')->where('user_id', $user->id)->first();

                    return Redirect::action('ControlCenterController@show_venue_bookings', array($origin_venue->venue_id));
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Venues. For Company
    */
    function show_company_venues(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return Redirect::to('/');
                } elseif ($group_id == "2") {
                    $user = Sentry::getUser();
                    $company = DB::table('companies')->where('user_id', $user->id)->first();

                    $venues = DB::table('venues')
                        ->join('countries', 'countries.id', '=', 'venues.country_id')
                        ->join('states', 'states.id', '=', 'venues.state_id')
                        ->join('cities', 'cities.id', '=', 'venues.city_id')
                        ->select('venues.venue_name', 'venues.id', 'venues.floor', 'venues.street_name' , 'venues.postal_code',
                            'cities.city_name', 'states.state_name',
                            'countries.country_name')
                        ->where('company_id', $company->id)->get();

                    return View::make('controlcenter.company_venues', array('company' => $company, 'venues' => $venues));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Financials. For Company
    */
    function show_company_financials(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return Redirect::to('/');
                } elseif ($group_id == "2") {
                    return View::make('controlcenter.company_financials');
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_financials');
                }
            }
        }catch(Exception $e){
            Log::error($e);
        }
    }

    /*
    * Show Settings. For Company
    */
    function show_company_settings(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_settings');
                } elseif ($group_id == "2") {
                    $user = Sentry::getUser();
                    $company = DB::table('companies')->where('user_id', $user->id)->first();

                    return Redirect::action('ControlCenterController@show_edit_company', array('id' => $company->id));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }


	/*
	* Show Calendar. For Company. Obsolete
	*/
	function show_company_calendar(){
		try{
            //Example for venue Id = 1
            $venue_id = '1';

            $venue_rooms = DB::table('rooms')->select('rooms.id','rooms.room_name')->where('venue_id', $venue_id)->get();
            
			return View::make('controlcenter.company_calendar', array('venue_rooms' => $venue_rooms));
		}catch(Exception $e){
			Log::error($e);
            print_r($e->getMessage());
		}
	}

    /*
    * JQUERY FULL CALENDAR FOR VENUES AND ROOMS AJAX
    *
    */

    function show_company_calendar_ajax(){

        try{
            return Response::json(array('status' => 'OK', 'message' => 'Fetched Bookings'));
        }catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }

    }

    /*
    * Send Example Events. Testing jQuery Full Calendar
    *
    */
    function example_calendar_events(){
        try{

            $calendar_events = DB::table('customer_bookings')->select('room_id', 'venue_id', 'start_time','end_time','title','timezone')->where('venue_id', '1')->get();

            return Response::json(array('status' => 'OK', 'message' => 'Example Events Fetched' , 'calendardata' => $calendar_events, 'test' =>'yes'));
        }catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));

        }

    }



    /**  For Venues **/

    /*
    * Show Venue Bookings. For Venue
    */
    function show_venue_bookings($id){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    $venue = DB::table('venues')->where('id', $id)->first();
                    $company = DB::table('companies')->where('companies.id', $venue->company_id)->first();

                    return View::make('controlcenter.company_venue_bookings', array('venue' => $venue, 'company' => $company));
                } elseif ($group_id == "3") {
                    $venue = DB::table('venues')->where('id', $id)->first();
                    $company = DB::table('companies')->where('companies.id', $venue->company_id)->first();

                    return View::make('controlcenter.company_venue_bookings', array('venue' => $venue, 'company' => $company));
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Venue Calendar. For Venue
    */
    function show_venue_calendar(){
        try{

        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Rooms. For Venue
    */
    function show_venue_rooms($id){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    $venue = DB::table('venues')->where('id', $id)->first();
                    $company = DB::table('companies')->where('id', $venue->company_id)->first();
                    $rooms = DB::table('rooms')
                        ->join('room_types', 'room_types.id', '=', 'room_types_id')
                        ->select('rooms.id', 'rooms.room_name', 'room_types.room_type_name')
                        ->where('venue_id', $venue->id)
                        ->get();

                    return View::make('controlcenter.company_venue_rooms', array('venue' => $venue, 'company' => $company, 'rooms' => $rooms));
                } elseif ($group_id == "3") {
                    $venue = DB::table('venues')->where('id', $id)->first();
                    $company = DB::table('companies')->where('id', $venue->company_id)->first();
                    $rooms = DB::table('rooms')
                        ->join('room_types', 'room_types.id', '=', 'room_types_id')
                        ->select('rooms.id', 'rooms.room_name', 'room_types.room_type_name')
                        ->where('venue_id', $venue->id)
                        ->get();

                    return View::make('controlcenter.company_venue_rooms', array('venue' => $venue, 'company' => $company, 'rooms' => $rooms));
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Financials. For Venue
    */
    function show_venue_financials(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    return View::make('controlcenter.company_venue_financials');;
                }
            }
            return View::make('controlcenter.company_venue_financials');
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Venue Settings - General. For Venue
    */
    function show_venue_settings($id){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    return Redirect::action('ControlCenterController@show_edit_venue', array($id));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_edit_venue', array($id));
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }



    /**  For Rooms **/

    /*
    * Show Room Bookings. For Room
    */
    function show_room_bookings($id){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    $room = DB::table('rooms')->where('id', $id)->first();
                    $venue = DB::table('venues')->where('id', $room->venue_id)->first();
                    $company = DB::table('companies')->where('id', $venue->company_id)->first();

                    return View::make('controlcenter.company_room_bookings', array('room' => $room, 'venue' => $venue, 'company' => $company));
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Financials. For Room
    */
    function show_room_financials(){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    return View::make('controlcenter.company_room_financials');
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }

    /*
    * Show Room Settings. For Room
    */
    function show_room_settings($id){
        try{
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    return Redirect::action('ControlCenterController@show_edit_room', array($id));;
                }
            }
        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
    }



    /**
     *  Control Center pages for adding, editing and removing Venues and Offices
     * 
     */

    /** Action for COMPANY **/

    /**
     * Show form for editing company
     */
    function show_edit_company($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    $company_information = DB::table('companies')->select(
                            'companies.id', 'companies.company_name', 'companies.postal_code', 'companies.street_name',
                            'companies.floor', 'companies.contact_person',
                            'companies.phone_office', 'companies.fax_office', 'companies.email_office' ,'companies.website_url',
                            'companies.city', 'companies.state', 'companies.country')
                        ->where('companies.id', $id)->get();

                    return View::make('controlcenter.company_edit_company', array('company_information' => $company_information));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
        }
    }

    /**
     * Update edited company in the database
     */
    function run_update_company($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    // Company Name
                    $company_name = Input::get('company_name');
                    // Company Address
                    $company_street = Input::get('company_street');
                    $company_floor = Input::get('company_floor');
                    $company_city = Input::get('company_city');
                    $company_state = Input::get('company_state');
                    $company_zip = Input::get('company_zip');
                    $company_country = Input::get('company_country');
                    // Company Contacts
                    $company_contact_name = Input::get('company_contact_name');
                    $company_telephone = Input::get('company_telephone');
                    $company_fax = Input::get('company_fax');
                    $company_email = Input::get('company_email');
                    $company_website = Input::get('company_website');

                    /* Inserting in the table */

                    // Add Company in the table
                    // Insert in the table
                    DB::table('companies')->where('id', $id)->update(
                        array('company_name' => $company_name, 'country' => $company_country, 'state' => $company_state,
                        'city' => $company_city, 'postal_code' => $company_zip, 'street_name' => $company_street, 'floor' => $company_floor,
                        'contact_person' => $company_contact_name ,'phone_office' => $company_telephone,
                        'fax_office' => $company_fax, 'email_office' => $company_email, 'website_url' => $company_website)
                    );

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }


    /** Action for VENUE **/

    /**
     * Show form for creating new venue
     */
    function show_create_venue()
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {
                    return View::make('controlcenter.company_create_venue');
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
        }
    }

    /**
     * Store newly created venue in the database
     */
    function run_store_venue()
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif ($group_id == "2") {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    // Venue Name
                    $venue_name = Input::get('venue_name');
                    $venue_description = Input::get('venue_description');
                    // Venue Address
                    $venue_street = Input::get('venue_street');
                    $venue_street_number = Input::get('venue_street_number');
                    $venue_floor = Input::get('venue_floor');
                    $venue_city = Input::get('venue_city');
                    $venue_state = Input::get('venue_state');
                    $venue_zip = Input::get('venue_zip');
                    $venue_country = Input::get('venue_country');
                    $venue_lat = Input::get('venue_lat');
                    $venue_lng = Input::get('venue_lng');
                    // Venue Contacts
                    $venue_contact_name = Input::get('venue_contact_name');
                    $venue_telephone = Input::get('venue_telephone');
                    $venue_fax = Input::get('venue_fax');
                    $venue_email = Input::get('venue_email');
                    $venue_website = Input::get('venue_website');
                    // Venue Time
                    $venue_timezone = Input::get('venue_timeZoneId');
                    $venue_timezonename = Input::get('venue_timeZoneName');
                    $venue_startmonday = Input::get('venue_startmonday');
                    $venue_endmonday = Input::get('venue_endmonday');
                    $venue_closedmonday = Input::get('venue_closedmonday');
                    $venue_starttuesday = Input::get('venue_starttuesday');
                    $venue_endtuesday = Input::get('venue_endtuesday');
                    $venue_closedtuesday = Input::get('venue_closedtuesday');
                    $venue_startwednesday = Input::get('venue_startwednesday');
                    $venue_endwednesday = Input::get('venue_endwednesday');
                    $venue_closedwednesday = Input::get('venue_closedwednesday');
                    $venue_startthursday = Input::get('venue_startthursday');
                    $venue_endthursday = Input::get('venue_endthursday');
                    $venue_closedthursday = Input::get('venue_closedthursday');
                    $venue_startfriday = Input::get('venue_startfriday');
                    $venue_endfriday = Input::get('venue_endfriday');
                    $venue_closedfriday = Input::get('venue_closedfriday');
                    $venue_startsaturday = Input::get('venue_startsaturday');
                    $venue_endsaturday = Input::get('venue_endsaturday');
                    $venue_closedsaturday = Input::get('venue_closedsaturday');
                    $venue_startsunday = Input::get('venue_startsunday');
                    $venue_endsunday = Input::get('venue_endsunday');
                    $venue_closedsunday = Input::get('venue_closedsunday');

                    // make time variables matching for time format in DB
                    $venue_startmonday = ''.$venue_startmonday.':00:00';
                    $venue_endmonday = ''.$venue_endmonday.':00:00';
                    $venue_starttuesday = ''.$venue_starttuesday.':00:00';
                    $venue_endtuesday = ''.$venue_endtuesday.':00:00';
                    $venue_startwednesday = ''.$venue_startwednesday.':00:00';
                    $venue_endwednesday = ''.$venue_endwednesday.':00:00';
                    $venue_startthursday = ''.$venue_startthursday.':00:00';
                    $venue_endthursday = ''.$venue_endthursday.':00:00';
                    $venue_startfriday = ''.$venue_startfriday.':00:00';
                    $venue_endfriday = ''.$venue_endfriday.':00:00';
                    $venue_startsaturday = ''.$venue_startsaturday.':00:00';
                    $venue_endsaturday = ''.$venue_endsaturday.':00:00';
                    $venue_startsunday = ''.$venue_startsunday.':00:00';
                    $venue_endsunday = ''.$venue_endsunday.':00:00';

                    /* Inserting in the table */
                    $user = Sentry::getUser();

                    $origin_company = DB::table('companies')->where('user_id', $user->id)->first();
                    $company_id = $origin_company->id;

                    // Add Venue in the table
                    // Create/associate entry for country, state and city
                    // Country
                    $existing_country = DB::table('countries')->where('country_name', $venue_country)->first();
                    if (is_null($existing_country))
                    {
                        $venue_country_id = DB::table('countries')->insertGetId(
                            array('country_name' => $venue_country)
                        );
                    } else {
                        $venue_country_id = $existing_country->id;
                    }
                    // State
                    $existing_state = DB::table('states')->where('state_name', $venue_state)->
                        where('country_id', $venue_country_id)->first();
                    if (is_null($existing_state))
                    {
                        $venue_state_id = DB::table('states')->insertGetId(
                            array('state_name' => $venue_state, 'country_id' => $venue_country_id)
                        );
                    } else {
                        $venue_state_id = $existing_state->id;
                    }
                    // City
                    $existing_city = DB::table('cities')->where('city_name', $venue_city)->
                        where('country_id', $venue_country_id)->where('state_id', $venue_state_id)->first();
                    if (is_null($existing_city))
                    {
                        $venue_city_id = DB::table('cities')->insertGetId(
                            array('city_name' => $venue_city, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id)
                        );
                    } else {
                        $venue_city_id = $existing_city->id;
                    }

                    // Check for real Timezone ID in "timezones" table
                    $find_timezoneid = DB::table('timezones')->where('zone_name', $venue_timezone)->first();
                    if (is_null($find_timezoneid))
                    {
                        $venue_timezoneid = '-1';
                    }
                    else
                    {
                        $venue_timezoneid = $find_timezoneid->id;
                    }

                    // Insert in the table
                    $venue_id = DB::table('venues')->insertGetId(
                        array('company_id' => $company->id, 'venue_name' => $venue_name, 'description' => $venue_description, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id,
                        'city_id' => $venue_city_id, 'postal_code' => $venue_zip, 'street_name' => $venue_street, 'street_number' => $venue_street_number ,'floor' => $venue_floor, 'contact_person' => $venue_contact_name, 'phone_office' => $venue_telephone,
                        'fax_office' => $venue_fax, 'email' => $venue_email, 'website_url' => $venue_website, 'timezone' => $venue_timezoneid, 'lat' => $venue_lat, 'lng' => $venue_lng)
                    );
                    // Insert opening hours in the table
                    DB::table('venue_opening_hours')->insert(array(
                        array('venue_id' => $venue_id, 'day_index' => '0', 'start_time' => $venue_startsunday, 'end_time' => $venue_endsunday, 'closed' => $venue_closedsunday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '1', 'start_time' => $venue_startmonday, 'end_time' => $venue_endmonday, 'closed' => $venue_closedmonday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '2', 'start_time' => $venue_starttuesday, 'end_time' => $venue_endtuesday, 'closed' => $venue_closedtuesday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '3', 'start_time' => $venue_startwednesday, 'end_time' => $venue_endwednesday, 'closed' => $venue_closedwednesday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '4', 'start_time' => $venue_startthursday, 'end_time' => $venue_endthursday, 'closed' => $venue_closedthursday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '5', 'start_time' => $venue_startfriday, 'end_time' => $venue_endfriday, 'closed' => $venue_closedfriday, 'timezone' => $venue_timezoneid),
                        array('venue_id' => $venue_id, 'day_index' => '6', 'start_time' => $venue_startsaturday, 'end_time' => $venue_endsaturday, 'closed' => $venue_closedsaturday, 'timezone' => $venue_timezoneid)
                    ));

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                } elseif ($group_id == "3") {
                    return Redirect::action('ControlCenterController@show_venue_bookings');
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }

    /**
     * Show form for editing venue - General
     */
    function show_edit_venue($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    $venue_information = DB::table('venues')->join('cities', 'cities.id', '=', 'venues.city_id')->join('states', 'states.id', '=', 'venues.state_id')
                        ->join('countries', 'countries.id', '=', 'venues.country_id')->join('timezones', 'timezones.id', '=', 'venues.timezone')
                        ->select('venues.id', 'venues.venue_name', 'venues.description', 'venues.email', 'venues.postal_code', 'venues.street_name', 'venues.street_number' ,'venues.floor', 'venues.contact_person',
                            'venues.phone_office', 'venues.fax_office', 'venues.website_url', 'venues.timezone', 'venues.lat', 'venues.lng', 'venues.company_id',
                            'venues.slug_url', 'venues.facebook_url', 'venues.twitter_url', 'venues.linkedin_url', 'venues.googleplus_url',
                            'venues.arrival_info',
                            'cities.city_name', 'states.state_name', 'countries.country_name', 'timezones.zone_name')
                        ->where('venues.id', $id)->get();

                    $venue_hours = DB::table('venue_opening_hours')->where('venue_id', $id)->select('day_index','start_time','end_time', 'closed')->get();

                    $company_information = DB::table('companies')->where('companies.id', $venue_information[0]->company_id)->first();

                    return View::make('controlcenter.company_edit_venue', array('venue_information' => $venue_information, 'venue_hours' => $venue_hours, 'company_information' => $company_information));
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
        }
    }

    /**
     * Update edited venue in the database - General
     */
    function run_update_venue($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    // Venue Name
                    $venue_name = Input::get('venue_name');
                    $venue_description = Input::get('venue_description');
                    // Venue Address
                    $venue_street = Input::get('venue_street');
                    $venue_street_number = Input::get('venue_street_number');
                    $venue_floor = Input::get('venue_floor');
                    $venue_city = Input::get('venue_city');
                    $venue_state = Input::get('venue_state');
                    $venue_zip = Input::get('venue_zip');
                    $venue_country = Input::get('venue_country');
                    $venue_lat = Input::get('venue_lat');
                    $venue_lng = Input::get('venue_lng');
                    $venue_arrival_info = Input::get('venue_arrival_info');
                    // Venue Contacts
                    $venue_contact_name = Input::get('venue_contact_name');
                    $venue_telephone = Input::get('venue_telephone');
                    $venue_fax = Input::get('venue_fax');
                    $venue_email = Input::get('venue_email');
                    $venue_website = Input::get('venue_website');
                    // Venue Time
                    $venue_timezone = Input::get('venue_timeZoneId');
                    $venue_timezonename = Input::get('venue_timeZoneName');
                    $venue_startmonday = Input::get('venue_startmonday');
                    $venue_endmonday = Input::get('venue_endmonday');
                    $venue_closedmonday = Input::get('venue_closedmonday');
                    $venue_starttuesday = Input::get('venue_starttuesday');
                    $venue_endtuesday = Input::get('venue_endtuesday');
                    $venue_closedtuesday = Input::get('venue_closedtuesday');
                    $venue_startwednesday = Input::get('venue_startwednesday');
                    $venue_endwednesday = Input::get('venue_endwednesday');
                    $venue_closedwednesday = Input::get('venue_closedwednesday');
                    $venue_startthursday = Input::get('venue_startthursday');
                    $venue_endthursday = Input::get('venue_endthursday');
                    $venue_closedthursday = Input::get('venue_closedthursday');
                    $venue_startfriday = Input::get('venue_startfriday');
                    $venue_endfriday = Input::get('venue_endfriday');
                    $venue_closedfriday = Input::get('venue_closedfriday');
                    $venue_startsaturday = Input::get('venue_startsaturday');
                    $venue_endsaturday = Input::get('venue_endsaturday');
                    $venue_closedsaturday = Input::get('venue_closedsaturday');
                    $venue_startsunday = Input::get('venue_startsunday');
                    $venue_endsunday = Input::get('venue_endsunday');
                    $venue_closedsunday = Input::get('venue_closedsunday');

                    $venue_twitter = Input::get('venue_twitter');
                    $venue_facebook = Input::get('venue_facebook');
                    $venue_linkedin = Input::get('venue_linkedin');
                    $venue_googleplus = Input::get('venue_googleplus');

                    // make time variables matching for time format in DB
                    $venue_startmonday = ''.$venue_startmonday.':00:00';
                    $venue_endmonday = ''.$venue_endmonday.':00:00';
                    $venue_starttuesday = ''.$venue_starttuesday.':00:00';
                    $venue_endtuesday = ''.$venue_endtuesday.':00:00';
                    $venue_startwednesday = ''.$venue_startwednesday.':00:00';
                    $venue_endwednesday = ''.$venue_endwednesday.':00:00';
                    $venue_startthursday = ''.$venue_startthursday.':00:00';
                    $venue_endthursday = ''.$venue_endthursday.':00:00';
                    $venue_startfriday = ''.$venue_startfriday.':00:00';
                    $venue_endfriday = ''.$venue_endfriday.':00:00';
                    $venue_startsaturday = ''.$venue_startsaturday.':00:00';
                    $venue_endsaturday = ''.$venue_endsaturday.':00:00';
                    $venue_startsunday = ''.$venue_startsunday.':00:00';
                    $venue_endsunday = ''.$venue_endsunday.':00:00';

                    /* Inserting in the table */
                     // $user = Sentry::getUser();

                    // Add Venue in the table
                    // Create/associate entry for country, state and city
                    // Country
                    $existing_country = DB::table('countries')->where('country_name', $venue_country)->first();
                    if (is_null($existing_country))
                    {
                        $venue_country_id = DB::table('countries')->insertGetId(
                            array('country_name' => $venue_country)
                        );
                    } else {
                        $venue_country_id = $existing_country->id;
                    }
                    // State
                    $existing_state = DB::table('states')->where('state_name', $venue_state)->
                        where('country_id', $venue_country_id)->first();
                    if (is_null($existing_state))
                    {
                        $venue_country_id = DB::table('states')->insertGetId(
                            array('state_name' => $venue_state, 'country_id' => $venue_country_id)
                        );
                    } else {
                        $venue_state_id = $existing_state->id;
                    }
                    // City
                    $existing_city = DB::table('cities')->where('city_name', $venue_city)->
                        where('country_id', $venue_country_id)->where('state_id', $venue_state_id)->first();
                    if (is_null($existing_city))
                    {
                        $venue_city_id = DB::table('cities')->insertGetId(
                            array('city_name' => $venue_city, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id)
                        );
                    } else {
                        $venue_city_id = $existing_city->id;
                    }

                    // Check for real Timezone ID in "timezones" table
                    $find_timezoneid = DB::table('timezones')->where('zone_name', $venue_timezone)->first();
                    if (is_null($find_timezoneid))
                    {
                        $venue_timezoneid = '-1';
                    }
                    else
                    {
                        $venue_timezoneid = $find_timezoneid->id;
                    }

                    // Insert in the table
                    DB::table('venues')->where('id', $id)->update(
                        array('venue_name' => $venue_name, 'description' => $venue_description, 'country_id' => $venue_country_id, 'state_id' => $venue_state_id,
                        'city_id' => $venue_city_id, 'postal_code' => $venue_zip, 'street_name' => $venue_street, 'street_number' => $venue_street_number ,'floor' => $venue_floor,
                        'contact_person' => $venue_contact_name, 'phone_office' => $venue_telephone, 'fax_office' => $venue_fax, 'email' => $venue_email, 'website_url' => $venue_website, 
                        'timezone' => $venue_timezoneid, 'lat' => $venue_lat, 'lng' => $venue_lng,
                        'twitter_url' => $venue_twitter, 'facebook_url' => $venue_facebook, 'linkedin_url' => $venue_linkedin, 'googleplus_url' => $venue_googleplus)
                    );
                    // Insert opening hours in the table
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '0')->update(
                        array('start_time' => $venue_startsunday, 'end_time' => $venue_endsunday, 'closed' => $venue_closedsunday));
                    
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '1')->update(
                        array('start_time' => $venue_startmonday, 'end_time' => $venue_endmonday, 'closed' => $venue_closedmonday));
                        
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '2')->update(
                        array('start_time' => $venue_starttuesday, 'end_time' => $venue_endtuesday, 'closed' => $venue_closedtuesday));
                        
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '3')->update(
                        array('start_time' => $venue_startwednesday, 'end_time' => $venue_endwednesday, 'closed' => $venue_closedwednesday));
                        
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '4')->update(
                        array('start_time' => $venue_startthursday, 'end_time' => $venue_endthursday, 'closed' => $venue_closedthursday));
                        
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '5')->update(
                        array('start_time' => $venue_startfriday, 'end_time' => $venue_endfriday, 'closed' => $venue_closedfriday));
                        
                    DB::table('venue_opening_hours')->where('venue_id', $id)->where('day_index', '6')->update(
                        array('start_time' => $venue_startsaturday, 'end_time' => $venue_endsaturday, 'closed' => $venue_closedsaturday));

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }

    /**
     * Show form for editing venue - Calendar
     */
    function show_edit_venue_calendar($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    $venue = DB::table('venues')
                        ->where('venues.id', $id)->first();

                    $company = DB::table('companies')->where('companies.id', $venue->company_id)->first();

                    return View::make('controlcenter.company_edit_venue_calendar', array('venue' => $venue, 'company' => $company));
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
        }
    }


    /**
     * Update edited venue in the database - Calendar
     */
    function run_update_venue_calendar($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    $username = Input::get('venue_calendar_username');
                    $password = Input::get('venue_calendar_password');

                    $digesta1 = md5($username.":SabreDAV:".$password);

                    /* Inserting/Updating in the table */
                    // Insert in the table
                    DB::table('venues')->where('id', $id)->update(
                        array('digesta1' => $digesta1)
                    );

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }


    /**
     * Show form for editing venue - Exception Rules
     */
    function show_edit_venue_exception_rules($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    $venue = DB::table('venues')->join('timezones', 'timezones.id', '=', 'venues.timezone')
                        ->select('venues.id', 'venues.venue_name', 'venues.description', 'venues.timezone', 'venues.company_id',
                            'timezones.zone_name')
                        ->where('venues.id', $id)->first();

                    $company = DB::table('companies')
                        ->where('companies.id', $venue->company_id)->first();

                    $exception_rules = DB::table('venue_exception_rule_events')->where('venue_id', $id)->get();

                    return View::make('controlcenter.company_edit_venue_exception_rules', array('venue' => $venue, 'company' => $company, 'exception_rules' => $exception_rules));
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
        }
    }


    /**
     * Update edited venue in the database - Calendar
     */
    function run_update_venue_exception_rules($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');

                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    $title = Input::get('modal_exception_rules_title');
                    $startdate = Input::get('modal_exception_rules_startdate');
                    $enddate = Input::get('modal_exception_rules_enddate');
                    $starttime = Input::get('modal_exception_rules_starttime');
                    $endtime = Input::get('modal_exception_rules_endtime');
                    $rr_type = Input::get('modal_exception_rules_recurring_type');

                    $temp_starttime = ''. $startdate .' '. $starttime .'';
                    $dtstart = Carbon::createFromFormat('d/m/Y H:i', $temp_starttime)->toDateTimeString();
                    $temp_endtime = ''. $enddate .' '. $endtime .'';
                    $dtend = Carbon::createFromFormat('d/m/Y H:i', $temp_endtime)->toDateTimeString();

                    // Get timezone id
                    $venue = DB::table('venues')->where('venues.id', $id)->first();

                    if ($rr_type == 'daily') {
                        DB::table('venue_exception_rule_events')->insert(
                            array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                'venue_id' => $id, 'timezone_id' => $venue->timezone)
                        );
                    } elseif ($rr_type == 'weekly') {
                        $byday = Input::get('modal_exception_rules_weekly_days');

                        DB::table('venue_exception_rule_events')->insert(
                            array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                'byday' => $byday, 'venue_id' => $id, 'timezone_id' => $venue->timezone)
                        );
                    } elseif ($rr_type == 'monthly') {
                        $bymonthday = Input::get('modal_exception_rules_monthly_month_days');
                        $bymonthly_count = Input::get('modal_exception_rules_monthly_count');
                        $bymonthly_day = Input::get('modal_exception_rules_monthly_day');

                        $byday = $bymonthly_count.''.$bymonthly_day;

                        if ($bymonthday == "") {
                            DB::table('venue_exception_rule_events')->insert(
                                array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                    'byday' => $byday, 'venue_id' => $id, 'timezone_id' => $venue->timezone)
                            );
                        } else {
                            DB::table('venue_exception_rule_events')->insert(
                                array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                    'bymonthday' => $bymonthday, 'venue_id' => $id, 'timezone_id' => $venue->timezone)
                            );
                        }
                    } elseif ($rr_type == 'yearly') {
                        $bymonth = Input::get('modal_exception_rules_yearly_months');
                        $bymonthday = Input::get('modal_exception_rules_yearly_month_days');
                        $byyearly_count = Input::get('modal_exception_rules_yearly_count');
                        $byyearly_day = Input::get('modal_exception_rules_yearly_day');

                        $byday = $byyearly_count.''.$byyearly_day;

                        if ($bymonthday == "") {
                            DB::table('venue_exception_rule_events')->insert(
                                array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                    'bymonth' => $bymonth, 'byday' => $byday, 'venue_id' => $id, 'timezone_id' => $venue->timezone)
                            );
                        } else {
                            DB::table('venue_exception_rule_events')->insert(
                            array('title' => $title, 'dtstart' => $dtstart, 'dtend' => $dtend, 'until' => $dtend, 'freq' => $rr_type,
                                'bymonth' => $bymonth, 'bymonthday' => $bymonthday, 'venue_id' => $id, 'timezone_id' => $venue->timezone)
                        );
                        }
                    }

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }



    /**
     * Show form for editing venue - User Management
     */
    function show_edit_venue_user_management($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    $venue_information = DB::table('venues')->join('cities', 'cities.id', '=', 'venues.city_id')->join('states', 'states.id', '=', 'venues.state_id')
                        ->join('countries', 'countries.id', '=', 'venues.country_id')->join('timezones', 'timezones.id', '=', 'venues.timezone')
                        ->select('venues.id', 'venues.venue_name', 'venues.description', 'venues.email', 'venues.postal_code', 'venues.street_name', 'venues.street_number' ,'venues.floor', 'venues.contact_person',
                            'venues.phone_office', 'venues.fax_office', 'venues.website_url', 'venues.timezone', 'venues.lat', 'venues.lng', 'venues.company_id',
                            'venues.slug_url', 'venues.facebook_url', 'venues.twitter_url', 'venues.linkedin_url', 'venues.googleplus_url',
                            'venues.arrival_info',
                            'cities.city_name', 'states.state_name', 'countries.country_name', 'timezones.zone_name')
                        ->where('venues.id', $id)->get();

                    $venue_hours = DB::table('venue_opening_hours')->where('venue_id', $id)->select('day_index','start_time','end_time', 'closed')->get();

                    $company_information = DB::table('companies')->where('companies.id', $venue_information[0]->company_id)->first();

                    return View::make('controlcenter.company_edit_venue_user_management', array('venue_information' => $venue_information, 'venue_hours' => $venue_hours, 'company_information' => $company_information));
                }
            }
        } catch (Exception $e) {
            // Log::exception($e);
            print_r($e->getMessage());
        }
    }


    /** Action for ROOMS **/

    /**
     * Show form for creating new room
     */
    function show_create_room($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    $venue = DB::table('venues')->where('id', $id)->first();
                    $company = DB::table('companies')->where('id', $venue->company_id)->first();

                    return View::make('controlcenter.company_create_room', array('venue' => $venue, 'company' => $company));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            print_r($e->getMessage());
        }
    }

    /**
     * Store newly created room in the database
     */
    function run_store_room($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    // Office Name
                    $office_name = Input::get('office_name');
                    $office_description = Input::get('office_description');
                    // Office Details
                    $office_types = Input::get('office_types');
                    $office_capacity = Input::get('office_capacity');
                    $office_amenities_public_wifi = Input::get('office_amenities_public_wifi');
                    $office_amenities_secure_wifi = Input::get('office_amenities_secure_wifi');
                    $office_amenities_wired_internet = Input::get('office_amenities_wired_internet');
                    $office_amenities_flatscreen = Input::get('office_amenities_flatscreen');
                    $office_amenities_video_conferencing = Input::get('office_amenities_video_conferencing');
                    $office_amenities_projector = Input::get('office_amenities_projector');
                    $office_amenities_whiteboard = Input::get('office_amenities_whiteboard');
                    $office_amenities_flipboard = Input::get('office_amenities_flipboard');
                    $office_amenities_phone_room = Input::get('office_amenities_phone_room');
                    $office_amenities_conference_phone = Input::get('office_amenities_conference_phone');
                    $office_amenities_print_scan_copy = Input::get('office_amenities_print_scan_copy');
                    $office_amenities_handicap_accessible = Input::get('office_amenities_handicap_accessible');
                    $office_amenities_coffee_tea = Input::get('office_amenities_coffee_tea');
                    $office_amenities_filtered_water = Input::get('office_amenities_filtered_water');
                    $office_amenities_on_site_restaurant = Input::get('office_amenities_on_site_restaurant');
                    $office_amenities_catering = Input::get('office_amenities_catering');
                    $office_amenities_concierge_service = Input::get('office_amenities_concierge_service');
                    $office_amenities_notary_service = Input::get('office_amenities_notary_service');
                    $office_amenities_shared_kitchen = Input::get('office_amenities_shared_kitchen');
                    $office_amenities_shower_facility = Input::get('office_amenities_shower_facility');
                    $office_amenities_pet_friendly = Input::get('office_amenities_pet_friendly');
                    $office_amenities_outdoor_space = Input::get('office_amenities_outdoor_space');
                    // Pricing
                    $office_currencies = Input::get('office_currencies');
                    $office_price_hourly = Input::get('office_price_hourly');
                    $office_flattax_hourly = Input::get('office_flattax_hourly');
                    $office_percentagetax_hourly = Input::get('office_percentagetax_hourly');
                    $office_price_daily = Input::get('office_price_daily');
                    $office_flattax_daily = Input::get('office_flattax_daily');
                    $office_percentagetax_daily = Input::get('office_percentagetax_daily');
                    $office_price_monthly = Input::get('office_price_monthly');
                    $office_flattax_monthly = Input::get('office_flattax_monthly');
                    $office_percentagetax_monthly = Input::get('office_percentagetax_monthly');
                    // Time Information
                    $office_startmonday = Input::get('office_startmonday');
                    $office_endmonday = Input::get('office_endmonday');
                    $office_closedmonday = Input::get('office_closedmonday');
                    $office_starttuesday = Input::get('office_starttuesday');
                    $office_endtuesday = Input::get('office_endtuesday');
                    $office_closedtuesday = Input::get('office_closedtuesday');
                    $office_startwednesday = Input::get('office_startwednesday');
                    $office_endwednesday = Input::get('office_endwednesday');
                    $office_closedwednesday = Input::get('office_closedwednesday');
                    $office_startthursday = Input::get('office_startthursday');
                    $office_endthursday = Input::get('office_endthursday');
                    $office_closedthursday = Input::get('office_closedthursday');
                    $office_startfriday = Input::get('office_startfriday');
                    $office_endfriday = Input::get('office_endfriday');
                    $office_closedfriday = Input::get('office_closedfriday');
                    $office_startsaturday = Input::get('office_startsaturday');
                    $office_endsaturday = Input::get('office_endsaturday');
                    $office_closedsaturday = Input::get('office_closedsaturday');
                    $office_startsunday = Input::get('office_startsunday');
                    $office_endsunday = Input::get('office_endsunday');
                    $office_closedsunday = Input::get('office_closedsunday');

                    // make time variables matching for time format in DB. For Room
                    $office_startmonday = ''.$office_startmonday.':00:00';
                    $office_endmonday = ''.$office_endmonday.':00:00';
                    $office_starttuesday = ''.$office_starttuesday.':00:00';
                    $office_endtuesday = ''.$office_endtuesday.':00:00';
                    $office_startwednesday = ''.$office_startwednesday.':00:00';
                    $office_endwednesday = ''.$office_endwednesday.':00:00';
                    $office_startthursday = ''.$office_startthursday.':00:00';
                    $office_endthursday = ''.$office_endthursday.':00:00';
                    $office_startfriday = ''.$office_startfriday.':00:00';
                    $office_endfriday = ''.$office_endfriday.':00:00';
                    $office_startsaturday = ''.$office_startsaturday.':00:00';
                    $office_endsaturday = ''.$office_endsaturday.':00:00';
                    $office_startsunday = ''.$office_startsunday.':00:00';
                    $office_endsunday = ''.$office_endsunday.':00:00';

                    // Get useful parente venue details
                    $venue_details = DB::table('venues')->where('id', $id)->first();
                    $venue_timezoneid = $venue_details->timezone;

                     // Inserting in the table 
                     // $user = Sentry::getUser();


                    // Add Office in the table
                    // Calculate room prices
                    // Hourly Price
                    if ($office_price_hourly == "") {
                        $office_price_hourly = 0;
                        $office_flattax_hourly = 0;
                        $office_percentagetax_hourly = 0;
                        $office_retailprice_hourly = 0;
                    } else {
                        // Hourly Retail Price
                        if ($office_percentagetax_hourly != "")
                        {
                            $office_retailprice_hourly = $office_price_hourly + ($office_price_hourly * (intval($office_percentagetax_hourly)/100));
                            if ($office_flattax_hourly != "") {
                                $office_retailprice_hourly = $office_retailprice_hourly + $office_flattax_hourly;
                            }
                        }
                    }

                    // Daily Price
                    if ($office_price_daily == "") {
                        $office_price_daily = 0;
                        $office_flattax_daily = 0;
                        $office_percentagetax_daily = 0;
                        $office_retailprice_daily = 0;
                    } else {
                        // Daily Retail Price
                        if ($office_percentagetax_daily != "")
                        {
                            $office_retailprice_daily = $office_price_daily + ($office_price_daily * (intval($office_percentagetax_daily)/100));
                            if ($office_flattax_daily != "") {
                                $office_retailprice_daily = $office_retailprice_daily + $office_flattax_daily;
                            }
                        }
                    }

                    // Monthly price
                    if ($office_price_monthly == "") {
                        $office_price_monthly = '0';
                        $office_flattax_monthly = '0';
                        $office_percentagetax_monthly = '0';
                        $office_retailprice_monthly = '0';
                    } else {
                        // monthly Retail Price
                        if ($office_percentagetax_monthly != "")
                        {
                            $office_retailprice_monthly = $office_price_monthly + ($office_price_monthly * (intval($office_percentagetax_monthly)/100));
                            if ($office_flattax_monthly != "") {
                                $office_retailprice_monthly = $office_retailprice_monthly + $office_flattax_monthly;
                            }
                        }
                    }

                    // Insert in the table
                    $room_id = DB::table('rooms')->insertGetId(
                        array('venue_id' => $id, 'room_name' => $office_name, 'description' => $office_description, 'room_types_id' => $office_types, 'currency' => $office_currencies,
                        'max_capacity' => $office_capacity)
                    );
                    // Insert prices in the table
                    DB::table('room_prices')->insert(array(
                        array('room_id' => $room_id, 'price_original' => $office_price_hourly, 'price_period' => '0', 'tax_flat' => $office_flattax_hourly,
                        'tax_percentage' => $office_percentagetax_hourly, 'price_retail' => $office_retailprice_hourly),
                        array('room_id' => $room_id, 'price_original' => $office_price_daily, 'price_period' => '1', 'tax_flat' => $office_flattax_daily,
                        'tax_percentage' => $office_percentagetax_daily, 'price_retail' => $office_retailprice_daily),
                        array('room_id' => $room_id, 'price_original' => $office_price_monthly, 'price_period' => '2', 'tax_flat' => $office_flattax_monthly,
                        'tax_percentage' => $office_percentagetax_monthly, 'price_retail' => $office_retailprice_monthly)
                    ));
                    // Insert opening hours in the table
                     DB::table('room_opening_hours')->insert(array(
                        array('room_id' => $room_id, 'day_index' => '0', 'start_time' => $office_startsunday, 'end_time' => $office_endsunday, 'closed' => $office_closedsunday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '1', 'start_time' => $office_startmonday, 'end_time' => $office_endmonday, 'closed' => $office_closedmonday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '2', 'start_time' => $office_starttuesday, 'end_time' => $office_endtuesday, 'closed' => $office_closedtuesday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '3', 'start_time' => $office_startwednesday, 'end_time' => $office_endwednesday, 'closed' => $office_closedwednesday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '4', 'start_time' => $office_startthursday, 'end_time' => $office_endthursday, 'closed' => $office_closedthursday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '5', 'start_time' => $office_startfriday, 'end_time' => $office_endfriday, 'closed' => $office_closedfriday, 'timezone' => $venue_timezoneid),
                        array('room_id' => $room_id, 'day_index' => '6', 'start_time' => $office_startsaturday, 'end_time' => $office_endsaturday, 'closed' => $office_closedsaturday, 'timezone' => $venue_timezoneid)
                    ));
                    // Insert amenities in the table
                    DB::table('amenities')->insert(
                        array('room_id' => $room_id, 'public_wifi' => $office_amenities_public_wifi, 'secure_wifi' => $office_amenities_secure_wifi,
                        'wired_internet' => $office_amenities_wired_internet, 'flatscreen' => $office_amenities_flatscreen, 'video_conferencing' => $office_amenities_video_conferencing,
                        'flipboard' => $office_amenities_flipboard, 'whiteboard' => $office_amenities_whiteboard, 'projector' => $office_amenities_projector,
                        'print_scan_copy' => $office_amenities_print_scan_copy, 'shared_kitchen' => $office_amenities_shared_kitchen, 'catering' => $office_amenities_catering,
                        'concierge_service' => $office_amenities_concierge_service, 'filtered_water' => $office_amenities_filtered_water, 'phone_room' => $office_amenities_phone_room,
                        'pet_friendly' => $office_amenities_pet_friendly, 'handicap_accessible' => $office_amenities_handicap_accessible, 'conference_phone' => $office_amenities_conference_phone,
                        'outdoor_space' => $office_amenities_outdoor_space, 'on_site_restaurant' => $office_amenities_on_site_restaurant, 'coffee_tea' => $office_amenities_coffee_tea,
                        'notary_service' => $office_amenities_notary_service, 'shower_facility' => $office_amenities_shower_facility)
                    );

                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }
            }

        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }

    /**
     * Show form for editing room
     */
    function show_edit_room($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {
                    $room_information = DB::table('rooms')->join('amenities', 'room_id', '=', 'rooms.id')->join('room_types', 'room_types.id', '=', 'room_types_id')
                        ->select('rooms.id' ,'rooms.venue_id', 'rooms.room_name', 'rooms.description', 'rooms.max_capacity', 'rooms.currency', 'rooms.room_types_id', 'room_types.room_type_name',
                            'rooms.preparation_time', 'rooms.pre_booking_period', 'rooms.buffer_time',
                            'amenities.public_wifi', 'amenities.secure_wifi', 'amenities.wired_internet', 'amenities.flatscreen', 'amenities.video_conferencing',
                            'amenities.flipboard', 'amenities.whiteboard', 'amenities.projector', 'amenities.print_scan_copy', 'amenities.shared_kitchen',
                            'amenities.catering', 'amenities.concierge_service', 'amenities.filtered_water', 'amenities.phone_room', 'amenities.pet_friendly',
                            'amenities.handicap_accessible', 'amenities.conference_phone', 'amenities.outdoor_space', 'amenities.on_site_restaurant',
                            'amenities.coffee_tea', 'amenities.notary_service', 'amenities.shower_facility')
                        ->where('rooms.id', $id)->get();

                    $room_prices = DB::table('room_prices')->where('room_id', $id)->select('room_prices.price_original', 'room_prices.price_period', 'room_prices.tax_flat',
                        'room_prices.tax_percentage')->get();

                    $room_hours = DB::table('room_opening_hours')->where('room_id', $id)->select('day_index','start_time','end_time', 'closed')->get();

                    $venue = DB::table('venues')->where('id', $room_information[0]->venue_id)->first();
                    $company = DB::table('companies')->where('id', $venue->company_id)->first();

                    return View::make('controlcenter.company_edit_room', array('room_information' => $room_information, 'room_prices' => $room_prices, 'room_hours' => $room_hours, 'venue' => $venue, 'company' => $company));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
        }
    }

    /**
     * Update edited room in the database
     */
    function run_update_room($id)
    {
        try {
            //check if logged in
            if ( ! Sentry::check())
            {
                // User is not logged in, or is not activated
                return Redirect::to('/');
            }
            else
            {
                //get session key
                $group_id = Session::get('group_id_key');
                //customer
                if($group_id == "1"){
                    return View::make('controlcenter.customer_bookings');
                } elseif (($group_id == "2") || ($group_id == "3")) {

                    // Useful variable
                    $datetime = new DateTime;

                    // Fetch INPUT posts
                    // Office Name
                    $office_name = Input::get('office_name');
                    $office_description = Input::get('office_description');
                    // Office Details
                    $office_types = Input::get('office_types');
                    $office_capacity = Input::get('office_capacity');
                    $office_amenities_public_wifi = Input::get('office_amenities_public_wifi');
                    $office_amenities_secure_wifi = Input::get('office_amenities_secure_wifi');
                    $office_amenities_wired_internet = Input::get('office_amenities_wired_internet');
                    $office_amenities_flatscreen = Input::get('office_amenities_flatscreen');
                    $office_amenities_video_conferencing = Input::get('office_amenities_video_conferencing');
                    $office_amenities_projector = Input::get('office_amenities_projector');
                    $office_amenities_whiteboard = Input::get('office_amenities_whiteboard');
                    $office_amenities_flipboard = Input::get('office_amenities_flipboard');
                    $office_amenities_phone_room = Input::get('office_amenities_phone_room');
                    $office_amenities_conference_phone = Input::get('office_amenities_conference_phone');
                    $office_amenities_print_scan_copy = Input::get('office_amenities_print_scan_copy');
                    $office_amenities_handicap_accessible = Input::get('office_amenities_handicap_accessible');
                    $office_amenities_coffee_tea = Input::get('office_amenities_coffee_tea');
                    $office_amenities_filtered_water = Input::get('office_amenities_filtered_water');
                    $office_amenities_on_site_restaurant = Input::get('office_amenities_on_site_restaurant');
                    $office_amenities_catering = Input::get('office_amenities_catering');
                    $office_amenities_concierge_service = Input::get('office_amenities_concierge_service');
                    $office_amenities_notary_service = Input::get('office_amenities_notary_service');
                    $office_amenities_shared_kitchen = Input::get('office_amenities_shared_kitchen');
                    $office_amenities_shower_facility = Input::get('office_amenities_shower_facility');
                    $office_amenities_pet_friendly = Input::get('office_amenities_pet_friendly');
                    $office_amenities_outdoor_space = Input::get('office_amenities_outdoor_space');
                    // Pricing
                    $office_currencies = Input::get('office_currencies');
                    $office_price_hourly = Input::get('office_price_hourly');
                    $office_flattax_hourly = Input::get('office_flattax_hourly');
                    $office_percentagetax_hourly = Input::get('office_percentagetax_hourly');
                    $office_price_daily = Input::get('office_price_daily');
                    $office_flattax_daily = Input::get('office_flattax_daily');
                    $office_percentagetax_daily = Input::get('office_percentagetax_daily');
                    $office_price_monthly = Input::get('office_price_monthly');
                    $office_flattax_monthly = Input::get('office_flattax_monthly');
                    $office_percentagetax_monthly = Input::get('office_percentagetax_monthly');
                    // Time Information
                    $office_startmonday = Input::get('office_startmonday');
                    $office_endmonday = Input::get('office_endmonday');
                    $office_closedmonday = Input::get('office_closedmonday');
                    $office_starttuesday = Input::get('office_starttuesday');
                    $office_endtuesday = Input::get('office_endtuesday');
                    $office_closedtuesday = Input::get('office_closedtuesday');
                    $office_startwednesday = Input::get('office_startwednesday');
                    $office_endwednesday = Input::get('office_endwednesday');
                    $office_closedwednesday = Input::get('office_closedwednesday');
                    $office_startthursday = Input::get('office_startthursday');
                    $office_endthursday = Input::get('office_endthursday');
                    $office_closedthursday = Input::get('office_closedthursday');
                    $office_startfriday = Input::get('office_startfriday');
                    $office_endfriday = Input::get('office_endfriday');
                    $office_closedfriday = Input::get('office_closedfriday');
                    $office_startsaturday = Input::get('office_startsaturday');
                    $office_endsaturday = Input::get('office_endsaturday');
                    $office_closedsaturday = Input::get('office_closedsaturday');
                    $office_startsunday = Input::get('office_startsunday');
                    $office_endsunday = Input::get('office_endsunday');
                    $office_closedsunday = Input::get('office_closedsunday');
                    // Extra Booking Information
                    $office_preparation_time = Input::get('office_preparation_time');
                    $office_pre_booking_period = Input::get('office_pre_booking_period');
                    $office_buffer_time = Input::get('office_buffer_time');

                    // make time variables matching for time format in DB. For Room
                    $office_startmonday = ''.$office_startmonday.':00:00';
                    $office_endmonday = ''.$office_endmonday.':00:00';
                    $office_starttuesday = ''.$office_starttuesday.':00:00';
                    $office_endtuesday = ''.$office_endtuesday.':00:00';
                    $office_startwednesday = ''.$office_startwednesday.':00:00';
                    $office_endwednesday = ''.$office_endwednesday.':00:00';
                    $office_startthursday = ''.$office_startthursday.':00:00';
                    $office_endthursday = ''.$office_endthursday.':00:00';
                    $office_startfriday = ''.$office_startfriday.':00:00';
                    $office_endfriday = ''.$office_endfriday.':00:00';
                    $office_startsaturday = ''.$office_startsaturday.':00:00';
                    $office_endsaturday = ''.$office_endsaturday.':00:00';
                    $office_startsunday = ''.$office_startsunday.':00:00';
                    $office_endsunday = ''.$office_endsunday.':00:00';

                    // Get useful parente venue details
                    $find_venue = DB::table('rooms')->where('id', $id)->first();
                    $venue_id = $find_venue->venue_id;
                    $venue_details = DB::table('venues')->where('id', $venue_id)->first();
                    $venue_timezoneid = $venue_details->timezone;


                    /* Inserting in the table */
                     // $user = Sentry::getUser();


                    // Add Office in the table
                    // Calculate room prices
                    // Hourly Price
                    if ($office_price_hourly == "") {
                        $office_price_hourly = 0;
                        $office_flattax_hourly = 0;
                        $office_percentagetax_hourly = 0;
                        $office_retailprice_hourly = 0;
                    } else {
                        // Hourly Retail Price
                        if ($office_percentagetax_hourly != "")
                        {
                            $office_retailprice_hourly = $office_price_hourly + ($office_price_hourly * (intval($office_percentagetax_hourly)/100));
                            if ($office_flattax_hourly != "") {
                                $office_retailprice_hourly = $office_retailprice_hourly + $office_flattax_hourly;
                            }
                        }
                    }

                    // Daily Price
                    if ($office_price_daily == "") {
                        $office_price_daily = 0;
                        $office_flattax_daily = 0;
                        $office_percentagetax_daily = 0;
                        $office_retailprice_daily = 0;
                    } else {
                        // Daily Retail Price
                        if ($office_percentagetax_daily != "")
                        {
                            $office_retailprice_daily = $office_price_daily + ($office_price_daily * (intval($office_percentagetax_daily)/100));
                            if ($office_flattax_daily != "") {
                                $office_retailprice_daily = $office_retailprice_daily + $office_flattax_daily;
                            }
                        }
                    }

                    // Monthly price
                    if ($office_price_monthly == "") {
                        $office_price_monthly = 0;
                        $office_flattax_monthly = 0;
                        $office_percentagetax_monthly = 0;
                        $office_retailprice_monthly = 0;
                    } else {
                        // monthly Retail Price
                        if ($office_percentagetax_monthly != "")
                        {
                            $office_retailprice_monthly = $office_price_monthly + ($office_price_monthly * (intval($office_percentagetax_monthly)/100));
                            if ($office_flattax_monthly != "") {
                                $office_retailprice_monthly = $office_retailprice_monthly + $office_flattax_monthly;
                            }
                        }
                    }

                    // Insert in the table
                    DB::table('rooms')->where('id', $id)->update(
                        array('room_name' => $office_name, 'description' => $office_description, 'room_types_id' => $office_types, 'currency' => $office_currencies,
                        'max_capacity' => $office_capacity, 'preparation_time' => $office_preparation_time, 'pre_booking_period' => $office_pre_booking_period,
                        'buffer_time' => $office_buffer_time)
                    );
                    // Insert prices in the table
                    DB::table('room_prices')->where('room_id', $id)->where('price_period', '0')->update(
                        array('price_original' => $office_price_hourly, 'tax_flat' => $office_flattax_hourly,
                        'tax_percentage' => $office_percentagetax_hourly, 'price_retail' => $office_retailprice_hourly));

                    DB::table('room_prices')->where('room_id', $id)->where('price_period', '1')->update(
                        array('price_original' => $office_price_daily, 'tax_flat' => $office_flattax_daily,
                        'tax_percentage' => $office_percentagetax_daily, 'price_retail' => $office_retailprice_daily));

                    DB::table('room_prices')->where('room_id', $id)->where('price_period', '2')->update(
                        array('price_original' => $office_price_monthly, 'tax_flat' => $office_flattax_monthly,
                        'tax_percentage' => $office_percentagetax_monthly, 'price_retail' => $office_retailprice_monthly));

                    // Insert opening hours in the table
                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '0')->update(
                        array('start_time' => $office_startsunday, 'end_time' => $office_endsunday, 'closed' => $office_closedsunday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '1')->update(
                        array('start_time' => $office_startmonday, 'end_time' => $office_endmonday, 'closed' => $office_closedmonday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '2')->update(
                        array('start_time' => $office_starttuesday, 'end_time' => $office_endtuesday, 'closed' => $office_closedtuesday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '3')->update(
                        array('start_time' => $office_startwednesday, 'end_time' => $office_endwednesday, 'closed' => $office_closedwednesday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '4')->update(
                        array('start_time' => $office_startthursday, 'end_time' => $office_endthursday, 'closed' => $office_closedthursday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '5')->update(
                        array('start_time' => $office_startfriday, 'end_time' => $office_endfriday, 'closed' => $office_closedfriday));

                    DB::table('room_opening_hours')->where('room_id', $id)->where('day_index', '6')->update(
                        array('start_time' => $office_startsaturday, 'end_time' => $office_endsaturday, 'closed' => $office_closedsaturday));

                    // Insert amenities in the table
                    DB::table('amenities')->where('room_id', $id)->update(
                        array('public_wifi' => $office_amenities_public_wifi, 'secure_wifi' => $office_amenities_secure_wifi,
                        'wired_internet' => $office_amenities_wired_internet, 'flatscreen' => $office_amenities_flatscreen, 'video_conferencing' => $office_amenities_video_conferencing,
                        'flipboard' => $office_amenities_flipboard, 'whiteboard' => $office_amenities_whiteboard, 'projector' => $office_amenities_projector,
                        'print_scan_copy' => $office_amenities_print_scan_copy, 'shared_kitchen' => $office_amenities_shared_kitchen, 'catering' => $office_amenities_catering,
                        'concierge_service' => $office_amenities_concierge_service, 'filtered_water' => $office_amenities_filtered_water, 'phone_room' => $office_amenities_phone_room,
                        'pet_friendly' => $office_amenities_pet_friendly, 'handicap_accessible' => $office_amenities_handicap_accessible, 'conference_phone' => $office_amenities_conference_phone,
                        'outdoor_space' => $office_amenities_outdoor_space, 'on_site_restaurant' => $office_amenities_on_site_restaurant, 'coffee_tea' => $office_amenities_coffee_tea,
                        'notary_service' => $office_amenities_notary_service, 'shower_facility' => $office_amenities_shower_facility));


                    /* Send response to the form wizard */
                    return Response::json(array('status' => 'success', 'message' => 'Inputs received.'));
                }
            }
        } catch (Exception $e) {
            Log::exception($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }


    /**
     *  EXPRESS  Calendar
     * 
     **/

    function show_express_calendar(){

       try {
            if(!Sentry::check())
            {
              return Redirect::to('/');

            }else{
                $user_id = '3';
                // Calculate venue _id 1


                $rooms = DB::table('rooms')->select('id', 'room_name')->where('venue_id', '1')->get();
                $venue = DB::table('venues')->where('id', '1')->first();
                return View::make('express.calendar', array('venue' =>$venue, 'rooms' => $rooms));
            } 
        } catch (Exception $e) {
            Log::exception($e);
        }
    }

    /**
     *  Initialize Express Calendar after Ajax call.
     */
    function init_express_calendar(){
        try{
            //Fetch correct venue timezone first...
            $now = Carbon::now(new DateTimeZone('Europe/Berlin'));
            //Join Room Name... maybe
            $calendar_events = DB::table('customer_bookings')->join('users','users.id','=','customer_bookings.user_id')->join('rooms','rooms.id','=','customer_bookings.room_id')->select('customer_bookings.id','rooms.room_name','users.first_name','users.last_name','customer_bookings.room_id', '.customer_bookings.venue_id', 'customer_bookings.start_time','customer_bookings.end_time','customer_bookings.title','customer_bookings.timezone','customer_bookings.created_at')->where('customer_bookings.venue_id', '1')->where('customer_bookings.start_time', '>', $now )->get();

            return Response::json(array('status' => 'OK', 'message' => 'Express Calendar Initialized' , 'calendardata' => $calendar_events, 'last_update' => $now->toDateTimeString()));
        }catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));

        }

    }


    //Longpolling express_calendar()
    function longpolling_express_calendar(){
        try{
            if(session_id())
            {
               session_write_close();
            }
            if (Input::has('last_updated_at')){
                $last_update = Input::get('last_updated_at');
                $now = Carbon::now(new DateTimeZone('Europe/Berlin'));
                $last_update_to_time = Carbon::createFromTimestamp(strtotime($last_update));
                $new_events = null;
                while(empty($new_events))
                {
                   sleep(5);
                   $new_events = DB::table('customer_bookings')->select('room_id', 'venue_id', 'start_time','end_time','title','timezone','created_at')->where('venue_id', '1')->where('created_at', '>', $last_update_to_time )->get();
                }
               
                //$new_events = $now->toDateTimeString();
                // $new_events ='testestest';
                return Response::json(array('status' => 'OK', 'message' => 'Longpolling in progress....' , 'calendardata' => $new_events  , 'last_update' => $now->toDateTimeString(), 'previous_last_update' => $last_update));
            } else {
                return Response::json(array('status' => 'Error', 'message' => 'No Inputs given', 'calendardata' => null));
            }
        }catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }

    // Post booking_id and send back booking details
    function express_booking_details(){
        //Check Permission later....
         try{
        if (Input::has('booking_id')){
              $booking_id = Input::get('booking_id');
              $booking_details = DB::table('customer_bookings')->join('users','users.id','=','customer_bookings.user_id')->join('rooms','rooms.id','=','customer_bookings.room_id')->select('customer_bookings.id','customer_bookings.comment','rooms.room_name','users.first_name','users.last_name','customer_bookings.room_id', '.customer_bookings.venue_id', 'customer_bookings.start_time','customer_bookings.end_time','customer_bookings.title','customer_bookings.timezone','customer_bookings.created_at')->where('customer_bookings.id', $booking_id)->get();
              return Response::json(array('status' => 'OK', 'message' => 'Booking Details fetched' ,'booking_details' => $booking_details));
        }}catch(Exception $e){
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
    }





    function express_test(){
       $now = Carbon::now(new DateTimeZone('Europe/Berlin'));
       $new_events = DB::table('customer_bookings')->select('room_id', 'venue_id', 'start_time','end_time','title','timezone','created_at')->where('venue_id', '1')->where('created_at', '>', $now )->get();
       print_r($new_events);
       if(empty($new_events)){
        echo "empty";
    }else{ echo "not empty";}
    }


}