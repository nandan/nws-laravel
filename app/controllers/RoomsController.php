<?php

class RoomsController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show_index_room()
	{
        return View::make('rooms.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function show_create_room()
	{
        return View::make('rooms.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function run_store_room()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show_room($id)
	{
		try {
	        $room = DB::table('rooms')
	        	->join('room_types', 'rooms.room_types_id', '=', 'room_types.id')
	        	->select('rooms.id', 'rooms.room_name', 'rooms.description', 'rooms.max_capacity',
	        		'rooms.currency', 'room_types.room_type_name', 'rooms.venue_id')
	        	->where('rooms.id', $id)
	        	->first();

	        $price_query = DB::table('rooms')
				->where('rooms.id', $id)
				->join('room_prices', 'rooms.id', '=', 'room_prices.room_id');

			$room_hourly_price = $price_query->min('price_retail');
			$room_daily_price = $price_query->max('price_retail');

	        $venue = DB::table('venues')
	        	->join('cities', 'venues.city_id', '=', 'cities.id')
	        	->join('states', 'venues.state_id', '=', 'states.id')
	        	->join('countries', 'venues.country_id', '=', 'countries.id')
	        	->where('venues.id', $room->venue_id)
	        	->first();

	        $amenities = DB::table('amenities')
	        	->where('room_id', $id)
	        	->first();

	        $other_rooms = DB::table('rooms')
	        	->join('room_prices', function($join) {
	        		$join->on('rooms.id', '=', 'room_prices.room_id')
	        			->where('room_prices.price_period', '=', '0');
	        	})
	        	->select('rooms.id', 'rooms.room_name', 'room_prices.price_period', 'room_prices.price_retail')
	        	->where('venue_id', $room->venue_id)
	        	->where('rooms.id', '!=', $id)
	        	->get();

	  	    // $queries = DB::getQueryLog();
			// $last_query = end($queries);
			// print_r($last_query);

	        // print_r($amenities);

			return View::make('rooms.show', array('room' => $room, 'venue' => $venue, 'amenities' => $amenities, 
				'room_hourly_price' => $room_hourly_price, 'room_daily_price' => $room_daily_price, 'other_rooms' => $other_rooms));
		}catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show_edit_room($id)
	{
        return View::make('rooms.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function run_update_room($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function run_destroy_room($id)
	{
		//
	}

}
