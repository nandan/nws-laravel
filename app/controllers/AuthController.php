<?php
/*
* HANDLING AUTHENTICATION
*/
class AuthController extends BaseController {


	/*
	* LOGIN WITH SENTRY 2 WITH AJAX
	*/
	function run_login(){
		try{
			$password = Input::get('password');
			$email = Input::get('email');
            // Set login credentials
			$credentials = array(
				'email' => $email,
				'password' => $password,
				);
            // Try to authenticate the user remember false
			$user = Sentry::authenticate($credentials, false);
            // Find Group ID
			$group_id = DB::table('users_groups')->where('user_id', $user->id)->pluck('group_id');   
            // Add Session Key
			Session::put('group_id_key', $group_id);
			return Response::json(array('status' => 'OK', 'message' => 'Successfully logged in', 'group_id' => $group_id));
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Login field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Wrong password, try again'));
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User was not found'));
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'auth_success'));
		} catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is suspended'));
		} catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is banned'));
		} catch (Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		}
	}

	/*
	* REGISTER A NEW USER WITH SENTRY 2 WITH AJAX
	*/
	function run_register(){
		try{
			$first_name = Input::get('first_name');
			$last_name = Input::get('last_name');
			$password = Input::get('passwordx');
			$email = Input::get('emailx');

        //Additional Validation for Email and Password length

			$user = Sentry::createUser(array(
				'email' => $email,
				'password' => $password,
				'activated' => true,
				'first_name' => $first_name,
				'last_name' => $last_name
				));
			$customer_group = Sentry::findGroupById(1);
			$user->addGroup($customer_group);

			$id = DB::table('users')->where('email', $email)->pluck('id');
			DB::table('customer_details')->insert(
				array('user_id' => $id, 'created_at' => new DateTime, 'updated_at' => new DateTime)
				);
			
			return Response::json(array('status' => 'OK', 'message' => 'Registered, Please check your activation email'));

		}catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Email field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User with this login already exists'));
		} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Group was not found'));
		}catch(Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		} 

	}

	/*
	* LOGOUT USER AND REDIRECT TO HOME
	*/
	function run_logout(){
		try{
			Sentry::logout();
			return Redirect::to('/');

		}catch(Exception $e){
			Log::error($e);
		}
	}


	/*
	* MODAL LOGIN for List your Workspace & Search Result Page
	*/
	function run_modal_login(){
		try{
			$password = Input::get('modal_password');
			$email = Input::get('modal_email');
            // Set login credentials
			$credentials = array(
				'email' => $email,
				'password' => $password,
				);
            // Try to authenticate the user remember false
			$user = Sentry::authenticate($credentials, false);
            // Find Group ID
			$group_id = DB::table('users_groups')->where('user_id', $user->id)->pluck('group_id');   
            // Add Session Key
			Session::put('group_id_key', $group_id);
			//Redirect::to('/wizard/create');

			return Response::json(array('status' => 'OK', 'message' => 'Successfully logged in', 'group_id' => $group_id));
		}catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Login field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Wrong password, try again'));
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User was not found'));
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'auth_success'));
		} catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is suspended'));
		} catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is banned'));
		}catch (Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		}

	}

	/*
	* MODAL REGISTER for List your Workspace & Search Result page
	*/

	function run_modal_register(){
		try{
			$first_name = Input::get('modal_first_name');
			$last_name = Input::get('modal_last_name');
			$password = Input::get('modal_passwordx');
			$email = Input::get('modal_emailx');

        //Additional Validation for Email and Password length

			$user = Sentry::createUser(array(
				'email' => $email,
				'password' => $password,
				'activated' => true,
				'first_name' => $first_name,
				'last_name' => $last_name
				));
			$customer_group = Sentry::findGroupById(1);
			$user->addGroup($customer_group);

			$id = DB::table('users')->where('email', $email)->pluck('id');
			DB::table('customer_details')->insert(
				array('user_id' => $id, 'created_at' => new DateTime, 'updated_at' => new DateTime)
				);
			
			return Response::json(array('status' => 'OK', 'message' => 'Registered, Please check your activation email'));

		}catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Email field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User with this login already exists'));
		} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Group was not found'));
		}catch(Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		} 
	}

	/*
	* MODAL REGISTER and quick login for Search Result Page
	*/

	function run_modal_register_login(){
		try{
			$first_name = Input::get('modal_first_name');
			$last_name = Input::get('modal_last_name');
			$password = Input::get('modal_passwordx');
			$email = Input::get('modal_emailx');

        //Additional Validation for Email and Password length

			$user = Sentry::createUser(array(
				'email' => $email,
				'password' => $password,
				'activated' => true,
				'first_name' => $first_name,
				'last_name' => $last_name
				));
			$customer_group = Sentry::findGroupById(1);
			$user->addGroup($customer_group);

			$id = DB::table('users')->where('email', $email)->pluck('id');
			DB::table('customer_details')->insert(
				array('user_id' => $id, 'created_at' => new DateTime, 'updated_at' => new DateTime)
				);
			
			// Set login credentials
			$credentials = array(
				'email' => $email,
				'password' => $password,
				);
            // Try to authenticate the user remember false
			$user = Sentry::authenticate($credentials, false);
            // Find Group ID
			// $group_id = DB::table('users_groups')->where('user_id', $user->id)->pluck('group_id');   
            // Add Session Key
			Session::put('group_id_key', $customer_group);

			return Response::json(array('status' => 'OK', 'message' => 'Registered, Please check your activation email'));

		}catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Email field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User with this login already exists'));
		} catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Group was not found'));
		}catch(Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		} 
	}


	/**
	 * LOGIN TO EXPRESS CALENDAR 
	 */
	 function run_express_login(){
	 	try{
			$password = Input::get('password');
			$email = Input::get('email');
            // Set login credentials
			$credentials = array(
				'email' => $email,
				'password' => $password,
				);
            // Try to authenticate the user remember false
			$user = Sentry::authenticate($credentials, false);
            // Find Group ID
			$group_id = DB::table('users_groups')->where('user_id', $user->id)->pluck('group_id');   
            // Add Session Key
			Session::put('group_id_key', $group_id);
			//Redirect::to('/wizard/create');

			return Response::json(array('status' => 'OK', 'message' => 'Successfully logged in', 'group_id' => $group_id));
		}catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Login field is required'));
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Password field is required'));
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'Wrong password, try again'));
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User was not found'));
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'auth_success'));
		} catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is suspended'));
		} catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
			return Response::json(array('status' => 'Error', 'message' => 'User is banned'));
		}catch (Exception $e){
			Log::error($e);
			return Response::json(array('status' => 'Error', 'message' => 'Exception Error'));
		}
		 	
	 }





}