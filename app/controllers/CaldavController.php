<?php
/*
* HANDLING CALDAV
*/


use Carbon\Carbon;
use Sabre;

class CaldavController extends BaseController {

	/**
	 * Check what version of sabreDAV is installed 
	 **/
	function installed(){

		echo 'SabreDAV ', \Sabre\DAV\Version::VERSION, ' is installed.';

	}

	
	/*
	 * Caldav Server
	 */
	function server(){

		include(app_path().'/caldavserver/caldavserver.php');

	}

	/*
	 * Hash Function test
	 */
	function hash(){

		//Realm SabreDAV
		
		$digesta1 = md5('venue1:SabreDAV:venue1');
		$password = Hash::make('venue1');
		echo 'DIGESTA1:  '.$digesta1;
		echo 'PASSWORD:  '.$password;

	}

	



}