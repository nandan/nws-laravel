<?php

use Carbon\Carbon;

class roomTimeTable {

	public $room_id = '1';
	public $sixam = 'freetime';
	public $sevenam = 'freetime';
	public $eigtham = 'freetime';
	public $nineam = 'freetime';
	public $tenam = 'freetime';
	public $elevenam = 'freetime';
	public $twelvepm = 'freetime';
	public $onepm = 'freetime';
	public $twopm = 'freetime';
	public $threepm = 'freetime';
	public $fourpm = 'freetime';
	public $fivepm = 'freetime';
	public $sixpm = 'freetime';
	public $sevenpm = 'freetime';
	public $eigthpm = 'freetime';
	public $ninepm = 'freetime';
	public $tenpm = 'freetime';
	public $elevenpm = 'freetime';
}

class VenuesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show_index_venu()
	{
        return View::make('venues.index');
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function show_create_venue()
	{
        return View::make('venues.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function run_store_venue()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show_venue($id)
	{
		$venue = DB::table('venues')
			->join('cities', 'venues.city_id', '=', 'cities.id')
        	->join('states', 'venues.state_id', '=', 'states.id')
        	->join('countries', 'venues.country_id', '=', 'countries.id')
			->where('venues.id', $id)
			->first();

		$rooms = DB::table('rooms')
			->join('room_types', 'rooms.room_types_id', '=', 'room_types.id')
			->join('amenities', 'rooms.id', '=', 'amenities.room_id')
        	->select('rooms.id', 'rooms.room_name', 'rooms.description', 'rooms.max_capacity',
        		'rooms.currency', 'room_types.room_type_name', 'rooms.venue_id',
        		'amenities.public_wifi', 'amenities.secure_wifi', 'amenities.wired_internet', 'amenities.flatscreen', 'amenities.video_conferencing', 
        		'amenities.flipboard', 'amenities.whiteboard', 'amenities.projector', 'amenities.print_scan_copy', 'amenities.shared_kitchen', 'amenities.catering',
        		'amenities.concierge_service', 'amenities.filtered_water', 'amenities.phone_room', 'amenities.pet_friendly', 'amenities.handicap_accessible',
        		'amenities.conference_phone', 'amenities.outdoor_space', 'amenities.on_site_restaurant', 'amenities.coffee_tea', 'amenities.notary_service',
        		'amenities.shower_facility')
			->where('venue_id', $id)
			->get();

		$other_venues = DB::table('venues')
	        	->select('venues.id', 'venues.venue_name')
	        	->where('city_id', $venue->city_id)
	        	->where('venues.id', '!=', $id)
	        	->get();


		// Get the day's calendar for Venues
		foreach ($rooms as $room) {
			$room_ids[] = $room->id;
		}

		// Passed booking events check
		// Now get all the events for the day
		$today = Carbon::now()->toDateString(); 
		
		$startwholedatetime = ''.$today.' 00:00:00';
		$endwholedatetime = ''.$today.' 23:59:59';

		$room_events = DB::table('booking_events')
			->whereIn('room_id', $room_ids)
			->where('booking_events.dtstart', '>', ''.$startwholedatetime.'')
			->where('booking_events.dtend', '<', ''.$endwholedatetime.'')
			->select('room_id', 'dtstart', 'dtend')
			->get();

		$room_calendar = array();

		if(empty($room_events)) {
			foreach ($room_ids as $room) {

				$newData = new roomTimeTable;

				$newData->room_id = $room;

				$room_calendar[] = $newData;
			}
		} else {
			foreach ($room_events as $event) {
				$room_event_ids[] = $event->room_id;
			}

			foreach ($final_room_ids as $room) {
				$subRoomData = array_keys($room_event_ids, $room);

				if (empty($subRoomData)) {

					$newData = new roomTimeTable;

					$newData->room_id = $room;

					$room_calendar[] = $newData;
				} else {

					$newData = new roomTimeTable;

					$newData->room_id = $room;

					foreach ($subRoomData as $subData) {
						
						$subData_start = intval(substr($room_events[$subData]->dtstart, 11, 2));
						$subData_end = intval(substr($room_events[$subData]->dtend, 11, 2));

						if ((6 >= $subData_start) && (7 <= $subData_end)) $newData->sixam = 'busytime';
						if ((7 >= $subData_start) && (8 <= $subData_end)) $newData->sevenam = 'busytime';
						if ((8 >= $subData_start) && (9 <= $subData_end)) $newData->eigtham = 'busytime';
						if ((9 >= $subData_start) && (10 <= $subData_end)) $newData->nineam = 'busytime';
						if ((10 >= $subData_start) && (11 <= $subData_end)) $newData->tenam = 'busytime';
						if ((11 >= $subData_start) && (12 <= $subData_end)) $newData->elevenam = 'busytime';
						if ((12 >= $subData_start) && (13 <= $subData_end)) $newData->twelveam = 'busytime';
						if ((13 >= $subData_start) && (14 <= $subData_end)) $newData->onepm = 'busytime';
						if ((14 >= $subData_start) && (15 <= $subData_end)) $newData->twopm = 'busytime';
						if ((15 >= $subData_start) && (16 <= $subData_end)) $newData->threepm = 'busytime';
						if ((16 >= $subData_start) && (17 <= $subData_end)) $newData->fourpm = 'busytime';
						if ((17 >= $subData_start) && (18 <= $subData_end)) $newData->fivepm = 'busytime';
						if ((18 >= $subData_start) && (19 <= $subData_end)) $newData->sixpm = 'busytime';
						if ((19 >= $subData_start) && (20 <= $subData_end)) $newData->sevenpm = 'busytime';
						if ((20 >= $subData_start) && (21 <= $subData_end)) $newData->eigthpm = 'busytime';
						if ((21 >= $subData_start) && (22 <= $subData_end)) $newData->ninepm = 'busytime';
						if ((22 >= $subData_start) && (23 <= $subData_end)) $newData->tenpm = 'busytime';
						if ((23 >= $subData_start) && (24 <= $subData_end)) $newData->elevenpm = 'busytime';
					}

					$room_calendar[] = $newData;
				}
			}
		}

		// print_r($today);

        return View::make('venues.show', array('venue' => $venue, 'rooms' => $rooms, 'room_calendar' => $room_calendar,
        	'other_venues' => $other_venues));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show_edit_venue($id)
	{
        return View::make('venues.edit');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function run_update_venue($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function run_destroy_venue($id)
	{
		//
	}

}
