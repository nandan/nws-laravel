<?php



use Carbon\Carbon;

// Convert array to an object
function arrayToObject($d) {
	if (is_array($d)) {
		/*
		* Return array converted to object
		* Using __FUNCTION__ (Magic constant)
		* for recursive call
		*/
		return (object) array_map(__FUNCTION__, $d);
	}
	else {
		// Return object
		return $d;
	}
}

// Sort the array after prices
function compPrice($a, $b) {
    return $a->total_price - $b->total_price;
}

// Remove objects with prices equals to zero
function removeZeroPrices($a) {
	if ($a->total_price != '0') {
		return $a;
	}
}

class SearchController extends BaseController {

	/*
	* Show search result
	*/
	function show_search_result(){
		try{

			$search_room_type = Input::get('search_room_type');
			$search_city = Input::get('search_city');
			$search_state = Input::get('search_state');
			$search_country = Input::get('search_country');
			$search_period = Input::get('search_period');
			$search_monthly_duration = Input::get('search_monthly_duration');
			$search_startdate = Input::get('search_startdate');
			// $search_startdate = "26/02/2014";
			$search_enddate = Input::get('search_enddate');
			// $search_enddate = "28/02/2014";
			$search_starttime = Input::get('search_starttime');
			// $search_starttime = "11:00";
			$search_endtime = Input::get('search_endtime');
			// $search_endtime = "13:00";
			$search_person = Input::get('search_person');
			$search_lat = Input::get('search_lat');
			$search_lng = Input::get('search_lng');
			$search_locid = Input::get('search_locid');
			$search_workspace_type = Input::get('search_workspace_type');

			if ($search_person == '') {
				$search_person = "1";
			}


			// For hourly booking
			if ($search_period == "hourly") {
				// Start date
				$search_startdatetime = ''.$search_startdate.' '.$search_starttime.'';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $search_startdatetime, 'Europe/Berlin');
				// End date
				$search_enddatetime = ''.$search_startdate.' '.$search_endtime.'';
				$c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $search_enddatetime, 'Europe/Berlin');

				// Find out, which day of the week it is
				$dayOfWeek = $c_startdatetime->dayOfWeek;

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_startdatetime->toDateString();

				$starttime = $c_startdatetime->toTimeString();
				$endtime = $c_enddatetime->toTimeString();	
			}
			// For daily booking
			if ($search_period == "daily") {
				// Start date
				$search_startdatetime = ''.$search_startdate.' 00:00';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $search_startdatetime, 'Europe/Berlin');
				// End date
				$search_enddatetime = ''.$search_enddate.' 23:59';
				$c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $search_enddatetime, 'Europe/Berlin');

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_enddatetime->toDateString();
			}
			
			// For monthly booking
			if ($search_period == "monthly") {
				// Start date
				$search_startdatetime = ''.$search_startdate.' 00:00';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $search_startdatetime, 'Europe/Berlin');
				$c_enddatetime = $c_startdatetime->copy()->addMonths($search_monthly_duration);

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_enddatetime->toDateString();
			}


			// If time is in past
			$search_message = "Time period fully/partially lies in the past. Please change the time above!";

			$now = Carbon::now('Europe/Berlin');

			if ($c_startdatetime->lt($now)) {
				// Return page for times in the past
				return View::make('search.result_just_message', array(
	            	'search_city' => $search_city , 'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
	            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
	            	'search_endtime' => $search_endtime, 'search_enddate' => $search_enddate, 'search_monthly_duration' => $search_monthly_duration,
	            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_period' => $search_period, 'search_state' => $search_state,
	            	'search_locid' => $search_locid, 'search_message' => $search_message ));
			}

			// $search_country_table = DB::table('countries')->where('country_name', $search_country)->first();
			// $search_country_id = $search_country_table->id;
			// $search_city_table = DB::table('cities')->where('city_name', $search_city)->where('country_id', $search_country_id)->first();
			$search_city_table = DB::table('cities')->where('google_loc_id', $search_locid)->first();

			// If city doesn't exist
			$search_message = "Unfortunately no city found with the given name, ".$search_city.".";

			if (empty($search_city_table)) {
				// Return page with either no results/results from nearby cities
				return View::make('search.result_just_message', array(
	            	'search_city' => $search_city , 'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
	            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
	            	'search_endtime' => $search_endtime, 'search_enddate' => $search_enddate, 'search_monthly_duration' => $search_monthly_duration,
	            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_period' => $search_period, 'search_state' => $search_state,
	            	'search_locid' => $search_locid, 'search_message' => $search_message ));
			}

			$search_city_id = $search_city_table->id;
			if ($search_room_type == 'Others') {
				$search_room_type_table = DB::table('room_types')->where('room_type_name', $search_workspace_type)->first();
			} else {
				$search_room_type_table = DB::table('room_types')->where('room_type_name', $search_room_type)->first();
			}
			$search_room_type_id = $search_room_type_table->id;

			/** 
			 * Now find all venues for the city location
			 */
			// Get venues from DB
			$init_venues = DB::table('venues')
				->where('city_id', $search_city_id)
				->select('venues.id')
				->get();


			// If no entries found in the city
			$search_message = "Unfortunately no entries found for the city.";

			if (empty($init_venues)) {
				// Return page with either no results/results from nearby cities
				return View::make('search.result_just_message', array(
	            	'search_city' => $search_city , 'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
	            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
	            	'search_endtime' => $search_endtime, 'search_enddate' => $search_enddate, 'search_monthly_duration' => $search_monthly_duration,
	            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_period' => $search_period, 'search_state' => $search_state,
	            	'search_locid' => $search_locid, 'search_message' => $search_message ));
			}


			foreach ($init_venues as $venue) {
				$init_venues_ids[] = $venue->id;
			}


			/** 
			 * Now Check for opening hours overlap for the venue
			 */
			// This check is important, if rooms are booked on "hourly" basis or free days may be important for
			// "daily" and "monthly" basis
			// Check whether time period is within opening hours for the day
			if ($search_period == "hourly") {
				$oh_check = DB::table('venue_opening_hours')
					->whereIn('venue_id', $init_venues_ids)
					->where('venue_opening_hours.day_index', $dayOfWeek)
					->where('closed', '!=', '1')
					->where('venue_opening_hours.start_time', '<=', $starttime)
					->where('venue_opening_hours.end_time', '>=', $endtime)
					->select('venue_id')
					->get();

				// If no venues found with matching opening hours
				$search_message = "Unfortunately no venues found for the city, which are opened during given time period.<br /> <br /> <em>Please change the time entry to search for deferred period!</em>";

				if (empty($oh_check)) {
					// Return page with either no results/ask for different times
					return View::make('search.result_just_message', array(
		            	'search_city' => $search_city , 'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
		            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
		            	'search_endtime' => $search_endtime, 'search_enddate' => $search_enddate, 'search_monthly_duration' => $search_monthly_duration,
		            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_period' => $search_period, 'search_state' => $search_state,
		            	'search_locid' => $search_locid, 'search_message' => $search_message));
				}

				foreach ($oh_check as $check) {
					$oh_check_ids[] = $check->venue_id;
				}
			} elseif ($search_period == "daily") {
				//
				$oh_check_ids = $init_venues_ids;
			} elseif ($search_period == "monthly") {
				//
				$oh_check_ids = $init_venues_ids;
			}

			// Exception Rules check
			// $er_check = DB::table('venue_exception_rule_events')
			// 	->whereIn('venue_id', $oh_check_ids)
			// 	->where('venue_exception_rule_events.dtstart', '<', $startdatetime)
			// 	->where('venue_exception_rule_events.dtend', '>', $enddatetime)
			// 	->select('venue_exception_rule_events.id', 'venue_id', 'freq', 'dtstart', 'dtend')
			// 	->get();

			// foreach ($er_check as $check) {
			// 	if ($check->freq == "daily") {
			// 		$orig_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $check->dtstart);
			// 		$orig_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $check->dtend);

			// 		$temp_startdatetime = ''.$startdate.' '.$orig_startdatetime->toTimeString().'';
			// 		$ch_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $temp_startdatetime);
			// 		$temp_enddatetime = ''.$enddate.' '.$orig_enddatetime->toTimeString().'';
			// 		$ch_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $temp_enddatetime);

			// 		if (!($c_startdatetime->between($ch_startdatetime, $ch_enddatetime, false)) && !($c_enddatetime->between($ch_startdatetime, $ch_enddatetime, false))) {
			// 			$er_check_ids[] = $check->venue_id;
			// 		}
			// 	} elseif ($check->freq == "weekly") {
			// 		# code...
			// 	} elseif ($check->freq == "monthly") {
			// 		# code...
			// 	} elseif ($check->freq == "yearly") {
			// 		# code...
			// 	}
			// }

			// Get all the rooms of the venues, which has desired opening hours and no exceptions to worry about
			$init_rooms = DB::table('rooms')
				->whereIn('venue_id', $oh_check_ids)
				->where('rooms.room_types_id', $search_room_type_id)
				->select('rooms.id')
				->get();

			foreach ($init_rooms as $room) {
				$init_rooms_ids[] = $room->id;
			}


			/** 
			 * Now Check for booking events overlap
			 */
			// Check whether there are any bookings for the time period
			// Room_ids to be discarded are selected
			// Hourly
			if ($search_period == "hourly") {
				$be_check = DB::table('booking_events')
				->whereIn('room_id', $init_rooms_ids)
				->where(function($query) use ($startdatetime, $enddatetime) {
					$query->where(function($query_start) use ($startdatetime) {
						$query_start->where('booking_events.dtstart', '<=', ''.$startdatetime.'')
						->where('booking_events.dtend', '>', ''.$startdatetime.'');
					})
					->orWhere(function($query_end) use ($enddatetime) {
						$query_end->where('booking_events.dtstart', '<', ''.$enddatetime.'')
						->Where('booking_events.dtend', '>=', ''.$enddatetime.'');
					});
				})
				->select('room_id')
				->get();	
			} elseif ($search_period == "daily") {
				$be_check = DB::table('booking_events')
				->whereIn('room_id', $init_rooms_ids)
				->where(function($query) use ($startdatetime, $enddatetime) {
					$query->where(function($query_start) use ($startdatetime) {
						$query_start->where('booking_events.dtstart', '<=', ''.$startdatetime.'')
						->where('booking_events.dtend', '>', ''.$startdatetime.'');
					})
					->orWhere(function($query_end) use ($enddatetime) {
						$query_end->where('booking_events.dtstart', '<', ''.$enddatetime.'')
						->Where('booking_events.dtend', '>=', ''.$enddatetime.'');
					});
				})
				->select('room_id')
				->get();
			} elseif ($search_period == "monthly") {
				$be_check = DB::table('booking_events')
				->whereIn('room_id', $init_rooms_ids)
				->where(function($query) use ($startdatetime, $enddatetime) {
					$query->where(function($query_start) use ($startdatetime) {
						$query_start->where('booking_events.dtstart', '<=', ''.$startdatetime.'')
						->where('booking_events.dtend', '>', ''.$startdatetime.'');
					})
					->orWhere(function($query_end) use ($enddatetime) {
						$query_end->where('booking_events.dtstart', '<', ''.$enddatetime.'')
						->Where('booking_events.dtend', '>=', ''.$enddatetime.'');
					});
				})
				->select('room_id')
				->get();
			}


			if(empty($be_check) || ($search_room_type_id == '3')) { 
				// If be_check is empty, then all rooms from init_room_ids have free slot
				// Pass it to Final list
				$final_room_ids = $init_rooms_ids;
			} else {
				// Multiple room_ids are possible. It should be brought to single entry.
				foreach ($be_check as $check) {
					$be_check_ids[] = $check->room_id;
				}

				$final_room_ids = array();

				foreach ($init_rooms_ids as $room_id) {
					if (in_array($room_id, $be_check_ids)) {
						
					} else {
						$final_room_ids[] = $room_id;
					}
				}
			}


			// If no rooms to be found, even after checking CalDAV and booking events, 
			// tell the user to change the time
			$search_message = "Unfortunately all available entries are booked for the given time period.<br /> <br /> <em>Please change the time entry to search for deferred period!</em>";

			if (empty($final_room_ids)) {
				// Return page with either no results/results from nearby cities
				return View::make('search.result_just_message', array(
	            	'search_city' => $search_city , 'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
	            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
	            	'search_endtime' => $search_endtime, 'search_enddate' => $search_enddate, 'search_monthly_duration' => $search_monthly_duration,
	            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_period' => $search_period, 'search_state' => $search_state,
	            	'search_locid' => $search_locid, 'search_message' => $search_message ));
			}


			// Passed booking events check 	
			$search_query = DB::table('rooms')
				->whereIn('rooms.id', $final_room_ids)
				->join('venues', 'rooms.venue_id', '=', 'venues.id')
				->join('room_types', 'rooms.room_types_id', '=', 'room_types.id')
				->join('amenities', 'rooms.id', '=', 'amenities.room_id');

			$search_count = $search_query->count();

			$search_results = $search_query
				->select('venues.venue_name', 'venues.street_name', 'venues.street_number',
					'venues.floor', 'venues.postal_code', 'venues.lat', 'venues.lng',
					'rooms.room_name', 'rooms.room_types_id', 
					'rooms.max_capacity', 'room_types.room_type_name', 'rooms.id as room_id',
					'amenities.public_wifi', 'amenities.secure_wifi', 'amenities.wired_internet', 'amenities.flatscreen', 'amenities.video_conferencing', 
	        		'amenities.flipboard', 'amenities.whiteboard', 'amenities.projector', 'amenities.print_scan_copy', 'amenities.shared_kitchen', 'amenities.catering',
	        		'amenities.concierge_service', 'amenities.filtered_water', 'amenities.phone_room', 'amenities.pet_friendly', 'amenities.handicap_accessible',
	        		'amenities.conference_phone', 'amenities.outdoor_space', 'amenities.on_site_restaurant', 'amenities.coffee_tea', 'amenities.notary_service',
	        		'amenities.shower_facility')
				->orderBy('room_id', 'asc')
				->get();

			foreach ($search_results as $result) {
				$temp_room_ids[] = $result->room_id;
			}

			/** 
			 * Check the prices for the room
			 */
			// Hourly
			$temp_price_hourly = DB::table('rooms')
				->whereIn('rooms.id', $temp_room_ids)
				->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
				->where('room_prices.price_period', '=', '0')
				->select('rooms.id', 'room_prices.price_retail', 'room_prices.tax_flat')
				->orderBy('rooms.id', 'asc')
				->get();

			for ($i = 0; $i < $search_count; $i++) {
				$search_results[$i]->price_retail_hourly = $temp_price_hourly[$i]->price_retail;
				$search_results[$i]->tax_flat_hourly = $temp_price_hourly[$i]->tax_flat;
			}

			// Daily
			$temp_price_daily = DB::table('rooms')
				->whereIn('rooms.id', $temp_room_ids)
				->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
				->where('room_prices.price_period', '=', '1')
				->select('rooms.id', 'room_prices.price_retail', 'room_prices.tax_flat')
				->orderBy('rooms.id', 'asc')
				->get();

			for ($i = 0; $i < $search_count; $i++) {
				$search_results[$i]->price_retail_daily = $temp_price_daily[$i]->price_retail;
				$search_results[$i]->tax_flat_daily = $temp_price_daily[$i]->tax_flat;
			}

			// Monthly
			$temp_price_monthly = DB::table('rooms')
				->whereIn('rooms.id', $temp_room_ids)
				->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
				->where('room_prices.price_period', '=', '2')
				->select('rooms.id', 'room_prices.price_retail', 'room_prices.tax_flat')
				->orderBy('rooms.id', 'asc')
				->get();

			for ($i = 0; $i < $search_count; $i++) {
				$search_results[$i]->price_retail_monthly = $temp_price_monthly[$i]->price_retail;
				$search_results[$i]->tax_flat_monthly = $temp_price_monthly[$i]->tax_flat;
			}


			// Find duration and determine the final price
			if ($search_period == "hourly") {
				$duration = $c_startdatetime->diffInHours($c_enddatetime);

				foreach ($search_results as $result) {
					$result->total_price = ($result->price_retail_hourly * $duration) + $result->tax_flat_hourly;
					$result->total_duration = ''.$duration.' Hours';
				}

			} elseif ($search_period == "daily") {
				// Find the duration (in # of days) betweeen start and end dates
				$duration = $c_startdatetime->diffInDays($c_enddatetime);

				// Find the days of the week for start and end dates
				$startday_index = $c_startdatetime->dayOfWeek;
				$endday_index = $c_enddatetime->dayOfWeek;


				// save the day indices of the days, when venue is closed
				$oh_check = DB::table('rooms')
								->whereIn('rooms.id', $temp_room_ids)
								->join('venue_opening_hours', 'rooms.venue_id', '=', 'venue_opening_hours.venue_id')
								->where('venue_opening_hours.closed', '=', '1')
								->select('rooms.id', 'venue_opening_hours.day_index')
								->get();

				// Convert object into arrays. For venue free days
				foreach ($oh_check as $check) {
					$oh_check_room_ids[] = $check->id;
					$oh_check_day_indices[] = $check->day_index;
				}

				foreach ($temp_room_ids as $room_id) {
					$non_working_days = 0;
					$working_days = 0;
					$temp_day_indices = array();

					$room_keys = array_keys($oh_check_room_ids, $room_id);

					if (empty($room_keys)) {
						$working_days = $duration + 1;

					} else {

						foreach ($room_keys as $key) {
							$temp_day_indices[] = $oh_check_day_indices[$key];
						}

						// Go thru' all day indices between start and end date
						// to see whether they match venue free day indices
						for ($i = 0; $i < $duration+1; $i++) {
							$temp_day = ($startday_index+$i) % 7;
							// Using modulo operation to find value between [0,6]
							if (in_array($temp_day, $temp_day_indices)) {
								$non_working_days++;
							} else {
								$working_days++;
							}
						}
					}

					$temp_price_key = array_search($room_id, $temp_room_ids);

					$search_results[$temp_price_key]->total_price = ($search_results[$temp_price_key]->price_retail_daily * $working_days) + $search_results[$temp_price_key]->tax_flat_daily;
					$search_results[$temp_price_key]->total_duration = ''.$working_days.' Days';
					
				}

			} elseif ($search_period == "monthly") {
				$duration = $search_monthly_duration;

				foreach ($search_results as $result) {
					if ($result->price_retail_monthly != '0') {
						$result->total_price = ($result->price_retail_monthly * $duration) + $result->tax_flat_monthly;
						$result->total_duration = ''.$duration.' Months';
					} else {
						$result->total_price = '0';
						$result->total_duration = '0';
					}
				}
				
				$search_results = array_filter($search_results, "removeZeroPrices");
			}

			$search_count = count($search_results);

			usort($search_results, "compPrice");
			$temp_search_min = $search_results;
			$price_min_array = array_reduce($temp_search_min,function($a,$b){
					    return $a->total_price < $b->total_price ? $a : $b;
					  }, array_shift($temp_search_min));
			$search_price_min = $price_min_array->total_price;

			$temp_search_max = $search_results;
			$price_max_array = array_reduce($temp_search_max,function($a,$b){
					    return $a->total_price > $b->total_price ? $a : $b;
					  }, array_shift($temp_search_max));
			$search_price_max = $price_max_array->total_price;

			// $queries = DB::getQueryLog();
			// $last_query = end($queries);
			// print_r($last_query);

			// print_r("Arrived at the end!");

            return View::make('search.result', array('search_results' => $search_results, 'search_count' => $search_count, 
            	'search_price_min' => $search_price_min, 'search_price_max' => $search_price_max, 'search_city' => $search_city , 'search_state' => $search_state,
            	'search_country' => $search_country, 'search_person' => $search_person, 'search_room_type' => $search_room_type, 
            	'search_room_type' => $search_room_type, 'search_startdate' => $search_startdate, 'search_starttime' => $search_starttime, 
            	'search_lat' => $search_lat, 'search_lng' => $search_lng, 'search_endtime' => $search_endtime,
            	'search_enddate' => $search_enddate, 'search_locid' => $search_locid, 'search_period' => $search_period,
            	'search_monthly_duration' => $search_monthly_duration));

        }catch(Exception $e){
            Log::error($e);
            print_r($e->getMessage());
        }
	}


	/*
	* Run modal search
	*/
	function run_search_modal(){
		try{
			// Collect the INPUTS
			$modal_room_id = Input::get('room_id');
			$modal_booking_startdate = Input::get('booking_startdate');
			$modal_booking_enddate = Input::get('booking_enddate');
			$modal_booking_starttime = Input::get('booking_starttime');
			$modal_booking_endtime = Input::get('booking_endtime');
			$modal_booking_period = Input::get('booking_period');
			$modal_booking_monthly_duration = Input::get('booking_monthly_duration');

			// For hourly booking
			if ($modal_booking_period == "hourly") {
				// Start date
				$s_startdatetime = ''.$modal_booking_startdate.' '.$modal_booking_starttime.'';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $s_startdatetime, 'Europe/Berlin');
				// End date
				$s_enddatetime = ''.$modal_booking_startdate.' '.$modal_booking_endtime.'';
				$c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $s_enddatetime, 'Europe/Berlin');

				// Find out, which day of the week it is
				$dayOfWeek = $c_startdatetime->dayOfWeek;

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_startdatetime->toDateString();

				$starttime = $c_startdatetime->toTimeString();
				$endtime = $c_enddatetime->toTimeString();	
			}
			// For daily booking
			if ($modal_booking_period == "daily") {
				// Start date
				$modal_booking_startdatetime = ''.$modal_booking_startdate.' 00:00';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $modal_booking_startdatetime, 'Europe/Berlin');
				// End date
				$modal_booking_enddatetime = ''.$modal_booking_enddate.' 23:59';
				$c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $modal_booking_enddatetime, 'Europe/Berlin');

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_enddatetime->toDateString();
			}
			
			// For monthly booking
			if ($modal_booking_period == "monthly") {
				// Start date
				$modal_booking_startdatetime = ''.$modal_booking_startdate.' 00:00';
				$c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $modal_booking_startdatetime, 'Europe/Berlin');
				$c_enddatetime = $c_startdatetime->copy()->addMonths($modal_booking_monthly_duration);

				$startdatetime = $c_startdatetime->toDateTimeString();
				$enddatetime = $c_enddatetime->toDateTimeString();

				$startdate = $c_startdatetime->toDateString();
				$enddate = $c_enddatetime->toDateString();
			}


			$room = DB::table('rooms')
				->where('rooms.id', $modal_room_id)
				->first();


			/** 
			 * Now Check for opening hours overlap for the venue
			 */
			// Check whether time period is within opening hours for the day
			if ($modal_booking_period == "hourly") {
				$oh_check = DB::table('venue_opening_hours')
					->where('venue_opening_hours.day_index', $dayOfWeek)
					->where('closed', '!=', '1')
					->where('venue_opening_hours.start_time', '<=', $starttime)
					->where('venue_opening_hours.end_time', '>=', $endtime)
					->where('venue_id', $room->venue_id)
					->count();

				// If no venues found with matching opening hours
				$err_message = "Desired time period is outside opening hours of the venue";

				if (empty($oh_check)) {
					// Return error
					return Response::json(array('status' => 'Error', 'message' => $err_message));
				}
			} elseif ($modal_booking_period == "daily") {
				//
			} elseif ($modal_booking_period == "monthly") {
				//
			}

			/**
			 * Now Check for booking events overlap
			 */

			// Check whether there are any bookings for the time period
			// Check valid for all periods, hourly, daily and monthly
			$be_check = DB::table('booking_events')
				->where('room_id', $modal_room_id)
				->where(function($query) use ($startdatetime, $enddatetime) {
					$query->where(function($query_start) use ($startdatetime) {
						$query_start->where('booking_events.dtstart', '<=', ''.$startdatetime.'')
						->where('booking_events.dtend', '>', ''.$startdatetime.'');
					})
					->orWhere(function($query_end) use ($enddatetime) {
						$query_end->where('booking_events.dtstart', '<', ''.$enddatetime.'')
						->Where('booking_events.dtend', '>=', ''.$enddatetime.'');
					});
				})
				->select('room_id')
				->get();

			$err_message = "No free timeslot is found for the given time period. Please select different time period!";

			if($be_check) { 
				// return error
				return Response::json(array('status' => 'Error', 'message' => $err_message));
			}


			// Get User Details to find out if he/she has creditcard registered
			$user = Sentry::getUser();
            $user_details = DB::table('customer_details')
                ->where('user_id', $user->id)
                ->first();

			/** 
			 * Check the prices for the room
			 */
			// Get prices for the room
			$price_query = DB::table('rooms')
				->where('rooms.id', $modal_room_id)
				->join('room_prices', 'rooms.id', '=', 'room_prices.room_id');

			$room_hourly_price = $price_query->where('price_period', '=', '0')->first();
			$room_daily_price = $price_query->where('price_period', '=', '1')->first();

			if ($modal_booking_period == "hourly") {
				$duration = $c_startdatetime->diffInHours($c_enddatetime);

				$price_query = DB::table('rooms')
								->where('rooms.id', $modal_room_id)
								->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
								->where('room_prices.price_period', '=', '0')
								->select('rooms.id', 'room_prices.price_original', 'room_prices.tax_percentage', 
									'room_prices.tax_flat', 'room_prices.price_retail')
								->first();

				$price_orig = ($price_query->price_original)*$duration;
				$price_tax = ($price_orig*(($price_query->tax_percentage)/100)) + ($price_query->tax_flat);
				$price_final = $price_orig + $price_tax;

				$price_duration = ''.$duration.' Hours';

			} elseif ($modal_booking_period == "daily") {
				$non_working_days = 0;
				$working_days = 0;

				// Find the duration (in # of days) betweeen start and end dates
				$duration = $c_startdatetime->diffInDays($c_enddatetime);

				// Find the days of the week for start and end dates
				$startday_index = $c_startdatetime->dayOfWeek;
				$endday_index = $c_enddatetime->dayOfWeek;

				// save the day indices of the days, when venue is closed
				$oh_check = DB::table('venue_opening_hours')
					->where('closed', '=', '1')
					->where('venue_id', $room->venue_id)
					->select('venue_opening_hours.day_index')
					->get();

				if (empty($oh_check)) {
					$working_days = $duration + 1;
				} else {
					// Convert object into arrays. For venue free days
					foreach ($oh_check as $check) {
						$oh_check_day_indices[] = $check->day_index;
					}


					// Go thru' all day indices between start and end date
					// to see whether they match venue free day indices
					for ($i = 0; $i < $duration+1; $i++) {
						$temp_day = ($startday_index+$i) % 7;
						// Using modulo operation to find value between [0,6]
						if (in_array($temp_day, $oh_check_day_indices)) {
							$non_working_days++;
						} else {
							$working_days++;
						}
					}	
				}

				$price_query = DB::table('rooms')
								->where('rooms.id', $modal_room_id)
								->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
								->where('room_prices.price_period', '=', '1')
								->select('rooms.id', 'room_prices.price_original', 'room_prices.tax_percentage', 
									'room_prices.tax_flat', 'room_prices.price_retail')
								->first();

				$price_orig = ($price_query->price_original)*$working_days;
				$price_tax = ($price_orig*(($price_query->tax_percentage)/100)) + ($price_query->tax_flat);
				$price_final = $price_orig + $price_tax;

				$price_duration = ''.$working_days.' Days';

			} elseif ($modal_booking_period == "monthly") {
				$duration = $modal_booking_monthly_duration;

				$price_query = DB::table('rooms')
								->where('rooms.id', $modal_room_id)
								->join('room_prices', 'rooms.id', '=', 'room_prices.room_id')
								->where('room_prices.price_period', '=', '2')
								->select('rooms.id', 'room_prices.price_original', 'room_prices.tax_percentage', 
									'room_prices.tax_flat', 'room_prices.price_retail')
								->first();

				$price_orig = ($price_query->price_original)*$duration;
				$price_tax = ($price_orig*(($price_query->tax_percentage)/100)) + ($price_query->tax_flat);
				$price_final = $price_orig + $price_tax;

				$price_duration = ''.$duration.' Months';

			}
			
			return Response::json(array('status' => 'OK', 'message' => 'Yes! It is available.', 'price_orig' => $price_orig,
				'price_tax' => $price_tax, 'price_final' => $price_final, 'price_duration' => $price_duration, 
				'cc_maskednr' => $user_details->last_CC));

		} catch(Exception $e){
            Log::error($e);
            // print_r($e->getMessage());
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }
	}



}