<?php

/*
* HANDLING BOOKINGS
*/

// Helper
use Carbon\Carbon;


// function __construct()
// {
// }

// Generic random string function
function mt_rand_str ($l, $c = 'abcdefghijklmnopqrstuvwxyz1234567890') {
    for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
    return $s;
}

// URL compatible base64 encoding
function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

// URL compatible base24 decoding
function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
} 

// Booking Controller class
class BookingsController extends BaseController {

    // Variables required for encryption
    var $reservation_key = 'nextworkspaceisgreat';
    var $gum_key = "nwsrocks";
    var $secret_iv = "NWS rules the world.";


    //Pay with Credit Card via Braintree. For New Users - DEPRECATED -
    function pay_new_customer($amount, $cardnumber, $cvcnumber, $exp_month, $exp_year){
        try{
            // Braintree configuration
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId('fs7x5w6jzq9tq99y');
            Braintree_Configuration::publicKey('crydq37299kyj34w');
            Braintree_Configuration::privateKey('4eab0f36953d002f455327c87e1169e3');

            // For new customer
            $result = Braintree_Transaction::sale(array(
                "amount" => $amount,
                "creditCard" => array(
                    "number" => $cardnumber,
                    "cvv" => $cvcnumber,
                    "expirationMonth" => $exp_month,
                    "expirationYear" => $exp_year
                ),
                // 'customer' => array(
                //     'email' => 'drew@example.com'
                //   ),
                "options" => array(
                    "submitForSettlement" => true
                )
            ));

            return $result;

        } catch(Exception $e){
            Log::error($e);
            // print_r($e->getMessage());
        }
        
    }

    //Pay with Credit Card via Braintree. For Existing Users
    function pay_existing_customer($amount, $bt_customer_id, $bt_cc_token){
        try{

            // Braintree configuration
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId('fs7x5w6jzq9tq99y');
            Braintree_Configuration::publicKey('crydq37299kyj34w');
            Braintree_Configuration::privateKey('4eab0f36953d002f455327c87e1169e3');

            // For new customer
            $result = Braintree_Transaction::sale(array(
                "amount" => $amount,
                "customerId" => $bt_customer_id,
                "paymentMethodToken" => $bt_cc_token,
                "options" => array(
                    "submitForSettlement" => true
                )
            ));

            return $result;

        } catch(Exception $e){
            Log::error($e);
            // print_r($e->getMessage());
        }
        
    }

    // Create new customer on Braintree
    function create_customer_braintree($email, $cardnumber, $cvcnumber, $exp_month, $exp_year) {
        try {

            // Braintree configuration
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId('fs7x5w6jzq9tq99y');
            Braintree_Configuration::publicKey('crydq37299kyj34w');
            Braintree_Configuration::privateKey('4eab0f36953d002f455327c87e1169e3');

            // Create new customer
            $result = Braintree_Customer::create(array(
                "email" => $email,
                "creditCard" => array(
                    "number" => $cardnumber,
                    "expirationMonth" => $exp_month,
                    "expirationYear" => $exp_year,
                    "cvv" => $cvcnumber,
                    "options" => array(
                        'verifyCard' => true
                    )
                ),
            ));

            return $result;

        } catch(Exception $e) {
            Log::error($e);
        }
    }


    // Update customer payment details on Braintree
    function update_customer_braintree($bt_customer_id, $bt_cc_token, $cardnumber, $cvcnumber, $exp_month, $exp_year) {
        try {

            // Braintree configuration
            Braintree_Configuration::environment('sandbox');
            Braintree_Configuration::merchantId('fs7x5w6jzq9tq99y');
            Braintree_Configuration::publicKey('crydq37299kyj34w');
            Braintree_Configuration::privateKey('4eab0f36953d002f455327c87e1169e3');

            // Update new customer            
            $result = Braintree_Customer::update(
                $bt_customer_id,
                array(
                    "creditCard" => array(
                        "number" => $cardnumber,
                        "expirationMonth" => $exp_month,
                        "expirationYear" => $exp_year,
                        "cvv" => $cvcnumber,
                        'options' => array(
                          'updateExistingToken' => $bt_cc_token,
                          'verifyCard' => true
                        )
                    )
                )
            );

            return $result;

        } catch(Exception $e) {
            Log::error($e);
        }
    }


	/**
     * Show new booking process in separate page, esp. for mobile devices
     */

    function show_create_booking()
    {


    }


    /**
     * Show booking details
     */

    function show_booking_details($id)
    {
        try {
            // Get the reservation code for the booking id from DB
            $booking_details = DB::table('customer_bookings')->where('customer_bookings.id', $id)->first();

            // Now off to making the payment
            // Get user information
            $user = DB::table('users')
                        ->where('users.id', $booking_details->user_id)
                        ->first();
            $user_details = DB::table('customer_details')
                            ->where('user_id', $booking_details->user_id)
                            ->first();

            // Get Rooms and Venue info
            $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
            $venue = DB::table('venues')
                        ->join('cities', 'venues.city_id', '=', 'cities.id')
                        ->join('states', 'venues.state_id', '=', 'states.id')
                        ->join('countries', 'venues.country_id', '=', 'countries.id')
                        ->where('venues.id', $booking_details->venue_id)->first();

            // Get booking date
            $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
            $booking_date = $b_date->toFormattedDateString();

            // Get booking period
            if ($booking_details->booking_period === "hourly") {
                $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                $b_date = $b_startdatetime->toFormattedDateString();

                $starttime = $b_startdatetime->toTimeString();
                $endtime = $b_enddatetime->toTimeString();

                $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
            } elseif ($booking_details->booking_period === "daily") {
                $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                $startdate = $b_startdatetime->toFormattedDateString();
                $enddate = $b_enddatetime->toFormattedDateString();

                $real_duration = ''.$startdate.' - '.$enddate.'';
            } elseif ($booking_details->booking_period === "monthly") {
                $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                $startdate = $b_startdatetime->toFormattedDateString();
                $enddate = $b_enddatetime->toFormattedDateString();

                $real_duration = ''.$startdate.' - '.$enddate.'';
            }

            return View::make('reservation.show', array('user' => $user,
                'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                'real_duration' => $real_duration));


        } catch (Exception $e) {
            Log::error($e);
        }

    }


    /**
     * Store new bookings in the database
     */

    function run_store_booking()
    {
    	try {
            // check if logged in..
            if ( ! Sentry::check())
            {
                // User is not logged in
                // This shouldn't happen. This function should only be accessed after logging in/registered
                return Response::json(array('status' => 'Error', 'message' => 'Please login first'));
            }
            else
            {
                // User is logged in
                
                // Get the values
                $booking_room_id = Input::get('booking_room_id');
                $booking_startdate = Input::get('booking_startdate');
                $booking_starttime = Input::get('booking_starttime');
                $booking_endtime = Input::get('booking_endtime');
                $booking_enddate = Input::get('booking_enddate');
                $booking_person = Input::get('booking_person');
                $booking_period = Input::get('booking_period');
                $booking_duration = Input::get('booking_duration');
                $booking_monthly_duration = Input::get('booking_monthly_duration');

                $booking_title = Input::get('booking_title');
                $booking_comments = Input::get('booking_comments');

                // $booking_currency = Input::get('booking_currency');
                $booking_currency = 'USD';
                $booking_price_orig = Input::get('booking_price_orig');
                $booking_price_tax = Input::get('booking_price_tax');
                $booking_price_final = Input::get('booking_price_final');

                $booking_payment_type = Input::get('booking_payment_type');
                $booking_cardnumber = Input::get('booking_cardnumber');
                $booking_expiration_month = Input::get('booking_expiration_month');
                $booking_expiration_year = Input::get('booking_expiration_year');
                $booking_cvcnumber = Input::get('booking_cvcnumber');
                $booking_zipcode = Input::get('booking_zipcode');
                $booking_cardholder_name = Input::get('booking_cardholder_name');
                
                $booking_coupon_code = Input::get('booking_coupon_code');
                $booking_cc_changed = Input::get('booking_cc_changed');


                // For hourly booking
                if ($booking_period == "hourly") {
                    // Start date
                    $s_startdatetime = ''.$booking_startdate.' '.$booking_starttime.'';
                    $c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $s_startdatetime, 'Europe/Berlin');
                    // End date
                    $s_enddatetime = ''.$booking_startdate.' '.$booking_endtime.'';
                    $c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $s_enddatetime, 'Europe/Berlin');

                    // Find out, which day of the week it is
                    $dayOfWeek = $c_startdatetime->dayOfWeek;

                    $startdatetime = $c_startdatetime->toDateTimeString();
                    $enddatetime = $c_enddatetime->toDateTimeString();

                    $startdate = $c_startdatetime->toDateString();
                    $enddate = $c_startdatetime->toDateString();

                    $starttime = $c_startdatetime->toTimeString();
                    $endtime = $c_enddatetime->toTimeString();  
                }
                // For daily booking
                if ($booking_period == "daily") {
                    // Start date
                    $booking_startdatetime = ''.$booking_startdate.' 00:00';
                    $c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $booking_startdatetime, 'Europe/Berlin');
                    // End date
                    $booking_enddatetime = ''.$booking_enddate.' 23:59';
                    $c_enddatetime = Carbon::createFromFormat('d/m/Y H:i', $booking_enddatetime, 'Europe/Berlin');

                    $startdatetime = $c_startdatetime->toDateTimeString();
                    $enddatetime = $c_enddatetime->toDateTimeString();

                    $startdate = $c_startdatetime->toDateString();
                    $enddate = $c_enddatetime->toDateString();

                    $starttime = $c_startdatetime->toTimeString();
                    $endtime = $c_enddatetime->toTimeString();  
                }
                
                // For monthly booking
                if ($booking_period == "monthly") {
                    // Start date
                    $booking_startdatetime = ''.$booking_startdate.' 00:00';
                    $c_startdatetime = Carbon::createFromFormat('d/m/Y H:i', $booking_startdatetime, 'Europe/Berlin');
                    $c_enddatetime = $c_startdatetime->copy()->addMonths($booking_monthly_duration);

                    $startdatetime = $c_startdatetime->toDateTimeString();
                    $enddatetime = $c_enddatetime->toDateTimeString();

                    $startdate = $c_startdatetime->toDateString();
                    $enddate = $c_enddatetime->toDateString();

                    $starttime = $c_startdatetime->toTimeString();
                    $endtime = $c_enddatetime->toTimeString();  
                }

                // get user
                $user = Sentry::getUser();
                $user_details = DB::table('customer_details')
                                ->where('user_id', $user->id)
                                ->first();

                // Get Braintree customer ID and cc_token of existing customers
                $bt_customer_id = $user_details->braintree_customer_id;
                $bt_cc_token = $user_details->braintree_cc_token;

                // Check if returning customer
                if ($bt_customer_id != "") {
                    // Is credit card changed?
                    if ($booking_cc_changed == '0') {
                        // Change the card details on Braintree
                        $updateCustomerResult = $this->update_customer_braintree($bt_customer_id, $bt_cc_token,
                            $booking_cardnumber,$booking_cvcnumber, $booking_expiration_month, $booking_expiration_year);
                        if ($updateCustomerResult->success) {
                            DB::table('customer_details')
                                ->where('user_id', $user->id)
                                ->update(array('last_CC' => $updateCustomerResult->customer->creditCards[0]->last4));
                        } else {
                            // Validation or verification wasn't successful
                            // Inform the customers
                            // 
                            // Get the verification information
                            $verification = $updateCustomerResult->creditCardVerification;

                            if ($verification) {
                                // verification status like, "processor_declined" or "gateway_rejected"
                                $verificationStatus = $verification->status;
                                Log::error("Verification status: ".$verificationStatus);

                                if ($verificationStatus == "processor_declined") {
                                    // Processor Reponse Code like, "2000"
                                    Log::error("Verification Response Code: ".$verification->processorResponseCode);

                                    // Error response code like, "Do Not Honor"
                                    $err_message = $verification->processorResponseText;
                                    Log::error("Verification Response Text: ".$err_message);

                                    // Return error
                                    return Response::json(array('status' => 'Error', 'message' => $err_message));
                                } elseif ($verificationStatus == "gateway_rejected") {
                                    // Log the reason
                                    $verificationGatewayRejectionReason = $verification->gatewayRejectionReason;
                                    Log::error($verificationGatewayRejectionReason);

                                    $err_message = "You've got ".$verificationGatewayRejectionReason." wrong.";

                                    // Return error
                                    return Response::json(array('status' => 'Error', 'message' => $err_message));
                                }
                            }

                            // Log all the errors ||| IS IT NEEDED?
                            foreach (($updateCustomerResult->errors->deepAll()) as $error) {
                                Log::error($error->code . ": " . $error->message . "\n");
                                // MUST BE CHANGED TO MATCH THIS VALIDATION LOGIC
                                // https://www.braintreepayments.com/docs/php/general/validation_errors
                                $err_message = $error->message;
                            }

                            // If came so far, then error occurred are not of verification type
                            // MUST BE REPORTED FOR QUICKER SCRUTINY
                            // $err_message = "Technical error occured. Team will be looking into the issue.";

                            // Return error
                            return Response::json(array('status' => 'Error', 'message' => $err_message));
                        }
                    }
                } else {
                    // Save new customer on Braintree
                    $createCustomerResult = $this->create_customer_braintree($user->email, $booking_cardnumber,
                        $booking_cvcnumber, $booking_expiration_month, $booking_expiration_year);
                    if ($createCustomerResult->success) {
                        DB::table('customer_details')
                            ->where('user_id', $user->id)
                            ->update(array('braintree_customer_id' => $createCustomerResult->customer->id,
                                'last_CC' => $createCustomerResult->customer->creditCards[0]->last4,
                                'braintree_cc_token' => $createCustomerResult->customer->creditCards[0]->token));
                    } else {
                        // Validation or verification wasn't successful
                        // Inform the customers
                        // 
                        // Get the verification information
                        $verification = $createCustomerResult->creditCardVerification;

                        if ($verification) {
                            // verification status like, "processor_declined" or "gateway_rejected"
                            $verificationStatus = $verification->status;
                            Log::error("Verification status: ".$verificationStatus);

                            if ($verificationStatus == "processor_declined") {
                                // Processor Reponse Code like, "2000"
                                Log::error("Verification Response Code: ".$verification->processorResponseCode);

                                // Error response code like, "Do Not Honor"
                                $err_message = $verification->processorResponseText;
                                Log::error("Verification Response Text: ".$err_message);

                                // Return error
                                return Response::json(array('status' => 'Error', 'message' => $err_message));
                            } elseif ($verificationStatus == "gateway_rejected") {
                                // Log the reason
                                $verificationGatewayRejectionReason = $verification->gatewayRejectionReason;
                                Log::error($verificationGatewayRejectionReason);

                                $err_message = "You've got ".$verificationGatewayRejectionReason." wrong.";

                                // Return error
                                return Response::json(array('status' => 'Error', 'message' => $err_message));
                            }
                        }

                        // Log all the errors ||| IS IT NEEDED?
                        foreach (($createCustomerResult->errors->deepAll()) as $error) {
                            Log::error($error->code . ": " . $error->message . "\n");
                            // MUST BE CHANGED TO MATCH THIS VALIDATION LOGIC
                            // https://www.braintreepayments.com/docs/php/general/validation_errors
                            $err_message = $error->message;
                        }

                        // If came so far, then error occurred are not of verification type
                        // MUST BE REPORTED FOR QUICKER SCRUTINY
                        // $err_message = "Technical error occured. Team will be looking into the issue.";

                        // Return error
                        return Response::json(array('status' => 'Error', 'message' => $err_message));
                    }
                }

                // -- Credit Card verification successful --
                // Create reservation code
                $reservation_code =  mt_rand_str(12, '0123456789ABCDEF');

                // Save the reservation in the database
                // Get Venue ID of the room
                $venue_id = DB::table('rooms')->where('rooms.id', $booking_room_id)->pluck('venue_id');

                // Get Timezone code of the venue
                $timezone_id = DB::table('venues')->where('venues.id', $venue_id)->pluck('timezone');

                // Get Currency ID
                $currency_id = DB::table('currencies')->where('currency_code', $booking_currency)->pluck('currencies.id');

                // start_datetime and end_datetime in 'customer_bookings' table are saved following way
                // hourly: 'Startdate + Start Time' to 'Startdate + End Time'
                // daily: 'Startdate + 00:00' to 'Enddate + 23:59'
                // monthly: 'Startdate + 00:00' to 'Enddate(month) + 00:00'
                // 
                // Save all reservation details in the DB
                $booking_id = DB::table('customer_bookings')->insertGetId(array(
                    'user_id' => $user->id, 'room_id' => $booking_room_id, 'venue_id' => $venue_id,
                    'start_datetime' => $startdatetime, 'end_datetime' => $enddatetime, 'title' => $booking_title,
                    'comment' => $booking_comments, 'currency_id' => $currency_id, 'status_id' => '5',
                    'price_orig' => $booking_price_orig, 'price_tax' => $booking_price_tax, 'price_final' => $booking_price_final,
                    'timezone' => $timezone_id, 'booking_code' => $reservation_code, 'booking_period' => $booking_period,
                    'booking_duration' => $booking_duration
                ));

                $rr_type = '';

                // Add new reservation to booking events table ind DB
                DB::table('booking_events')->insert(
                    array('dtstart' => $startdatetime, 'dtend' => $enddatetime, 'until' => $enddatetime, 'freq' => $rr_type,
                        'venue_id' => $venue_id, 'room_id' => $booking_room_id, 'booking_id' => $booking_id, 'timezone_id' => $timezone_id)
                );


                // Create confirmation code
                // Combine booking_id with reservation code
                $combination_code = $booking_id.$this->gum_key.''.$reservation_code;

                // Hide the combination code with encryption
                $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
                $iv = substr(hash('sha256', $this->secret_iv), 0, $iv_size);
                $confirmation_code = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->reservation_key, $combination_code, MCRYPT_MODE_CFB, $iv);
                $confirmation_code = base64url_encode($confirmation_code);


                // Email to Workspace Owner
                // Get Rooms and Venue info
                $room = DB::table('rooms')->where('rooms.id', $booking_room_id)->first();
                $venue = DB::table('venues')->where('venues.id', $room->venue_id)->first();

                // Create booking date
                $today = Carbon::today();
                $booking_date = $today->toFormattedDateString();

                // Email contents
                $mail_data_venue = array(
                    'reservation_code' => ''.$reservation_code.'',
                    'confirmation_code' => ''.$confirmation_code.'',
                    'room_name' => $room->room_name,
                    'venue_name' => $venue->venue_name,
                    'title' => $booking_title,
                    'comment' => $booking_comments, 
                    'booking_date' => $booking_date,
                    'currency' => $booking_currency,
                    'price_orig' => $booking_price_orig, 
                    'price_tax' => $booking_price_tax, 
                    'price_final' => $booking_price_final,
                );

                // Get email address for venue administration
                // $venue_email = $venue->admin_email;
                $venue_email = 'nandanito+venue1@gmail.com';

                // Mailgun to workspace owner
                // Mailgun::send('emails.confirmation_request_venue', $mail_data_venue, function($message) use ($venue, $venue_email)
                // {
                //     $message->to($venue_email, $venue->venue_name)->subject('Test successful');
                // });


                // Email to Customer with details of reservation
                // Email contents
                $mail_data_customer = array(
                    'customer' => ''.$user->first_name.' '.$user->last_name.'',
                    'reservation_code' => ''.$reservation_code.'',
                    'room_name' => $room->room_name,
                    'venue_name' => $venue->venue_name,
                    'title' => $booking_title,
                    'comment' => $booking_comments, 
                    'booking_date' => $booking_date,
                    'currency' => $booking_currency,
                    'price_orig' => $booking_price_orig, 
                    'price_tax' => $booking_price_tax, 
                    'price_final' => $booking_price_final,
                );

                // Mailgun to customer
                // Mailgun::send('emails.reservation_applied_customer', $mail_data_customer, function($message) use ($user)
                // {
                //     $message->to($user->email, $user->last_name)->subject('Test successful');
                // });

                return Response::json(array('status' => 'Success', 'message' => 'Your reservation was successful.<br/><br/>Your reservation code is '.$reservation_code.'<br/><br/><br/>You will receive the reservation email shortly.<br/>'.$confirmation_code));
            }
        } catch (Exception $e) {
            Log::error($e);
            return Response::json(array('status' => 'Error', 'message' => $e->getMessage()));
        }

    }


    /**
     * Confirm the reservation with YES
     */

    function run_approve_booking($code)
    {
        try {
            // Find the booking id from confirmation code
            // Decrypt the confirmation code
            $combi_code = base64url_decode($code);
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
            $iv = substr(hash('sha256', $this->secret_iv), 0, $iv_size);
            $combination_code = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->reservation_key, $combi_code, MCRYPT_MODE_CFB, $iv);

            // Code includes booking_id and reservation_code
            $booking_id = strstr($combination_code, $this->gum_key, true);
            $reservation_code = substr(strstr($combination_code, $this->gum_key), strlen($this->gum_key));

            // Get the reservation code for the booking id from DB
            $booking_details = DB::table('customer_bookings')->where('customer_bookings.id', $booking_id)->first();

            // See what status it's got
            if ($booking_details->status_id === 5) {
                // This means reservation is still pending
                if ($reservation_code === $booking_details->booking_code) {
                    // But now, reservation is confirmed

                    // Now off to making the payment
                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();
                    $user_details = DB::table('customer_details')
                                    ->where('user_id', $booking_details->user_id)
                                    ->first();

                    // Get Braintree customer ID and cc_token of customer
                    $bt_customer_id = $user_details->braintree_customer_id;
                    $bt_cc_token = $user_details->braintree_cc_token;

                    if ($bt_customer_id != "") {
                        $paymentResult = $this->pay_existing_customer($booking_details->price_final, $bt_customer_id, $bt_cc_token);
                    } else {
                        // Impossible Area ...
                        // Create error ...
                    }

                    // Paying customer details saved/modified
                    if ($paymentResult->success) {
                        // Save the user in DB
                        $transaction_id = DB::table('transactions')->insertGetId(array(
                            'user_id' => $booking_details->user_id, 'currency_id' => $booking_details->currency_id, 
                            'amount' => $booking_details->price_final, 'status' => '11',
                            'braintree_transaction_id' => $paymentResult->transaction->id
                        ));
                    } else if ($paymentResult->transaction) {
                        // verification status like, "processor_declined" or "gateway_rejected"
                        $transactionStatus = $paymentResult->transaction->status;
                        Log::error("transaction status: ".$transactionStatus);

                        if ($transactionStatus == "processor_declined") {
                            // Processor Reponse Code like, "2000"
                            Log::error("transaction Response Code: ".$transaction->processorResponseCode);

                            // Error response code like, "Do Not Honor"
                            $err_message = $transaction->processorResponseText;
                            Log::error("transaction Response Text: ".$err_message);

                            // Return error
                            return Response::json(array('status' => 'Error', 'message' => $err_message));
                        } elseif ($transactionStatus == "gateway_rejected") {
                            // Log the reason
                            $transactionGatewayRejectionReason = $transaction->gatewayRejectionReason;
                            Log::error($transactionGatewayRejectionReason);

                            $err_message = "You've got ".$transactionGatewayRejectionReason." wrong.";

                            // Return error
                            return Response::json(array('status' => 'Error', 'message' => $err_message));
                        }
                    } else {
                        // Log all the errors ||| IS IT NEEDED?
                        foreach (($paymentResult->errors->deepAll()) as $error) {
                            Log::error($error->code . ": " . $error->message . "\n");
                            // MUST BE CHANGED TO MATCH THIS VALIDATION LOGIC
                            // https://www.braintreepayments.com/docs/php/general/validation_errors
                            $err_message = $error->message;
                        }

                        // If came so far, then error occurred are not of verification type
                        // MUST BE REPORTED FOR QUICKER SCRUTINY
                        // $err_message = "Technical error occured. Team will be looking into the issue.";

                        // Return error
                        return Response::json(array('status' => 'Error', 'message' => $err_message));
                    }

                    // Make changes to DB
                    // Change the booking status
                    DB::table('customer_bookings')
                        ->where('customer_bookings.id', $booking_id)
                        ->update(array('status_id' => '6'));

                    /* Send email to both, customer and workspace owners */
                    // Email to workspace owner
                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }

                    // Email contents
                    $mail_data_venue = array(
                        'reservation_code' => ''.$reservation_code.'',
                        'room_name' => $room->room_name,
                        'venue_name' => $venue->venue_name,
                        'title' => $booking_details->title,
                        'comment' => $booking_details->comment, 
                        'booking_date' => $booking_date,
                        'currency' => $booking_details->currency_id,
                        'price_orig' => $booking_details->price_orig, 
                        'price_tax' => $booking_details->price_tax, 
                        'price_final' => $booking_details->price_final,
                    );

                    // Get email address for venue administration
                    // $venue_email = $venue->admin_email;
                    $venue_email = 'nandanito+venue1@gmail.com';

                    // Mailgun::send('emails.confirmation_approved_venue', $mail_data_venue, function($message) use ($venue, $venue_email)
                    // {
                    //     $message->to($venue_email, $venue->venue_name)->subject('Reservation '.$reservation_code.' approved');
                    // });

                    // Email to Customer with details of reservation
                    // Email contents
                    $mail_data_customer = array(
                        'customer' => ''.$user->first_name.' '.$user->last_name.'',
                        'reservation_code' => ''.$reservation_code.'',
                        'room_name' => $room->room_name,
                        'venue_name' => $venue->venue_name,
                        'title' => $booking_details->title,
                        'comment' => $booking_details->comment, 
                        'booking_date' => $booking_date,
                        'currency' => $booking_details->currency_id,
                        'price_orig' => $booking_details->price_orig, 
                        'price_tax' => $booking_details->price_tax, 
                        'price_final' => $booking_details->price_final,
                    );

                    // Mailgun::send('emails.reservation_approved_customer', $mail_data_customer, function($message) use ($user)
                    // {
                    //     $message->to($user->email, $user->last_name)->subject('Your reservation '.$reservation_code.' approved');
                    // });
                    $approval_status = 'newly_approved';

                    return View::make('reservation.approved', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));
                } else {
                    // No reservation with the given code found
                    
                    // return view::make('reservation.nonexistent', array());
                }
            } else {
                if ($booking_details->status_id === 6) {
                    # This means reservation is already confirmed
                    # Send to page with reservation already confirmed alert
                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();

                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }
                    
                    $approval_status = 'already_approved';

                    return View::make('reservation.approved', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));

                } elseif ($booking_details->status_id === 7) {
                    # This means reservation has been rejected
                    # Send to page showing the reservation was cancelled and by whom
                    # This means reservation is already confirmed
                    # Send to page with reservation already confirmed alert
                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();

                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }
                    
                    $approval_status = 'already_rejected';

                    return View::make('reservation.rejected', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));
                } elseif ($booking_details->status_id === 8) {
                    # Send to page showing that it was cancelled
                    # Either by customer himself or by workspace owner before
                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();

                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }
                    
                    $approval_status = 'already_cancelled';

                    return View::make('reservation.cancelled', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));
                }
            }

        } catch (Exception $e) {
            Log::error($e);
            print_r($e->getMessage());
        }

    }


    /**
     * Reject the reservation with NO
     */

    function run_reject_booking($code)
    {
        try {
            // Find the booking id from confirmation code
            // Decrypt the confirmation code
            $combi_code = base64url_decode($code);
            $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CFB);
            $iv = substr(hash('sha256', $this->secret_iv), 0, $iv_size);
            $combination_code = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->reservation_key, $combi_code, MCRYPT_MODE_CFB, $iv);

            // Code includes booking_id and reservation_code
            $booking_id = strstr($combination_code, $this->gum_key, true);
            $reservation_code = substr(strstr($combination_code, $this->gum_key), strlen($this->gum_key));
            
            // Get the reservation code for the booking id from DB
            $booking_details = DB::table('customer_bookings')->where('customer_bookings.id', $booking_id)->first();

            // See what status it's got
            if ($booking_details->status_id === 5) {
                # This means reservation is pending
                // Checking if this code really deals with same reservation
                // before moving on to rejection
                if ($reservation_code === $booking_details->booking_code) {
                    // Off to rejecting the reservation

                    // Make changes to DB
                    // Change the booking status
                    DB::table('customer_bookings')
                        ->where('customer_bookings.id', $booking_id)
                        ->update(array('status_id' => '7'));

                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();
                    $user_details = DB::table('customer_details')
                                    ->where('user_id', $booking_details->user_id)
                                    ->first();

                    /* Send email to both, customer and workspace owners */
                    // Email to workspace owner
                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Email contents
                    $mail_data_venue = array(
                        'reservation_code' => ''.$reservation_code.'',
                        'room_name' => $room->room_name,
                        'venue_name' => $venue->venue_name,
                        'title' => $booking_details->title,
                        'comment' => $booking_details->comment, 
                        'booking_date' => $booking_date,
                        'currency' => $booking_details->currency_id,
                        'price_orig' => $booking_details->price_orig, 
                        'price_tax' => $booking_details->price_tax, 
                        'price_final' => $booking_details->price_final,
                    );

                    // Get email address for venue administration
                    // $venue_email = $venue->admin_email;
                    $venue_email = 'nandanito+venue1@gmail.com';

                    // Mailgun::send('emails.confirmation_rejected_venue', $mail_data_venue, function($message) use ($venue, $venue_email)
                    // {
                    //     $message->to($venue_email, $venue->venue_name)->subject('Reservation '.$reservation_code.' approved');
                    // });

                    // Email to Customer with details of reservation
                    // Email contents
                    $mail_data_customer = array(
                        'customer' => ''.$user->first_name.' '.$user->last_name.'',
                        'reservation_code' => ''.$reservation_code.'',
                        'room_name' => $room->room_name,
                        'venue_name' => $venue->venue_name,
                        'title' => $booking_details->title,
                        'comment' => $booking_details->comment, 
                        'booking_date' => $booking_date,
                        'currency' => $booking_details->currency_id,
                        'price_orig' => $booking_details->price_orig, 
                        'price_tax' => $booking_details->price_tax, 
                        'price_final' => $booking_details->price_final,
                    );

                    // Mailgun::send('emails.reservation_rejected_customer', $mail_data_customer, function($message) use ($user)
                    // {
                    //     $message->to($user->email, $user->last_name)->subject('Your reservation '.$reservation_code.' approved');
                    // });
                    

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }
                    
                    $approval_status = 'newly_rejected';

                    return View::make('reservation.rejected', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));
                } else {
                    // No reservation with the given code found
                    
                    // return view::make('reservation.nonexistent', array());
                }
            } else {
                if ($booking_details->status_id === 6) {
                    # This means reservation is already confirmed
                    # Send to page with reservation already confirmed alert
                    # It cannot be rejected, but cancelled
                    # Only possible from Control Center
                    # ...
                    
                    // return View::make('reservation.approved', array());
                } elseif ($booking_details->status_id === 7) {
                    # This means reservation has already been rejected
                    # Send to page showing the reservation was rejected and by whom
                    // Get user information
                    $user = DB::table('users')
                                ->where('users.id', $booking_details->user_id)
                                ->first();

                    // Get Rooms and Venue info
                    $room = DB::table('rooms')->where('rooms.id', $booking_details->room_id)->first();
                    $venue = DB::table('venues')
                                ->join('cities', 'venues.city_id', '=', 'cities.id')
                                ->join('states', 'venues.state_id', '=', 'states.id')
                                ->join('countries', 'venues.country_id', '=', 'countries.id')
                                ->where('venues.id', $booking_details->venue_id)->first();

                    // Get booking date
                    $b_date = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->created_at, 'Europe/Berlin');
                    $booking_date = $b_date->toFormattedDateString();

                    // Get booking period
                    if ($booking_details->booking_period === "hourly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $b_date = $b_startdatetime->toFormattedDateString();

                        $starttime = $b_startdatetime->toTimeString();
                        $endtime = $b_enddatetime->toTimeString();

                        $real_duration = 'On '.$b_date.' between '.$starttime.' - '.$endtime.'';
                    } elseif ($booking_details->booking_period === "daily") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    } elseif ($booking_details->booking_period === "monthly") {
                        $b_startdatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->start_datetime, 'Europe/Berlin');
                        $b_enddatetime = Carbon::createFromFormat('Y-m-d H:i:s', $booking_details->end_datetime, 'Europe/Berlin');

                        $startdate = $b_startdatetime->toFormattedDateString();
                        $enddate = $b_enddatetime->toFormattedDateString();

                        $real_duration = ''.$startdate.' - '.$enddate.'';
                    }
                    
                    $approval_status = 'already_rejected';

                    return View::make('reservation.rejected', array('user' => $user, 'reservation_code' => $reservation_code,
                        'room' => $room, 'venue' => $venue, 'booking_details' => $booking_details, 'booking_date' => $booking_date,
                        'real_duration' => $real_duration, 'approval_status' => $approval_status));
                } elseif ($booking_details->status_id === 8) {
                    # This means reservation is cancelled
                    # Send to page showing that it was cancelled
                    # Either by customer himself or by workspace owner before
                    # ...
                    
                    // return View::make('reservation.cancelled', array());
                }
            }

        } catch (Exception $e) {
            Log::error($e);
        }

    }

    /**
     * Cancel reservation
     */

    function run_cancel_booking($id)
    {
        try {
            /* Check for cancellation timeframe */
            // Important, if cancellation coming from customer,
            // but irrelevant for workspace owner


            /* Is it customer, who cancelled it? */
            // If Yes
            // Cancelled before approval from workspace owner?
            
            // Get the reservation code for the booking id from DB
            $booking_details = DB::table('customer_bookings')->where('customer_bookings.id', $id)->first();

            // See what status it's got
            if ($booking_details->status_id === '5') {
                # This means reservation is still pending

            } else {
                // Cancelled after it is approved?
                // If yes, then do refund
                
                # ...

                if ($booking_details->status_id === '6') {
                    # This means reservation is confirmed
                    
                    # ...
                    
                    // return View::make('reservation.cancelled', array());
                } elseif ($booking_details->status_id === '7') {
                    # This means reservation had been rejected
                    # Impossible Area
                    # Throw errors ...
                } elseif ($booking_details->status_id === '8') {
                    # This means reservation was cancelled
                    # Impossible Area
                    # Throw errors ...
                }
            }
            
            // Inform workspace owner, that reservation has been cancelled
            

            // Inform customer that it has been successfully cancelled
            // and refund is on the way
            



            /* Is it workspace owner, who cancelled it? */
            // If Yes
            // It must have been approved before
            // Do refund
            
            // Inform customer that reservation has been cancelled
            // for following reason ...
            // And he will be refunded
            
            
            // Inform workspace owner, that cancellation was successful

        } catch (Exception $e) {
            Log::error($e);
        }

    }

}