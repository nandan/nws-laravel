@extends('layouts.main')

@section('page_css')
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<span class="current-page">Personal Information</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->

				</div><!-- end sidebar  -->

			</div><!-- end sidebar outer  -->
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>Settings</h2>
				</div>
				<div class="clear mb-30"></div>
					<div class="blog-item padding-20">
					<div class="col-xs-12 col-sm-6 col-md-3">

						<h3>Profile Picture</h3>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-9">
						<form role="form" id="profile_picture_form" name="profile_picture_form" action="{{URL::route('run_profile_picture')}}" method="post" enctype="multipart/form-data">
						<div class="clearfix mb-10">
							<div class="col-md-6">
								<div id="profile_picture_div">
									@if($personal_information[0]->photo_id == '0')
										<img data-src="holder.js/200x200">
									@else
										<img src="{{$personal_picture->url}}">
									@endif
								</div>
								
							</div>
							<div class="col-md-6">
								
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-6">
								<input type="file" name="file" id="file">
							</div>
							<div class="col-md-6">
								 
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
							
							</div>
							<div class="col-md-8">
								<button type="submit" class="btn btn-primary pull-right">Upload</button>
								</form>
								
								<button class="btn btn-primary pull-right" id="delete_bt" >Delete</button>
							</div>
							
						</div>
					
					</div>
				</div>
		

				<div class="blog-item padding-20">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<h3>Personal Information</h3>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-9">
						<form role="form" id="personal_information_form" name="personal_information_form" action="" method="post">
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Title
							</div>
							<div class="col-md-8">
								<select id="title" name="title" class="form-control mySelectBoxClass hasCustomSelect">
									<option selected="selected">{{$personal_information[0]->title}}</option>
									@if ($personal_information[0]->title !='Mr.')
									<option>Mr.</option>
									@endif
									@if ($personal_information[0]->title !='Mrs.')
									<option>Mrs.</option>
									@endif
									

								</select>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Firstname
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="first_name" name="first_name" value="{{$personal_information[0]->first_name}}"/>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Lastname
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="last_name" name="last_name" value="{{$personal_information[0]->last_name}}"/>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Company
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="company" name="company" value="{{$personal_information[0]->company}}"/>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Telephone Mobile
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="telephone_mobile" name="telephone_mobile" placeholder="+XX XXXXXXXXXX" value="{{$personal_information[0]->phone_mobile}}" />
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Telephone Office
							</div>
							<div class="col-md-8">
								<input type="text" class="form-control" id="telephone_office" name="telephone_office" placeholder="+XX XXXXXXXXXX" value="{{$personal_information[0]->phone_office}}"/>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
							
							</div>
							<div class="col-md-8">
								<button type="submit" class="btn btn-primary pull-right">Save</button>
							</div>
						</div>
					</form>
					</div>
				</div>
				
				<div class="clear"></div>

				<div class="blog-item padding-20">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<h3>Localization</h3>
					</div>
					<form role="form" id="localization_form" name="localization_form" action="" method="post">
					<div class="col-xs-12 col-sm-6 col-md-9">

						<div class="clearfix mb-10">
							<div class="col-md-4">
								Language
							</div>
							<div class="col-md-8">
								<select class="form-control mySelectBoxClass hasCustomSelect" id="language" name="language">
									<option selected="selected">{{$personal_information[0]->language}}</option>
									@if ($personal_information[0]->language !='English')
									<option>English</option>
									@endif
									@if ($personal_information[0]->language !='German')
									<option>German</option>
									@endif
									@if ($personal_information[0]->language !='Spanish')
									<option>Spanish</option>
									@endif
								</select>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Currency
							</div>
							<div class="col-md-8">
								<select class="form-control mySelectBoxClass hasCustomSelect" id="currency" name="currency">
									<option selected="selected">{{$personal_information[0]->currency}}</option>
									@if ($personal_information[0]->currency !='EUR')
									<option>EUR</option>
									@endif	
									@if ($personal_information[0]->currency !='USD')
									<option>USD</option>
									@endif	
									@if ($personal_information[0]->currency !='GBP')
									<option>GBP</option>
									@endif	

									</select>
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
							
							</div>
							<div class="col-md-8">
								<button class="btn btn-primary pull-right">Save</button>
							</div>
							
						</div>
					</form>
					</div>
				</div>

				<div class="clear"></div>
				<div class="blog-item padding-20">
					<div class="col-xs-12 col-sm-6 col-md-3">
						<h3>Login Details</h3>
					</div>

					<div class="col-xs-12 col-sm-6 col-md-9">

						<div class="clearfix mb-10">
							<div class="col-md-4">
								Email
							</div>
							<div class="col-md-8">
								pepsi@pepsi.deeeee
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-4">
								Password
							</div>
							<div class="col-md-8">
								*****************
							</div>
						</div>
						<div class="clearfix mb-10">
							<div class="col-md-2">
							
							</div>
							<div class="col-md-10">
								<button class="btn btn-primary pull-right">Delete Account</button>
								<button class="btn btn-primary pull-right">Change Password</button>
								<button class="btn btn-primary pull-right">Change Email</button>
							</div>
							
						</div>

					</div>
				</div>


			</div>



			<div class="clear"></div>

		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('custom_js/jquery.form.min.js')}}"></script>
<script type="text/javascript" src="{{asset('custom_js/customer_settings.js')}}"></script>
@stop



