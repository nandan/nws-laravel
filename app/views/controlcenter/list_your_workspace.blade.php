@extends('layouts.main')

@section('page_css')
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Create Wizard</div>
		<div class="breadcrumb clearfix">
			<span class="current-page">New Workspace</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">
					<div class="sidebar-item white-bg padding-20 mb-20">
						<button onclick="javascript:list_your_workspace()" class="btn btn-success" >
							Start Listing your Workspace
						</button>
					</div><!-- end categories -->
					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->
				</div><!-- end sidebar  -->
			</div><!-- end sidebar outer  -->
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>List Your Workspace for FREE</h1>
					</div>
					<div class="clear mb-30"></div>
					<div class="blog-item">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-3">
								<img data-src="holder.js/200x200">
							</div>
							<div class="col-xs-12 col-sm-8 col-md-9">
								<h3 class="padding-20">Increase your Revenues</h3>
								<p>Get new customers....</p>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="blog-item">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-3">
								<img data-src="holder.js/200x200">
							</div>
							<div class="col-xs-12 col-sm-8 col-md-9">
								<h3 class="padding-20">Maximize your Workspace´s potential</h3>
							</div>
						</div>	
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Login or Register</h4>
					</div>
					<div class="modal-body">
						<div id="div_modal_login_form">
							<form role="form" id="modal_login_form" name="modal_login_form" action="" method="post">
								<div class="form-group">
									<label for="email">Email</label>
									<input type="text" class="form-control" id="modal_email"  name="modal_email" placeholder="Email">
								</div>
								<div class="form-group">
									<label for="password">Password</label>
									<input type="password" class="form-control" id="modal_password" name="modal_password" placeholder="Password">
								</div>
								<div class="checkbox">
									<label>
										<input type="checkbox" id="modal_remember_me" name="modal_remember_me"> Remember Me
									</label>
								</div>
								<button type="submit" class="btn btn-primary">Login</button>
								<div class="clear mb-10"></div>
								<a href="#" class="forgot">Forgot Your Password?</a>
							</form>
						</div>

						<div id="div_modal_register_form" style="display:none">
							<form role="form" id="modal_register_form" name="modal_register_form" action="" method="post">
									<div class="form-group">
										<label for="first_name">Firstname</label>
										<input type="text" class="form-control" id="modal_first_name" name="modal_first_name" placeholder="Firstname">
									</div>
									<div class="form-group">
										<label for="last_name">Lastname</label>
										<input type="text" class="form-control" id="modal_last_name" name="modal_last_name" placeholder="Lastname">
									</div>
									<div class="form-group">
										<label for="emailx">Email</label>
										<input type="text" class="form-control" id="modal_emailx" name="modal_emailx" placeholder="Email">
									</div>
									<div class="form-group">
										<label for="passwordx">Password</label>
										<input type="password" class="form-control" id="modal_passwordx"  name="modal_passwordx" placeholder="Password min 6.char">
									</div>
									<div class="form-group">
										<label for="password_again">Confirm Password</label>
										<input type="password" class="form-control" id="modal_password_again" name="modal_password_again" placeholder="Confirm  Password">
									</div>
										<div class="checkbox">
										<label>
											<input type="checkbox" id="modal_tos" name="modal_tos"> Accept TOS
										</label>
									</div>
									<button type="submit" class="btn btn-success">Register</button>
									<div class="clear mb-10"></div>
								</form>
						</div>


					</div>
					<div class="modal-footer">
						<div id="modal_footer_register"><a href="javascript:toggle_modal_form()">Not yet registered? Register here</a></div>
						<div id="modal_footer_login" style="display:none"><a href="javascript:toggle_modal_form()">Already have an account</a></div>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div><!-- end content_wrapper -->



	<!-- Main Content End -->
	<!-- Footer -->
	@include('layouts.footer')

	@stop

	@section('page_js')
	<script type="text/javascript" src="{{asset('custom_js/list_your_workspace.js')}}"></script>
	@stop



