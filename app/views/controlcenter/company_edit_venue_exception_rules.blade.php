@extends('layouts.main')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.datetimepicker.css')}}" />
<style>
	/* Wizard Menu at the top */
	.form-title {
		font-size: 18px;
		font-weight: 700;
		line-height: 1;
		padding: 0px 20px 13px 20px;
	}

	
	/* Wizard items and elements */
	.wizard-item {
		padding: 10px;
	}
	.wizard-element {
		margin-bottom: 10px;
	}
	.wizard-element.address-field {
		height: 34px;
	}
	.wizard-button {
		margin-right: -15px;
	}
	.wizard-button button {
		margin-right: 35px;
		width: 100px;
	}


	/* Sidebar */
	.sidebar ul li {
		/*padding-top: 5px;*/
	}
	.sidebar ul li.active {
		background-color: #eee;
	}
	

	table.exception-table tr th{
		text-align: center;
		font-weight: 700;
	}

	table.exception-table tr td{
		text-align: center;
	}

	/* Modal Window */
	#exceptionModal h4,h5{
		padding-top: 7px;
	}

	/* Tabs withing Modal Window */
	.tabs li a {
		font-size: 12px; 
		padding: 5px 10px;
	}
	.tabs li a.active {
		background-color: #EBEBEB
	}

	ul#preference-tab {
		background-color: #EBEBEB
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="{{ URL::route('controlcenter_company_bookings') }}">{{ $company->company_name }}</a>
			<span class="current-page">{{ $venue->venue_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">
					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>{{ $venue->venue_name }}</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_venue_bookings', array($venue->id))}}">Bookings</a></li>
							<li><a href="{{URL::route('controlcenter_venue_calendar')}}">Calendar</a></li>
							<li><a href="{{URL::route('controlcenter_venue_rooms', array($venue->id))}}">Rooms</a></li>
							<li><a href="#">Financials</a></li>
							<li>Settings
								<ul class="nav">
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue', array($venue->id))}}" style="padding:0">General</a>
									</li>
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_calendar', array($venue->id))}}" style="padding:0">Calendar</a>
									</li>
									<li class="active" style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_exception_rules', array($venue->id))}}" style="padding:0">Exception Rules</a>
									</li>
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_user_management', array($venue->id))}}" style="padding:0">User Management</a>
									</li>
								</ul>
							</li>
						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->
				</div><!-- end sidebar  -->
			</div><!-- end sidebar outer  -->

			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<span class="form-title">Edit Venue Settings</span>
				</div>
				<div class="clear mb-30"></div>
				@include('venues.edit_exception_rules')
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div><!-- end content_wrapper -->

<!-- Modal -->
<div class="modal fade" id="exceptionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #F0F0F0;">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Add new exception rules</h4>
      	</div>
      	<div id="new_exception_rule">
	      	<form role="form" id="modal_exception_rule_form" name="modal_exception_rule_form" action="" method="post">
			<div class="modal-body">
				<div class="row white-bg padding-20">
					<div class="row mb-10">
		        		<div class="col-md-4">
		        			<h4 class="pull-right">Title</h4>
		        		</div>
		        		<div class="col-md-8">
				        	<input type="text" class="form-control" id="modal_exception_rules_title" name="modal_exception_rules_title" placeholder="Title for exception rule" maxlength="35">
						</div>
					</div>
		        	<div class="bb2 mb-20"></div>
		        	<div class="row mb-10">
		        		<div class="col-md-4">
		        			<h4 class="pull-right">Day Period</h4>
		        		</div>
		        		<div class="col-md-4">
				        	<div class="form-group">
								<div id="date" class="datepicker-wrapper">
									<input type="text" class="form-control" id="modal_exception_rules_startdate" name="modal_exception_rules_startdate" placeholder="dd/mm/yy" value="">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="hidden" class="form-control" id="modal_room_id" name="modal_room_id" value="{{ $venue->id }}" />
								<input type="hidden" class="form-control" id="modal_customer_id" name="modal_customer_id" value="{{ Sentry::getUser()->id }}" />
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<div id="date" class="datepicker-wrapper">
									<input type="text" class="form-control" id="modal_exception_rules_enddate" name="modal_exception_rules_enddate" placeholder="dd/mm/yy" value="">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
					</div>
		        	<div class="bb2 mb-20"></div>
		        	<div class="row mb-10">
		        		<div class="col-md-4">
		        			<h4 class="pull-right">Time Period</h4>
		        		</div>
		        		<div class="col-md-4">
							<div class="form-group">
								<div id="starttime">
									<input type="text" class="form-control" id="modal_exception_rules_starttime" name="modal_exception_rules_starttime" placeholder="from" value="">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<div id="endtimes">
									<input type="text" class="form-control" id="modal_exception_rules_endtime" name="modal_exception_rules_endtime" placeholder="until">
								</div>
							</div>
						</div>
					</div>
		        	<div class="bb2 mb-20"></div>
		        	<div class="row mb-10">
			        	<div class="col-md-4">
		        			<h4 class="pull-right">Recurrence</h4>
		        		</div>
		        		<div class="col-md-8">
			        		<div class="row">
				        		<div class="col-md-6">
									<h5 class="pull-right">Type</h5>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<div id="recurring_type">
											<select class="form-control mySelectBoxClass" id="modal_exception_rules_recurring_type" name="modal_exception_rules_recurring_type">
												<option selected value="none">None</option>
											 	<option value="daily">Daily</option>
											 	<option value="weekly">Weekly</option>
												<option value="monthly">Monthly</option>
												<option value="yearly">Yearly</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div id="modal_recurring_weekly_details">
								<div class="row mt-20">
									<span>Select the recurring day of the week <i class="fa fa-question-circle"></i></span>
								</div>
								<div class="row mt-10 col-md-12">
									<fieldset>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="MO">
												Mo
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="TU">
												Tu
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="WE">
												We
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="TH">
												Th
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="FR">
												Fr
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="SA">
												Sa
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="weekday" value="SO">
												So
											</label>
										</div>
									</fieldset>
									<input type="hidden" class="form-control" id="modal_exception_rules_weekly_days" name="modal_exception_rules_weekly_days" value="" />
								</div>
							</div>
							<div id="modal_recurring_monthly_details">
								<ul class="tabs">
									<li><a href="#exact_date_monthly">Exact date</a></li>
									<li><a href="#particular_day_monthly" class="active">Particular day</a></li>
								</ul>
								<ul id="preference-tab" class="tabs-content">
									<li id="exact_date_monthly">
										<div class="row mt-20">
											<span class="ml-20">Enter exact date(s) of the month (e.g. 1,4,5,29) <i class="fa fa-question-circle"></i></span>
										</div>
										<div class="row mt-10">
											<div class="col-md-12 form-group">
												<div id="monthly_month_day">
													<input type="text" class="form-control" id="modal_exception_rules_monthly_month_days" name="modal_exception_rules_monthly_month_days" placeholder="days..." value="">
												</div>
											</div>
										</div>
									</li>
									<li id="particular_day_monthly" class="active">
										<div class="row mt-20">
											<span class="ml-20">Select the recurring day of the week <i class="fa fa-question-circle"></i></span>
										</div>
										<div class="row mt-10">
											<div class="col-md-5">
												<div class="form-group">
													<div id="endtimes">
														<select class="form-control mySelectBoxClass" id="modal_exception_rules_monthly_count" name="modal_exception_rules_monthly_count">
															<option selected value="">Every</option>
														 	<option value="1">First</option>
														 	<option value="2">Second</option>
															<option value="3">Third</option>
															<option value="4">Fourth</option>
															<option value="5">Fifth</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<span><em>of</em></span>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<div id="endtimes">
														<select class="form-control mySelectBoxClass" id="modal_exception_rules_monthly_day" name="modal_exception_rules_monthly_day">
														 	<option selected value="SU">Sunday</option>
														 	<option value="MO">Monday</option>
															<option value="TU">Tuesday</option>
															<option value="WE">Wednesday</option>
															<option value="TH">Thursday</option>
															<option value="FR">Friday</option>
															<option value="SA">Saturday</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</li>
							</div>
							<div id="modal_recurring_yearly_details">
								<div class="row mt-20">
									<span class="ml-20">Select the recurring month(s) <i class="fa fa-question-circle"></i></span>
								</div>
								<div class="row mt-10 col-md-12">
									<fieldset>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="1">
												Jan
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="2">
												Feb
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="3">
												Mar
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="4">
												Apr
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="5">
												May
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="6">
												Jun
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="7">
												Jul
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="8">
												Aug
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="9">
												Sep
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="10">
												Oct
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="11">
												Nov
											</label>
										</div>
										<div class="styled-checkbox" style="display:inline; margin-right:5px;">
											<label style="margin-left:-5px">
												<input type="checkbox" rel="yearmonth" value="12">
												Dec
											</label>
										</div>
									</fieldset>
									<input type="hidden" class="form-control" id="modal_exception_rules_yearly_months" name="modal_exception_rules_yearly_months" value="" />
								</div>
								<ul class="tabs mt-20">
									<li><a href="#exact_date_yearly">Exact date</a></li>
									<li><a href="#particular_day_yearly" class="active">Particular day</a></li>
								</ul>
								<ul id="preference-tab" class="tabs-content">
									<li id="exact_date_yearly">
										<div class="row mt-20">
											<span class="ml-20">Enter exact date(s) of the month (e.g. 1,4,5,29) <i class="fa fa-question-circle"></i></span>
										</div>
										<div class="row mt-10">
											<div class="col-md-12 form-group">
												<div id="yearly_month_day">
													<input type="text" class="form-control" id="modal_exception_rules_yearly_month_days" name="modal_exception_rules_yearly_month_days" placeholder="days..." value="">
												</div>
											</div>
										</div>
									</li>
									<li id="particular_day_yearly" class="active">
										<div class="row mt-20">
											<span class="ml-20">Select recurring day of the week <i class="fa fa-question-circle"></i></span>
										</div>
										<div class="row mt-10">
											<div class="col-md-5">
												<div class="form-group">
													<div id="endtimes">
														<select class="form-control mySelectBoxClass" id="modal_exception_rules_yearly_count" name="modal_exception_rules_yearly_count">
															<option selected value="">Every</option>
														 	<option value="1">First</option>
														 	<option value="2">Second</option>
															<option value="3">Third</option>
															<option value="4">Fourth</option>
															<option value="5">Fifth</option>
														</select>
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<span><em>of</em></span>
											</div>
											<div class="col-md-5">
												<div class="form-group">
													<div id="endtimes">
														<select class="form-control mySelectBoxClass" id="modal_exception_rules_yearly_day" name="modal_exception_rules_yearly_day">
														 	<option selected value="SU">Sunday</option>
														 	<option value="MO">Monday</option>
															<option value="TU">Tuesday</option>
															<option value="WE">Wednesday</option>
															<option value="TH">Thursday</option>
															<option value="FR">Friday</option>
															<option value="SA">Saturday</option>
														</select>
													</div>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="bb2 mb-20"></div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="submit" class="btn btn-primary">Submit</button>
			</div>
			</form>
		</div>
		<div id="finish_exception_rule">
			<div class="modal-body">
				<h3 id="response"></h3>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Finish</button>
			</div>
		</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script src="{{asset('custom_js/jquery.form.js')}}"></script>
<script src="{{asset('custom_js/jquery.datetimepicker.js')}}"></script>
<script>
	$(function() {
		console.log("off goes the form...");

		$("#finish_edit_venue").hide();
		$("#new_exception_rule").show();
 		$("#finish_exception_rule").hide();
		$("#modal_recurring_weekly_details").hide();
		$("#modal_recurring_monthly_details").hide();
		$("#modal_recurring_yearly_details").hide();

		$("#add_new_rules").click(function() {
			$("#exceptionModal").modal();
		});

		$("#modal_exception_rules_startdate").datetimepicker({
			timepicker:false,
			format:'d/m/Y',
			minDate:0,
			onShow:function( ct ){
				this.setOptions({
					maxDate:$('#modal_exception_rules_enddate').val()?$('#modal_exception_rules_enddate').val():false
				})
			},
		});

		$("#modal_exception_rules_enddate").datetimepicker({
			timepicker:false,
			format:'d/m/Y',
			minDate:0,
			onShow:function( ct ){
				this.setOptions({
			    	minDate:$('#modal_exception_rules_startdate').val()?$('#modal_exception_rules_startdate').val():false
				})
			},
		});

		$("#modal_exception_rules_starttime").datetimepicker({
			datepicker:false,
			format:'H:i',
			step: 15,
			minTime:0,
		});

		$("#modal_exception_rules_endtime").datetimepicker({
			datepicker:false,
			format:'H:i',
			minTime:0,
		});

		$("#modal_exception_rules_recurring_type").change(function() {
			if ($("#modal_exception_rules_recurring_type").val() == "daily") {
				$("#modal_recurring_yearly_details").hide();
				$("#modal_recurring_monthly_details").hide();
				$("#modal_recurring_weekly_details").hide();
			}
			if ($("#modal_exception_rules_recurring_type").val() == "weekly") {
				$("#modal_recurring_yearly_details").hide();
				$("#modal_recurring_monthly_details").hide();
				$("#modal_recurring_weekly_details").show();
			}
			if ($("#modal_exception_rules_recurring_type").val() == "monthly") {
				$("#modal_recurring_yearly_details").hide();
				$("#modal_recurring_weekly_details").hide();
				$("#modal_recurring_monthly_details").show();
			}
			if ($("#modal_exception_rules_recurring_type").val() == "yearly") {
				$("#modal_recurring_weekly_details").hide();
				$("#modal_recurring_monthly_details").hide();
				$("#modal_recurring_yearly_details").show();
			}
		});

		// Reset after canceling the modal window
		$('#exceptionModal').on('hidden.bs.modal', function (e) {
			$("#modal_recurring_weekly_details").hide();
			$("#modal_recurring_monthly_details").hide();
			$("#modal_recurring_yearly_details").hide();
			$("#new_exception_rule").show();
	 		$("#finish_exception_rule").hide();
		});

		// prepare Options Object 
		var optionsExceptionRules = { 
		    target:     '#response',
		    dataType: 'json', 
		    url: "{{ URL::route('controlcenter_update_venue_exception_rules', array($venue->id)) }}",
		    beforeSerialize: runModifications,
		    beforeSubmit: showRequest,
		    success: showResponse,
		    resetForm: true
		};

		// bind to the form's submit event 
	    $('#modal_exception_rule_form').submit(function() { 
	        // inside event callbacks 'this' is the DOM element so we first 
	        // wrap it in a jQuery object and then invoke ajaxSubmit 
	        $(this).ajaxSubmit(optionsExceptionRules); 
	 
	 		$("#new_exception_rule").hide();
	 		$("#finish_exception_rule").show();
	 		
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	        return false; 
	    }); 

	});

	// All actions before form is serialized
	function runModifications() {
		// If one tab selected in recurrence option, other tab should be nullified in values
		// Monthly
		if ($("#exact_date_monthly").hasClass("active")) {
			$("#modal_exception_rules_monthly_count").val("");
			$("#modal_exception_rules_monthly_day").val("");
		}

		if ($("#particular_day_monthly").hasClass("active")) {
			$("#modal_exception_rules_monthly_month_days").val("");
		}

		// yearly
		if ($("#exact_date_yearly").hasClass("active")) {
			$("#modal_exception_rules_yearly_count").val("");
			$("#modal_exception_rules_yearly_day").val("");
		}

		if ($("#particular_day_yearly").hasClass("active")) {
			$("#modal_exception_rules_yearly_month_days").val("");
		}

		// Weekly selections
		// serialize all weekdays to be compatible with RRULE
		var checkboxes_days = $("[rel=weekday]");
		var vals_days = "";

		for (var i=0, n=checkboxes_days.length;i<n;i++) {
		  if (checkboxes_days[i].checked) 
		  {
		  vals_days += ","+checkboxes_days[i].value;
		  }
		}
		if (vals_days) vals_days = vals_days.substring(1);

		document.getElementById("modal_exception_rules_weekly_days").value = vals_days;

		// Yearly selections
		// serialize all months to be compatible with RRULE
		var checkboxes_months = $("[rel=yearmonth]");
		var vals_months = "";

		for (var i=0, n=checkboxes_months.length;i<n;i++) {
		  if (checkboxes_months[i].checked) 
		  {
		  vals_months += ","+checkboxes_months[i].value;
		  }
		}
		if (vals_months) vals_months = vals_months.substring(1);

		document.getElementById("modal_exception_rules_yearly_months").value = vals_months;
	}

	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 
	 
	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    alert('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	} 
	 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
	 
	    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.message + 
	        '\n\nThe output div should have already been updated with the responseText.'); 

	    $("#response").text(responseText.message);

	    console.log(responseText.message);
	}

</script>

@stop

