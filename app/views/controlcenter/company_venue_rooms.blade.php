@extends('layouts.main')

@section('page_css')
<style>
	.sidebar ul li {
		/*padding-top: 5px;*/
	}
	.sidebar ul li.active {
		background-color: #eee;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="{{ URL::route('controlcenter_company_bookings') }}">{{ $company->company_name }}</a>
			<span class="current-page">{{ $venue->venue_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">

						<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Venue - {{ $venue->venue_name }}</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_venue_bookings', array($venue->id ))}}">Bookings</a></li>
							<li><a href="{{URL::route('controlcenter_venue_calendar')}}">Calendar</a></li>
							<li class="active">Rooms</li>
							<li><a href="#">Financials</a></li>
							<li><a href="{{URL::route('controlcenter_venue_settings', array($venue->id ))}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_venue_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->

				</div><!-- end sidebar  -->

			</div><!-- end sidebar outer  -->
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>Rooms</h2>
				</div>
				<div class="clear mb-30"></div>
				<div class="blog-item padding-20">
					
					<table class="table table-striped">
						<tr>
							<th>Name</th>
							<th>Type</th>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
						@foreach ($rooms as $room)
							<tr>
								<td>{{ $room->room_name }}</td>
								<td>{{ $room->room_type_name }}</td>
								<td><a href="{{URL::route('controlcenter_room_bookings', array($room->id ))}}"><button class="btn btn-primary"><i class="fa fa-dashboard"></i></button></a></td>
								<td><button class="btn btn-primary"><i class="fa fa-calendar"></i></button></td>
								<td><button class="btn btn-primary"><i class="fa fa-play-circle-o"></i></button></td>
								<td><a href="{{URL::route('controlcenter_edit_room', array($room->id ))}}"><button class="btn btn-primary"><i class="fa fa-cog"></i></button></a></td>
							</tr>
						@endforeach
					</table>

					<a class="btn btn-default pull-right" href="{{URL::route('controlcenter_create_room', array($venue->id))}}">Add Room</a>
				</div>
				
					





				<div class="clear"></div>



			</div>



			<div class="clear"></div>

		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')



@stop

