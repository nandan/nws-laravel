@extends('layouts.main')

@section('page_css')
<style>
	.sidebar ul li {
		/*padding-top: 5px;*/
	}
	.sidebar ul li.active {
		background-color: #eee;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="{{ URL::route('controlcenter_company_bookings') }}">{{ $company->company_name }}</a>
			<a href="{{ URL::route('controlcenter_venue_bookings', array($venue->id)) }}">{{ $venue->venue_name }}</a>
			<span class="current-page">{{ $room->room_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">

						<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>{{ $room->room_name }}</h4>
						<ul class="cat-list">
							<li class="active">Bookings</li>
							<li><a href="#">Financials</a></li>
							<li><a href="{{ URL::route('controlcenter_room_settings', array($room->id)) }}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->

				</div><!-- end sidebar  -->

			</div><!-- end sidebar outer  -->
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>Bookings</h2>
				</div>
				<div class="clear mb-30"></div>
				<div class="blog-item padding-20">
					
					<table class="table table-striped">
						<tr>
							<th>ID</th>
							<th>Customer</th>
							<th>Date</th>
							<th>Time</th>
							<th>Location</th>
							<th>Price</th>
							<th>Status</th>
							<th>Details</th>
						</tr>
						<tr>
							<td>123</td>
							<td>Mr. John Doe</td>
							<td>19/01/2014</td>
							<td>9:00 - 14:00 GMT+1</td>
							<td>Berlinerstraße 2, Berlin</td>
							<td>192,50€</td>
							<td>Booked</td>
							<td><button class="btn btn-primary">>>></button></td>
						</tr>
						<tr>
							<td>1243</td>
							<td>Meeting with Hans</td>
							<td>12/01/2014</td>
							<td>9:00 - 15:00 GMT+1</td>
							<td>New york 2, NY</td>
							<td>232,50€</td>
							<td>Canceled</td>
							<td><button class="btn btn-primary">>>></button></td>
						</tr>

					</table>
				</div>
				
					





				<div class="clear"></div>



			</div>



			<div class="clear"></div>

		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')



@stop



