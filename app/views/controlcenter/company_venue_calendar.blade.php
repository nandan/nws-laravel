@extends('layouts.main')

@section('page_css')
<!-- Jquery Full Calendar -->
<link href="{{asset('custom_css/fullcalendar.css')}}" rel='stylesheet' type='text/css'>
<link href="{{asset('custom_css/fullcalendar.print.css')}}" rel='stylesheet' type='text/css'>
<link href="{{asset('custom_css/company_calendar.css')}}" rel='stylesheet' type='text/css'>
<style>
	.sidebar ul li {
		/*padding-top: 5px;*/
	}
	.sidebar ul li.active {
		background-color: #eee;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="index.html">Jens Kippels Company</a>
			<span class="current-page">Big Mama House</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">
					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Berlin Business Center GmbH</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_venue_bookings')}}">Bookings</a></li>
							<li class="active">Calendar</li>
							<li><a href="#">Rooms</a></li>
							<li><a href="#">Financials</a></li>
							<li><a href="{{URL::route('controlcenter_venue_settings')}}">Settings</a></li>
						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->

				</div><!-- end sidebar  -->

			</div><!-- end sidebar outer  -->
			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>Real-Time Calendar</h2>
					<p><div class="btn-group">
						<button type="button" class="btn btn-primary">All Rooms</button>
						@foreach ($venue_rooms as $venue_room)
						<button type="button" class="btn btn-default">{{$venue_room->room_name}}</button>
						@endforeach
						<button type="button" class="btn btn-success" id="examples">Add Example Events from Server</button>
					</div></p>
				</div>
				
				
				<div class="blog-item padding-20">
					
					<div id='calendar'></div>

				</div>

				<div class="clear"></div>



			</div>



			<div class="clear"></div>

		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<!-- Jquery Full Calendar -->
<script type="text/javascript" src="{{asset('custom_js/fullcalendar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('custom_js/company_calendar.js')}}"></script>


@stop



