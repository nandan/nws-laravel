@extends('layouts.main')

@section('page_css')
<style>
	/* Wizard Menu at the top */
	.form-title {
		font-size: 18px;
		font-weight: 700;
		line-height: 1;
		padding: 0px 20px 13px 20px;
	}

	
	/* Wizard items and elements */
	.wizard-item {
		padding: 10px;
	}
	.wizard-element {
		margin-bottom: 10px;
	}
	.wizard-element.address-field {
		height: 34px;
	}
	.wizard-button {
		margin-right: -15px;
	}
	.wizard-button button {
		margin-right: 35px;
		width: 100px;
	}


	/* Sidebar */
	.sidebar ul li {
		/*padding-top: 5px;*/
	}
	.sidebar ul li.active {
		background-color: #eee;
	}
	

	table.time-table tr  td.table-days{
		padding-top: 15px;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="{{ URL::route('controlcenter_company_bookings') }}">{{ $company->company_name }}</a>
			<span class="current-page">{{ $venue->venue_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>{{ $venue->venue_name }}</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_venue_bookings', array($venue->id))}}">Bookings</a></li>
							<li><a href="{{URL::route('controlcenter_venue_calendar')}}">Calendar</a></li>
							<li><a href="{{URL::route('controlcenter_venue_rooms', array($venue->id))}}">Rooms</a></li>
							<li><a href="#">Financials</a></li>
							<li>Settings
								<ul class="nav">
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue', array($venue->id))}}" style="padding:0">General</a>
									</li>
									<li class="active" style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_calendar', array($venue->id))}}" style="padding:0">Calendar</a>
									</li>
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_exception_rules', array($venue->id))}}" style="padding:0">Exception Rules</a>
									</li>
									<li style="height: 30px;">
										<a href="{{URL::route('controlcenter_edit_venue_user_management', array($venue->id))}}" style="padding:0">User Management</a>
									</li>
								</ul>
							</li>
						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->
				</div><!-- end sidebar  -->
			</div><!-- end sidebar outer  -->

			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<span class="form-title">Edit Venue Settings</span>
				</div>
				<div class="clear mb-30"></div>
				<form id="edit_venue_form" method="POST" action="{{URL::route('controlcenter_update_venue', array($venue->id))}}">
					<div id="edit_venue">
						@include('venues.edit_calendar')
						<div class="clear"></div>
						<div class="col-md-12">
							<div class="row white-bg padding-20 wizard-button">
								<button type="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
							</div>
						</div>
					</div>
					<div id="finish_edit_venue">
						<div class="col-md-12">
							<div class="row white-bg padding-20">
								<div class="wizard-item">
									<h2>You have successfully created new venue.</h2>
									<h3 id="response"></h3>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script src="{{asset('custom_js/jquery.form.js')}}"></script>
<script>
	$(function() {
		console.log("off goes the form...");

		$("#finish_edit_venue").hide();
		$("#new_pass").hide();


		// If old password should be changed
		$("button#change_pass").click(function(e) {
			e.preventDefault();
			$("#old_pass").hide();
			$("#new_pass").show();
		});

		// prepare Options Object 
		var options = { 
		    target:     '#response',
		    dataType: 'json', 
		    url: "{{ URL::route('controlcenter_update_venue_calendar', array($venue->id)) }}",
		    beforeSubmit: showRequest,
		    success: showResponse,
		    resetForm: true
		};

		// bind to the form's submit event 
	    $('#edit_venue_form').submit(function() { 
	        // inside event callbacks 'this' is the DOM element so we first 
	        // wrap it in a jQuery object and then invoke ajaxSubmit 
	        $(this).ajaxSubmit(options); 
	 
	 		$("#edit_venue").hide();
	 		$("#finish_edit_venue").show();
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	        return false; 
	    }); 

	});

	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 
	 
	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm; 
	 
	    alert('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	} 
	 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
	 
	    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.message + 
	        '\n\nThe output div should have already been updated with the responseText.'); 

	    $("#response").text(responseText.message);

	    console.log(responseText.message);
	}

</script>

@stop

