<body>
    Dear {{ $customer }}, <br /><br /><br />

	The reservation code is {{ $code }}<br/><br />

    Please visit the following URL to activate the account: <br> {{ $url }}<br /><br />

    Sincerely,<br />
    NextWorkspace.com
</body>