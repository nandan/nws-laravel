@extends('layouts.main')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.datetimepicker.css')}}" />
<style>
	/*** Price Range **/
	.price-range {
	    padding: 18px 12px 12px;
	    overflow: hidden;
	}
	#price_start-val,
	#price_end-val {
	    display: inline-block;
	    border: none;
	    float: left;
	    padding: 0;
	    color: #000;
	    font-size: 13px;
	    font-family: 'Open Sans', sans-serif;
	    max-width: 50px;
	}
	#price_end-val {
	   float: right;
	   text-align: right;
	}

	/*** Evaluations Range **/
	.evaluations-range {
	    padding: 18px 12px 12px;
	    overflow: hidden;
	}
	#evaluations_start-val,
	#evaluations_end-val {
	    display: inline-block;
	    border: none;
	    float: left;
	    padding: 0;
	    color: #000;
	    font-size: 13px;
	    font-family: 'Open Sans', sans-serif;
	    max-width: 50px;
	}
	#evaluations_end-val {
	   float: right;
	   text-align: right;
	}

	/* List JS classes */
	ul.search-list {
		margin: 0px;
		padding: 0px;
		list-style: none;
	}

	ul.search-list li {
		padding: 0;
	}

	/* Test CSS for List JS */
	.pagination li {
		display:inline-block;
		padding:5px;
	}
	.sort {
	  /*padding:8px 30px;*/
	  border-radius: 6px;
	  border:none;
	  display:inline-block;
	  color:#fff;
	  text-decoration: none;
	  background-color: #28a8e0;
	  height:40px;
	}
	.sort:hover {
	  text-decoration: none;
	  background-color:#1b8aba;
	}
	.sort:focus {
	  outline:none;
	}
	.sort:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-bottom: 5px solid transparent;
	  content:"";
	  position: relative;
	  top:-10px;
	  right:-5px;
	}
	.sort.asc:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-top: 5px solid #fff;
	  content:"";
	  position: relative;
	  top:13px;
	  right:-5px;
	}
	.sort.desc:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-bottom: 5px solid #fff;
	  content:"";
	  position: relative;
	  top:-10px;
	  right:-5px;
	}


	/* Review */
	.list-item-evaluation {
		position: absolute;
		top: 20px;
		right: 120px;
	}

	.rating-static{
		float: right;
	}


	.list-style  a.btn {
		margin-top: 98px;
		margin-left: 15px;
		bottom: 15px;
		right: 20px;
		padding: 2px 8px;
		text-indent: 0;
		border-left: 0;
	}

	/* hourly */
	.list-item-price-hourly {
		position: absolute;
		top: 17px;
		right: 20px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
	}
	.list-item-price-hourly a {
		color: #7F64B5;
		font-size: 16px;
		position: absolute;
		top: -4px;
		right: 0px;
		border: 1px solid #7F64B5;
		padding: 2px 4px 2px 4px;
		border-radius: 50%;
	}
	.list-item-price-hourly a:hover {
		background: #7F64B5;
		color: #FFF;
	}
	.list-style .list-item-price-hourly {
		top: 70px;
		left: 25px;
	}
	.list-item-price-hourly span {
		font-size: 10px;
		color: #333;
	}

	/* daily */
	.list-item-price-daily {
		position: absolute;
		top: 17px;
		right: 20px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
	}
	.list-item-price-daily a {
		color: #7F64B5;
		font-size: 16px;
		position: absolute;
		top: -4px;
		right: 0px;
		border: 1px solid #7F64B5;
		padding: 2px 4px 2px 4px;
		border-radius: 50%;
	}
	.list-item-price-daily a:hover {
		background: #7F64B5;
		color: #FFF;
	}
	.list-style .list-item-price-daily {
		top: 95px;
		left: 15px;
	}
	.list-item-price-daily span {
		font-size: 10px;
		color: #333;
	}

	/* monthly */
	.list-item-price-monthly {
		position: absolute;
		top: 17px;
		right: 20px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
	}
	.list-item-price-monthly a {
		color: #7F64B5;
		font-size: 16px;
		position: absolute;
		top: -4px;
		right: 0px;
		border: 1px solid #7F64B5;
		padding: 2px 4px 2px 4px;
		border-radius: 50%;
	}
	.list-item-price-monthly a:hover {
		background: #7F64B5;
		color: #FFF;
	}
	.list-style .list-item-price-monthly {
		top: 120px;
		left: 15px;
	}
	.list-item-price-monthly span {
		font-size: 10px;
		color: #333;
	}

	/* Final Price */
	.list-item-final-price {
		position: absolute;
		top: 90px;
		right: 15px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
		width: 50px;
		text-align: right;
	}

	.list-item-final-price span {
		font-size: 10px;
		color: #333;
		text-align: right;
	}

	/** Price List Table **/
	.price-table {
		position: absolute;
		top: 85px;
		right: 120px;
		width: 150px;
	}

	.price-table th {
		font-size: 10px;
		color: #333;
		text-align: center;
	}

	.price-table td {
		color: #C77A6E;
		font-size: 16px;
		line-height: 1.1;
		text-align: center;
	}	
	
	/** Amenities List **/
	ul.amenities {
		margin: 0px;
		padding: 0px;
		list-style: none;
		
	}
	.list-style ul.amenities {
		position: absolute;
		bottom: 3px;
		right:  110px;
	}
	.amenities li{
		width:28px; 
		height:28px; 
		/*background:url('') #fff; border:2px solid #efefef;*/
		/*border:1px solid #ebebeb;*/
		padding:0px;
		color:#999;
		/*-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	*/
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}
	.amenities li:hover{}

	/** Booking Calendar Board **/
	ul.booking {
		margin: 0px;
		padding: 0px;
		list-style: none;
		position: absolute;
		bottom: 10px;
		left:  35px;
	}
	.booking li{
		width:20px; 
		height:20px; 
		background-color: #ccc;
		border:1px solid #ebebeb;
		padding:5px;
		color:#999;
		-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}

	.booking li.lastblock {
		margin-right: 5px;
	}

	.booking li.freetime{
		background-color: #A2BF8A;
	}

	.booking li.busytime {
		background-color: #DB524B;
	}
</style>
@stop


@section('content')
<!-- <div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Search Result</div>
		<div class="breadcrumb clearfix">
			<span class="current-page">Location: {{ $search_city }}, {{ $search_country }}</span>
		</div>
	</div>
</div> --><!-- end slider_wrapper -->

<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix" style="margin: 20px 0px 0 0px;">
			<div id="search_bar" class="sort-by-wrapper">
				<form id="search_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
				<div class="col-md-3">
					<div class="xs-mb sm-mb">
						<div id="location">
							<input type="text" id="search_location" class="form-control" placeholder="Location" value="{{ $search_city }}, {{ $search_country }}" />
							<input type="hidden" class="form-control" id="locality" name="search_city" value="{{ $search_city }}" />
							<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" value="{{ $search_state }}" />
							<input type="hidden" class="form-control" id="country" name="search_country" value="{{ $search_country }}" />
							<input type="hidden" class="form-control" id="search_lat" name="search_lat" value="{{ $search_lat }}" />
							<input type="hidden" class="form-control" id="search_lng" name="search_lng" value="{{ $search_lng }}" />
							<input type="hidden" class="form-control" id="search_locid" name="search_locid" value="{{ $search_locid }}" />
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_room_type" name="search_room_type" rel="tooltip" data-toggle="tooltip" title="Workspace Type" data-placement="left">
						 	<option value="Office">Office</option>
				        	<option value="Meeting Room">Meeting Room</option>
				        	<option value="Coworking">Coworking</option>
				        	<option value="Others">Others</option>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_period" name="search_period" rel="tooltip" data-toggle="tooltip" title="Period" data-placement="left">
							<option value="hourly">Hrly</option>
							<option value="daily">Daily</option>
							<option value="monthly">Monthly</option>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="xs-mb sm-mb datepicker-wrapper">
						<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy" value="{{ $search_startdate }}" title="Start Date"/>
						<i class="fa fa-calendar"></i>
					</div>
				</div>
				<div class="for_hourly">
					<div class="col-md-1">
						<div class="xs-mb sm-mb">
							<input type="text" rel="time" class="form-control" id="search_starttime" name="search_starttime" placeholder="Time" value="{{ $search_starttime }}" title="Start Time" />
						</div>
					</div>
					<div class="col-md-1">
						<div class="xs-mb sm-mb">
							<input type="text" rel="time" class="form-control" id="search_endtime" name="search_endtime" placeholder="End" value="{{ $search_endtime }}" title="End Time">
						</div>
					</div>
				</div>
				<div class="for_daily none">
					<div class="col-md-2">
						<div class="xs-mb sm-mb datepicker-wrapper">
							<input type="text" rel="date" class="form-control" id="search_enddate" name="search_enddate" placeholder="dd/mm/yy" value="{{ $search_enddate }}" title="End Date">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
				</div>
				<div class="for_monthly none">
					<div class="col-xs-12 col-sm-4 col-md-2">
						<div class="xs-mb sm-mb">
							<select class="form-control mySelectBoxClass" id="search_monthly_duration" name="search_monthly_duration" rel="tooltip" data-toggle="tooltip" title="Duration" data-placement="left">
								<!-- <option selected value="{{ $search_monthly_duration }}">{{ $search_monthly_duration }}</option> -->
								<option value="1">&nbsp;1 Month</option>
								<option value="2">&nbsp;2 Months</option>
								<option value="3">&nbsp;3 Months</option>
								<option value="4">&nbsp;4 Months</option>
								<option value="5">&nbsp;5 Months</option>
								<option value="6">&nbsp;6 Months</option>
								<option value="7">&nbsp;7 Months</option>
								<option value="8">&nbsp;8 Months</option>
								<option value="9">&nbsp;9 Months</option>
								<option value="10">&nbsp;10 Months</option>
								<option value="11">&nbsp;11 Months</option>
								<option value="12">&nbsp;12 Months</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_person" name="search_person" rel="tooltip" data-toggle="tooltip" title="Persons" data-placement="left">
						 	<!-- <option selected value="{{ $search_person }}">{{ $search_person }}</option> -->
						 	<option value="1">&nbsp;1</option>
							<option value="2">&nbsp;2</option>
							<option value="3">&nbsp;3</option>
							<option value="4">&nbsp;4</option>
							<option value="5">&nbsp;&gt; 5</option>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<button type="submit" name="submit" class="btn btn-primary">Search</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<div class="row clearfix pv-30">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">
					<div class="counting-result">
						<span class="count_search"></span> results found. Starting at 
						<div class="counting-price">$<span class="count_price"></span></div>
					</div>
					<div class="clear" style="height: 5px;"></div>
					<div class="another-toggle">
						<!-- <img src="{{asset('custom_js/holder.js')}}/225x225">			 -->
						<div id="maps-canvas" style="height:300px;">Maps</div>
					</div><!-- /another-toggle -->
					<div class="another-toggle">
						<h5 class="active">Price Range</h5>
						<div class="another-toggle-inner">
							<div class="price-range">
								<!-- div to become a slider -->
								<div class="price_slider"></div>
								<input type="text" id="price_start-val"/>
								<input type="text" id="price_end-val"/>
							</div>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->

					<div class="another-toggle">
						<h5 class="active">Evaluations</h5>
						<div class="another-toggle-inner">
							<div class="evaluations-range">
								<div class="evaluations_slider"></div>
								<input type="text" id="evaluations_start-val"/>
								<input type="text" id="evaluations_end-val"/>
							</div>
						</div>
					</div>
					
					<div class="another-toggle">
						<h5 class="active">Room Type</h5>
						<div class="another-toggle-inner">
							<div class="clear mb-10"></div>
							<form action="#" method="post">
								<fieldset>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType1" value="option1" checked>
											All
											<span class="counting">({{ $search_count + 3 +4 + 3}})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType2" value="option2">
											Office
											<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType3" value="option3">
											Meeting Room<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType4" value="option4">
											Coworking<span class="counting">(4)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType5" value="option5">
											Others<span class="counting">(3)</span>
										</label>
									</div>
								</fieldset>
							</form>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->
					
					<div class="another-toggle">
						<h5 class="active">Amenities</h5>
						<div class="another-toggle-inner">
							<div class="clear mb-10"></div>
							<form action="#" method="post">
								<fieldset>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities1" value="option1" checked>
											Any
											<span class="counting">({{ $search_count + 3 + 4 + 3}})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities1" value="option1">
											Public WiFi
											<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities2" value="option2">
											Secure WiFi
											<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities3" value="option3">
											Wired Internet<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities4" value="option4">
											Flatscreen<span class="counting">(2)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities5" value="option5">
											Video Conferencing<span class="counting">(5)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Projector<span class="counting">(6)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Whiteboard<span class="counting">(4)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Flipboard<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Phone Room<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Conference Phone<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Print/Scan/Copy Service<span class="counting">({{ $search_count }})</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Handicap Accessible<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Coffee/Tea<span class="counting">(5)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Filtered Water<span class="counting">(7)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											On-Site Restaurant<span class="counting">(4)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Catering<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Concierge Service<span class="counting">(4)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Notary Service<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Shared Kitchen<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Shower Facility<span class="counting">(5)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Pet Friendly<span class="counting">(3)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Outdoor Space<span class="counting">(2)</span>
										</label>
									</div>
								</fieldset>
							</form>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->
					
				</div>
			</div>
			<div id="search-result-list">
				<div class="col-xs-12 col-sm-8 col-md-9">
					<div class="sort-by-wrapper">
						<div class="sort-by-text mr-15 xs-mb sm-mb">								
							Sort by :
						</div>			
						<div class="sort-option mr-15 xs-mb sm-mb" style="width:150px;">
							<!-- <button class="sort" data-sort="name">
						      Sort by name
						    </button> -->
						<!-- <input type="text" class="search" /> -->
							<select class="form-control mySelectBoxClass">
							  <option selected>Name</option>
							  <option>A to Z</option>
							  <option>Z to A</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb sm-mb" style="width:150px;">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Price</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb sm-mb" style="width:150px;">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Response time</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb sm-mb" style="width:150px;">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Distance to</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
					</div>
					
					<div class="clear mb-30"></div>
					<?php $j = 1; ?>
					<ul class="search-list row items-container list-style">
						@for($i = 0; $i < $search_count ; $i++)
							<li id="{{ $search_results[$i]->room_id }}" class="col-xs-12 col-sm-12 col-md-12">
								<div class="list-item">
									<div class="list-item-image">
										<div class="image-inner">
											<a class="link-image" href="page-details.html">
												<img src="/images/list-items/office-0{{ $j }}.jpg" alt="" />
												<div class="image-overlay">
													<div class="overlay-content">
														<div class="overlay-icon"><i class="icon_link"></i></div>
													</div>
												</div>
											</a>	
										</div>
									</div>
									<div class="list-item-label">
										<div class="left">
											<h4 class="list-item-title"><a href="{{URL::route('show_room_details', array($search_results[$i]->room_id))}}" class="inverse"><span id="list_room_name_{{ $search_results[$i]->room_id }}" class="search-name">{{ $search_results[$i]->room_name }}</span></a></h4>
											<span id="list_room_address_{{ $search_results[$i]->room_id }}" class="list-item-location">{{ $search_results[$i]->street_name }} {{ $search_results[$i]->street_number }}, {{ $search_city }}, {{ $search_results[$i]->postal_code }}, {{ $search_country }} </span>
											<div class="list-item-details">
												<div class="clear mt-10"></div>
												<span rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="Distance from city center" class="fa fa-dot-circle-o"> 1.8 km</span> <span rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="Distance from nearest subway" class="fa fa-truck ml-10"> 3.1 km</span> <span rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="Distance from nearest airport" class="fa fa-plane ml-10"> 4.3 km</span><br />
												<p class="small" style="margin-top:50px;"><br />
													<span class="fa fa-user fa-lg">&nbsp;</span> max. {{ $search_results[$i]->max_capacity }}</p>
												<div class="list-item-evaluation">
													<span rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="24" class="rating-static rating-30"></span>
												</div>
												<div class="list-item-evaluation" style="top:37px;">
													<span rel="tooltip" data-toggle="tooltip" data-placement="bottom" title="Response" class="small" style="color:#BBB4AB;"><span class="fa fa-clock-o"></span> <strong>{{ rand(1,14) }} hrs</strong></span>
												</div>
												<table class="table table-condensed price-table">
													<thead>
														<tr>
															<th>Hour</th>
															<th>Day</th>
															@if ($search_results[$i]->price_retail_monthly != '0') <th>Month</th> @endif
														</tr>
													</thead>
													<tbody>
														<tr>
															<td>${{ $search_results[$i]->price_retail_hourly }}</td>
															<td>${{ $search_results[$i]->price_retail_daily }}</td>
															@if ($search_results[$i]->price_retail_monthly != '0') <td>${{ $search_results[$i]->price_retail_monthly }}</td> @endif
														</tr>
													</tbody>
												</table>
												<ul class="amenities">
													@if (($search_results[$i]->public_wifi == '0') || ($search_results[$i]->secure_wifi == '0')) <li><span id="amenities_wifi" rel="tooltip" data-toggle="tooltip" title="WiFi" class="fa fa-rss"></span></li> @endif
													@if ($search_results[$i]->wired_internet == '0') <li><span id="amenities_wired_internet" rel="tooltip" data-toggle="tooltip" title="Wired Internet" class="fa fa-sitemap"></span></li> @endif
													@if ($search_results[$i]->flatscreen == '0') <li><span id="amenities_flatscreen" rel="tooltip" data-toggle="tooltip" title="Flatscreen" class="fa fa-desktop"></span></li> @endif
													@if ($search_results[$i]->video_conferencing == '0') <li><span id="amenities_video_conferencing" rel="tooltip" data-toggle="tooltip" title="Video Conferencing" class="fa fa-video-camera"></span></li> @endif
													@if (($search_results[$i]->flipboard == '0') || ($search_results[$i]->whiteboard == '0')) <li><span id="amenities_flipboard" rel="tooltip" data-toggle="tooltip" title="Flipboard/Whiteboard" class="fa fa-clipboard"></span></li> @endif
													@if (($search_results[$i]->phone_room == '0') || ($search_results[$i]->conference_phone == '0')) <li><span id="amenities_phone" rel="tooltip" data-toggle="tooltip" title="Phone Room/Conference Phone" class="fa fa-phone-square"></span></li> @endif
													@if ($search_results[$i]->projector == '0') <li><span id="amenities_projector" rel="tooltip" data-toggle="tooltip" title="Projector" class="fa fa-picture-o"></span></li> @endif
													@if ($search_results[$i]->print_scan_copy == '0') <li><span id="amenities_print_copy_fax" rel="tooltip" data-toggle="tooltip" title="Print/Copy/Fax Service" class="fa fa-print"></span></li> @endif
													@if (($search_results[$i]->coffee_tea == '0') || ($search_results[$i]->filtered_water == '0')) <li><span id="amenities_tea_coffee" rel="tooltip" data-toggle="tooltip" title="Tea/Coffee/Filtered Water" class="fa fa-coffee"></span></li> @endif
													@if (($search_results[$i]->on_site_restaurant == '0') || ($search_results[$i]->catering == '0') || ($search_results[$i]->shared_kitchen == '0')) <li><span id="amenities_on_site_resturant" rel="tooltip" data-toggle="tooltip" title="On-Site Resturant/Catering/Shared Kitchen" class="fa fa-cutlery"></span></li> @endif
													@if ($search_results[$i]->handicap_accessible == '0') <li><span id="amenities_handicap_accessible" rel="tooltip" data-toggle="tooltip" title="Handicap Accessible" class="fa fa-wheelchair"></span></li> @endif
												</ul>
												<div class="clear mb-10"></div>
											</div>
										</div>
										<div class="right">
											<div class="list-item-meta">
												<span id="book-now" class="btn btn-danger">Reserve &nbsp;<i class="fa fa-arrow-circle-right"></i></span>
											</div>
											<div class="list-item-final-price">
												${{ $search_results[$i]->total_price }} <br/><span>/ {{ $search_results[$i]->total_duration }}</span>
											</div>
											<a href="{{URL::route('show_room_details', array($search_results[$i]->room_id))}}" class="btn">Details</a>
										</div>
									</div>
								</div>
							</li>
							<?php $j++; ?>
						@endfor
					</ul>
					
					<div class="clear"></div>
					
					<nav class="paging pull-right">
						<!-- <p>Page 01 of 06</p> -->
						<ul class="pagination">
							<!-- <li class="disabled"><a href="#">&laquo;</a></li>
							<li class="active"><a href="#">1</a></li>
							<li><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">6</a></li>
							<li><a href="#">&raquo;</a></li> -->
						</ul>
					</nav>
					
				</div>
			</div>

			<div class="clear"></div>
			
		</div>
	</div>
</div><!-- end content_wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="background-color: #F0F0F0;">
      	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        	<h4 class="modal-title" id="myModalLabel">Book your {{ $search_room_type }}</h4>
      	</div>
		<div id="booking_action">
			<div class="modal-body">
		        <div class="row white-bg padding-20">
		        	<div class="row mb-10">
		        		<div class="ml-15 mb-20" style="font-weight: 700; font-size: 16px;">Booking Period: <em>{{ $search_period }}</em></div>
		        		<div id="search_datetime_error" class="ml-20 mr-20 alert alert-danger none" style="padding: 0 0 0 10px">Unavailable for the given time period. Change it below! </div>
			        	<div class="form-group">
				        	<div class="col-md-4">
								<div id="date" class="datepicker-wrapper" rel="tooltip" data-toggle="tooltip" title="Start Date">
									<input type="text" class="form-control" rel="date" id="modal_booking_startdate" name="modal_booking_startdate" placeholder="dd/dm/yy" value="{{ $search_startdate }}">
									<i class="fa fa-calendar"></i>
								</div>
								<input type="hidden" class="form-control" id="modal_room_id" name="modal_room_id" />
								<input type="hidden" class="form-control" id="modal_customer_id" name="modal_customer_id" />
								<input type="hidden" class="form-control" id="modal_booking_period" name="modal_booking_period" />
							</div>
						</div>
						<div class="booking_hourly">
							<div class="form-group">
								<div class="col-md-2">
									<input type="text" class="form-control" rel="time" id="modal_booking_starttime" name="modal_booking_starttime" placeholder="from" value="{{ $search_starttime }}" rel="tooltip" data-toggle="tooltip" title="Start Time">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<input type="text" class="form-control" rel="time" id="modal_booking_endtime" name="modal_booking_endtime" value="{{ $search_endtime }}" placeholder="until" rel="tooltip" data-toggle="tooltip" title="End Time">
								</div>
							</div>
						</div>
						<div class="booking_daily none">
							<div class="col-md-4">
								<div id="date" class="datepicker-wrapper" rel="tooltip" data-toggle="tooltip" title="End Date">
									<input type="text" class="form-control" rel="date" id="modal_booking_enddate" name="modal_booking_enddate" placeholder="dd/dm/yy" value="{{ $search_enddate }}">
									<i class="fa fa-calendar"></i>
								</div>
							</div>
						</div>
						<div class="booking_monthly none">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<select class="form-control mySelectBoxClass" id="modal_booking_monthly_duration" name="modal_booking_monthly_duration" rel="tooltip" data-toggle="tooltip" title="Duration">
										<option selected value="{{ $search_monthly_duration }}">{{ $search_monthly_duration }} Months</option>
										<option value="1">1 Month</option>
										<option value="2">2 Months</option>
										<option value="3">3 Months</option>
										<option value="4">4 Months</option>
										<option value="5">5 Months</option>
										<option value="6">6 Months</option>
										<option value="7">7 Months</option>
										<option value="8">8 Months</option>
										<option value="9">9 Months</option>
										<option value="10">10 Months</option>
										<option value="11">11 Months</option>
										<option value="12">12 Months</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<select class="form-control mySelectBoxClass" id="modal_booking_person" name="modal_booking_person" rel="tooltip" data-toggle="tooltip" title="Persons">
								 	<option selected value="{{ $search_person }}">{{ $search_person }}</option>
								 	<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">&gt; 5</option>
								</select>
							</div>
						</div>
					</div>
		        	<div class="bb2 mb-20"></div>
		        	<div class="row mb-10">
			        	<div class="summary-header mb-30 ml-15 mr-15">
							<img src="{{asset('images/list-items/small-item-01.jpg')}}" />
							<h3 id="modal_room_name" class="no-mb">Kyoto Kokusai Hotel</h3>
							<span id="modal_room_address">Kyoto, Japan</span>
							<span class="rating-static rating-45"></span>
						</div>
					</div>
					<div class="bb2 mb-20"></div>
					<div class="row mb-10">
						<div class="row">
							<div class="form-group">
								<div class="col-md-3">
									<label for="modal_booking_title" class="pull-right">Title</label>
								</div>
								<div class="col-md-8">
									<input type="text" class="form-control" id="modal_booking_title"  name="modal_booking_title" placeholder="Title">
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
						<div class="row mt-20">
							<div class="form-group">
								<div class="col-md-3">
									<label for="modal_booking_comments" class="pull-right">Comments</label>
								</div>
								<div class="col-md-8">
									<textarea class="form-control" rows="3" id="modal_booking_comments" name="modal_booking_comments" placeholder="Enter your comments and extra wishes here..."></textarea> 
								</div>
								<div class="col-md-1">&nbsp;</div>
							</div>
						</div>
					</div>
				</div>
		        <div id="modal_signin">
		        	<div class="clear mb-20"></div>
			        <div class="row white-bg pv-20">
			        	<div class="mr-15">
							<div class="row">
								<div id="modal_register"><span style="margin-left: 80px;">Sign in to continue or <a href="javascript:toggle_modal_form()">Register</a> to enjoy the benefits of NextWorkspace</span></div>
								<div id="modal_login" style="display:none;"><span style="margin-left: 60px;">Register to enjoy the benefits of NextWorkspace. Or <a href="javascript:toggle_modal_form()">Already have an account?</a></span></div>
							</div>
							<div id="div_modal_login_form">
								<form role="form" id="modal_login_form" name="modal_login_form" action="" method="post">
									<div class="row mt-20">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_email" class="pull-right">Email</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" id="modal_email"  name="modal_email" placeholder="Email">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_password" class="pull-right">Password</label>
											</div>
											<div class="col-md-6">
												<input type="password" class="form-control" id="modal_password" name="modal_password" placeholder="Password">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">&nbsp;</div>
										<div class="col-md-6">
											<div class="checkbox">
													<input type="checkbox" id="modal_remember_me" name="modal_remember_me"> Remember Me </input>
											</div>
										</div>
										<div class="col-md-2">&nbsp;</div>
									</div>
									<div style="margin-left:190px;">
										<button type="submit" class="btn btn-primary">Login</button>
										<div class="clear mb-10"></div>
										<a href="#" class="forgot">Forgot Your Password?</a>
									</div>
								</form>
							</div>
							<div id="div_modal_register_form" style="display:none;">
								<form role="form" id="modal_register_form" name="modal_register_form" action="" method="post">
									<div class="row mt-20">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_first_name" class="pull-right">First Name</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" id="modal_first_name"  name="modal_first_name" placeholder="Email">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_last_name" class="pull-right">Last Name</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" id="modal_last_name" name="modal_last_name" placeholder="Password">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_emailx" class="pull-right">Email</label>
											</div>
											<div class="col-md-6">
												<input type="text" class="form-control" id="modal_emailx"  name="modal_emailx" placeholder="Email">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_passwordx" class="pull-right">Password</label>
											</div>
											<div class="col-md-6">
												<input type="password" class="form-control" id="modal_passwordx" name="modal_passwordx" placeholder="Password">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row mt-10">
										<div class="form-group">
											<div class="col-md-4">
												<label for="modal_password" class="pull-right">Confirm Password</label>
											</div>
											<div class="col-md-6">
												<input type="password" class="form-control" id="modal_confirm_password" name="modal_confirm_password" placeholder="Password">
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">&nbsp;</div>
										<div class="col-md-6">
											<div class="checkbox">
													<input type="checkbox" id="modal_tos" name="modal_tos"> Accept <a href="#">Terms of Service</a> </input>
											</div>
										</div>
										<div class="col-md-2">&nbsp;</div>
									</div>
									<div style="margin-left:190px;">
										<button type="submit" class="btn btn-primary">Register</button>
										<div class="clear mb-10"></div>
									</div>
								</form>
							</div>
						</div>
			        </div>
			    </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				@if (Sentry::check())
					<button id="booking_action_continue" type="button" class="btn btn-primary">Continue</button>
			    @else
					<button id="booking_action_continue" type="button" class="btn btn-primary" disabled="true">Sign-in to continue</button>
				@endif
			</div>
		</div>
		<div id="booking_payment">
			<form role="form" id="modal_booking_form" name="modal_booking_form" action="" method="post">
			<div class="modal-body">
				<div class="row white-bg padding-20">
					<div class="summary-header mb-30">
						<img src="{{asset('images/list-items/small-item-01.jpg')}}" />
						<h3 id="modal_booking_name" class="no-mb">Kyoto Kokusai Hotel</h3>
						<span id="modal_booking_address">Kyoto, Japan</span>
						<span class="rating-static rating-45"></span>
						<input type="hidden" class="form-control" id="booking_room_id" name="booking_room_id" />
					</div>
			        <div class="bb2 mb-20"></div>
			        <table class="table table-bordered table-striped mb-30">
						<tr>
							<td colspan=2><span id="booking_duration_text" class="text-bold">duration</span>: <span id="booking_time_period">period</span></td>
							<input type="hidden" class="form-control" id="booking_startdate" name="booking_startdate" />
							<input type="hidden" class="form-control" id="booking_starttime" name="booking_starttime" />
							<input type="hidden" class="form-control" id="booking_endtime" name="booking_endtime" />
							<input type="hidden" class="form-control" id="booking_enddate" name="booking_enddate" />
							<input type="hidden" class="form-control" id="booking_period" name="booking_period" />
							<input type="hidden" class="form-control" id="booking_duration" name="booking_duration" />
							<input type="hidden" class="form-control" id="booking_monthly_duration" name="booking_monthly_duration" />
							<input type="hidden" class="form-control" id="booking_person" name="booking_person" />
							<input type="hidden" class="form-control" id="booking_title" name="booking_title" />
							<input type="hidden" class="form-control" id="booking_comments" name="booking_comments" />
						</tr>
						<tr>
							<td>
								Price <em>excl. taxes</em>	
								<br/>
								Taxes &amp; Fees
								<div class="clearfix"></div>
							</td>
							<td class="center">
								<span id="price_orig"></span><br/>
								<span id="price_tax"></span><br/>
							</td>
						</tr>
					</table>
					<div class="bb2 mb-20"></div>
									
					<span class="left">Total Fee:</span>
					<span id="price_final" class="right text-success text-bold font24"></span>
					<span id="test"></span>
					<input type="hidden" class="form-control" id="booking_price_orig" name="booking_price_orig" />
					<input type="hidden" class="form-control" id="booking_price_tax" name="booking_price_tax" />
					<input type="hidden" class="form-control" id="booking_price_final" name="booking_price_final" />
					<div class="clear mb-10"></div>
				</div>
				<div class="clear mb-20"></div>
				<div class="row white-bg padding-20">
					<div class="clear"></div>
					<div id="cc_exist">
						<div class="clearfix">
							<div class="col-md-5">
								Your Credit Card
							</div>
							<div class="col-md-7">
								<span id="cc_mnr"></span><button id="change_cc" class="btn btn-sm btn-info ml-15">Change</button>
							</div>
						</div>
					</div>
					<div id="new_cc_form">
						<div class="row mb-10">
							<div class="form-group">
								<div id="booking_payment_error" class="ml-20 mr-20 alert alert-danger none" style="padding: 0 0 0 10px">Something wrong with payment details!</div>
								<div for="creditcard" class="col-md-4 control-label">Supported Credit Cards: </div>
								<div class="col-md-8">
									<div style="float:left; margin-right: 20px;">
										<img src="{{asset('images/credit-card/visa.png')}}" alt="Visa" >
									</div>
									<div style="float:left; margin-right: 20px;">
										<img src="{{asset('images/credit-card/mastercard.png')}}" alt="Master Card" >
									</div>
									<div style="float:left; margin-right: 20px;">
										<img src="{{asset('images/credit-card/cirrus.png')}}" alt="Cirrus" >
									</div>
									<div style="float:left; margin-right: 20px;">
										<img src="{{asset('images/credit-card/amex.png')}}" alt="Amex" >
									</div>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cardnumber" class="col-md-4 control-label">Credit Card Number: <span class="text-danger">*</span></label>
								<div class="col-md-8">
									<input type="text" size="20" class="form-control" id="booking_cardnumber" data-encrypted-name="booking_cardnumber" placeholder="Credit Card Number">
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_expiration_date" class="col-md-4 control-label">Expiration Date: <span class="text-danger">*</span></label>
								<div class="col-md-3 xs-mb">
									<select class="form-control mySelectBoxClass" id="booking_expiration_month" name="booking_expiration_month">
										<option selected>Month</option>
										<option value="01">01 JAN</option>
										<option value="02">02 FEB</option>
										<option value="03">03 MAR</option>
										<option value="04">04 APR</option>
										<option value="05">05 MAY</option>
										<option value="06">06 JUN</option>
										<option value="07">07 JUL</option>
										<option value="08">08 AUG</option>
										<option value="09">09 SEP</option>
										<option value="10">10 OCT</option>
										<option value="11">11 NOV</option>
										<option value="12">12 DEC</option>
									</select>
								</div>
								<div class="col-md-3">
									<select class="form-control mySelectBoxClass" id="booking_expiration_year" name="booking_expiration_year">
										<option selected>Year</option>
										<option value="2013">2013</option>
										<option value="2014">2014</option>
										<option value="2015">2015</option>
										<option value="2016">2016</option>
										<option value="2017">2017</option>
										<option value="2018">2018</option>
										<option value="2019">2019</option>
										<option value="2020">2020</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cvcnumber" class="col-xs-12 col-sm-4 col-md-4 control-label">CVV Number: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" size="4" class="form-control" id="booking_cvcnumber" data-encrypted-name="booking_cvcnumber" placeholder="Card Identification Number">
								</div>
								<div class="col-xs-12 col-sm-2 col-md-3">
									<span class="font12">What's this?</span>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_zipcode" class="col-xs-12 col-sm-4 col-md-4 control-label">Billing ZIP Code: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_zipcode" name="booking_zipcode" placeholder="Billing ZIP Code">
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cardholder_name" class="col-xs-12 col-sm-4 col-md-4 control-label">Cardholder Name: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_cardholder_name" name="booking_cardholder_name" placeholder="Cardholder Number">
								</div>
								<div class="col-xs-12 col-sm-2 col-md-3">
									<span class="font12">(as on the card)</span>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_coupon_code" class="col-xs-12 col-sm-4 col-md-4 control-label">Coupon code: <span class="font12">(optional)</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_coupon_code" name="booking_coupon_code" placeholder="If Your Have">
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" class="form-control" id="booking_cc_changed" name="booking_cc_changed" />
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button id="booking_payment_confirm" type="submit" class="btn btn-primary">Confirm</button>
			</div>
			</form>
		</div>
		<div id="booking_confirm">
			<div class="modal-body">
				<h3 id="response"></h3>
			</div>
			<div class="modal-footer">
				<button id="booking_confirm_finish" type="button" class="btn btn-default" data-dismiss="modal">Finish</button>
			</div>
		</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script src="{{asset('custom_js/jquery.form.js')}}"></script>
<script src="{{asset('custom_js/list.js')}}"></script>
<script src="{{asset('custom_js/list.pagination.min.js')}}"></script>
<script src="{{asset('custom_js/jquery.datetimepicker.js')}}"></script>
<script src="{{asset('custom_js/date.js')}}"></script>
<script src="https://js.braintreegateway.com/v1/braintree.js"></script>
<script>
  var braintree = Braintree.create("MIIBCgKCAQEAtyXehMGN3dHhlxKmifLETNCjdZfCV6/P7B0bPyYoEzwHUbb6g832MmfM18XSVFPDYY9alZkDZDhCLis9VCGucYw0AklEwOSdxtwGdyLNqknPXu6Lotz51Vz9jKngG7LHYbjHMEl5Egws24bGkqi7V3DurVVX2n9nV511/ue5r2Hj0ApJiDv5TtB3syPQDrPyLF2tNSbXWwmzXL8WNTVGGUH7/fgTC3OXeh86UZlCzOK1tBY6RGnZuJpN0Vti2QzSSb1k+RUwsGUzbxT8dPDQ2pIlwvy+8mZV/mwTsc3FcCyAizQllV4SRlKRvvq6ZAbT9NKhwYHrlVxBAm+7jI+LTQIDAQAB");
  braintree.onSubmitEncryptForm('modal_booking_form');
</script>
<script>
	$(function() {
		console.log("off goes the search result...");

		var search_room_type = '{{ $search_room_type }}';
		var search_period = '{{ $search_period }}';
		var search_monthly_duration = '{{ $search_monthly_duration }}';
		var search_person = '{{ $search_person }}';

		// NOT SHOWING ON RESULT PAGE; DEBUG SOON!!!
		$("select#search_room_type").val(search_room_type);
		$("select#search_period").val(search_period);
		$("select#search_monthly_duration").val(search_monthly_duration);
		$("select#search_person").val(search_person);

		if (search_period == "hourly") {
			$("#search_bar .for_hourly").removeClass('none');
			$("#search_bar .for_daily").addClass('none');
			$("#search_bar .for_monthly").addClass('none');
		}
		if (search_period == "daily") {
			$("#search_bar .for_hourly").addClass('none');
			$("#search_bar .for_daily").removeClass('none');
			$("#search_bar .for_monthly").addClass('none');
		}
		if (search_period == "monthly") {
			$("#search_bar .for_hourly").addClass('none');
			$("#search_bar .for_daily").addClass('none');
			$("#search_bar .for_monthly").removeClass('none');
		}

		// List JS activation
		var optionsList = {
			valueNames: ['search-name'],
			listClass: 'search-list',
			page: 5,
			plugins: [ ListPagination({}) ]
		};

		var searchList = new List('search-result-list', optionsList);

		// Price Counter
		jQuery(function(jQuery) {
			jQuery('.count_price').countTo({
				from: 5,
				to: {{ $search_price_min }},
				speed: 1200,
				refreshInterval: 50,
				// onComplete: function(value) {
				// 	console.debug(this);
				// }
			});
			jQuery('.count_search').countTo({
				from: 1,
				to: {{ $search_count }},
				speed: 2000,
				refreshInterval: 50,
				// onComplete: function(value) {
				// 	console.debug(this);
				// }
			});			
		});

		try {
			// Price Range Slider
			for( var i = 10; i <= 1000; i++ ){
				$('#price_start-val').append(
					'<option value="' + i + '">' + i + '</option>'
				);
			}
			// Initialise noUiSlider for Price Range
			$('.price_slider').noUiSlider({
				range: [{{ $search_price_min }}, {{ $search_price_max }}],
				start: [{{ $search_price_min }}, {{ $search_price_max }}],
				handles: 2,
				connect: true,
				step: 1,
				serialization: {
					to: [ $('#price_start-val'),
						$('#price_end-val') ],
					resolution: 1
				}
			});

			// Evaluations Slider
			for( var i = 1; i <= 10; i++ ){
				$('#evaluations_start-val').append(
					'<option value="' + i + '">' + i + '</option>'
				);
			}
			// Initialise noUiSlider for Price Range
			$('.evaluations_slider').noUiSlider({
				range: [1,5],
				start: [1,5],
				handles: 2,
				connect: true,
				step: 1,
				serialization: {
					to: [ $('#evaluations_start-val'),
						$('#evaluations_end-val') ],
					resolution: 1
				}
			});
		} catch(err) {

		}

		// Bootstrap Tooltip.js initialization
		if ($("[rel=tooltip]").length) {
	    	$("[rel=tooltip]").tooltip();
	    }

	    // DateTimePicker
	    // For main search bar
	    $("[rel=date]").datetimepicker({
			timepicker:false,
			format:'d/m/Y',
			minDate:0,
		});

		$("[rel=time]").datetimepicker({
			datepicker:false,
			format:'H:i',
			step: 15,
			// minTime:0,
		});

		// initialize();

		// Office Form
		$("#search_bar #search_period").on('change', function() {
			if ($(this).val() == "hourly") {
				$("#search_bar .for_hourly").removeClass('none');
				$("#search_bar .for_daily").addClass('none');
				$("#search_bar .for_monthly").addClass('none');
			}
			if ($(this).val() == "daily") {
				$("#search_bar .for_hourly").addClass('none');
				$("#search_bar .for_daily").removeClass('none');
				$("#search_bar .for_monthly").addClass('none');
			}
			if ($(this).val() == "monthly") {
				$("#search_bar .for_hourly").addClass('none');
				$("#search_bar .for_daily").addClass('none');
				$("#search_bar .for_monthly").removeClass('none');
			}
		});
	    // 
	    // 

	    // Hide Payment and Confirmation stage of modal
	    $("#booking_payment").hide();
	    $("#booking_confirm").hide();

	    // If already logged in, then don't show login window in Modal
	    @if (Sentry::check())
	    	$("#modal_signin").hide();
	    @endif

	    // Modal window for booking
		$("span#book-now").click(function() {

			var li_id = $(this).closest('li').attr('id');
			var room_name = document.getElementById("list_room_name_"+li_id+"").innerHTML;
			var room_address = document.getElementById("list_room_address_"+li_id+"").innerHTML;

			document.getElementById("modal_room_name").innerHTML = room_name;
			document.getElementById("modal_room_address").innerHTML = room_address;
			document.getElementById("modal_room_id").value = li_id;

			document.getElementById("modal_booking_name").innerHTML = room_name;
			document.getElementById("modal_booking_address").innerHTML = room_address;
			document.getElementById("booking_room_id").value = li_id;

			document.getElementById("modal_booking_period").value = search_period;

			// document.getElementById("modal_booking_startdate").value = document.getElementById("search_startdate").value;
			// document.getElementById("modal_booking_starttime").value = document.getElementById("search_starttime").value;
			// document.getElementById("modal_booking_endtime").value = document.getElementById("search_endtime").value;
			// document.getElementById("modal_booking_enddate").value = document.getElementById("search_enddate").value;
			// document.getElementById("modal_booking_person").value = document.getElementById("search_person").value;

			if (search_period == "daily") {
				$(".booking_hourly").addClass('none');
				$(".booking_daily").removeClass('none');
			} else if (search_period == "monthly") {
				$(".booking_hourly").addClass('none');
				$(".booking_monthly").removeClass('none');
			}

			$('#myModal').modal();
		});

		$('#myModal').on('hidden.bs.modal', function (e) {
			$("#booking_payment").hide();
			$("#booking_confirm").hide();
			$("#booking_action").show();
		});


		// Check the datetime for booking again
		var booking_action = $('button#booking_action_continue');
		var booking_check_url = 'http://localhost:8000/search/booking_modal';

		booking_action.on('click', function(e) {
			e.preventDefault(); 
			console.log('Checking booking datetimes again...');

			var room_id = document.getElementById("modal_room_id").value;
			var booking_startdate = document.getElementById("modal_booking_startdate").value;
			var booking_starttime = document.getElementById("modal_booking_starttime").value;
			var booking_endtime = document.getElementById("modal_booking_endtime").value;
			var booking_enddate = document.getElementById("modal_booking_enddate").value;
			var booking_monthly_duration = document.getElementById("modal_booking_monthly_duration").value;
			var booking_period = search_period;

			// Date JS Manipulation
			if (search_period == 'hourly') {
				var b_date_starttime = Date.parseExact(''+ booking_startdate +' '+ booking_starttime +'', 'd/M/yyyy HH:mm');
				var b_date_endtime = Date.parseExact(''+ booking_startdate +' '+ booking_endtime +'', 'd/M/yyyy HH:mm');

				var booking_time_period = ''+ b_date_starttime.toString('d MMM yyyy HH:mm') +' - '+ b_date_endtime.toString('d MMM yyyy HH:mm') +'';
			} else if (search_period == 'daily') {
				var b_startdate = Date.parseExact(''+ booking_startdate +'', 'd/M/yyyy');
				var b_enddate = Date.parseExact(''+ booking_enddate +'', 'd/M/yyyy');

				var booking_time_period = ''+ b_startdate.toString('d MMM yyyy') +' - '+ b_enddate.toString('d MMM yyyy') +'';
			} else if (search_period == 'monthly') {
				var b_startdate = Date.parseExact(''+ booking_startdate +'', 'd/M/yyyy');
				var b_enddate = Date.parseExact(''+ booking_startdate +'', 'd/M/yyyy');
				b_enddate.addMonths(parseInt(booking_monthly_duration));

				var booking_time_period = ''+ b_startdate.toString('d MMM yyyy') +' - '+ b_enddate.toString('d MMM yyyy') +'';
			};

			$("#booking_time_period").text(booking_time_period);

			$.ajax({
				url: booking_check_url, 
				type: 'POST', 
				dataType: 'json', 
				data: {room_id : room_id, booking_startdate : booking_startdate, booking_starttime : booking_starttime, booking_endtime : booking_endtime, booking_enddate : booking_enddate,
					booking_monthly_duration : booking_monthly_duration, booking_period : booking_period }, 
				beforeSend: function() {

				},
				success: function(data) {
					// console.log(data);

					if (data.status == 'Error') {
						$.growl.error({message: data.message});
						$('#search_datetime_error').removeClass('none');
						$('#search_datetime_error').text(data.message);
					}


					if (data.status == 'OK') {
						$.growl.notice({message: data.message});

						$("#price_orig").text('$'+data.price_orig);
						$("#price_tax").text('$'+data.price_tax);
						$("#price_final").text('$'+data.price_final);
						$("#booking_duration_text").text(data.price_duration);

						$("#booking_duration").val(data.price_duration);
						$("#booking_price_orig").val(data.price_orig);
						$("#booking_price_tax").val(data.price_tax);
						$("#booking_price_final").val(data.price_final);

						$("#test").text(data.test);

						if (data.cc_maskednr != "") {
							$("#new_cc_form").hide();
							$("#cc_exist").show();

							$("#cc_mnr").text('*****'+data.cc_maskednr);
						} else {
							$("#cc_exist").hide();
						}

						$("#booking_action").hide();
						$("#booking_payment").show();

						document.getElementById("booking_startdate").value = document.getElementById("modal_booking_startdate").value;
						document.getElementById("booking_starttime").value = document.getElementById("modal_booking_starttime").value;
						document.getElementById("booking_endtime").value = document.getElementById("modal_booking_endtime").value;
						document.getElementById("booking_enddate").value = document.getElementById("modal_booking_enddate").value;
						document.getElementById("booking_period").value = document.getElementById("modal_booking_period").value;
						document.getElementById("booking_monthly_duration").value = document.getElementById("modal_booking_monthly_duration").value;

						document.getElementById("booking_person").value = document.getElementById("modal_booking_person").value;			
						document.getElementById("booking_title").value = document.getElementById("modal_booking_title").value;
						document.getElementById("booking_comments").value = document.getElementById("modal_booking_comments").value;
					}
				},
				error: function(e) {
					console.log(e);
				}
			});
		});

		$("button#change_cc").click(function(e) {
			e.preventDefault();

			$("#cc_exist").hide();
			$("#new_cc_form").show();

			$("#booking_cc_changed").val('0');
		});

		// $("button#booking_payment_confirm").click(function() {
		// 	$("#booking_payment").hide();
		// 	$("#booking_confirm").show();
		// });

		$("button#booking_confirm_finish").click(function() {
			$("#booking_confirm").hide(function() {
				$("#booking_action").show();
			});
		});

		// Modal Form Login
		var modal_login_form = $('#modal_login_form');
		var modal_login_url = 'http://localhost:8000/auth/modal_login';
		var modal_register_form = $('#modal_register_form');
		var modal_register_url = 'http://localhost:8000/auth/modal_register_login';


		modal_login_form.on('submit', function(e) {
			e.preventDefault(); 
			console.log('Logging in modal...');
			$.ajax({
				url: modal_login_url, 
				type: 'POST', 
				dataType: 'json', 
				data: modal_login_form.serialize(), 
				beforeSend: function() {

				},
				success: function(data) {
					console.log(data);

					if (data.status == 'Error') {
						$.growl.error({message: data.message});
					}


					if (data.status == 'OK') {
						$.growl.notice({message: data.message});
						$('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextOfficeSpace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul></nav></div></div>');

						$("#modal_signin").hide();
						document.getElementById("booking_action_continue").disabled = false;
						document.getElementById("booking_action_continue").innerHTML = "Continue";
					}
				},
				error: function(e) {
					console.log(e);
				}
			});
		});


		// Modal Form Register

		modal_register_form.on('submit', function(e) {
			e.preventDefault(); 
			console.log('Registering new account...');
			$.ajax({
				url: modal_register_url, 
				type: 'POST', 
				dataType: 'json', 
				data: modal_register_form.serialize(), 
				beforeSend: function() {

				},
				success: function(data) {
					console.log(data);

					if (data.status == 'Error') {
						$.growl.error({message: data.message});
					}


					if (data.status == 'OK') {
						$.growl.notice({message: data.message});
						// modal_register_form.reset();
						// toggle_modal_form();
						$('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextOfficeSpace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul></nav></div></div>');
						
						$("#modal_signin").hide();
						document.getElementById("booking_action_continue").disabled = false;
						document.getElementById("booking_action_continue").innerHTML = "Continue";
					}
				},
				error: function(e) {
					console.log(e);
				}
			});
		});

		// prepare Options Object 
		var optionsBooking = { 
		    target:   '#response',
		    dataType: 'json', 
		    url: "{{ URL::route('create_booking') }}",
		    beforeSubmit: showRequest,
		    success: showResponse,
		    resetForm: true
		};

		// bind to the form's submit event 
	    $('#modal_booking_form').submit(function() {
	    	console.log("submitting...");
	        // inside event callbacks 'this' is the DOM element so we first 
	        // wrap it in a jQuery object and then invoke ajaxSubmit 
	         
	        $('#booking_payment_error').addClass('none');

	        $(this).ajaxSubmit(optionsBooking);
	 
	        // !!! Important !!! 
	        // always return false to prevent standard browser submit and page navigation 
	        return false; 
	    });

	});

	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 
	 
	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    alert('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	} 
	 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
	 
	    if (responseText.status === "Error") {
	    	$.growl.error({message: responseText.message});

	    	$('#booking_payment_error').removeClass('none');
			$('#booking_payment_error').text(responseText.message);
	    }

	    if (responseText.status === "Success") {
	    	$("#booking_payment").hide();
			$("#booking_confirm").show();

		    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.message + 
		        '\n'); 

		    $("#response").html(responseText.message);
		}
	}

	// Hide and Show div Form Modal

	function toggle_modal_form(){
		$('#div_modal_login_form').toggle();
		$('#div_modal_register_form').toggle();
		$('#modal_register').toggle();
		$('#modal_login').toggle();
	}


	// Google Maps Places API Autocompletion
	var autocomplete;
	var componentForm = {
	  locality: 'long_name',
	  administrative_area_level_1: 'short_name',
	  country: 'long_name',
	};

	// Google Maps Autocomplete
	function initialize() {
		// Create an array of styles.
		var styles = [
			{
				featureType: 'road.highway',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 72 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'water',
				elementType: 'all',
				stylers: [
					{ hue: '#30a5dc' },
					{ saturation: 47 },
					{ lightness: -31 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'road',
				elementType: 'all',
				stylers: [
					{ hue: '#cccccc' },
					{ saturation: -100 },
					{ lightness: 44 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'landscape',
				elementType: 'all',
				stylers: [
					{ hue: '#ffffff' },
					{ saturation: -100 },
					{ lightness: 100 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'poi.park',
				elementType: 'all',
				stylers: [
					{ hue: '#d2df9f' },
					{ saturation: 12 },
					{ lightness: -4 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'road.arterial',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 56 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'administrative.locality',
				elementType: 'all',
				stylers: [
					{ hue: '#000000' },
					{ saturation: 0 },
					{ lightness: 0 },
					{ visibility: 'on' }
				]
			}
		];


		// Define your locations: HTML content for the info window, latitude, longitude
	    var locations = [
	    	@foreach ($search_results as $result)
	    		['<h6>{{ $result->venue_name }}</h6>', {{ $result->lat }}, {{ $result->lng }}],
	    	@endforeach
	    ];
	    
	    // Setup the different icons and shadows
	    var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
	    
	    var icons = [
	      iconURLPrefix + 'red-dot.png',
	      iconURLPrefix + 'green-dot.png',
	      iconURLPrefix + 'blue-dot.png',
	      iconURLPrefix + 'orange-dot.png',
	      iconURLPrefix + 'purple-dot.png',
	      iconURLPrefix + 'pink-dot.png',      
	      iconURLPrefix + 'yellow-dot.png'
	    ]
	    var icons_length = icons.length;
	    
	    
	    var shadow = {
	      anchor: new google.maps.Point(15,33),
	      url: iconURLPrefix + 'msmarker.shadow.png'
	    };

	    var map = new google.maps.Map(document.getElementById('maps-canvas'), {
	      zoom: 10,
	      center: new google.maps.LatLng(52.520007, 13.404954),
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
	      mapTypeControl: false,
	      streetViewControl: false,
	      panControl: false,
	      zoomControlOptions: {
	         position: google.maps.ControlPosition.LEFT_BOTTOM
	      }
	    });

	    var infowindow = new google.maps.InfoWindow({
	      maxWidth: 120
	    });

	    var marker;
	    var markers = new Array();

	    var iconCounter = 0;
    
	    // Add the markers and infowindows to the map
	    for (var i = 0; i < locations.length; i++) {  
	      marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map,
	        icon : icons[iconCounter],
	        shadow: shadow
	      });

	      markers.push(marker);

	      google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	          infowindow.setContent(locations[i][0]);
	          infowindow.open(map, marker);
	        }
	      })(marker, i));
	      
	      iconCounter++;
	      // We only have a limited number of possible icon colors, so we may have to restart the counter
	      if(iconCounter >= icons_length){
	      	iconCounter = 0;
	      }
	    }

	    function AutoCenter() {
	      //  Create a new viewpoint bound
	      var bounds = new google.maps.LatLngBounds();
	      //  Go through each...
	      $.each(markers, function (index, marker) {
	        bounds.extend(marker.position);
	      });
	      //  Fit these bounds to the map
	      map.fitBounds(bounds);
	    }
	    AutoCenter();

		// var myLatlng = new google.maps.LatLng({{ $search_lat }}, {{ $search_lng }});

		// Create a new StyledMapType object, passing it the array of styles,
		// as well as the name to be displayed on the map type control.
		// var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});


		// Create a map object, and include the MapTypeId to add
		// to the map type control.
		// var mapOptions = {
		// 	zoom: 15,
		// 	center: myLatlng,
		// 	mapTypeControlOptions: {
		// 	  mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
		// 	}
		// };

		// var map = new google.maps.Map(document.getElementById('maps-canvas'),
		// mapOptions);

		// var marker = new google.maps.Marker({
		//   position: myLatlng,
		//   map: map,
		//   title: 'Hello World!'
		// });


		//Associate the styled map with the MapTypeId and set it to display.
		// map.mapTypes.set('map_style', styledMap);
		// map.setMapTypeId('map_style');
			


		/** Autocomplete for search bar **/
		var options = {
			types: ['(cities)']
		}

		var input = (document.getElementById('search_location')); 
		// Create the autocomplete object, restricting the search
		// to geographical location types.
		autocomplete = new google.maps.places.Autocomplete(input, options);

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
		    fillInAddress();
		});

	}	
	
	// The START and END in square brackets define a snippet for our documentation:
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = autocomplete.getPlace();

		for (var component in componentForm) {
		document.getElementById(component).value = '';
		}

		// Get each component of the address from the place details
		// and fill the corresponding field on the form.
		for (var i = 0; i < place.address_components.length; i++) {
		    var addressType = place.address_components[i].types[0];
		    if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				document.getElementById(addressType).value = val;
		    }
		}

		console.log(place.id);

		document.getElementById("search_locid").value = place.id;

		// Get Latitutde and Longitude of the place
	    var lat = place.geometry.location.lat();
		var lng = place.geometry.location.lng();
		
		document.getElementById("search_lat").value = lat;
		document.getElementById("search_lng").value = lng;
		
		console.log(lat);
		console.log(lng);
	}

	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(function(position) {
	      		var geolocation = new google.maps.LatLng(
	          	position.coords.latitude, position.coords.longitude);
	      		autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
	          	geolocation));
	    	});
	  	}
	}

	google.maps.event.addDomListener(window, 'load', initialize);

</script>

@stop

