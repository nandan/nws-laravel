@extends('layouts.main')

@section('page_css')
<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.datetimepicker.css')}}" />
<style>
	/*** Price Range **/
	.price-range {
	    padding: 18px 12px 12px;
	    overflow: hidden;
	}
	#price_start-val,
	#price_end-val {
	    display: inline-block;
	    border: none;
	    float: left;
	    padding: 0;
	    color: #000;
	    font-size: 13px;
	    font-family: 'Open Sans', sans-serif;
	    max-width: 50px;
	}
	#price_end-val {
	   float: right;
	   text-align: right;
	}

	/*** Evaluations Range **/
	.evaluations-range {
	    padding: 18px 12px 12px;
	    overflow: hidden;
	}
	#evaluations_start-val,
	#evaluations_end-val {
	    display: inline-block;
	    border: none;
	    float: left;
	    padding: 0;
	    color: #000;
	    font-size: 13px;
	    font-family: 'Open Sans', sans-serif;
	    max-width: 50px;
	}
	#evaluations_end-val {
	   float: right;
	   text-align: right;
	}

	/* List JS classes */
	ul.search-list {
		margin: 0px;
		padding: 0px;
		list-style: none;
	}

	ul.search-list li {
		padding: 0;
	}

	/* Test CSS for List JS */
	.pagination li {
		display:inline-block;
		padding:5px;
	}
	.sort {
	  /*padding:8px 30px;*/
	  border-radius: 6px;
	  border:none;
	  display:inline-block;
	  color:#fff;
	  text-decoration: none;
	  background-color: #28a8e0;
	  height:40px;
	}
	.sort:hover {
	  text-decoration: none;
	  background-color:#1b8aba;
	}
	.sort:focus {
	  outline:none;
	}
	.sort:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-bottom: 5px solid transparent;
	  content:"";
	  position: relative;
	  top:-10px;
	  right:-5px;
	}
	.sort.asc:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-top: 5px solid #fff;
	  content:"";
	  position: relative;
	  top:13px;
	  right:-5px;
	}
	.sort.desc:after {
	  width: 0;
	  height: 0;
	  border-left: 5px solid transparent;
	  border-right: 5px solid transparent;
	  border-bottom: 5px solid #fff;
	  content:"";
	  position: relative;
	  top:-10px;
	  right:-5px;
	}


	/* Review */
	.list-item-evaluation {
		position: absolute;
		top: 20px;
		right: 120px;
	}

	.rating-static{
		float: right;
	}


	.list-style  a.btn {
		margin-top: 98px;
		margin-left: 15px;
		bottom: 15px;
		right: 20px;
		padding: 2px 8px;
		text-indent: 0;
		border-left: 0;
	}

	/* hourly */
	.list-item-price-hourly {
		position: absolute;
		top: 17px;
		right: 20px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
	}
	.list-item-price-hourly a {
		color: #7F64B5;
		font-size: 16px;
		position: absolute;
		top: -4px;
		right: 0px;
		border: 1px solid #7F64B5;
		padding: 2px 4px 2px 4px;
		border-radius: 50%;
	}
	.list-item-price-hourly a:hover {
		background: #7F64B5;
		color: #FFF;
	}
	.list-style .list-item-price-hourly {
		top: 80px;
		left: 15px;
	}
	.list-item-price-hourly span {
		font-size: 10px;
		color: #333;
	}

	/* daily */
	.list-item-price-daily {
		position: absolute;
		top: 17px;
		right: 20px;
		color: #C77A6E;
		font-size: 18px;
		line-height: 1.1;
	}
	.list-item-price-daily a {
		color: #7F64B5;
		font-size: 16px;
		position: absolute;
		top: -4px;
		right: 0px;
		border: 1px solid #7F64B5;
		padding: 2px 4px 2px 4px;
		border-radius: 50%;
	}
	.list-item-price-daily a:hover {
		background: #7F64B5;
		color: #FFF;
	}
	.list-style .list-item-price-daily {
		top: 110px;
		left: 15px;
	}
	.list-item-price-daily span {
		font-size: 10px;
		color: #333;
	}
	
	/** Amenities List **/
	ul.amenities {
		margin: 0px;
		padding: 0px;
		list-style: none;
		
	}
	.list-style ul.amenities {
		position: absolute;
		bottom: 48px;
		right:  110px;
	}
	.amenities li{
		width:28px; 
		height:28px; 
		/*background:url('') #fff; border:2px solid #efefef;*/
		/*border:1px solid #ebebeb;*/
		padding:0px;
		color:#999;
		/*-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	*/
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}
	.amenities li:hover{}

	/** Booking Bard **/
	ul.booking {
		margin: 0px;
		padding: 0px;
		list-style: none;
		position: absolute;
		bottom: 10px;
		left:  35px;
	}
	.booking li{
		width:20px; 
		height:20px; 
		background-color: #ccc;
		border:1px solid #ebebeb;
		padding:5px;
		color:#999;
		-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}

	.booking li.lastblock {
		margin-right: 5px;
	}

	.booking li.freetime{
		background-color: #A2BF8A;
	}

	.booking li.busytime {
		background-color: #DB524B;
	}
</style>
@stop


@section('content')
<!-- <div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Search Result</div>
		<div class="breadcrumb clearfix">
			<span class="current-page">Location: {{ $search_city }}, {{ $search_country }}</span>
		</div>
	</div>
</div> --><!-- end slider_wrapper -->

<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix" style="margin: 20px 0px 0 0px;">
			<div id="search_bar" class="sort-by-wrapper">
				<form id="search_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
				<div class="col-md-3">
					<div class="xs-mb sm-mb">
						<div id="location">
							<input type="text" id="search_location" class="form-control" placeholder="Location" value="{{ $search_city }}, {{ $search_country }}" />
							<input type="hidden" class="form-control" id="locality" name="search_city" value="{{ $search_city }}" />
							<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" value="{{ $search_state }}" />
							<input type="hidden" class="form-control" id="country" name="search_country" value="{{ $search_country }}" />
							<input type="hidden" class="form-control" id="search_lat" name="search_lat" value="{{ $search_lat }}" />
							<input type="hidden" class="form-control" id="search_lng" name="search_lng" value="{{ $search_lng }}" />
							<input type="hidden" class="form-control" id="search_locid" name="search_locid" value="{{ $search_locid }}" />
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_room_type" name="search_room_type" rel="tooltip" data-toggle="tooltip" title="Workspace Type" data-placement="left">
						 	<option value="Office">Office</option>
				        	<option value="Meeting Room">Meeting Room</option>
				        	<option value="Coworking">Coworking</option>
				        	<option value="Others">Others</option>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_period" name="search_period" rel="tooltip" data-toggle="tooltip" title="Period" data-placement="left">
							<option value="hourly">Hrly</option>
							<option value="daily">Daily</option>
							<option value="monthly">Monthly</option>
						</select>
					</div>
				</div>
				<div class="col-md-2">
					<div class="xs-mb sm-mb datepicker-wrapper">
						<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy" value="{{ $search_startdate }}" title="Start Date"/>
						<i class="fa fa-calendar"></i>
					</div>
				</div>
				<div class="for_hourly">
					<div class="col-md-1">
						<div class="xs-mb sm-mb">
							<input type="text" rel="time" class="form-control" id="search_starttime" name="search_starttime" placeholder="Time" value="{{ $search_starttime }}" title="Start Time" />
						</div>
					</div>
					<div class="col-md-1">
						<div class="xs-mb sm-mb">
							<input type="text" rel="time" class="form-control" id="search_endtime" name="search_endtime" placeholder="End" value="{{ $search_endtime }}" title="End Time">
						</div>
					</div>
				</div>
				<div class="for_daily none">
					<div class="col-md-2">
						<div class="xs-mb sm-mb datepicker-wrapper">
							<input type="text" rel="date" class="form-control" id="search_enddate" name="search_enddate" placeholder="dd/mm/yy" value="{{ $search_enddate }}" title="End Date">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
				</div>
				<div class="for_monthly none">
					<div class="col-xs-12 col-sm-4 col-md-2">
						<div class="xs-mb sm-mb">
							<select class="form-control mySelectBoxClass" id="search_monthly_duration" name="search_monthly_duration" rel="tooltip" data-toggle="tooltip" title="Duration" data-placement="left">
								<!-- <option selected value="{{ $search_monthly_duration }}">{{ $search_monthly_duration }}</option> -->
								<option value="1">1 Month</option>
								<option value="2">2 Months</option>
								<option value="3">3 Months</option>
								<option value="4">4 Months</option>
								<option value="5">5 Months</option>
								<option value="6">6 Months</option>
								<option value="7">7 Months</option>
								<option value="8">8 Months</option>
								<option value="9">9 Months</option>
								<option value="10">10 Months</option>
								<option value="11">11 Months</option>
								<option value="12">12 Months</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<select class="form-control mySelectBoxClass" id="search_person" name="search_person" rel="tooltip" data-toggle="tooltip" title="Persons" data-placement="left">
						 	<!-- <option selected value="{{ $search_person }}">{{ $search_person }}</option> -->
						 	<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">&gt; 5</option>
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="xs-mb sm-mb">
						<button type="submit" name="submit" class="btn btn-primary">Search</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<div class="row clearfix pv-30">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">
					<div class="counting-result">
						0 results found.
					</div>
					<div class="clear" style="height: 5px;"></div>
					<div class="another-toggle">
						<!-- <img src="{{asset('custom_js/holder.js')}}/225x225">			 -->
						<div id="maps-canvas" style="height:300px;">Maps</div>
					</div><!-- /another-toggle -->
					<div class="another-toggle">
						<h5>Price Range</h5>
						<div class="another-toggle-inner">
							<div class="price-range">
								<!-- div to become a slider -->
								<div class="price_slider"></div>
								<input type="text" id="price_start-val"/>
								<input type="text" id="price_end-val"/>
							</div>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->

					<div class="another-toggle">
						<h5>Evaluations</h5>
						<div class="another-toggle-inner">
							<div class="evaluations-range">
								<!-- div to become a slider -->
								<div class="evaluations_slider"></div>
								<input type="text" id="evaluations_start-val"/>
								<input type="text" id="evaluations_end-val"/>
							</div>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->
					
					<div class="another-toggle">
						<h5>Room Type</h5>
						<div class="another-toggle-inner">
							<div class="clear mb-10"></div>
							<form action="#" method="post">
								<fieldset>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType1" value="option1" checked>
											All
											<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType2" value="option2">
											Office
											<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType3" value="option3">
											Meeting Room<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType4" value="option4">
											Coworking<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="accommodationType" id="accommodationType5" value="option5">
											Others<span class="counting">(xx)</span>
										</label>
									</div>
								</fieldset>
							</form>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->
					
					<div class="another-toggle">
						<h5>Amenities</h5>
						<div class="another-toggle-inner">
							<div class="clear mb-10"></div>
							<form action="#" method="post">
								<fieldset>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities1" value="option1" checked>
											Any
											<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities1" value="option1">
											Public WiFi
											<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities2" value="option2">
											Secure WiFi
											<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities3" value="option3">
											Wired Internet<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities4" value="option4">
											Flatscreen<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities5" value="option5">
											Video Conferencing<span class="counting">(xxx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Projector<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Whiteboard<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Flipboard<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Phone Room<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Conference Phone<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Print/Scan/Copy Service<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Handicap Accessible<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Coffee/Tea<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Filtered Water<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											On-Site Restaurant<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Catering<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Concierge Service<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Notary Service<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Shared Kitchen<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Shower Facility<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Pet Friendly<span class="counting">(xx)</span>
										</label>
									</div>
									<div class="styled-checkbox">
										<label>
											<input type="checkbox" name="amenities" id="amenities6" value="option6">
											Outdoor Space<span class="counting">(xx)</span>
										</label>
									</div>
								</fieldset>
							</form>
						</div><!-- /another-toggle-inner -->
					</div><!-- /another-toggle -->
					
				</div>
			</div>
			<div id="search-result-list">
				<div class="col-xs-12 col-sm-8 col-md-9">
					<div class="sort-by-wrapper">
						<div class="sort-by-text mr-15 xs-mb sm-mb">								
							Sort by :
						</div>			
						<div class="sort-option mr-15 xs-mb sm-mb">
							<!-- <button class="sort" data-sort="name">
						      Sort by name
						    </button> -->
						<!-- <input type="text" class="search" /> -->
							<select class="form-control mySelectBoxClass ">
							  <option selected>Name</option>
							  <option>A to Z</option>
							  <option>Z to A</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb sm-mb">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Price</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb sm-mb">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Evaluations</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
						<div class="sort-option mr-15 xs-mb">
							<select class="form-control mySelectBoxClass ">
							  <option selected>Distance to</option>
							  <option>Ascending</option>
							  <option>Descending</option>
							</select>
						</div>
					</div>
					
					<div class="clear mb-30"></div>
					
					<div class="white-bg padding-20">{{ $search_message }}</div>
					
					<div class="clear"></div>
					
				</div>
			</div>

			<div class="clear"></div>
			
		</div>
	</div>
</div><!-- end content_wrapper -->


<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script src="{{asset('custom_js/jquery.form.js')}}"></script>
<script src="{{asset('custom_js/jquery.datetimepicker.js')}}"></script>
<script>
	$(function() {
		console.log("off goes the search result...");

		var search_room_type = '{{ $search_room_type }}';
		var search_period = '{{ $search_period }}';
		var search_monthly_duration = '{{ $search_monthly_duration }}';
		var search_person = '{{ $search_person }}';

		// NOT SHOWING ON RESULT PAGE; DEBUG SOON!!!
		$("select#search_room_type").val(search_room_type);
		$("select#search_period").val(search_period);
		$("select#search_monthly_duration").val(search_monthly_duration);
		$("select#search_person").val(search_person);

		if (search_period == "hourly") {
			$("#search_bar .for_hourly").removeClass('none');
			$("#search_bar .for_daily").addClass('none');
			$("#search_bar .for_monthly").addClass('none');
		}
		if (search_period == "daily") {
			$("#search_bar .for_hourly").addClass('none');
			$("#search_bar .for_daily").removeClass('none');
			$("#search_bar .for_monthly").addClass('none');
		}
		if (search_period == "monthly") {
			$("#search_bar .for_hourly").addClass('none');
			$("#search_bar .for_daily").addClass('none');
			$("#search_bar .for_monthly").removeClass('none');
		}


	    // Bootstrap Tooltip.js initialization
		if ($("[rel=tooltip]").length) {
	    	$("[rel=tooltip]").tooltip();
	    }

	    // DateTimePicker
	    // For main search bar
	    $("[rel=date]").datetimepicker({
			timepicker:false,
			format:'d/m/Y',
			minDate:0,
		});

		$("[rel=time]").datetimepicker({
			datepicker:false,
			format:'H:i',
			step: 15,
			// minTime:0,
		});

		// Search Form
		$("#search_bar #search_period").on('change', function() {
			if ($(this).val() == "hourly") {
				$("#search_bar .for_hourly").removeClass('none');
				$("#search_bar .for_daily").addClass('none');
				$("#search_bar .for_monthly").addClass('none');
			}
			if ($(this).val() == "daily") {
				$("#search_bar .for_hourly").addClass('none');
				$("#search_bar .for_daily").removeClass('none');
				$("#search_bar .for_monthly").addClass('none');
			}
			if ($(this).val() == "monthly") {
				$("#search_bar .for_hourly").addClass('none');
				$("#search_bar .for_daily").addClass('none');
				$("#search_bar .for_monthly").removeClass('none');
			}
		});


	});


	// Google Maps Places API Autocompletion
	var autocomplete;
	var componentForm = {
	  locality: 'long_name',
	  administrative_area_level_1: 'short_name',
	  country: 'long_name',
	};

	// Google Maps Autocomplete
	function initialize() {
		// Create an array of styles.
		var styles = [
			{
				featureType: 'road.highway',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 72 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'water',
				elementType: 'all',
				stylers: [
					{ hue: '#30a5dc' },
					{ saturation: 47 },
					{ lightness: -31 },
					{ visibility: 'simplified' }
				]
			},{
				featureType: 'road',
				elementType: 'all',
				stylers: [
					{ hue: '#cccccc' },
					{ saturation: -100 },
					{ lightness: 44 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'landscape',
				elementType: 'all',
				stylers: [
					{ hue: '#ffffff' },
					{ saturation: -100 },
					{ lightness: 100 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'poi.park',
				elementType: 'all',
				stylers: [
					{ hue: '#d2df9f' },
					{ saturation: 12 },
					{ lightness: -4 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'road.arterial',
				elementType: 'all',
				stylers: [
					{ hue: '#e5e5e5' },
					{ saturation: -100 },
					{ lightness: 56 },
					{ visibility: 'on' }
				]
			},{
				featureType: 'administrative.locality',
				elementType: 'all',
				stylers: [
					{ hue: '#000000' },
					{ saturation: 0 },
					{ lightness: 0 },
					{ visibility: 'on' }
				]
			}
		];


		// Define your locations: HTML content for the info window, latitude, longitude
	    // var locations = [
	    //   ['<h6>Dussmann Office Europa Center</h6>', 52.504658, 13.336998],
	    //   ['<h6>Dussmann Office Friedrichstrasse</h6>', 52.518299, 13.388490],
	    //   ['<h6>Excellent Business Center Berlin Hbf</h6>', 52.525214, 13.368219],
	    //   ['<h6>Excellent Business Center Unter den Linden</h6>', 52.517132, 13.389215],
	    //   ['<h6>Excellent Business Center Friedrichstrasse</h6>', 52.517502, 13.388816]
	    // ];
	    
	    // Setup the different icons and shadows
	    var iconURLPrefix = 'http://maps.google.com/mapfiles/ms/icons/';
	    
	    var icons = [
	      iconURLPrefix + 'red-dot.png',
	      iconURLPrefix + 'green-dot.png',
	      iconURLPrefix + 'blue-dot.png',
	      iconURLPrefix + 'orange-dot.png',
	      iconURLPrefix + 'purple-dot.png',
	      iconURLPrefix + 'pink-dot.png',      
	      iconURLPrefix + 'yellow-dot.png'
	    ]
	    var icons_length = icons.length;
	    
	    
	    var shadow = {
	      anchor: new google.maps.Point(15,33),
	      url: iconURLPrefix + 'msmarker.shadow.png'
	    };

	    var map = new google.maps.Map(document.getElementById('maps-canvas'), {
	      zoom: 10,
	      center: new google.maps.LatLng(52.520007, 13.404954),
	      mapTypeId: google.maps.MapTypeId.ROADMAP,
	      mapTypeControl: false,
	      streetViewControl: false,
	      panControl: false,
	      zoomControlOptions: {
	         position: google.maps.ControlPosition.LEFT_BOTTOM
	      }
	    });

	    var infowindow = new google.maps.InfoWindow({
	      maxWidth: 120
	    });

	    var marker;
	    var markers = new Array();

	    var iconCounter = 0;
    
	    // Add the markers and infowindows to the map
	    // for (var i = 0; i < locations.length; i++) {  
	    //   marker = new google.maps.Marker({
	    //     position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	    //     map: map,
	    //     icon : icons[iconCounter],
	    //     shadow: shadow
	    //   });

	    //   markers.push(marker);

	    //   google.maps.event.addListener(marker, 'click', (function(marker, i) {
	    //     return function() {
	    //       infowindow.setContent(locations[i][0]);
	    //       infowindow.open(map, marker);
	    //     }
	    //   })(marker, i));
	      
	    //   iconCounter++;
	    //   // We only have a limited number of possible icon colors, so we may have to restart the counter
	    //   if(iconCounter >= icons_length){
	    //   	iconCounter = 0;
	    //   }
	    // }

	    // function AutoCenter() {
	    //   //  Create a new viewpoint bound
	    //   var bounds = new google.maps.LatLngBounds();
	    //   //  Go through each...
	    //   $.each(markers, function (index, marker) {
	    //     bounds.extend(marker.position);
	    //   });
	    //   //  Fit these bounds to the map
	    //   map.fitBounds(bounds);
	    // }
	    // AutoCenter();

		/** Autocomplete for search bar **/
		var options = {
			types: ['(cities)']
		}

		var input = (document.getElementById('search_location')); 
		// Create the autocomplete object, restricting the search
		// to geographical location types.
		autocomplete = new google.maps.places.Autocomplete(input, options);

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
		    fillInAddress();
		});

	}	
	
	// The START and END in square brackets define a snippet for our documentation:
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = autocomplete.getPlace();

		for (var component in componentForm) {
		document.getElementById(component).value = '';
		}

		// Get each component of the address from the place details
		// and fill the corresponding field on the form.
		for (var i = 0; i < place.address_components.length; i++) {
		    var addressType = place.address_components[i].types[0];
		    if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				document.getElementById(addressType).value = val;
		    }
		}

		console.log(place.id);

		document.getElementById("search_locid").value = place.id;

		// Get Latitutde and Longitude of the place
	    var lat = place.geometry.location.lat();
		var lng = place.geometry.location.lng();
		
		document.getElementById("search_lat").value = lat;
		document.getElementById("search_lng").value = lng;
		
		console.log(lat);
		console.log(lng);
	}

	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(function(position) {
	      		var geolocation = new google.maps.LatLng(
	          	position.coords.latitude, position.coords.longitude);
	      		autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
	          	geolocation));
	    	});
	  	}
	}

	google.maps.event.addDomListener(window, 'load', initialize);

</script>

@stop
