@extends('layouts.express')

@section('page_css')
<link href="{{asset('custom_css/fullcalendar.css')}}" rel='stylesheet' type='text/css'>
<link href="{{asset('custom_css/fullcalendar.print.css')}}" rel='stylesheet' type='text/css'>
<link href="{{asset('custom_css/express_calendar.css')}}" rel='stylesheet' type='text/css'>
<!-- Jquery fullcalendar event styles -->
<style>
.eventbooked,
.eventbooked div,
.eventbooked span {
    background-color: white; /* background color */
    border-color: green;     /* border color */
    color: black;           /* text color */
}
</style>
@stop


@section('content')
<!-- Main Content -->
<!--
<div id="page_title">
	<div class="container clearfix">
		<div class="pagename">
			<div class="pull-right">Last Updated: 18.01.2014 17:40:22</div>
		</div>
	</div>
</div> -->
<div id="content_wrapper">

	<div class="row clearfix">

		<div class="col-xs-12 col-sm-8 col-md-8">

			<div class="clear"></div>

			<div class="white-bg padding-20 mt-20 ml-10">
				<div id='calendar'></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- Begin sidebar -->
		<div class="col-xs-12 col-sm-4 col-md-4">
			
			<div class="sidebar pv-20 mr-10">
				<!--
				<div id="bookings_sidebar">
					<div id="sidebar_title" class="sidebar-item white-bg padding-20 mb-20 mr-10">
						<h2>Venue Bookings (32)</h2><div id="last_updated_info"></div>
						<select class="mySelectClass customText">
							<option>Sort: Upcoming</option>
							<option>Sort: Newest Booking</option>

						</select>
					</div><

					<div id="sidebar_content_item" class="sidebar-item white-bg padding-20 mr-10">
						<table class="table table-bordered">
							<tr>
								<td>Name:</td>
								<td>Mr. Miagi Yohsino</td>
							</tr>
							<tr>
								<td>Date:</td>
								<td>18.01.2014</td>
							</tr>
							<tr>
								<td>Time</td>
								<td>18:00:00 - 21:00:00</td>
							</tr>
							<tr>
								<td>Room:</td>
								<td>Executive Room</td>
							</tr>
						</table>

					</div> -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel-group" id="accordion">
								<div id="sidebar_content_injection">
										<!--
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion" href="#event_123">18.01.2014</a>
											</h4>
										</div>
									
										<div id="event_123" class="panel-collapse collapse in">
											<div class="panel-body" id="panel_id">
												<div class="row">
													<table class="table borderless" id="table_">
														<tr>
															<td>Mr Miagi sfsdfds</td>
															<td>18:30 - 19:30</td>
															<td>Executive Office</td>
															<td>Status: Booked</td>
															<td><button class="btn btn-primary">Details</button></td>
														</tr>
													</table>
												</div>

											</div>
										</div>
									-->
									</div>
									
								</div>
							</div>
						</div>
					</div>

				</div>

			</div>

		</div><!-- end sidebar  -->

	</div><!-- end sidebar outer  -->


	<div class="clear"></div>

</div>

</div><!-- end content_wrapper -->




<!-- Sidebar Click Details Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal_title">Booking</h4>
      </div>
      <div class="modal-body" id="modal_content">
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Contact Customer</button>
        <button type="button" class="btn btn-danger">Cancel Booking</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- Date Entry Modal -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="modal_title2" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal_title2">Occupy a timeslot</h4>
      </div>
      <div class="modal-body" id="modal_content2">
      	<form role="form" id="modal_content2_form" name="modal_content2_form" action="" method="post">
									<div class="form-group">
										<label for="modal2_title">Title</label>
										<input type="text" class="form-control" id="modal2_title" placeholder="Please enter a title">
									</div>
									<div class="form-group">
										<label for="modal2_date">Date</label>
										<input type="text" class="form-control" id="modal2_date" >
									</div>
									<div class="form-group">
										<label for="modal_2_start_time">Start Time</label>
										<input type="text" class="form-control" id="modal2_start_time">
									</div>
									<div class="form-group">
										<label for="modal_2_end_time">End Time</label>
										<input type="text" class="form-control" id="modal2_end_time">
									</div>
									<div class="form-group" style="display:none">
										
										<input type="text" class="form-control" id="modal2_room_id">
									</div>


								</form>
      	<p>You can occupy a timeslot, if a certain timeslot is not available, because
      	your received a booking via telephone or other reasons.
      	For recurring occupations please visit the control center.</p>
      

      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="insert_event">Confirm</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->





<!-- Main Content End -->

@include('layouts.footer')
@stop

@section('page_js')
<script type="text/javascript" src="{{asset('custom_js/fullcalendar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('custom_js/list.min.js')}}"></script>
<script type="text/javascript" src="{{asset('custom_js/list.pagination.min.js')}}"></script>


@if (Sentry::check()) 
<script type="text/javascript" src="{{asset('custom_js/express_calendar.js')}}"></script>
<script type="text/javascript" src="{{asset('custom_js/date.js')}}"></script>
@endif 

@stop

