@extends('layouts.main')

@section('page_css')
@stop


@section('content')

<!-- Main Content -->
	

	
		<div id="slider_wrapper">
			<div class="owl-slider-wrapper center-capture">
				<div id="owl-slider" class="owl-carousel owl-theme">
					 
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/owlSlider-01.jpg" alt="image">
						
					</div>
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/owlSlider-02.jpg" alt="image">
						
					</div>		
				</div>
			
			</div>
		</div><!-- end slider_wrapper -->
		
		<div id="content_wrapper">
			<div class="container">
				
				<div class="travel-search" style="margin-top:-200px">
					<div class="travel-search-inner">
						<ul class="tabs searchTab">
							<li><a href="#vacationsSearch" class="active"><span class="vacation mi"></span>Office</a></li>
							<li><a href="#carsSearch"><span class="car mi"></span>Meetingroom</a></li>

						</ul><!-- tabs -->
						
						<ul class="tabs-content">
						
							<li id="vacationsSearch" class="active">
								<form role="form" class="row">
									<div class="col-xs-12 col-sm-4 col-md-4">
										<div class="form-group">
											<label for="vacationLocation">Where to Stay?</label>
											<input type="text" class="form-control" id="vacationLocation" placeholder="Thailand">
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="vacation-check-in">Check-in</label>
											<input type="text" class="form-control date" id="vacation-check-in" placeholder="dd/dm/yy">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="vacation-check-out">Check-out</label>
											<input type="text" class="form-control date" id="vacation-check-out" placeholder="dd/dm/yy">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-1">
										<div class="form-group">
											<label for="vacationAdult">Adult</label>
											<select class="form-control mySelectBoxClass" id="vacationAdult">
												<option>0</option>
												<option selected>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-1">
										<div class="form-group">
											<label for="vacationChild">Child</label>
											<select class="form-control mySelectBoxClass" id="vacationChild">
												<option>0</option>
												<option selected>1</option>
												<option>2</option>
												<option>3</option>
												<option>4</option>
												<option>5</option>
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-2">
										<div class="clear"></div>
										<button type="submit" class="btn btn-primary btn-block">Search</button>
									</div>
								</form>
							</li>
							<li id="carsSearch">
								<form role="form" class="row">
									<div class="col-xs-12 col-sm-4 col-md-1">
										<div class="form-group">
											<label for="carFrom">From</label>
											<input type="text" class="form-control" id="carFrom" placeholder="Bangkok">
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-1">
										<div class="form-group">
											<label for="carTo">To</label>
											<input type="text" class="form-control" id="carTo" placeholder="Pattaya">
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="carPickUp">Pick Up</label>
											<input type="text" class="form-control date" id="carPickUp" placeholder="dd/dm/yy">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="pickUpTime">Hour</label>
											<select class="form-control mySelectBoxClass" id="pickUpTime">
												<option>12:00 AM</option>
												<option>12:30 AM</option>
												<option>01:00 AM</option>
												<option>01:30 AM</option>
												<option>02:00 AM</option>
												<option>02:30 AM</option>
												<option>03:00 AM</option>
												<option>03:30 AM</option>
												<option>04:00 AM</option>
												<option>04:30 AM</option>
												<option>05:00 AM</option>
												<option>05:30 AM</option>
												<option>06:00 AM</option>
												<option>06:30 AM</option>
												<option>07:00 AM</option>
												<option>07:30 AM</option>
												<option>08:00 AM</option>
												<option>08:30 AM</option>
												<option>09:00 AM</option>
												<option>09:30 AM</option>
												<option>10:00 AM</option>
												<option selected>10:30 AM</option>
												<option>11:00 AM</option>
												<option>11:30 AM</option>
												<option>12:00 PM</option>
												<option>12:30 PM</option>								  
												<option>01:00 PM</option>
												<option>01:30 PM</option>
												<option>02:00 PM</option>
												<option>02:30 PM</option>
												<option>03:00 PM</option>
												<option>03:30 PM</option>
												<option>04:00 PM</option>
												<option>04:30 PM</option>
												<option>05:00 PM</option>
												<option>05:30 PM</option>
												<option>06:00 PM</option>
												<option>06:30 PM</option>
												<option>07:00 PM</option>
												<option>07:30 PM</option>
												<option>08:00 PM</option>
												<option>08:30 PM</option>
												<option>09:00 PM</option>
												<option>09:30 PM</option>
												<option>10:00 PM</option>
												<option>10:30 PM</option>
												<option>11:00 PM</option>
												<option>11:30 PM</option>		
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-sm-4 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="carDropOff">Drop Off</label>
											<input type="text" class="form-control date" id="carDropOff" placeholder="dd/dm/yy">
											<i class="fa fa-calendar"></i>
										</div>
									</div>
									<div class="col-xs-12 col-sm-3 col-md-2">
										<div class="form-group datepicker-wrapper">
											<label for="dropOffTime">Hour</label>
											<select class="form-control mySelectBoxClass" id="dropOffTime">
												<option>12:00 AM</option>
												<option>12:30 AM</option>
												<option>01:00 AM</option>
												<option>01:30 AM</option>
												<option>02:00 AM</option>
												<option>02:30 AM</option>
												<option>03:00 AM</option>
												<option>03:30 AM</option>
												<option>04:00 AM</option>
												<option>04:30 AM</option>
												<option>05:00 AM</option>
												<option>05:30 AM</option>
												<option>06:00 AM</option>
												<option>06:30 AM</option>
												<option>07:00 AM</option>
												<option>07:30 AM</option>
												<option>08:00 AM</option>
												<option>08:30 AM</option>
												<option>09:00 AM</option>
												<option>09:30 AM</option>
												<option>10:00 AM</option>
												<option selected>10:30 AM</option>
												<option>11:00 AM</option>
												<option>11:30 AM</option>
												<option>12:00 PM</option>
												<option>12:30 PM</option>								  
												<option>01:00 PM</option>
												<option>01:30 PM</option>
												<option>02:00 PM</option>
												<option>02:30 PM</option>
												<option>03:00 PM</option>
												<option>03:30 PM</option>
												<option>04:00 PM</option>
												<option>04:30 PM</option>
												<option>05:00 PM</option>
												<option>05:30 PM</option>
												<option>06:00 PM</option>
												<option>06:30 PM</option>
												<option>07:00 PM</option>
												<option>07:30 PM</option>
												<option>08:00 PM</option>
												<option>08:30 PM</option>
												<option>09:00 PM</option>
												<option>09:30 PM</option>
												<option>10:00 PM</option>
												<option>10:30 PM</option>
												<option>11:00 PM</option>
												<option>11:30 PM</option>	
											</select>
										</div>
									</div>
									<div class="col-xs-12 col-sm-2 col-md-2">
										<div class="clear"></div>
										<button type="submit" class="btn btn-primary btn-block">Search</button>
									</div>
								</form>
							</li>
							
						</ul>
					</div>
				</div>
				
				<div class="clear"></div>
				
			</div>
			
			<div class="clear"></div>
			
			<div class="white-bg">
				<div class="container">
					<div class="last-min">
						
						<div class="block clearfix mv-15"><strong>Instant Booking</strong> - Day/Hourly Offices - Meetingrooms - Desks - Traingrooms - Hotel Lobby</div>
						<a href="#">More Details</a>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="container">
								
				<div class="row pv-30 mt-20 product-4">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/item-wide-05.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Zealously prevailed be arranging do. Set arranging too dejection.</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Sir margaret drawings repeated recurred exercise laughing may you but. Do repeated whatever to welcomed absolute no. Fat surprise although outlived and informed shy dissuade property.</p>
								<a href="#" class="more-link">More</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/item-wide-04.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Greatly hearted has who believe. Drift allow green son walls years.</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Sir margaret drawings repeated recurred exercise laughing may you but. Do repeated whatever to welcomed absolute no. Fat surprise although outlived and informed shy dissuade property.</p>
								<a href="#" class="more-link">More</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/item-wide-06.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Partments occasional boisterous as solicitude to introduced.</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Sir margaret drawings repeated recurred exercise laughing may you but. Do repeated whatever to welcomed absolute no. Fat surprise although outlived and informed shy dissuade property.</p>
								<div class=""><a href="#" class="more-link">More</a></div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div><!-- end content_wrapper -->
<!-- Main Content End -->






<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')


@stop



