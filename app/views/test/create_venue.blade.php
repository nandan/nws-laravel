@extends('layouts.main')

@section('page_css')
<style>
	/* Wizard Menu at the top */
	.wizard-menu {
		list-style: none;
		font-size: 15px;
		font-weight: 700;
		line-height: 1;
		padding: 0px 20px 13px 20px;
	}
	.wizard-menu > li {
		position: relative;
		text-shadow: none;
		float: left;
	}

	.wizard-menu > li.waiting {
		color: #bdc3c7;
	}

	.wizard-menu > li:after {
	  color: #bdc3c7;
	  content: "\f054";
	  font-family: FontAwesome;
	  display: inline-block;
	  font-size: 9.75px;
	  margin: -4px 9px 0 13px;
	  vertical-align: middle;
	  -webkit-font-smoothing: antialiased;
	}
	
	.wizard-menu > li:last-child:after {
		display: none;
	}
	
	.wizard-menu .active {
	  color: #bdc3c7;
	  cursor: default;
	}
	.wizard-menu .active:after {
	  display: none;
	}
	.wizard-menu > li + li:before {
	  content: "";
	  padding: 0;
	}
	
	
	/* Wizard items and elements */
	.wizard-item {
		padding: 10px;
	}
	.wizard-element {
		margin-bottom: 10px;
	}
	.wizard-element.address-field {
		height: 34px;
	}
	.wizard-button {
		margin-right: -15px;
	}
	.wizard-button button {
		margin-right: 35px;
		width: 100px;
	}
	
	/* Google Maps Places */
	#maps-canvas {
		height: 500px;
		width: 500px;
	}
	.controls {
		margin-top: 16px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	}
	
	#pac-input {
		background-color: #fff;
		padding: 0 11px 0 13px;
		width: 300px;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		text-overflow: ellipsis;
	}
	
	#pac-input:focus {
		border-color: #4d90fe;
		margin-left: -1px;
		padding-left: 14px;  /* Regular padding-left + 1. */
		width: 401px;
	}
	
	.pac-container {
		font-family: Roboto;
	}
	
	table.time-table tr  td.table-days{
		padding-top: 15px;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Dashboard</div>
		<div class="breadcrumb clearfix">
			<a href="index.html">Jens Kippels Company</a>
			<span class="current-page">Big Mama House</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			<!-- Begin sidebar -->
			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar">

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Berlin Business Center GmbH</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_company_bookings')}}">Bookings</a></li>
							<li><a href="{{URL::route('controlcenter_company_calendar')}}">Calendar</a></li>
							<li><a href="#">Venues</a></li>
							<li><a href="#">Financials</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->

					<div class="sidebar-item white-bg padding-20 mb-20">
						<h4>Firstname Lastname</h4>
						<ul class="cat-list">
							<li><a href="{{URL::route('controlcenter_bookings')}}">Bookings</a></li>
							<li><a href="#">Invoice</a></li>
							<li><a href="#">Favorites</a></li>
							<li><a href="{{URL::route('controlcenter_settings')}}">Settings</a></li>

						</ul>	
					</div><!-- end categories -->


					<div class="sidebar-item white-bg padding-20 mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div><!-- end assistance  -->
				</div><!-- end sidebar  -->
			</div><!-- end sidebar outer  -->

			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<h2>Bookings</h2>
				</div>
				<div class="clear mb-30"></div>
				@include('venues.create')
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div><!-- end content_wrapper -->



<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')



@stop



