<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Office Booking</title>
	<meta name="keywords" content="Hotel, Travel, Tour, Trip, Booking , CSS3, HTML5, Repsonsive"/>
	<meta name="description" content="Responsive Template for Travel Agency and Complete Booking System">
	<meta name="author" content="hoomey">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<!-- ============================================
		Fav and touch icons
		============================================= -->
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
		<link rel="apple-touch-icon-precomposed" href="{{asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
		<link rel="shortcut icon" href="{{asset('images/ico/favicon.png')}}">

		<!-- Core Styles -->
		<link rel="stylesheet" type="text/css" href="{{asset('bootstrap/css/bootstrap.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/external-styled.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('style.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/icons.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/jquery-ui.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/smoothness/jquery-ui-1.8.16.custom.css')}}" media="screen" />
		<link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}" media="screen" />

		<!-- Google Fonts -->
		<link href="{{asset('css/oswald.css')}}" rel='stylesheet' type='text/css'>
		<link href="{{asset('css/roboto.css')}}" rel='stylesheet' type='text/css'>

		<!-- Custom Core CSS -->
		<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.growl.css')}}" media="screen" />

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->

      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
      <!-- page specific css -->
      @yield('page_css')

  </head>

  <body>

  	<div id="preloader">
  		<div id="status">&nbsp;</div>
  	</div>

  	<div id="body_wrapper" class="full">
  		<!-- Navigation Bar Selection -->
  		@if (!Sentry::check()) 
  		@include('navigation.header_unregistered_express')
  		@else 
  		@include('navigation.header_registered_express')
  		@endif 
  		<!-- Navigation Bar Selection End -->
  		@yield('content')

  	</div><!-- end body_wrapper -->


  	<!-- Core Scripts -->
  	<script type="text/javascript" src="{{asset('js/jquery.v2.0.3.js')}}"></script>
  	<script type="text/javascript" src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
  	<script type="text/javascript" src="{{asset('js/jquery-ui.min.js')}}"></script>
  	<script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>
  	<script type="text/javascript" src="{{asset('js/sliders.js')}}"></script>
  	<script type="text/javascript" src="{{asset('js/custom.js')}}"></script>
  	<!-- Custom Core Scripts -->
  	<script type="text/javascript" src="{{asset('custom_js/jquery.growl.js')}}"></script>
    <script type="text/javascript" src="{{asset('custom_js/holder.js')}}"></script>


  	<!-- page specific js -->
  	@yield('page_js')
  	<!-- Use Authentication.js only if not logged in -->
	@if (!Sentry::check()) 
	<script type="text/javascript" src="{{asset('custom_js/express_authentication.js')}}"></script>
	@endif 
  </body>
  </html>
