<footer id="footer">
			<div class="primary-footer">
				<div class="container">
					<div class="row pt-50 pb-30">
						<div class="col-xs-12 col-sm-3 col-md-3 rs-mb">
							<h4 class="widget-title mb-30">About Us</h4>
							<div class="logo-footer mt-20 mb-10">
								<h1>NextWorkspace</h1>
							</div>
							<p>Instant Workspaces &amp; Real-Time Booking <a href="#" style="font-weight: 300; font-style: italic">view more</a></p>
							<div class="social with_color">
								<a href="#" class="bottomtip" title="Twitter"><i class="fa-twitter"></i></a>
								<a href="#" class="bottomtip" title="Facebook"><i class="fa-facebook"></i></a>
								<a href="#" class="bottomtip" title="LinkedIn"><i class="fa-linkedin"></i></a>
								<a href="#" class="bottomtip" title="Youtube"><i class="fa-youtube"></i></a>
								<a href="#" class="bottomtip" title="RSS"><i class="fa-rss"></i></a>
							</div><!-- end social -->
							<div class="clear"></div>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3 xs-mb">
							<h4 class="widget-title mb-20">For Workspace Owners</h4>
							<ul class="link-list">
								<li><a href="#" title="Travel Specialists">List your workspace!</a></li>
								<li><a href="#" title="Travel Specialists">Features</a></li>
								<li><a href="#" title="Travel Specialists">Benefits</a></li>
								<li><a href="#" title="Travel Specialists">Tutorials</a></li>
								<li><a href="#" title="Travel Specialists">Customer Service</a></li>
								<li><a href="#" title="Travel Specialists">Personal Solutions</a></li>
								<li><a href="#" title="Travel Specialists">Case Studies</a></li>
								<li><a href="#" title="Travel Specialists">Contact Us!</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3 xs-mb">
							<h4 class="widget-title mb-20">Quick Links</h4>
							<ul class="link-list">
								<li><a href="#" title="Quick Links">About Us</a></li>
								<li><a href="#" title="Quick Links">FAQ</a></li>
								<li><a href="#" title="Quick Links">Payment Options</a></li>
								<li><a href="#" title="Quick Links">Terms of Use</a></li>
								<li><a href="#" title="Quick Links">Privacy Policy</a></li>
								<li><a href="#" title="Quick Links">Cancellation Policy</a></li>
								<li><a href="#" title="Quick Links">Terms &amp; Conditions</a></li>
								<li><a href="#" title="Quick Links">Press Room</a></li>
							</ul>
						</div>
						<div class="col-xs-12 col-sm-3 col-md-3">
							<h4 class="widget-title mb-20">Newsletter</h4>
							<div class="newsletter mt-20">
								<form name="email-validation" id="email-validation" class="fixed-position" action="#" novalidate>
									<input type="email" name="form-control" id="input-email" class="form-control" placeholder="Enter Your Email" value="">
									<button type="submit" class="btn btn-primary submit-button"><i class="arrow_carrot-right_alt2"></i></button>
								</form>
							</div><!-- End newsletter -->
							<div class="clear mb-30"></div>
							<h4 class="widget-title">Customer Support</h4>
							<p class="phone-number text-info"><i class="icon icon-phone-support mr-10"></i>0 800 12 345 67</p>
							<a href="mailto:dowenstation@gmail.com" class="font16 font300">office@nextworkspace.com</a>
						</div>
					</div>
					
				</div>
			</div>
			<div class="bottom-footer">
				<div class="container pv-15 bt">
					<p class="copy-right text-center">&copy; 2014 <a href="index.html">NextWorkspace</a>. All Right Reserved</p>
					<div id="toTop" class="toptip" title="Back to Top"><i class="icon icon-up-open-big"></i></div><!-- Back to top -->
				</div>
			</div>
		</footer>