<div class="col-md-12">
	<div class="row white-bg padding-20">
		<div class="wizard-item">
			<div class="col-xs-12 col-sm-6 col-md-3">
				<h3>Company Name</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="col-md-4">
					Name
				</div>
				<div class="col-md-8">
					<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Default" />
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<h3>Company Address</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Street
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_street" name="company_street" placeholder="street name and house number" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Floor
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_floor" name="company_floor" placeholder="floor/suite" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						City
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_city" name="company_city" placeholder="city name" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						State
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_state" name="company_state" placeholder="state" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						ZIP/Postal Code
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_zip" name="company_zip" placeholder="postal code" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Country
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_country" name="company_country" placeholder="country" />
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<h3>Company Contacts</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Contact Person
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_contact_name" name="company_contact_name" placeholder="name of the contact person" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Telephone
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_telephone" name="company_telephone" placeholder="+XX XXXXXXXXX" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Fax
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_fax" name="company_fax" placeholder="+XX XXXXXXXXX" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Email
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_email" name="company_email" placeholder="example@example.com" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Company Website
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="company_website" name="company_website" placeholder="http://www..." />
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
