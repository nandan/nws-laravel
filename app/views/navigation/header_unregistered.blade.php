<header id="header">
			<div class="top-header">
				<div class="clearfix">
					<div class="top-header-inner clearfix">
						<div class="header-currency mr-20">
							<a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a>
							<div class="currency-show">
								<ul>
									<li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li>
									<li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li>
								</ul>
							</div><!-- end other -->
						</div>
					
						<div class="header-language ml-10">
							<a href="#" class="english"><span>English</span></a>
							<div class="other_languages">
								<ul class="clearfix">
									<li><a href="#" class="deutsch"><span>Deutsch</span></a></li>
									<li><a href="#" class="espanol"><span>Español</span></a></li>
									<li><a href="#" class="italiano"><span>Italiano</span></a></li>
								</ul>
							</div><!-- end other -->
						</div>
						<div class="header-register">
							<a href="#" class="br"><i class="icon icon icon-user-add mi"></i>Register</a>
							<div class="register_show">
								<form role="form" id="register_form" name="register_form" action="" method="post">
									<div class="form-group">
										<label for="first_name">Firstname</label>
										<input type="text" class="form-control" id="first_name" name="first_name" placeholder="Firstname">
									</div>
									<div class="form-group">
										<label for="last_name">Lastname</label>
										<input type="text" class="form-control" id="last_name" name="last_name" placeholder="Lastname">
									</div>
									<div class="form-group">
										<label for="emailx">Email</label>
										<input type="text" class="form-control" id="emailx" name="emailx" placeholder="Email">
									</div>
									<div class="form-group">
										<label for="passwordx">Password</label>
										<input type="password" class="form-control" id="passwordx"  name="passwordx" placeholder="Password min 6.char">
									</div>
									<div class="form-group">
										<label for="password_again">Confirm Password</label>
										<input type="password" class="form-control" id="password_again" name="password_again" placeholder="Confirm  Password">
									</div>
										<div class="checkbox">
										<label>
											<input type="checkbox" id="tos" name="tos"> Accept TOS
										</label>
									</div>
									<button type="submit" class="btn btn-success">Register</button>
									<div class="clear mb-10"></div>
								</form>
							</div><!-- end other -->
						</div>
						<div class="header-login mr-15">
							<a href="#" class="br"><i class="fa-sign-in mi"></i>Login</a>
							<div class="login_show">
								<form role="form" id="login_form" name="login_form" action="" method="post">
									<div class="form-group">
										<label for="email">Email</label>
										<input type="text" class="form-control" id="email"  name="email" placeholder="Email">
									</div>
									<div class="form-group">
										<label for="password">Password</label>
										<input type="password" class="form-control" id="password" name="password" placeholder="Password">
									</div>
									<div class="checkbox">
										<label>
											<input type="checkbox" id="remember_me" name="remember_me"> Remember Me
										</label>
									</div>
									<button type="submit" class="btn btn-primary">Login</button>
									<div class="clear mb-10"></div>
									<a href="#" class="forgot">Forgot Your Password?</a>
								</form>
							</div><!-- end other -->
						</div>
						<div class="clear xss-mb-10"></div>
					</div><!-- end top-header-inner -->
				</div><!-- end container -->
			</div><!-- end top-header -->
			<div class="clear"></div>
			<div class="large-header">
				<div class="clearfix">
					<div class="logo">
						<a href="{{URL::to('/')}}" title="Logo"><h1>NextWorkspace</h1></a>
					</div><!-- end logo -->
					<nav>
						<ul class="sf-menu">
							<li><a href="#">How it works</a></li>
							<li><a href="#">Help Center</a></li>
							<li><a href="{{URL::route('listworkspace')}}">List your Workspace</a></li>
						</ul><!-- end menu -->
					</nav><!-- end nav -->
				</div><!-- end container -->
			</div><!-- end large-header -->
		</header><!-- end header -->