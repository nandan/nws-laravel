<header id="header">
			<div class="top-header">
				<div class="clearfix">
					<div class="top-header-inner clearfix">
						<div class="header-currency mr-20">
							<a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a>
							<div class="currency-show">
								<ul>
									<li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li>
									<li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li>
								</ul>
							</div><!-- end other -->
						</div>
					
						<div class="header-language ml-10">
							<a href="#" class="english"><span>English</span></a>
							<div class="other_languages">
								<ul class="clearfix">
									<li><a href="#" class="deutsch"><span>Deutsch</span></a></li>
									<li><a href="#" class="espanol"><span>Español</span></a></li>
									<li><a href="#" class="italiano"><span>Italiano</span></a></li>
								</ul>
							</div><!-- end other -->
						</div>
						<div class="header-login mr-15">
							<a href="{{URL::route('auth_logout')}}" class="br"><i class="fa-sign-in mi"></i>Logout</a>
						</div>
						<div class="clear xss-mb-10"></div>
					</div><!-- end top-header-inner -->
				</div><!-- end container -->
			</div><!-- end top-header -->
			<div class="clear"></div>
			<div class="large-header">
				<div class="clearfix">
					<div class="logo">
						<a href="{{URL::to('/')}}" title="Logo"><h1>NextWorkSpace</h1></a>
					</div><!-- end logo -->
					<nav>
						<ul class="sf-menu">
							<li><a href="{{URL::route('controlcenter')}}">Control Center</a></li>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Help Center</a></li>
							<li><a href="{{URL::route('listworkspace')}}">List your Workspace</a></li>
							
						</ul><!-- end menu -->
					</nav><!-- end nav -->
				</div><!-- end container -->
			</div><!-- end large-header -->
		</header><!-- end header -->