<div class="col-md-12">
	<div class="row white-bg padding-20">
		<div class="wizard-item">
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Office Name</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Name
					</div>
					<div class="col-md-8">
				        <input type="text" class="form-control" id="office_name" name="office_name" placeholder="office name" value="{{$room_information[0]->room_name}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Description
					</div>
					<div class="col-md-8">
						<textarea class="form-control" rows="4" id="office_description" name="office_description" placeholder="Enter description here...">{{$room_information[0]->description}}</textarea> 
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Office Details</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Office Type
					</div>
					<div class="col-md-8">
				        <select id="office_types" name="office_types" class="form-control mySelectBoxClass">
				        	<option value="{{$room_information[0]->room_types_id}}">{{$room_information[0]->room_type_name}}</option>
				        	<option value="1">Office</option>
				        	<option value="2">Meeting Room</option>
				        	<option value="3">Coworking</option>
				        	<option value="4">Others</option>
				        </select>
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Maximum Capacity
					</div>
					<div class="col-md-8">
					    <input type="text" id="office_capacity" name="office_capacity" class="form-control" placeholder="maximum capacity of the office" value="{{$room_information[0]->max_capacity}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Amenities
					</div>
					<div class="col-md-8">
					    <table class="table table-striped table-hover">
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_public_wifi">
					    		  <input type="checkbox" value="0" id="office_amenities_public_wifi" name="office_amenities_public_wifi" data-toggle="checkbox" @if ($room_information[0]->public_wifi != null) checked @endif >
					    		  Public WiFi
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_secure_wifi">
					    		  <input type="checkbox" value="0" id="office_amenities_secure_wifi" name="office_amenities_secure_wifi" data-toggle="checkbox" @if ($room_information[0]->secure_wifi != null) checked @endif >
					    		  Secure WiFi
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_wired_internet">
					    		  <input type="checkbox" value="0" id="office_amenities_wired_internet" name="office_amenities_wired_internet" data-toggle="checkbox" @if ($room_information[0]->wired_internet != null) checked @endif >
					    		  Wired Internet
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_flatscreen">
					    		  <input type="checkbox" value="0" id="office_amenities_flatscreen" name="office_amenities_flatscreen" data-toggle="checkbox" @if ($room_information[0]->flatscreen != null) checked @endif >
					    		  Flatscreen
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_video_conferencing">
					    		  <input type="checkbox" value="0" id="office_amenities_video_conferencing" name="office_amenities_video_conferencing" data-toggle="checkbox" @if ($room_information[0]->video_conferencing != null) checked @endif >
					    		  Video Conferencing
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_projector">
					    		  <input type="checkbox" value="0" id="office_amenities_projector" name="office_amenities_projector" data-toggle="checkbox" @if ($room_information[0]->projector != null) checked @endif >
					    		  Projector
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_whiteboard">
					    		  <input type="checkbox" value="0" id="office_amenities_whiteboard" name="office_amenities_whiteboard" data-toggle="checkbox" @if ($room_information[0]->whiteboard != null) checked @endif >
					    		  Whiteboard
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_flipboard">
					    		  <input type="checkbox" value="0" id="office_amenities_flipboard" name="office_amenities_flipboard" data-toggle="checkbox" @if ($room_information[0]->flipboard != null) checked @endif >
					    		  Flipboard
					    		  </label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_phone_room">
					    		  <input type="checkbox" value="0" id="office_amenities_phone_room" name="office_amenities_phone_room" data-toggle="checkbox" @if ($room_information[0]->phone_room != null) checked @endif >
					    		  Phone Room
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_conference_phone">
					    		  <input type="checkbox" value="0" id="office_amenities_conference_phone" name="office_amenities_conference_phone" data-toggle="checkbox" @if ($room_information[0]->conference_phone != null) checked @endif >
					    		  Conference Phone
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_print_scan_copy">
					    		  <input type="checkbox" value="0" id="office_amenities_print_scan_copy" name="office_amenities_print_scan_copy" data-toggle="checkbox" @if ($room_information[0]->print_scan_copy != null) checked @endif >
					    		  Print/Scan/Copy Service
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_handicap_accessible">
					    		  <input type="checkbox" value="0" id="office_amenities_handicap_accessible" name="office_amenities_handicap_accessible" data-toggle="checkbox" @if ($room_information[0]->handicap_accessible != null) checked @endif >
					    		  Handicap Accessible
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_coffee_tea">
					    		  <input type="checkbox" value="0" id="office_amenities_coffee_tea" name="office_amenities_coffee_tea" data-toggle="checkbox" @if ($room_information[0]->coffee_tea != null) checked @endif >
					    		  Coffee/Tea
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_filtered_water">
					    		  <input type="checkbox" value="0" id="office_amenities_filtered_water" name="office_amenities_filtered_water" data-toggle="checkbox" @if ($room_information[0]->filtered_water != null) checked @endif >
					    		  Filtered Water
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_on_site_restaurant">
					    		  <input type="checkbox" value="0" id="office_amenities_on_site_restaurant" name="office_amenities_on_site_restaurant" data-toggle="checkbox" @if ($room_information[0]->on_site_restaurant != null) checked @endif >
					    		  On-Site Restaurant
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_catering">
					    		  <input type="checkbox" value="0" id="office_amenities_catering" name="office_amenities_catering" data-toggle="checkbox" @if ($room_information[0]->catering != null) checked @endif >
					    		  Catering
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_concierge_service">
					    		  <input type="checkbox" value="0" id="office_amenities_concierge_service" name="office_amenities_concierge_service" data-toggle="checkbox" @if ($room_information[0]->concierge_service != null) checked @endif >
					    		  Concierge Service
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_notary_service">
					    		  <input type="checkbox" value="0" id="office_amenities_notary_service" name="office_amenities_notary_service" data-toggle="checkbox" @if ($room_information[0]->notary_service != null) checked @endif >
					    		  Notary Service
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_shared_kitchen">
					    		  <input type="checkbox" value="0" id="office_amenities_shared_kitchen" name="office_amenities_shared_kitchen" data-toggle="checkbox" @if ($room_information[0]->shared_kitchen != null) checked @endif >
					    		  Shared Kitchen
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_shower_facility">
					    		  <input type="checkbox" value="0" id="office_amenities_shower_facility" name="office_amenities_shower_facility" data-toggle="checkbox" @if ($room_information[0]->shower_facility != null) checked @endif >
					    		  Shower Facility
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td><label class="checkbox" for="office_amenities_pet_friendly">
					    		  <input type="checkbox" value="0" id="office_amenities_pet_friendly" name="office_amenities_pet_friendly" data-toggle="checkbox" @if ($room_information[0]->pet_friendly != null) checked @endif >
					    		  Pet Friendly
					    		</label></td>
					    		<td><label class="checkbox" for="office_amenities_outdoor_space">
					    		  <input type="checkbox" value="0" id="office_amenities_outdoor_space" name="office_amenities_outdoor_space" data-toggle="checkbox" @if ($room_information[0]->outdoor_space != null) checked @endif >
					    		  Outdoor Space
					    		</label></td>
					    	</tr>
					    </table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Pricing</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Currency
					</div>
					<div class="col-md-8">
						<!-- This must be solved soon -->
					    <select id="office_currencies" name="office_currencies" class="form-control mySelectBoxClass">
					    	<option value="1">US Dollar</option>
					    	<option value="2">Euro</option>
					    	<option value="3">Japanese Yen</option>
					    	<option value="4">Korean Won</option>
					    	<option value="5">Indian Rupee</option>
					    	<option value="6">Chinese Ren Min Bi</option>
					    </select>
					</div>
				</div>
				<div class="clearfix wizard-element">
					<table class="table table-striped table-hover">
						<tr>
							<th rowspan="2" style="text-align: center;">Period</th>
							<th rowspan="2" style="text-align: center;">Price exkl. Taxes <i class="fa fa-question-circle"></i></th>
							<th colspan="2" style="text-align: center;">Taxes</th>
						</tr>
						<tr>
							<th style="text-align: center;">flat</th>
							<th style="text-align: center;">percentage</th>
						</tr>
						<tr>
							<!-- Check for right values w.r.t. time period must be implemented -->
							<td>Hourly</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_price_hourly" name="office_price_hourly" class="form-control" placeholder="00.00" value="{{$room_prices[0]->price_original}}"/>
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_flattax_hourly" name="office_flattax_hourly" class="form-control" placeholder="00.00" value="{{$room_prices[0]->tax_flat}}"/>
								</div>
							</td>
							<td>
								<div class="input-group">
									<input type="text" id="office_percentagetax_hourly" name="office_percentagetax_hourly" class="form-control" placeholder="00.00" value="{{$room_prices[0]->tax_percentage}}"/>
									<span class="input-group-addon">%</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Daily</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_price_daily" name="office_price_daily" class="form-control" placeholder="00.00" value="{{$room_prices[1]->price_original}}" />
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_flattax_daily" name="office_flattax_daily" class="form-control" placeholder="00.00" value="{{$room_prices[1]->tax_flat}}" />
								</div>
							</td>
							<td>
								<div class="input-group">
									<input type="text" id="office_percentagetax_daily" name="office_percentagetax_daily" class="form-control" placeholder="00.00" value="{{$room_prices[1]->tax_percentage}}" />
									<span class="input-group-addon">%</span>
								</div>
							</td>
						</tr>
						<tr>
							<td>Monthly</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_price_monthly" name="office_price_monthly" class="form-control" placeholder="00.00" />
								</div>
							</td>
							<td>
								<div class="input-group">
									<span class="input-group-addon">$</span>
									<input type="text" id="office_flattax_monthly" name="office_flattax_monthly" class="form-control" placeholder="00.00" />
								</div>
							</td>
							<td>
								<div class="input-group">
									<input type="text" id="office_percentagetax_monthly" name="office_percentagetax_monthly" class="form-control" placeholder="00.00" />
									<span class="input-group-addon">%</span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Time Information</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Opening Hours
					</div>
					<div class="col-md-8">
					    <table class="table table-striped table-hover time-table">
					    	<tr>
					    		<td class="table-days">Monday</td>
					    		<td><select name="office_startmonday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[1]->start_time, 0, 2) }}">{{ substr($room_hours[1]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endmonday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[1]->end_time, 0 ,2) }}">{{ substr($room_hours[1]->end_time, 0, 5) }}</option>
									<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
								<td><label class="checkbox" for="office_closedmonday">
								  <input type="checkbox" value="0" id="office_closedmonday" name="office_closedmonday" data-toggle="checkbox" @if ($room_hours[1]->closed != null) checked @endif >
								  Closed
								</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Tuesday</td>
					    		<td><select name="office_starttuesday" class="form-control mySelectBoxClass" style="width: 70px;">
						    		<option value="{{ substr($room_hours[2]->start_time, 0, 2) }}">{{ substr($room_hours[2]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endtuesday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[2]->end_time, 0 ,2) }}">{{ substr($room_hours[2]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedtuesday">
					    		  <input type="checkbox" value="0" id="office_closedtuesday" name="office_closedtuesday" data-toggle="checkbox" @if ($room_hours[2]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Wednesday</td>
					    		<td><select name="office_startwednesday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[3]->start_time, 0, 2) }}">{{ substr($room_hours[3]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endwednesday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[3]->end_time, 0 ,2) }}">{{ substr($room_hours[3]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedwednesday">
					    		  <input type="checkbox" value="0" id="office_closedwednesday" name="office_closedwednesday" data-toggle="checkbox" @if ($room_hours[3]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Thursday</td>
					    		<td><select name="office_startthursday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[4]->start_time, 0, 2) }}">{{ substr($room_hours[4]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endthursday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[4]->end_time, 0 ,2) }}">{{ substr($room_hours[4]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedthursday">
					    		  <input type="checkbox" value="0" id="office_closedthursday" name="office_closedthursday" data-toggle="checkbox" @if ($room_hours[4]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Friday</td>
					    		<td><select name="office_startfriday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[5]->start_time, 0, 2) }}">{{ substr($room_hours[5]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endfriday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[5]->end_time, 0 ,2) }}">{{ substr($room_hours[5]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedfriday">
					    		  <input type="checkbox" value="0" id="office_closedfriday" name="office_closedfriday" data-toggle="checkbox" @if ($room_hours[5]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Saturday</td>
					    		<td><select name="office_startsaturday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[6]->start_time, 0, 2) }}">{{ substr($room_hours[6]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endsaturday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[6]->end_time, 0 ,2) }}">{{ substr($room_hours[6]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedsaturday">
					    		  <input type="checkbox" value="0" id="office_closedsaturday" name="office_closedsaturday" data-toggle="checkbox" @if ($room_hours[6]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Sunday</td>
					    		<td><select name="office_startsunday" class="form-control mySelectBoxClass" style="width: 70px;">
					    			<option value="{{ substr($room_hours[0]->start_time, 0, 2) }}">{{ substr($room_hours[0]->start_time, 0 ,5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="office_endsunday" class="form-control mySelectBoxClass" style="width: 70px;">
					    				<option value="{{ substr($room_hours[0]->end_time, 0 ,2) }}">{{ substr($room_hours[0]->end_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="office_closedsunday">
					    		  <input type="checkbox" value="0" id="office_closedsunday" name="office_closedsunday" data-toggle="checkbox" @if ($room_hours[0]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    </table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h4>Extra Booking Information</h4>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Preparation Time <i class="fa fa-question-circle"></i>
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="office_preparation_time" name="office_preparation_time" placeholder="how long preparation time is required?" value="{{$room_information[0]->preparation_time}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Pre-booking Period <i class="fa fa-question-circle"></i>
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="office_pre_booking_period" name="office_pre_booking_period" value="{{$room_information[0]->pre_booking_period}}" placeholder="when earliest booking can be done?" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Buffer Time <i class="fa fa-question-circle"></i>
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="office_buffer_time" name="office_buffer_time" value="{{$room_information[0]->buffer_time}}" placeholder="when at earliest can the booking be cancelled?" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>