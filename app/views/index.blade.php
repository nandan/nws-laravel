@extends('layouts.main')

@section('page_css')
<link href="{{asset('custom_css/themes/default.css')}}" rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.datetimepicker.css')}}" />
@stop


@section('content')

<!-- Main Content -->
		<div id="slider_wrapper">
			<div class="owl-slider-wrapper center-capture">
				<div id="owl-slider" class="owl-carousel owl-theme">
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/NewYorkCity.jpg" alt="image">
					</div>
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/Singapore.jpg" alt="image">
					</div>
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/Dubai.jpg" alt="image">
					</div>	
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/Shanghai.jpg" alt="image">
					</div>	
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/London.jpg" alt="image">
					</div>
					<div class="item">
						<img class="lazyOwl" data-src="images/sliders/Paris.jpg" alt="image">
					</div>	
				</div>
			</div>
		</div><!-- end slider_wrapper -->
		
		<div id="content_wrapper">
			<div class="container">
					<div class="travel-search" style="margin-top:-10%">
						<div class="travel-search-inner">
							<ul class="tabs searchTab">
								<li><a href="#office" class="active"><span class="fa fa-briefcase"></span> Office</a></li>
								<li><a href="#meetingroom"><span class="fa fa-group"></span> Meeting Room</a></li>
								<li><a href="#coworking"><span class="fa fa-laptop"></span> Open Desk</a></li>
								<li><a href="#others"><span class="fa fa-ellipsis-horizontal"></span> Others</a></li>
							</ul><!-- tabs -->
							
							<ul class="tabs-content">
								<li id="office" class="active">
									<form id="office_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
										<input type="hidden" class="form-control" id="search_room_type" name="search_room_type" value="Office" />
										<div class="col-xs-12 col-sm-4 col-md-4">
											<div class="form-group">
												<label for="search_location">Location</label>
												<input type="text" class="form-control" id="search_location" placeholder="Singapore">
												<input type="hidden" class="form-control" id="locality" name="search_city" />
												<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" />
												<input type="hidden" class="form-control" id="country" name="search_country" />
												<input type="hidden" class="form-control" id="search_lat" name="search_lat" />
												<input type="hidden" class="form-control" id="search_lng" name="search_lng" />
												<input type="hidden" class="form-control" id="search_locid" name="search_locid" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group">
												<label for="search_period">Period</label>
												<select class="form-control mySelectBoxClass" id="search_period" name="search_period">
													<option selected value="hourly">Hourly</option>
													<option value="daily">Daily</option>
													<option value="monthly">Monthly</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group datepicker-wrapper">
												<label for="search_startdate">Date</label>
												<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
										<div class="for_hourly">
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_starttime">Start Time</label>
													<input type="text" rel="time" class="form-control " id="search_starttime" name="search_starttime" placeholder="Time">
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_endtime">End Time</label>
													<input type="text" rel="time" class="form-control " id="search_endtime" name="search_endtime" placeholder="Time">
												</div>
											</div>
										</div>
										<div class="for_daily none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group datepicker-wrapper">
													<label for="search_enddate">End Date</label>
													<input type="text" rel="date" class="form-control" id="search_enddate" name="search_enddate" placeholder="dd/mm/yy">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
										<div class="for_monthly none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label for="search_monthly_duration">Duration</label>
													<select class="form-control mySelectBoxClass" id="search_monthly_duration" name="search_monthly_duration">
														<option selected value="1">1 Month</option>
														<option value="2">2 Months</option>
														<option value="3">3 Months</option>
														<option value="4">4 Months</option>
														<option value="5">5 Months</option>
														<option value="6">6 Months</option>
														<option value="7">7 Months</option>
														<option value="8">8 Months</option>
														<option value="9">9 Months</option>
														<option value="10">10 Months</option>
														<option value="11">11 Months</option>
														<option value="12">12 Months</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="clear"></div>
											<button type="submit" name="submit" class="btn btn-primary btn-block">Search</button>
										</div>
									</form>
								</li>
								<li id="meetingroom">
									<form id="meetingroom_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
										<input type="hidden" class="form-control" id="search_room_type" name="search_room_type" value="Meeting Room" />
										<input type="hidden" class="form-control" id="search_period" name="search_period" value="hourly" />
										<div class="col-xs-12 col-sm-4 col-md-4">
											<div class="form-group">
												<label for="search_location">Location</label>
												<input type="text" class="form-control" id="search_location" placeholder="Singapore">
												<input type="hidden" class="form-control" id="locality" name="search_city" />
												<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" />
												<input type="hidden" class="form-control" id="country" name="search_country" />
												<input type="hidden" class="form-control" id="search_lat" name="search_lat" />
												<input type="hidden" class="form-control" id="search_lng" name="search_lng" />
												<input type="hidden" class="form-control" id="search_locid" name="search_locid" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group datepicker-wrapper">
												<label for="search_startdate">Date</label>
												<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-1">
											<div class="form-group">
												<label for="search_starttime">Start Time</label>
												<input type="text" rel="time" class="form-control " id="search_starttime" name="search_starttime" placeholder="Time">
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-1">
											<div class="form-group">
												<label for="search_endtime">End Time</label>
												<input type="text" rel="time" class="form-control " id="search_endtime" name="search_endtime" placeholder="Time">
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group">
												<label for="search_person">Person</label>
												<select class="form-control mySelectBoxClass" id="search_person" name="search_person">
													<option selected value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">&gt; 5</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="clear"></div>
											<button type="submit" name="submit" class="btn btn-primary btn-block">Search</button>
										</div>
									</form>
								</li>
								<li id="coworking">
									<form id="coworking_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
										<input type="hidden" class="form-control" id="search_room_type" name="search_room_type" value="Coworking" />
										<div class="col-xs-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label for="search_location">Location</label>
												<input type="text" class="form-control" id="search_location" placeholder="Singapore">
												<input type="hidden" class="form-control" id="locality" name="search_city" />
												<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" />
												<input type="hidden" class="form-control" id="country" name="search_country" />
												<input type="hidden" class="form-control" id="search_lat" name="search_lat" />
												<input type="hidden" class="form-control" id="search_lng" name="search_lng" />
												<input type="hidden" class="form-control" id="search_locid" name="search_locid" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group">
												<label for="search_period">Period</label>
												<select class="form-control mySelectBoxClass" id="search_period" name="search_period">
													<option selected value="hourly">Hourly</option>
													<option value="daily">Daily</option>
													<option value="monthly">Monthly</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group datepicker-wrapper">
												<label for="search_startdate">Date</label>
												<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
										<div class="for_hourly">
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_starttime">Start Time</label>
													<input type="text" rel="time" class="form-control " id="search_starttime" name="search_starttime" placeholder="Time">
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_endtime">End Time</label>
													<input type="text" rel="time" class="form-control " id="search_endtime" name="search_endtime" placeholder="Time">
												</div>
											</div>
										</div>
										<div class="for_daily none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group datepicker-wrapper">
													<label for="search_enddate">End Date</label>
													<input type="text" rel="date" class="form-control" id="search_enddate" name="search_enddate" placeholder="dd/mm/yy">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
										<div class="for_monthly none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label for="search_monthly_duration">Duration</label>
													<select class="form-control mySelectBoxClass" id="search_monthly_duration" name="search_monthly_duration">
														<option selected value="1">1 Month</option>
														<option value="2">2 Months</option>
														<option value="3">3 Months</option>
														<option value="4">4 Months</option>
														<option value="5">5 Months</option>
														<option value="6">6 Months</option>
														<option value="7">7 Months</option>
														<option value="8">8 Months</option>
														<option value="9">9 Months</option>
														<option value="10">10 Months</option>
														<option value="11">11 Months</option>
														<option value="12">12 Months</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-1">
											<div class="form-group">
												<label for="search_person">Person</label>
												<select class="form-control mySelectBoxClass" id="search_person" name="search_person">
													<option selected value="1">1</option>
													<option value="2">2</option>
													<option value="3">3</option>
													<option value="4">4</option>
													<option value="5">&gt; 5</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="clear"></div>
											<button type="submit" name="submit" class="btn btn-primary btn-block">Search</button>
										</div>
									</form>
								</li>
								<li id="others">
									<form id="more_form" role="form" class="row" method="GET" action="{{URL::route('show_search_result')}}">
										<input type="hidden" class="form-control" id="search_room_type" name="search_room_type" value="Others" />
										<div class="col-xs-12 col-sm-4 col-md-3">
											<div class="form-group">
												<label for="search_location">Location</label>
												<input type="text" class="form-control" id="search_location" placeholder="Singapore">
												<input type="hidden" class="form-control" id="locality" name="search_city" />
												<input type="hidden" class="form-control" id="administrative_area_level_1" name="search_state" />
												<input type="hidden" class="form-control" id="country" name="search_country" />
												<input type="hidden" class="form-control" id="search_lat" name="search_lat" />
												<input type="hidden" class="form-control" id="search_lng" name="search_lng" />
												<input type="hidden" class="form-control" id="search_locid" name="search_locid" />
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group">
												<label for="search_workspace_type">Workspace Type</label>
												<select class="form-control mySelectBoxClass" id="search_workspace_type" name="search_workspace_type">
													<option selected value="Private Office">Private Office</option>
													<option value="Meeting Room">Meeting Room</option>
													<option value="Conference Room">Conference Room</option>
													<option value="Boardroom">Boardroom</option>
													<option value="Deposition Room">Deposition Room</option>
													<option value="Coworking">Coworking</option>
													<option value="Cubicle">Cubicle</option>
													<option value="Training Room">Training Room</option>
													<option value="Presentation Room">Presentation Room</option>
													<option value="Remote Office">Remote Office</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-1">
											<div class="form-group">
												<label for="search_period">Period</label>
												<select class="form-control mySelectBoxClass" id="search_period" name="search_period">
													<option selected value="hourly">Hourly</option>
													<option value="daily">Daily</option>
													<option value="monthly">Monthly</option>
												</select>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="form-group datepicker-wrapper">
												<label for="search_startdate">Date</label>
												<input type="text" rel="date" class="form-control" id="search_startdate" name="search_startdate" placeholder="dd/mm/yy">
												<i class="fa fa-calendar"></i>
											</div>
										</div>
										<div class="for_hourly">
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_starttime">Start Time</label>
													<input type="text" rel="time" class="form-control " id="search_starttime" name="search_starttime" placeholder="Time">
												</div>
											</div>
											<div class="col-xs-12 col-sm-4 col-md-1">
												<div class="form-group">
													<label for="search_endtime">End Time</label>
													<input type="text" rel="time" class="form-control " id="search_endtime" name="search_endtime" placeholder="Time">
												</div>
											</div>
										</div>
										<div class="for_daily none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group datepicker-wrapper">
													<label for="search_enddate">End Date</label>
													<input type="text" rel="date" class="form-control" id="search_enddate" name="search_enddate" placeholder="dd/mm/yy">
													<i class="fa fa-calendar"></i>
												</div>
											</div>
										</div>
										<div class="for_monthly none">
											<div class="col-xs-12 col-sm-4 col-md-2">
												<div class="form-group">
													<label for="search_monthly_duration">Duration</label>
													<select class="form-control mySelectBoxClass" id="search_monthly_duration" name="search_monthly_duration">
														<option selected value="1">1 Month</option>
														<option value="2">2 Months</option>
														<option value="3">3 Months</option>
														<option value="4">4 Months</option>
														<option value="5">5 Months</option>
														<option value="6">6 Months</option>
														<option value="7">7 Months</option>
														<option value="8">8 Months</option>
														<option value="9">9 Months</option>
														<option value="10">10 Months</option>
														<option value="11">11 Months</option>
														<option value="12">12 Months</option>
													</select>
												</div>
											</div>
										</div>
										<div class="col-xs-12 col-sm-4 col-md-2">
											<div class="clear"></div>
											<button type="submit" name="submit" class="btn btn-primary btn-block">Search</button>
										</div>
									</form>
								</li>
							</ul>
						</div>
					</div>
				
				<div class="clear"></div>
				
			</div>
			
			<div class="clear"></div>
			
			<div class="white-bg">
				<div class="container">
					<div class="last-min">
						<div class="block clearfix mv-15"><strong>Quick Reservation</strong> - Day/Hourly Offices - Meeting Rooms - Coworking Space</div>
						<a href="#">More Details</a>
					</div>
				</div>
			</div>
			
			<div class="clear"></div>
			
			<div class="container">
								
				<div class="row pv-30 mt-20 product-4">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/front-01.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Find workspaces anywhere in the world</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a href="#" class="more-link">More</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/front-02.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Quick &amp; simple reservation</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<a href="#" class="more-link">More</a>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<div class="list-item">
							<div class="list-item-image">
								<div class="image-inner">
									<a class="link-image" href="page-details.html">
										<img src="images/list-items/front-03.jpg" alt="" />
										<div class="image-overlay">
											<div class="overlay-content">
												<div class="overlay-icon"><i class="icon_link"></i></div>
											</div><!--/ .extra-content-->	
										</div><!--/ .image-extra-->
									</a>	
								</div>
							</div><!-- end list-item -->
							<div class="product-4-label">
								<h3><a href="#">Available for all devices</a></h3>
								<div class="bb2 mb-20"></div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
								<div class=""><a href="#" class="more-link">More</a></div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div><!-- end content_wrapper -->
<!-- Main Content End -->

<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script type="text/javascript" src="{{asset('custom_js/picker.js')}}"></script>
<script src="{{asset('custom_js/jquery.datetimepicker.js')}}"></script>
<script>
	$(function() {
		console.log("off goes the form to search...");

		$("[rel=date]").datetimepicker({
			timepicker:false,
			format:'d/m/Y',
			minDate:0,
		});

		$("[rel=time]").datetimepicker({
			datepicker:false,
			format:'H:i',
			step: 15,
			// minTime:0,
		});

		// initialize();

		// Office Form
		$("li#office #search_period").on('change', function() {
			if ($(this).val() == "hourly") {
				$("li#office label[for=search_startdate]").text('Date');
				$("li#office .for_hourly").removeClass('none');
				$("li#office .for_daily").addClass('none');
				$("li#office .for_monthly").addClass('none');
			}
			if (($(this).val() == "daily")) {
				$("li#office label[for=search_startdate]").text('Start Date');
				$("li#office .for_hourly").addClass('none');
				$("li#office .for_daily").removeClass('none');
				$("li#office .for_monthly").addClass('none');
			}
			if ($(this).val() == "monthly") {
				$("li#office label[for=search_startdate]").text('Start Date');
				$("li#office .for_hourly").addClass('none');
				$("li#office .for_daily").addClass('none');
				$("li#office .for_monthly").removeClass('none');
			}
		});

		// Open Desk Form
		$("li#coworking #search_period").on('change', function() {
			if ($(this).val() == "hourly") {
				$("li#coworking label[for=search_startdate]").text('Date');
				$("li#coworking .for_hourly").removeClass('none');
				$("li#coworking .for_daily").addClass('none');
				$("li#coworking .for_monthly").addClass('none');
			}
			if (($(this).val() == "daily")) {
				$("li#coworking label[for=search_startdate]").text('Start Date');
				$("li#coworking .for_hourly").addClass('none');
				$("li#coworking .for_daily").removeClass('none');
				$("li#coworking .for_monthly").addClass('none');
			}
			if ($(this).val() == "monthly") {
				$("li#coworking label[for=search_startdate]").text('Start Date');
				$("li#coworking .for_hourly").addClass('none');
				$("li#coworking .for_daily").addClass('none');
				$("li#coworking .for_monthly").removeClass('none');
			}
		});

		// Other Form
		$("li#others #search_period").on('change', function() {
			if ($(this).val() == "hourly") {
				$("li#others label[for=search_startdate]").text('Date');
				$("li#others .for_hourly").removeClass('none');
				$("li#others .for_daily").addClass('none');
				$("li#others .for_monthly").addClass('none');
			}
			if (($(this).val() == "daily")) {
				$("li#others label[for=search_startdate]").text('Start Date');
				$("li#others .for_hourly").addClass('none');
				$("li#others .for_daily").removeClass('none');
				$("li#others .for_monthly").addClass('none');
			}
			if ($(this).val() == "monthly") {
				$("li#others label[for=search_startdate]").text('Start Date');
				$("li#others .for_hourly").addClass('none');
				$("li#others .for_daily").addClass('none');
				$("li#others .for_monthly").removeClass('none');
			}
		});

	});


	// Google Maps Places API Autocompletion
	var autocomplete;
	var componentForm = {
	  locality: 'long_name',
	  administrative_area_level_1: 'short_name',
	  country: 'long_name',
	};

	// Google Maps Autocomplete
	function initialize() {
			
		var options = {
			types: ['(cities)']
		}

		var input = (document.getElementById('search_location')); 
		// Create the autocomplete object, restricting the search
		// to geographical location types.
		autocomplete = new google.maps.places.Autocomplete(input, options);

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
		    fillInAddress();
		});

	}	
	
	// The START and END in square brackets define a snippet for our documentation:
	function fillInAddress() {
		// Get the place details from the autocomplete object.
		var place = autocomplete.getPlace();

		for (var component in componentForm) {
		document.getElementById(component).value = '';
		}

		// Get each component of the address from the place details
		// and fill the corresponding field on the form.
		for (var i = 0; i < place.address_components.length; i++) {
		    var addressType = place.address_components[i].types[0];
		    if (componentForm[addressType]) {
				var val = place.address_components[i][componentForm[addressType]];
				console.log(val);
				document.getElementById(addressType).value = val;
		    }
		}

		console.log(place.id);

		document.getElementById("search_locid").value = place.id;

		// Get Latitutde and Longitude of the place
	    var lat = place.geometry.location.lat();
		var lng = place.geometry.location.lng();
		
		document.getElementById("search_lat").value = lat;
		document.getElementById("search_lng").value = lng;
		
		console.log(lat);
		console.log(lng);
	}

	// Bias the autocomplete object to the user's geographical location,
	// as supplied by the browser's 'navigator.geolocation' object.
	function geolocate() {
		if (navigator.geolocation) {
	    	navigator.geolocation.getCurrentPosition(function(position) {
	      		var geolocation = new google.maps.LatLng(
	          	position.coords.latitude, position.coords.longitude);
	      		autocomplete.setBounds(new google.maps.LatLngBounds(geolocation,
	          	geolocation));
	    	});
	  	}
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
@stop
