@extends('layouts.main')

@section('page_css')
<style>
	.sidebar-item {
		background-color: #fff;
		padding: 20px;
	}
</style>
@stop


@section('content')

<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Reservation</div>
		<div class="breadcrumb clearfix">
			<a href="{{ URL::route('show_venue_details', array($room->venue_id)) }}">{{ $venue->venue_name }}</a>
			<span class="current-page">{{ $room->room_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">

		<div class="row clearfix pv-50">
			@if ($approval_status === 'already_cancelled')
				<div class="bs-example ml-15 mr-15">
					<div class="alert alert-danger fade in text-center">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						This reservation had already been <strong>cancelled!</strong>
					</div>
				</div>
			@endif
			
			<div class="col-xs-12 col-sm-8 col-md-8 xs-mb">
				<div class="white-bg payment-wrapper">
					<h2 class="section-title center mb-10"><span class="text-danger">CANCELLED</span>, reservation was cancelled!</h2>
					<p class="center mb-40">Reservation number was <span class="text-danger text-bold">{{ $reservation_code }}<span></span>
					
					<h3>Reservation Information</h3>
					<div class="bb2 mb-30"></div>
					
					<table class="table table-striped">
						<tbody>
							<tr>
								<td>First Name: </td>
								<td>{{ $user->first_name }}</td>
							</tr>
							<tr>
								<td>Last Name: </td>
								<td>{{ $user->last_name }}</td>
							</tr>
							<tr>
								<td>Room: </td>
								<td>{{ $room->room_name }}</td>
							</tr>
							<tr>
								<td>Venue: </td>
								<td>{{ $venue->venue_name }}, {{ $venue->floor }} {{ $venue->street_name }} {{ $venue->street_number}}, {{ $venue->city_name }}, {{ $venue->country_name }}</td>
							</tr>
							<tr>
								<td>Duration: </td>
								<td>{{ $real_duration }}</td>
							</tr>
							<tr>
								<td>Booking date: </td>
								<td>{{ $booking_date }}</td>
							</tr>
						</tbody>
					</table>
					
					<h3>Title</h3>
					<div class="bb2 mb-10"></div>
					<p>{{ $booking_details->title }}</p>
					<div class="clear mb-30"></div>
					
					<h3>Requested Amenities</h3>
					<div class="bb2 mb-10"></div>
					<p>CREATE GRID OF AMENITIES</p>
					<div class="clear mb-30"></div>
					
					<h3>Comments</h3>
					<div class="bb2 mb-10"></div>
					<p>{{ $booking_details->comment}}</p>
					<div class="clear mb-30"></div>
					
					<div class="bb2 mb-15"></div>
					<div class="mb-15 center">Grand Total: <span class="font24 text-danger">${{ $booking_details->price_final}}</span></div>
					<div class="bb2 mb-30"></div>
					<button type="submit" class="btn btn-primary">Print This Bill</button>
					<div class="clear"></div>
				</div>
			</div>
			
			<div class="col-xs-12 col-sm-4 col-md-4">
				<div class="sidebar hotel-sidebar">
					<div class="sidebar-item mb-20">
						<div class="summary-payment">
							<div class="summary-header mb-30">
								<img src="{{asset('images/list-items/small-item-01.jpg')}}" alt="Alternative Hotel" />
								<h3 class="no-mb">{{ $venue->venue_name }}</h3>
								<span>{{ $venue->city_name }}, {{ $venue->country_name }}</span>
								<span class="rating-static rating-45"></span>
							</div>
							<div class="bb2 mb-30"></div>
							<table class="table table-bordered table-striped mb-30">
								<tr>
									<td colspan=2><span class="text-bold">{{$booking_details->booking_duration}}</span>: <span>{{ $real_duration }}</span></td>
								</tr>
								<tr>
									<td>
										Price <em>excl. taxes</em>	
										<br/>
										Taxes &amp; Fees
										<div class="clearfix"></div>
									</td>
									<td class="center">
										<span>${{ $booking_details->price_orig }}</span><br/>
										<span>${{ $booking_details->price_tax }}</span><br/>
									</td>
								</tr>
							</table>
							
							<div class="bb2 mb-20"></div>
							
							<span class="left">Total Fee:</span>
							<span class="right text-danger text-bold font24">${{ $booking_details->price_final }}</span>
							
							<div class="clear mb-10"></div>
							
						</div>
					</div>
					
					<div class="sidebar-item mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</div><!-- end content_wrapper -->


<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')



@stop

