<div class="col-md-12">
	<div class="row white-bg padding-20">
		<div class="wizard-item">
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Venue Name</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Name
					</div>
					<div class="col-md-8">
				        <input type="text" class="form-control" id="venue_name" name="venue_name" value="{{$venue_information[0]->venue_name}}" placeholder="Default" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Description
					</div>
					<div class="col-md-8">
						<textarea class="form-control" rows="4" id="venue_description" name="venue_description" placeholder="Enter description here...">{{$venue_information[0]->description}}</textarea> 
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Venue Address</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<input id="pac-input" class="controls" type="text" placeholder="Enter a location" >
					<div id="maps-canvas">Maps</div>
					<input type="hidden" class="form-control" id="venue_lat" name="venue_lat" value="{{$venue_information[0]->lat}}"/>
					<input type="hidden" class="form-control" id="venue_lng" name="venue_lng" value="{{$venue_information[0]->lng}}"/>
					<input type="hidden" class="form-control" id="venue_timeZoneId" name="venue_timeZoneId" value="{{$venue_information[0]->zone_name}}" />
					<input type="hidden" class="form-control" id="venue_timeZoneName" name="venue_timeZoneName" />
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						Street
					</div>
					<div class="col-md-6">
						<!-- <h4><span id="route"></span> <span id="street_number"></span></h4>
						<input type="hidden" class="form-control" id="route" name="venue_street" /> -->
				        <input type="text" class="form-control" id="route" name="venue_street" value="{{$venue_information[0]->street_name}}" placeholder="enter above"/>
					</div>
					<div class="col-md-2">
						<input type="text" class="form-control" id="street_number" name="venue_street_number" value="{{$venue_information[0]->street_number}}" placeholder="Nr."/>
					</div>
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						Extra (e.g. Floor/Suite)
					</div>
					<div class="col-md-8">
					    <input type="text" class="form-control" id="venue_floor" name="venue_floor" placeholder="extra information" value="{{$venue_information[0]->floor}}"/>
					</div>
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						City
					</div>
					<div class="col-md-8">
						<!-- <h4 id="locality"></h4>
						<input type="hidden" class="form-control" id="locality" name="venue_city" /> -->
					    <input type="text" class="form-control" id="locality" name="venue_city" placeholder="enter above" value="{{$venue_information[0]->city_name}}" />
					</div>
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						State
					</div>
					<div class="col-md-8">
<!-- 						<h4 id="administrative_area_level_1"></h4>
						<input type="hidden" class="form-control" id="administrative_area_level_1" name="venue_state" /> -->
					    <input type="text" class="form-control" id="administrative_area_level_1" name="venue_state" placeholder="enter above" value="{{$venue_information[0]->state_name}}" />
					</div>
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						ZIP/Postal Code
					</div>
					<div class="col-md-8">
<!-- 						<h4 id="postal_code"></h4>
						<input type="hidden" class="form-control" id="postal_code" name="venue_zip" /> -->
					    <input type="text" class="form-control" id="postal_code" name="venue_zip" placeholder="enter above" value="{{$venue_information[0]->postal_code}}" />
					</div>
				</div>
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						Country
					</div>
					<div class="col-md-8">
<!-- 						<h4 id="country"></h4>
						<input type="hidden" class="form-control" id="country" name="venue_country" /> -->
					    <input type="text" class="form-control" id="country" name="venue_country" placeholder="enter above" value="{{$venue_information[0]->country_name}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Arrival Info
					</div>
					<div class="col-md-8">
						<textarea class="form-control" rows="4" id="venue_arrival_info" name="venue_arrival_info" placeholder="Enter description here...">{{$venue_information[0]->arrival_info}}</textarea> 
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h3>Venue Contacts</h3>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Contact Person
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_contact_name" name="venue_contact_name" placeholder="name of the contact person" value="{{$venue_information[0]->contact_person}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Telephone
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_telephone" name="venue_telephone" placeholder="+XX XXXXXXXXX" value="{{$venue_information[0]->phone_office}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Fax
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_fax" name="venue_fax" placeholder="+XX XXXXXXXXX" value="{{$venue_information[0]->fax_office}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Email
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_email" name="venue_email" placeholder="example@example.com" value="{{$venue_information[0]->email}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Company Website
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_website" name="venue_website" placeholder="http://www..." value="{{$venue_information[0]->website_url}}" />
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h4>Time Information</h4>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element address-field">
					<div class="col-md-4">
						Time Zone
					</div>
					<div class="col-md-8">
						<h5 id="timezone">{{$venue_information[0]->zone_name}}</h5>
					</div>
				</div>
<!-- 				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Time Zone
					</div>
					<div class="col-md-8">
						<h4 id="timezone"></h4>
					    <select id="timezone" name="venue_timezone" class="form-control mySelectBoxClass">
					    	<option timeZoneId="1" gmtAdjustment="GMT-12:00" useDaylightTime="0" value="-12">(GMT-12:00) International Date Line West</option>
					    	<option timeZoneId="2" gmtAdjustment="GMT-11:00" useDaylightTime="0" value="-11">(GMT-11:00) Midway Island, Samoa</option>
					    	<option timeZoneId="3" gmtAdjustment="GMT-10:00" useDaylightTime="0" value="-10">(GMT-10:00) Hawaii</option>
					    	<option timeZoneId="4" gmtAdjustment="GMT-09:00" useDaylightTime="1" value="-9">(GMT-09:00) Alaska</option>
					    	<option timeZoneId="5" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Pacific Time (US & Canada)</option>
					    	<option timeZoneId="6" gmtAdjustment="GMT-08:00" useDaylightTime="1" value="-8">(GMT-08:00) Tijuana, Baja California</option>
					    	<option timeZoneId="7" gmtAdjustment="GMT-07:00" useDaylightTime="0" value="-7">(GMT-07:00) Arizona</option>
					    	<option timeZoneId="8" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Chihuahua, La Paz, Mazatlan</option>
					    	<option timeZoneId="9" gmtAdjustment="GMT-07:00" useDaylightTime="1" value="-7">(GMT-07:00) Mountain Time (US & Canada)</option>
					    	<option timeZoneId="10" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Central America</option>
					    	<option timeZoneId="11" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Central Time (US & Canada)</option>
					    	<option timeZoneId="12" gmtAdjustment="GMT-06:00" useDaylightTime="1" value="-6">(GMT-06:00) Guadalajara, Mexico City, Monterrey</option>
					    	<option timeZoneId="13" gmtAdjustment="GMT-06:00" useDaylightTime="0" value="-6">(GMT-06:00) Saskatchewan</option>
					    	<option timeZoneId="14" gmtAdjustment="GMT-05:00" useDaylightTime="0" value="-5">(GMT-05:00) Bogota, Lima, Quito, Rio Branco</option>
					    	<option timeZoneId="15" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Eastern Time (US & Canada)</option>
					    	<option timeZoneId="16" gmtAdjustment="GMT-05:00" useDaylightTime="1" value="-5">(GMT-05:00) Indiana (East)</option>
					    	<option timeZoneId="17" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Atlantic Time (Canada)</option>
					    	<option timeZoneId="18" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Caracas, La Paz</option>
					    	<option timeZoneId="19" gmtAdjustment="GMT-04:00" useDaylightTime="0" value="-4">(GMT-04:00) Manaus</option>
					    	<option timeZoneId="20" gmtAdjustment="GMT-04:00" useDaylightTime="1" value="-4">(GMT-04:00) Santiago</option>
					    	<option timeZoneId="21" gmtAdjustment="GMT-03:30" useDaylightTime="1" value="-3.5">(GMT-03:30) Newfoundland</option>
					    	<option timeZoneId="22" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Brasilia</option>
					    	<option timeZoneId="23" gmtAdjustment="GMT-03:00" useDaylightTime="0" value="-3">(GMT-03:00) Buenos Aires, Georgetown</option>
					    	<option timeZoneId="24" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Greenland</option>
					    	<option timeZoneId="25" gmtAdjustment="GMT-03:00" useDaylightTime="1" value="-3">(GMT-03:00) Montevideo</option>
					    	<option timeZoneId="26" gmtAdjustment="GMT-02:00" useDaylightTime="1" value="-2">(GMT-02:00) Mid-Atlantic</option>
					    	<option timeZoneId="27" gmtAdjustment="GMT-01:00" useDaylightTime="0" value="-1">(GMT-01:00) Cape Verde Is.</option>
					    	<option timeZoneId="28" gmtAdjustment="GMT-01:00" useDaylightTime="1" value="-1">(GMT-01:00) Azores</option>
					    	<option timeZoneId="29" gmtAdjustment="GMT+00:00" useDaylightTime="0" value="0">(GMT+00:00) Casablanca, Monrovia, Reykjavik</option>
					    	<option timeZoneId="30" gmtAdjustment="GMT+00:00" useDaylightTime="1" value="0">(GMT+00:00) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London</option>
					    	<option timeZoneId="31" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</option>
					    	<option timeZoneId="32" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague</option>
					    	<option timeZoneId="33" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Brussels, Copenhagen, Madrid, Paris</option>
					    	<option timeZoneId="34" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb</option>
					    	<option timeZoneId="35" gmtAdjustment="GMT+01:00" useDaylightTime="1" value="1">(GMT+01:00) West Central Africa</option>
					    	<option timeZoneId="36" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Amman</option>
					    	<option timeZoneId="37" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Athens, Bucharest, Istanbul</option>
					    	<option timeZoneId="38" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Beirut</option>
					    	<option timeZoneId="39" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Cairo</option>
					    	<option timeZoneId="40" gmtAdjustment="GMT+02:00" useDaylightTime="0" value="2">(GMT+02:00) Harare, Pretoria</option>
					    	<option timeZoneId="41" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius</option>
					    	<option timeZoneId="42" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Jerusalem</option>
					    	<option timeZoneId="43" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Minsk</option>
					    	<option timeZoneId="44" gmtAdjustment="GMT+02:00" useDaylightTime="1" value="2">(GMT+02:00) Windhoek</option>
					    	<option timeZoneId="45" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Kuwait, Riyadh, Baghdad</option>
					    	<option timeZoneId="46" gmtAdjustment="GMT+03:00" useDaylightTime="1" value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</option>
					    	<option timeZoneId="47" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Nairobi</option>
					    	<option timeZoneId="48" gmtAdjustment="GMT+03:00" useDaylightTime="0" value="3">(GMT+03:00) Tbilisi</option>
					    	<option timeZoneId="49" gmtAdjustment="GMT+03:30" useDaylightTime="1" value="3.5">(GMT+03:30) Tehran</option>
					    	<option timeZoneId="50" gmtAdjustment="GMT+04:00" useDaylightTime="0" value="4">(GMT+04:00) Abu Dhabi, Muscat</option>
					    	<option timeZoneId="51" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Baku</option>
					    	<option timeZoneId="52" gmtAdjustment="GMT+04:00" useDaylightTime="1" value="4">(GMT+04:00) Yerevan</option>
					    	<option timeZoneId="53" gmtAdjustment="GMT+04:30" useDaylightTime="0" value="4.5">(GMT+04:30) Kabul</option>
					    	<option timeZoneId="54" gmtAdjustment="GMT+05:00" useDaylightTime="1" value="5">(GMT+05:00) Yekaterinburg</option>
					    	<option timeZoneId="55" gmtAdjustment="GMT+05:00" useDaylightTime="0" value="5">(GMT+05:00) Islamabad, Karachi, Tashkent</option>
					    	<option timeZoneId="56" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Sri Jayawardenapura</option>
					    	<option timeZoneId="57" gmtAdjustment="GMT+05:30" useDaylightTime="0" value="5.5">(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi</option>
					    	<option timeZoneId="58" gmtAdjustment="GMT+05:45" useDaylightTime="0" value="5.75">(GMT+05:45) Kathmandu</option>
					    	<option timeZoneId="59" gmtAdjustment="GMT+06:00" useDaylightTime="1" value="6">(GMT+06:00) Almaty, Novosibirsk</option>
					    	<option timeZoneId="60" gmtAdjustment="GMT+06:00" useDaylightTime="0" value="6">(GMT+06:00) Astana, Dhaka</option>
					    	<option timeZoneId="61" gmtAdjustment="GMT+06:30" useDaylightTime="0" value="6.5">(GMT+06:30) Yangon (Rangoon)</option>
					    	<option timeZoneId="62" gmtAdjustment="GMT+07:00" useDaylightTime="0" value="7">(GMT+07:00) Bangkok, Hanoi, Jakarta</option>
					    	<option timeZoneId="63" gmtAdjustment="GMT+07:00" useDaylightTime="1" value="7">(GMT+07:00) Krasnoyarsk</option>
					    	<option timeZoneId="64" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi</option>
					    	<option timeZoneId="65" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Kuala Lumpur, Singapore</option>
					    	<option timeZoneId="66" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Irkutsk, Ulaan Bataar</option>
					    	<option timeZoneId="67" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Perth</option>
					    	<option timeZoneId="68" gmtAdjustment="GMT+08:00" useDaylightTime="0" value="8">(GMT+08:00) Taipei</option>
					    	<option timeZoneId="69" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</option>
					    	<option timeZoneId="70" gmtAdjustment="GMT+09:00" useDaylightTime="0" value="9">(GMT+09:00) Seoul</option>
					    	<option timeZoneId="71" gmtAdjustment="GMT+09:00" useDaylightTime="1" value="9">(GMT+09:00) Yakutsk</option>
					    	<option timeZoneId="72" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Adelaide</option>
					    	<option timeZoneId="73" gmtAdjustment="GMT+09:30" useDaylightTime="0" value="9.5">(GMT+09:30) Darwin</option>
					    	<option timeZoneId="74" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Brisbane</option>
					    	<option timeZoneId="75" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Canberra, Melbourne, Sydney</option>
					    	<option timeZoneId="76" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Hobart</option>
					    	<option timeZoneId="77" gmtAdjustment="GMT+10:00" useDaylightTime="0" value="10">(GMT+10:00) Guam, Port Moresby</option>
					    	<option timeZoneId="78" gmtAdjustment="GMT+10:00" useDaylightTime="1" value="10">(GMT+10:00) Vladivostok</option>
					    	<option timeZoneId="79" gmtAdjustment="GMT+11:00" useDaylightTime="1" value="11">(GMT+11:00) Magadan, Solomon Is., New Caledonia</option>
					    	<option timeZoneId="80" gmtAdjustment="GMT+12:00" useDaylightTime="1" value="12">(GMT+12:00) Auckland, Wellington</option>
					    	<option timeZoneId="81" gmtAdjustment="GMT+12:00" useDaylightTime="0" value="12">(GMT+12:00) Fiji, Kamchatka, Marshall Is.</option>
					    	<option timeZoneId="82" gmtAdjustment="GMT+13:00" useDaylightTime="0" value="13">(GMT+13:00) Nuku'alofa</option>
					    </select>
					</div>
				</div> -->
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Opening Hours
					</div>
					<div class="col-md-8">
					    <table class="table table-striped table-hover time-table">
					    	<tr>
					    		<td class="table-days">Monday</td>
					    		<td><select name="venue_startmonday" class="form-control mySelectBoxClass" style="width:70px">
										<option value="{{ substr($venue_hours[1]->start_time, 0, 2)}}">{{ substr($venue_hours[1]->start_time, 0, 5) }}</option>
						    			<option value="00">00:00</option>
										<option value="01">01:00</option>
										<option value="02">02:00</option>
										<option value="03">03:00</option>
										<option value="04">04:00</option>
										<option value="05">05:00</option>
										<option value="06">06:00</option>
										<option value="07">07:00</option>
										<option value="08">08:00</option>
										<option value="09">09:00</option>
										<option value="10">10:00</option>
										<option value="11">11:00</option>
										<option value="12">12:00</option>
										<option value="13">13:00</option>
										<option value="14">14:00</option>
										<option value="15">15:00</option>
										<option value="16">16:00</option>
										<option value="17">17:00</option>
										<option value="18">18:00</option>
										<option value="19">19:00</option>
										<option value="20">20:00</option>
										<option value="21">21:00</option>
										<option value="22">22:00</option>
										<option value="23">23:00</option>
										<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endmonday" class="form-control mySelectBoxClass" style="width:70px">
					    				<option value="{{ substr($venue_hours[1]->end_time, 0, 2) }}">{{ substr($venue_hours[1]->end_time, 0, 5)}}</option>
										<option value="00">00:00</option>
										<option value="01">01:00</option>
										<option value="02">02:00</option>
										<option value="03">03:00</option>
										<option value="04">04:00</option>
										<option value="05">05:00</option>
										<option value="06">06:00</option>
										<option value="07">07:00</option>
										<option value="08">08:00</option>
										<option value="09">09:00</option>
										<option value="10">10:00</option>
										<option value="11">11:00</option>
										<option value="12">12:00</option>
										<option value="13">13:00</option>
										<option value="14">14:00</option>
										<option value="15">15:00</option>
										<option value="16">16:00</option>
										<option value="17">17:00</option>
										<option value="18">18:00</option>
										<option value="19">19:00</option>
										<option value="20">20:00</option>
										<option value="21">21:00</option>
										<option value="22">22:00</option>
										<option value="23">23:00</option>
										<option value="24">24:00</option>
					    			</select>
					    		</td>
								<td><label class="checkbox" for="venue_closedmonday">
									<input type="checkbox" value="0" id="venue_closedmonday" name="venue_closedmonday" data-toggle="checkbox" @if ($venue_hours[1]->closed != null) checked @endif >
									Closed
									</label></td>
								  <!-- <input type="checkbox" value="0" id="venue_closedmonday" name="venue_closedmonday" data-toggle="checkbox" @if ($venue_hours[1]->closed != null) checked @endif >
								  Closed
								</label></td> -->
					    	</tr>
					    	<tr>
					    		<td class="table-days">Tuesday</td>
					    		<td><select id="venue_starttuesday" name="venue_starttuesday" class="form-control mySelectBoxClass">
					    			<option value="{{ substr($venue_hours[2]->start_time, 0 ,2) }}">{{ substr($venue_hours[2]->start_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td>
					    			<select name="venue_endtuesday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[2]->end_time, 0, 2) }}">{{substr($venue_hours[2]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedtuesday">
					    		  <input type="checkbox" value="0" id="venue_closedtuesday" name="venue_closedtuesday" data-toggle="checkbox" @if ($venue_hours[2]->closed != null) checked @endif>
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Wednesday</td>
					    		<td><select name="venue_startwednesday" class="form-control mySelectBoxClass">
					    			<option value="{{ substr($venue_hours[3]->start_time, 0 ,2) }}">{{ substr($venue_hours[3]->start_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endwednesday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[3]->end_time, 0, 2) }}">{{substr($venue_hours[3]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedwednesday">
					    		  <input type="checkbox" value="0" id="venue_closedwednesday" name="venue_closedwednesday" data-toggle="checkbox" @if ($venue_hours[3]->closed != null) checked @endif >
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Thursday</td>
					    		<td><select name="venue_startthursday" class="form-control mySelectBoxClass">
					    			<option value="{{ substr($venue_hours[4]->start_time, 0 ,2) }}">{{ substr($venue_hours[4]->start_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endthursday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[4]->end_time, 0, 2) }}">{{substr($venue_hours[4]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedthursday">
					    		  <input type="checkbox" value="0" id="venue_closedthursday" name="venue_closedthursday" data-toggle="checkbox" @if ($venue_hours[4]->closed != null) checked @endif>
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Friday</td>
					    		<td><select name="venue_startfriday" class="form-control mySelectBoxClass">
					    			<option value="{{ substr($venue_hours[5]->start_time, 0 ,2) }}">{{ substr($venue_hours[5]->start_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endfriday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[5]->end_time, 0, 2) }}">{{substr($venue_hours[5]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedfriday">
					    		  <input type="checkbox" value="0" id="venue_closedfriday" name="venue_closedfriday" data-toggle="checkbox" @if ($venue_hours[5]->closed != null) checked @endif>
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Saturday</td>
					    		<td><select name="venue_startsaturday" class="form-control mySelectBoxClass">
					    			<option value="{{ substr($venue_hours[6]->start_time, 0 ,2) }}">{{ substr($venue_hours[6]->start_time, 0, 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endsaturday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[6]->end_time, 0, 2) }}">{{substr($venue_hours[6]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedsaturday">
					    		  <input type="checkbox" value="0" id="venue_closedsaturday" name="venue_closedsaturday" data-toggle="checkbox" @if ($venue_hours[6]->closed != null) checked @endif>
					    		  Closed
					    		</label></td>
					    	</tr>
					    	<tr>
					    		<td class="table-days">Sunday</td>
					    		<td><select name="venue_startsunday" class="form-control mySelectBoxClass">
										<option value="{{ substr($venue_hours[0]->start_time, 0 ,2) }}">{{ substr($venue_hours[0]->start_time, 0, 5) }}</option>
										<option value="00">00:00</option>
										<option value="01">01:00</option>
										<option value="02">02:00</option>
										<option value="03">03:00</option>
										<option value="04">04:00</option>
										<option value="05">05:00</option>
										<option value="06">06:00</option>
										<option value="07">07:00</option>
										<option value="08">08:00</option>
										<option value="09">09:00</option>
										<option value="10">10:00</option>
										<option value="11">11:00</option>
										<option value="12">12:00</option>
										<option value="13">13:00</option>
										<option value="14">14:00</option>
										<option value="15">15:00</option>
										<option value="16">16:00</option>
										<option value="17">17:00</option>
										<option value="18">18:00</option>
										<option value="19">19:00</option>
										<option value="20">20:00</option>
										<option value="21">21:00</option>
										<option value="22">22:00</option>
										<option value="23">23:00</option>
										<option value="24">24:00</option>
					    			</select></td><td class="table-days"> - </td>
					    			<td><select name="venue_endsunday" class="form-control mySelectBoxClass">
					    				<option value="{{ substr($venue_hours[0]->end_time, 0, 2) }}">{{substr($venue_hours[0]->end_time, 0 , 5) }}</option>
					    			<option value="00">00:00</option>
									<option value="01">01:00</option>
									<option value="02">02:00</option>
									<option value="03">03:00</option>
									<option value="04">04:00</option>
									<option value="05">05:00</option>
									<option value="06">06:00</option>
									<option value="07">07:00</option>
									<option value="08">08:00</option>
									<option value="09">09:00</option>
									<option value="10">10:00</option>
									<option value="11">11:00</option>
									<option value="12">12:00</option>
									<option value="13">13:00</option>
									<option value="14">14:00</option>
									<option value="15">15:00</option>
									<option value="16">16:00</option>
									<option value="17">17:00</option>
									<option value="18">18:00</option>
									<option value="19">19:00</option>
									<option value="20">20:00</option>
									<option value="21">21:00</option>
									<option value="22">22:00</option>
									<option value="23">23:00</option>
									<option value="24">24:00</option>
					    			</select>
					    		</td>
					    		<td><label class="checkbox" for="venue_closedsunday">
					    		  <input type="checkbox" value="0" id="venue_closedsunday" name="venue_closedsunday" data-toggle="checkbox" @if ($venue_hours[0]->closed != null) checked @endif>
					    		  Closed
					    		</label></td>
					    	</tr>
					    </table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h4>Social Information</h4>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Twitter
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_twitter" name="venue_twitter" placeholder="@twitter_handle" value="{{$venue_information[0]->twitter_url}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Facebook
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_facebook" name="venue_facebook" placeholder="link to Facebook page" value="{{$venue_information[0]->facebook_url}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						LinkedIn
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_linkedin" name="venue_linkedin" placeholder="link to LinkedIn page" value="{{$venue_information[0]->linkedin_url}}" />
					</div>
				</div>
				<div class="clearfix wizard-element">
					<div class="col-md-4">
						Google Plus
					</div>
					<div class="col-md-8">
						<input type="text" class="form-control" id="venue_googleplus" name="venue_googleplus" placeholder="link to Google+ page" value="{{$venue_information[0]->googleplus_url}}" />
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>