<div class="col-md-12">
	<div class="row white-bg padding-20">
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-6 col-md-3">
			    <h4>Calendar Information</h4>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-9">
				<div class="clearfix wizard-element">
					<div class="col-md-5">
						Express Calendar Username
					</div>
					<div class="col-md-7">
						<h5>{{$venue->username}}</h5>
						<input type="hidden" id="venue_calendar_username" name="venue_calendar_username" value="{{$venue->username}}">
					</div>
				</div>
				@if ($venue->digesta1 == "")
					<div class="clearfix wizard-element">
						<div class="col-md-5">
							Express Calendar Password
						</div>
						<div class="col-md-7">
							<input type="password" class="form-control" id="venue_calendar_password" name="venue_calendar_password" placeholder="type calendar password" />
						</div>
					</div>
					<div class="clearfix wizard-element">
						<div class="col-md-5">
							Confirm Password
						</div>
						<div class="col-md-7">
							<input type="password" class="form-control" id="venue_calendar_confirm_password" name="venue_calendar_confirm_password" placeholder="type calendar password again" />
						</div>
					</div>
				@else
					<div id="old_pass">
						<div class="clearfix wizard-element">
							<div class="col-md-5">
								Express Calendar Password
							</div>
							<div class="col-md-7">
								******** <button id="change_pass" class="btn btn-sm btn-info ml-15">Change</button>
							</div>
						</div>
					</div>
					<div id="new_pass">
						<div class="clearfix wizard-element">
							<div class="col-md-5">
								Express Calendar Password
							</div>
							<div class="col-md-7">
								<input type="password" class="form-control" id="venue_calendar_password" name="venue_calendar_password" placeholder="type calendar password" />
							</div>
						</div>
						<div class="clearfix wizard-element">
							<div class="col-md-5">
								Confirm Password
							</div>
							<div class="col-md-7">
								<input type="password" class="form-control" id="venue_calendar_confirm_password" name="venue_calendar_confirm_password" placeholder="type calendar password again" />
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>