@extends('layouts.main')


@section('page_css')
<link rel="stylesheet" type="text/css" href="{{asset('css/weathericons.css')}}" media="screen" />
<link rel="stylesheet" type="text/css" href="{{asset('custom_css/jquery.datetimepicker.css')}}" />
<style>
	.sidebar-item {
		background-color: #fff;
		padding: 20px;
	}

	/** Petty Search Bar **/
	.petty-search-wrapper {
		position: relative;
	}
	.petty-search-wrapper i {
		position: absolute;
		right: 10px;
		top: 5px;
	}

	.availability .petty-search-wrapper i {
		top: 28px;
	}
	.availability .petty-search-wrapper {
		width: 150px;
		float: left;
	}

	/** Amenities List **/
	ul.amenities {
		margin: 0px 10px 0 0;
		padding: 0px;
		list-style: none;
		float: right;	
	}
	/*.list-style ul.amenities {
		position: absolute;
		bottom: 48px;
		right:  110px;
	}*/
	.amenities li{
		width:28px; 
		height:28px; 
		/*background:url('') #fff; border:2px solid #efefef;*/
		/*border:1px solid #ebebeb;*/
		padding:0px;
		color:#999;
		/*-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	*/
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}
	.amenities li:hover{}


	/** Booking Calendar Board **/
	ul.booking {
		margin: 35px 30px 20px 0;
		padding: 0px;
		list-style: none;
		float: right;
		/*position: absolute;
		bottom: 10px;
		right:  165px;*/
	}
	.booking li{
		width:18px; 
		height:18px; 
		background-color: #ccc;
		border:1px solid #ebebeb;
		padding:5px;
		color:#999;
		-webkit-transition:.2s;-moz-transition:.2s;transition:.2s;	
		float:left;
		margin-left:2px;
		margin-bottom:2px;
	}

	.booking li.lastblock {
		margin-right: 5px;
	}

	.booking li.freetime{
		background-color: #A2BF8A;
	}

	.booking li.busytime {
		background-color: #DB524B;
	}
</style>
@stop


@section('content')
<div id="page_title">
	<div class="container clearfix">
		<div class="page-name">Venue</div>
		<div class="breadcrumb clearfix">
			<span class="current-page">{{ $venue->venue_name }}</span>
		</div>
	</div>
</div><!-- end slider_wrapper -->

<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-50">

			<div class="col-xs-12 col-sm-8 col-md-9">

				<div id="detail-slider">

					<div>
						<div id="exposure"></div>
						<div class="clear"></div>	
					</div>

					<div class="panel">	
						<div id="left"><a href="javascript:void(0);" class="left-arrow"></a></div>				
						<ul id="details-slider-images">
						<li><a href="{{ asset('images/details-slider/slide-01-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-01-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-02-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-02-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-03-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-03-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-04-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-04-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-05-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-05-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-06-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-06-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-07-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-07-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						<li><a href="{{ asset('images/details-slider/slide-08-large.jpg') }}"><img src="{{ asset('images/details-slider/slide-08-thumb.jpg') }}" alt="Detailts" title="Donec sollicitudin mi sit amet mauris elementum quam ullamcorper ante." /></a></li>
						</ul>
						<div id="right"><a href="javascript:void(0);" class="right-arrow"></a></div>
						<div class="clear"></div>

					</div>

				</div>

				<div class="clear mb-20"></div>

				<ul class="tabs">
					<li><a href="#details-tab1" class="active">Overview</a></li>
					<li><a href="#details-tab2">Rooms</a></li>
					<li onclick="loadScript()"><a href="#details-tab3">Map</a></li>
					<li><a href="#details-tab4">How to arrive</a></li>
					<li><a href="#details-tab5">Reviews</a></li>
				</ul><!-- tabs -->

				<ul id="detail-tab" class="tabs-content xss-mb">
					<li id="details-tab1">
						<p>{{ $venue->description }}</p>

						<ul class="address">
							<li><i class="fa-map-marker"></i>@if ($venue->floor != "") {{ $venue->floor }} , @endif {{ $venue->street_name }} {{ $venue->street_number }}, {{ $venue->city_name }}, {{ $venue->postal_code }}, {{ $venue->state_name }}, {{ $venue->country_name }}</li>
							<li><i class="fa-phone"></i> {{ $venue->phone_office }}</li>
							<li><i class="fa-print"></i> {{ $venue->fax_office }}</li>
							<li><i class="fa-globe"></i> <a href="#">{{ $venue->website_url }}</a></li>
						</ul>    
						<div class="clear mb-20"></div>
					</li><!-- tab content -->

					<li id="details-tab2" class="active">
						<div class="availability">
							<div class="availability-inner">
								<div class="form-group petty-search-wrapper mr-10">
									<label for="search_date">Date</label>
									<input type="text" class="form-control" id="search_date" name="search_date" placeholder="dd/mm/yyyy">
									<i class="fa fa-calendar"></i>
								</div>						
								<div class="form-group petty-search-wrapper mr-20">
									<label for="search_time">Time</label>
									<div id="time">
										<input type="text" class="form-control" id="search_time" name="search_time" placeholder="11:00" />
									</div>
								</div>							
								<div class="form-group petty-search-wrapper mr-20">
									<label for="search_duration">Duration</label>
									<select class="form-control mySelectBoxClass" id="search_duration" name="search_duration">
										<option value="1" selected>1 Hr</option>
										<option value="2">2 Hrs</option>
										<option value="3">3 Hrs</option>
										<option value="4">4 Hrs</option>
										<option value="5">1 Day</option>
									</select>
								</div>
								<div class="form-group petty-search-wrapper mr-20">
									<label for="search_person">Persons</label>
									<select class="form-control mySelectBoxClass" id="search_person" name="search_person">
										<option value="1" selected>1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">&gt; 5</option>
									</select>
								</div>
								<div class="update">
									<button type="button" class="btn btn-info">Update <i class="fa-check"></i></button>
								</div>
							</div>
						</div>
						<div class="clear mb-30"></div>
						<h2>Rooms</h2>
						<ul class="detail-item">
							@foreach ($rooms as $room)
								<li id="{{ $room->id }}" class="clearfix">
									<img src="{{ asset('images/room-types/room-type-01.jpg') }}" alt="room type" />
									<div class="br2">
										<h3 id="room_name_{{ $room->id }}"><a href="{{ URL::route('show_room_details', array($room->id)) }}">{{ $room->room_name }}</a></h3>
										<span>Room Type: {{ $room->room_type_name }}</span><br />
										<span>Max Capacity: {{ $room->max_capacity }}</span>
										<ul class="amenities">
											@if (($room->public_wifi == '0') || ($room->secure_wifi == '0')) <li><span id="amenities_wifi" rel="tooltip" data-toggle="tooltip" title="WiFi" class="fa fa-rss"></span></li> @endif
											@if ($room->wired_internet == '0') <li><span id="amenities_wired_internet" rel="tooltip" data-toggle="tooltip" title="Wired Internet" class="fa fa-sitemap"></span></li> @endif
											@if ($room->flatscreen == '0') <li><span id="amenities_flatscreen" rel="tooltip" data-toggle="tooltip" title="Flatscreen" class="fa fa-desktop"></span></li> @endif
											@if ($room->video_conferencing == '0') <li><span id="amenities_video_conferencing" rel="tooltip" data-toggle="tooltip" title="Video Conferencing" class="fa fa-video-camera"></span></li> @endif
											@if (($room->flipboard == '0') || ($room->whiteboard == '0')) <li><span id="amenities_flipboard" rel="tooltip" data-toggle="tooltip" title="Flipboard/Whiteboard" class="fa fa-clipboard"></span></li> @endif
											@if (($room->phone_room == '0') || ($room->conference_phone == '0')) <li><span id="amenities_phone" rel="tooltip" data-toggle="tooltip" title="Phone Room/Conference Phone" class="fa fa-phone-square"></span></li> @endif
											@if ($room->projector == '0') <li><span id="amenities_projector" rel="tooltip" data-toggle="tooltip" title="Projector" class="fa fa-picture-o"></span></li> @endif
											@if ($room->print_scan_copy == '0') <li><span id="amenities_print_copy_fax" rel="tooltip" data-toggle="tooltip" title="Print/Copy/Fax Service" class="fa fa-print"></span></li> @endif
											@if (($room->coffee_tea == '0') || ($room->filtered_water == '0')) <li><span id="amenities_tea_coffee" rel="tooltip" data-toggle="tooltip" title="Tea/Coffee/Filtered Water" class="fa fa-coffee"></span></li> @endif
											@if (($room->on_site_restaurant == '0') || ($room->catering == '0') || ($room->shared_kitchen == '0')) <li><span id="amenities_on_site_resturant" rel="tooltip" data-toggle="tooltip" title="On-Site Resturant/Catering/Shared Kitchen" class="fa fa-cutlery"></span></li> @endif
											@if ($room->handicap_accessible == '0') <li><span id="amenities_handicap_accessible" rel="tooltip" data-toggle="tooltip" title="Handical Accessible" class="fa fa-wheelchair"></span></li> @endif
										</ul>
										<ul id="daycal_{{ $room->id }}" class="booking">
								    		<li class="6am" rel="tooltip" data-toggle="tooltip" title="6 am"></li>
								    		<li class="7am" rel="tooltip" data-toggle="tooltip" title="7 am"></li>
								    		<li class="8am lastblock" rel="tooltip" data-toggle="tooltip" title="8 am"></li>
								    		<li class="9am" rel="tooltip" data-toggle="tooltip" title="9 am"></li>
								    		<li class="10am" rel="tooltip" data-toggle="tooltip" title="10 am"></li>
								    		<li class="11am lastblock" rel="tooltip" data-toggle="tooltip" title="11 am"></li>
								    		<li class="12pm" rel="tooltip" data-toggle="tooltip" title="12pm"></li>
								    		<li class="1pm" rel="tooltip" data-toggle="tooltip" title="1 pm"></li>
								    		<li class="2pm lastblock" rel="tooltip" data-toggle="tooltip" title="2 pm" ></li>
								    		<li class="3pm" rel="tooltip" data-toggle="tooltip" title="3 pm"></li>
								    		<li class="4pm" rel="tooltip" data-toggle="tooltip" title="4 pm"></li>
								    		<li class="5pm lastblock" rel="tooltip" data-toggle="tooltip" title="5 pm" ></li>
								    		<li class="6pm" rel="tooltip" data-toggle="tooltip" title="6 pm"></li>
								    		<li class="7pm" rel="tooltip" data-toggle="tooltip" title="7 pm"></li>
								    		<li class="8pm lastblock" rel="tooltip" data-toggle="tooltip" title="8 pm" ></li>
								    		<li class="9pm" rel="tooltip" data-toggle="tooltip" title="9 pm"></li>
								    		<li class="10pm" rel="tooltip" data-toggle="tooltip" title="10 pm"></li>
								    		<li class="11pm" rel="tooltip" data-toggle="tooltip" title="11 pm"></li>
								     	</ul>
										<div class="clearfix"></div>
									</div>
									<div class="detail-meta">
										<div class="detail-price" style="font-size: 16px;">
											$30 <span style="display:inline;">/Hour</span><br />
											$150 <span style="display:inline; padding-right:15px;">/Day</span>
										</div>
										<span id="room_status" class="text-danger text-bold" style="padding-bottom:5px;">&nbsp;</span>
										<a data-toggle="modal" href="#bookModal"><button id="book-now" class="btn btn-info">Book Now!</button></a>
									</div>
								</li>
							@endforeach
						</ul>
					</li><!-- tab content -->
							
					<li id="details-tab3">
						<div id="maps-canvas" style="height:400px;"></div>   
					</li><!-- tab content -->
							
					<li id="details-tab4">
						<h4>How to arrive</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, aliquid, error odit saepe amet ipsa ab dignissimos ipsam magni unde quisquam repellendus. Deserunt, repellat labore dolorum in perferendis nam adipisci! </p>
					</li><!-- tab content -->
							
					<li id="details-tab5">
						<div class="availability">
							<div class="availability-inner">
								<div class="row mb-20">
									<div class="col-md-6">
										<p class="ave-rate clearfix mt-10"><span>4.5/5</span> Lorem ipsum dolor sit amet, conse ctetur adipiscing elit Aliquam.</p>
										<span class="rating-static rating-40"></span>
									</div>
									<div class="col-md-6">
										<div class="progress-bar stripes">
											<span rel="85" style="background-color: #F07F7F"></span>
											<div class="progress-bar-text">Overall <span>4.5 out of 5</span></div>
										</div>
										<div class="progress-bar stripes mb-10">
											<span rel="90" style="background-color: #69D869"></span>
											<div class="progress-bar-text">Guests recommend <span>90%</span></div>
										</div>
										<div class="clear"></div>
										Ratings based on 5 Verified Reviews
									</div>	
								</div>
							</div>
						</div>
						<div class="clear mb-30"></div>								
						<h4 class="mb-20">Average ratings</h4>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-2">
								Cleanliness <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-2">
								Room Comfort <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-2">
								Location <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-2">
								Serivce & Staff <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-2">
								Sleep Quality <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-2">
								Value for Price <br/>
								<span class="rating-static rating-40 no-margin"></span>
							</div>
						</div>
						<div class="clear mb-30"></div>								
						<div class="bb2 mb-20"></div>
						<h4 class="mb-20">Guests Review</h4>
						<ul class="user-review">
							<li>
								<div class="left">
									<img class="avatar" src="{{ asset('images/testimonial/testimonial-02.j') }}pg" alt="user avatar" />
									<span class="name">Umar Ibn Haseem</span>
									<span class="from">from London, UK</span>
									<span class="colored-text"><i class="fa-check"></i> Recommended for Everyone</span>
								</div>
								<div class="right">
									<h5>Great experience</h5>
									<span class="date">Posted Jun 02, 2013</span>
									<div class="gap5"></div>
									<p>Now months esteem oppose nearer enable too six. She numerous unlocked you perceive speedily.</p>	
									<ul class="circle-list">
										<li>4.5</li>
										<li>3.0</li>
										<li>4.0</li>
										<li>5.0</li>
										<li>3.0</li>
										<li>4.0</li>
									</ul>
								</div>
							</li>
							<li>
								<div class="left">
									<img class="avatar" src="{{ asset('images/testimonial/testimonial-02.j') }}pg" alt="user avatar" />
									<span class="name">Umar Ibn Haseem</span>
									<span class="from">from London, UK</span>
									<span class="colored-text"><i class="fa-check"></i> Recommended for Everyone</span>
								</div>
								<div class="right">
									<h5>Great experience</h5>
									<span class="date">Posted Jun 02, 2013</span>
									<div class="gap5"></div>
									<p>Breakfast agreeable incommode departure it an. By ignorant at on wondered relation. Enough at tastes really so cousin am of. Extensive therefore supported by extremity of contented. Is pursuit compact demesne invited elderly be. View him she roof tell her case has sigh. Moreover is possible he admitted sociable concerns. By in cold no less been sent hard hill.</p>	
									<ul class="circle-list">
										<li>4.5</li>
										<li>3.0</li>
										<li>4.0</li>
										<li>5.0</li>
										<li>3.0</li>
										<li>4.0</li>
									</ul>
								</div>
							</li>
							<li>
								<div class="left">
									<img class="avatar" src="{{ asset('images/testimonial/testimonial-02.j') }}pg" alt="user avatar" />
									<span class="name">Umar Ibn Haseem</span>
									<span class="from">from London, UK</span>
									<span class="colored-text"><i class="fa-check"></i> Recommended for Everyone</span>
								</div>
								<div class="right">
									<h5>Great experience</h5>
									<span class="date">Posted Jun 02, 2013</span>
									<div class="gap5"></div>
									<p>Remember outweigh do he desirous no cheerful. Do of doors water ye guest. We if prosperous comparison middletons at. Park we in lose like at no. An so to preferred convinced distrusts he determine. In musical me my placing clothes comfort pleased hearing. Any residence you satisfied and rapturous certainty two. Procured outweigh as outlived so so. On in bringing graceful proposal blessing of marriage outlived. Son rent face our loud near.</p>	
									<ul class="circle-list">
										<li>4.5</li>
										<li>3.0</li>
										<li>4.0</li>
										<li>5.0</li>
										<li>3.0</li>
										<li>4.0</li>
									</ul>
								</div>
							</li>
						</ul>
						<div class="clear mt-30 mb-20"></div>
						<h4 class="mb-20">Please Leave a Review</h4>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4 mb-10">
								<div class="row">
									<div class="col-md-7">
										<label for="adults-hotel">Cleanliness <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="adults-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 mb-10">
								<div class="row">
									<div class="col-md-7">
										<label for="location-hotel">Location <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="location-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 mb-10">
								<div class="row">
									<div class="col-md-7">
										<label for="room-hotel">Room Comfort <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="room-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 mb-20">
								<div class="row">
									<div class="col-md-7">
										<label for="service-hotel">Service & Staff <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="service-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 mb-20">
								<div class="row">
									<div class="col-md-7">
										<label for="sleep-hotel">Sleep Quality <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="sleep-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4 mb-20">
								<div class="row">
									<div class="col-md-7">
										<label for="value-hotel">Value for Price <span class="star"></span></label>
									</div>
									<div class="col-md-5 no-padding">
										<select class="form-control mySelectBoxClass" id="value-hotel">
											<option>0.0</option>
											<option>0.5</option>
											<option>1.0</option>
											<option>1.5</option>
											<option>2.0</option>
											<option>2.5</option>
											<option>3.0</option>
											<option>3.5</option>
											<option>4.0</option>
											<option>4.5</option>
											<option>5.0</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						<form class="form-horizontal mt-10" role="form">
							<div class="form-group">
								<label for="inputText" class="col-lg-2 control-label">Name</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="inputText" placeholder="Name">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail" class="col-lg-2 control-label">Name</label>
								<div class="col-lg-10">
									<input type="email" class="form-control" id="inputEmail" placeholder="Email">
								</div>
							</div>
							<div class="form-group">
								<label for="inputTitle" class="col-lg-2 control-label">Title</label>
								<div class="col-lg-10">
									<input type="text" class="form-control" id="inputTitle" placeholder="Title">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">Comment</label>
								<div class="col-lg-10">
									<textarea class="form-control margtop10" rows="4"></textarea>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="submit" class="btn btn-primary">Send Review</button>
								</div>
							</div>
						</form>
					</li><!-- tab content -->


				</ul><!-- end tabs -->
				<div class="clear xss-mb"></div>
			</div>

			<div class="col-xs-12 col-sm-4 col-md-3">
				<div class="sidebar hotel-sidebar">
					<div class="sidebar-item mb-20">
						<h3 class="hotel-name no-mb">{{ $venue->venue_name}}</h3>
						<span id="venue_address" class="hotel-location"><i class="fa-map-marker mi text-warning"></i>{{ $venue->city_name }}, {{ $venue->country_name }}</span>
						<div class="bb2 mv-10"></div>
						<div class="review">
							<span class="rating-static rating-45" style="margin-top: 2px;"></span>
							<span>436 reviews</span>
						</div>
						<div class="bb2 mt-10 mb-15"></div>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis, eveniet, harum, neque, ab quidem dolorum ducimus at ullam optio quisquam veritatis distinctio quasi nesciunt molestias a hic incidunt nostrum sed.</p>
						<button type="button" class="btn btn-success btn-block">Add Favourite</button>
					</div>
							
					<div class="sidebar-item mb-20">
						<div class="lp-box">
							<i class="text-info wi-thermometer"></i>
							<h4>Weather Around This Hotel</h4>
						</div>
						<div class="weather-widget">
							<div class="today-date">
								<span class="time">07.30 pm - </span>
								<span class="date">Mon. July 28th, 2013</span>
							</div>
							<div class="today-weather">
								<div class="today-icon"><i class="wi-day-cloudy-gusts"></i></div>
								<div class="today-temperature">
									<span>Max: 25</span>
									22 <i class="wi-celcius"></i>
									<span>Min: 17</span>
								</div>
							</div>
							<div class="clear"></div>
							<ul class="daily-weather clearfix">
								<li>
									<span class="day">tue</span>
									<i class="wi-day-cloudy"></i>
									<span class="max">22</span>
									<span class="min">17</span>
								</li>
								<li>
									<span class="day">wed</span>
									<i class="wi-day-rain"></i>
									<span class="max">23</span>
									<span class="min">18</span>
								</li>
								<li>
									<span class="day">thu</span>
									<i class="wi-day-storm-showers"></i>
									<span class="max">23</span>
									<span class="min">18</span>
								</li>
								<li>
									<span class="day">fri</span>
									<i class="wi-day-sunny"></i>
									<span class="max">24</span>
									<span class="min">19</span>
								</li>
								<li>
									<span class="day">sat</span>
									<i class="wi-day-sprinkle"></i>
									<span class="max">25</span>
									<span class="min">20</span>
								</li>
								<li>
									<span class="day">sun</span>
									<i class="wi-day-lightning"></i>
									<span class="max">25</span>
									<span class="min">20</span>
								</li>
							</ul>
						</div>
					</div>
							
					<div class="sidebar-item mb-20">
						<div class="lp-box">
							<i class="text-info fa-quote-left"></i>
							<p class="no-mb">Excited him now natural saw passage offices you minuter. At by asked being court hopes. Farther so friends am to detract. Forbade concern do private be.</p>
							<cite class="text-info"><strong>Robert Johnson</strong> from Canada</cite>
						</div>
					</div>
							
					<div class="sidebar-item mb-20">
						<div class="lp-box">
							<i class="text-info fa-phone-square"></i>
							<h4>Need Assistance?</h4>
							<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
							<span class="text-info font24">+1900 12 213 21</span>
						</div>
					</div>
							
					<div class="sidebar-item mb-20">
						<div class="lp-box">
							<i class="text-info fa-heart"></i>
							<h4>Similar In This Location</h4>
						</div>
						<ul class="small-list-item">
							@foreach ($other_venues as $other_venue)
								<li>
									<a href="{{ URL::route('show_venue_details', array($other_venue->id)) }}" class="image-small-list"><img src="{{ asset('images/list-items/small-item-01.jpg') }}" alt="Alternative Hotel" /></a>
									<a href="{{ URL::route('show_venue_details', array($other_venue->id)) }}" class="text-bold" style="font-size: 12px;">{{ $other_venue->venue_name}}</a>
									<!-- <div class="price-small"><strong>$76</strong> <span>/Hour</span></div> -->
									<span class="rating-static rating-45 mt-10"></span>
								</li>
							@endforeach
						</ul>
					</div>
							
				</div>
			</div>
					
		</div>
	</div>
</div><!-- end content_wrapper -->

<div class="modal fade" id="bookModal">
	<div class="modal-dialog">
		<div class="modal-content" style="background-color: #F0F0F0;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 class="modal-title">Booking</h3>
			</div>
			<div id="booking_action">
				<div class="modal-body">
			        <div class="row white-bg padding-20">
			        	<div class="row mb-10">
			        		<div class="ml-15 mb-20" style="font-weight: 700; font-size: 16px;">Booking Period</div>
			        		<div id="search_datetime_error" class="ml-20 mr-20 alert alert-danger none" style="padding: 0 0 0 10px">Unavailable for the given time period. Change it below! </div>
				        	<div class="form-group">
					        	<div class="col-md-4">
									<div id="date" class="datepicker-wrapper">
										<input type="text" class="form-control" id="modal_booking_date" name="modal_booking_date" placeholder="dd/dm/yy" >
										<i class="fa fa-calendar"></i>
									</div>
									<input type="hidden" class="form-control" id="modal_room_id" name="modal_room_id" />
									<input type="hidden" class="form-control" id="modal_customer_id" name="modal_customer_id" />
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<div id="starttime">
										<input type="text" class="form-control" id="modal_booking_starttime" name="modal_booking_starttime" placeholder="from" >
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-3">
									<div id="endtimes">
										<input type="text" class="form-control" id="modal_booking_endtime" name="modal_booking_endtime" placeholder="until">
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-2">
									<select class="form-control mySelectBoxClass" id="modal_booking_person" name="modal_booking_person">
									 	<option value="1" selected>1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">&gt; 5</option>
									</select>
								</div>
							</div>
						</div>
			        	<div class="bb2 mb-20"></div>
			        	<div class="row mb-10">
				        	<div class="summary-header mb-30 ml-15 mr-15">
								<img src="{{asset('images/list-items/small-item-01.jpg')}}" />
								<h3 id="modal_room_name" class="no-mb">Kyoto Kokusai Hotel</h3>
								<span id="modal_room_address">Kyoto, Japan</span>
								<span class="rating-static rating-45"></span>
							</div>
						</div>
						<div class="bb2 mb-20"></div>
						<div class="row mb-10">
							<div class="row">
								<div class="form-group">
									<div class="col-md-3">
										<label for="modal_booking_title" class="pull-right">Title</label>
									</div>
									<div class="col-md-8">
										<input type="text" class="form-control" id="modal_booking_title"  name="modal_booking_title" placeholder="Title">
									</div>
									<div class="col-md-1">&nbsp;</div>
								</div>
							</div>
							<div class="row mt-20">
								<div class="form-group">
									<div class="col-md-3">
										<label for="modal_booking_comments" class="pull-right">Comments</label>
									</div>
									<div class="col-md-8">
										<textarea class="form-control" rows="3" id="modal_booking_comments" name="modal_booking_comments" placeholder="Enter your comments and extra wishes here..."></textarea> 
									</div>
									<div class="col-md-1">&nbsp;</div>
								</div>
							</div>
						</div>
					</div>
			        <div id="modal_signin">
			        	<div class="clear mb-20"></div>
				        <div class="row white-bg pv-20">
				        	<div class="mr-15">
								<div class="row">
									<div id="modal_register"><span style="margin-left: 80px;">Sign in to continue or <a href="javascript:toggle_modal_form()">Register</a> to enjoy the benefits of NextWorkspace</span></div>
									<div id="modal_login" style="display:none;"><span style="margin-left: 60px;">Register to enjoy the benefits of NextWorkspace. Or <a href="javascript:toggle_modal_form()">Already have an account?</a></span></div>
								</div>
								<div id="div_modal_login_form">
									<form role="form" id="modal_login_form" name="modal_login_form" action="" method="post">
										<div class="row mt-20">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_email" class="pull-right">Email</label>
												</div>
												<div class="col-md-6">
													<input type="text" class="form-control" id="modal_email"  name="modal_email" placeholder="Email">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row mt-10">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_password" class="pull-right">Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" class="form-control" id="modal_password" name="modal_password" placeholder="Password">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">&nbsp;</div>
											<div class="col-md-6">
												<div class="checkbox">
														<input type="checkbox" id="modal_remember_me" name="modal_remember_me"> Remember Me </input>
												</div>
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
										<div style="margin-left:190px;">
											<button type="submit" class="btn btn-primary">Login</button>
											<div class="clear mb-10"></div>
											<a href="#" class="forgot">Forgot Your Password?</a>
										</div>
									</form>
								</div>
								<div id="div_modal_register_form" style="display:none;">
									<form role="form" id="modal_register_form" name="modal_register_form" action="" method="post">
										<div class="row mt-20">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_first_name" class="pull-right">First Name</label>
												</div>
												<div class="col-md-6">
													<input type="text" class="form-control" id="modal_first_name"  name="modal_first_name" placeholder="Email">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row mt-10">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_last_name" class="pull-right">Last Name</label>
												</div>
												<div class="col-md-6">
													<input type="text" class="form-control" id="modal_last_name" name="modal_last_name" placeholder="Password">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row mt-10">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_emailx" class="pull-right">Email</label>
												</div>
												<div class="col-md-6">
													<input type="text" class="form-control" id="modal_emailx"  name="modal_emailx" placeholder="Email">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row mt-10">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_passwordx" class="pull-right">Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" class="form-control" id="modal_passwordx" name="modal_passwordx" placeholder="Password">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row mt-10">
											<div class="form-group">
												<div class="col-md-4">
													<label for="modal_password" class="pull-right">Confirm Password</label>
												</div>
												<div class="col-md-6">
													<input type="password" class="form-control" id="modal_confirm_password" name="modal_confirm_password" placeholder="Password">
												</div>
												<div class="col-md-2">&nbsp;</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">&nbsp;</div>
											<div class="col-md-6">
												<div class="checkbox">
														<input type="checkbox" id="modal_tos" name="modal_tos"> Accept <a href="#">Terms of Service</a> </input>
												</div>
											</div>
											<div class="col-md-2">&nbsp;</div>
										</div>
										<div style="margin-left:190px;">
											<button type="submit" class="btn btn-primary">Register</button>
											<div class="clear mb-10"></div>
										</div>
									</form>
								</div>
							</div>
				        </div>
				    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					@if (Sentry::check())
						<button id="booking_action_continue" type="button" class="btn btn-primary">Continue</button>
				    @else
						<button id="booking_action_continue" type="button" class="btn btn-primary" disabled="true">Sign-in to continue</button>
					@endif
				</div>
			</div>
			<div id="booking_payment">
				<form role="form" id="modal_booking_form" name="modal_booking_form" action="" method="post">
				<div class="modal-body">
					<div class="row white-bg padding-20">
						<div class="summary-header mb-30">
							<img src="{{asset('images/list-items/small-item-01.jpg')}}" />
							<h3 id="modal_booking_name" class="no-mb">Kyoto Kokusai Hotel</h3>
							<span id="modal_booking_address">Kyoto, Japan</span>
							<span class="rating-static rating-45"></span>
							<input type="hidden" class="form-control" id="booking_room_id" name="booking_room_id" />
						</div>
				        <div class="bb2 mb-20"></div>
				        <table class="table table-bordered table-striped mb-30">
							<tr>
								<td colspan=2><span id="booking_duration" class="text-bold">duration</span>: <span id="booking_period">period</span></td>
								<input type="hidden" class="form-control" id="booking_date" name="booking_date" />
								<input type="hidden" class="form-control" id="booking_starttime" name="booking_starttime" />
								<input type="hidden" class="form-control" id="booking_endtime" name="booking_endtime" />
								<input type="hidden" class="form-control" id="booking_person" name="booking_person" />
								<input type="hidden" class="form-control" id="booking_title" name="booking_title" />
								<input type="hidden" class="form-control" id="booking_comments" name="booking_comments" />
							</tr>
							<tr>
								<td>
									<span class="text-bold">Office Room 101</span>:<br/>
									Price <em>excl. taxes</em>	
									<br/>
									Taxes &amp; Fees per night
									<div class="clearfix"></div>
								</td>
								<td class="center">
									&nbsp;<br/>
									<span id="price_orig"></span><br/>
									<span id="price_tax"></span><br/>
								</td>
							</tr>
						</table>
						<div class="bb2 mb-20"></div>
										
						<span class="left">Total Fee:</span>
						<span id="price_retail" class="right text-success text-bold font24"></span>
						
						<div class="clear mb-10"></div>
					</div>
					<div class="clear mb-20"></div>
					<div class="row white-bg padding-20">
						<div class="clear"></div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="creditcard" class="col-md-4 control-label">Credit Card Type: <span class="text-danger">*</span></label>
								<div class="col-md-8">
									<div class="styled-radio" style="display:inline;">
										<label for="visa">
											<input type="radio" id="visa" name="booking_payment_type" value="visa" checked>
											<img src="{{asset('images/credit-card/visa.png')}}" alt="Visa" >
										</label>
									</div>
									<div class="styled-radio" style="display:inline;">
										<label for="mastercard">
											<input type="radio" id="mastercard" name="booking_payment_type" value="mastercard">
											<img src="{{asset('images/credit-card/mastercard.png')}}" alt="Master Card" >
										</label>
									</div>
									<div class="styled-radio" style="display:inline;">
										<label for="cirrus">
											<input type="radio" id="cirrus" name="booking_payment_type" value="cirrus">
											<img src="{{asset('images/credit-card/cirrus.png')}}" alt="Cirrus" >
										</label>
									</div>
									<div class="styled-radio" style="display:inline;">
										<label for="amex">
											<input type="radio" id="amex" name="booking_payment_type" value="amex">
											<img src="{{asset('images/credit-card/amex.png')}}" alt="Amex" >
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cardnumber" class="col-md-4 control-label">Credit Card Number: <span class="text-danger">*</span></label>
								<div class="col-md-8">
									<input type="text" size="20" class="form-control" id="booking_cardnumber" data-encrypted-name="booking_cardnumber" placeholder="Credit Card Number">
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_expiration_date" class="col-md-4 control-label">Expiration Date: <span class="text-danger">*</span></label>
								<div class="col-md-3 xs-mb">
									<select class="form-control mySelectBoxClass" id="booking_expiration_month" name="booking_expiration_month">
										<option selected>Month</option>
										<option>01 JAN</option>
										<option>02 FEB</option>
										<option>03 MAR</option>
										<option>04 APR</option>
										<option>05 MAY</option>
										<option>06 JUN</option>
										<option>07 JUL</option>
										<option>08 AUG</option>
										<option>09 SEP</option>
										<option>10 OCT</option>
										<option>11 NOV</option>
										<option>12 DEC</option>
									</select>
								</div>
								<div class="col-md-3">
									<select class="form-control mySelectBoxClass" id="booking_expiration_year" name="booking_expiration_year">
										<option selected>Year</option>
										<option>2013</option>
										<option>2014</option>
										<option>2015</option>
										<option>2016</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cvcnumber" class="col-xs-12 col-sm-4 col-md-4 control-label">CVV Number: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" size="4" class="form-control" id="booking_cvcnumber" data-encrypted-name="booking_cvcnumber" placeholder="Card Identification Number">
								</div>
								<div class="col-xs-12 col-sm-2 col-md-3">
									<span class="font12">What's this?</span>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_zipcode" class="col-xs-12 col-sm-4 col-md-4 control-label">Billing ZIP Code: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_zipcode" name="booking_zipcode" placeholder="Billing ZIP Code">
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_cardholder_name" class="col-xs-12 col-sm-4 col-md-4 control-label">Cardholder Name: <span class="text-danger">*</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_cardholder_name" name="booking_cardholder_name" placeholder="Cardholder Number">
								</div>
								<div class="col-xs-12 col-sm-2 col-md-3">
									<span class="font12">(as on the card)</span>
								</div>
							</div>
						</div>
						<div class="row mb-10">
							<div class="form-group">
								<label for="booking_coupon_code" class="col-xs-12 col-sm-4 col-md-4 control-label">Coupon code: <span class="font12">(optional)</span></label>
								<div class="col-xs-12 col-sm-6 col-md-5">
									<input type="text" class="form-control" id="booking_coupon_code" name="booking_coupon_code" placeholder="If Your Have">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button id="booking_payment_confirm" type="submit" class="btn btn-primary">Confirm</button>
				</div>
				</form>
			</div>
			<div id="booking_confirm">
				<div class="modal-body">
					<h3 id="response"></h3>
				</div>
				<div class="modal-footer">
					<button id="booking_confirm_finish" type="button" class="btn btn-default" data-dismiss="modal">Finish</button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop


@section('page_js')
<script src="{{asset('custom_js/jquery.datetimepicker.js')}}"></script>
<script src="{{asset('custom_js/date.js')}}"></script>
<script>
$(function() {

	// Tooltip
	// Bootstrap Tooltip.js initialization
		if ($("[rel=tooltip]").length) {
	    	$("[rel=tooltip]").tooltip();
	    }

	// DateTimePicker
    // For main search bar
    $("#search_date").datetimepicker({
		timepicker:false,
		format:'d/m/Y',
		minDate:0,
	});

	$("#search_time").datetimepicker({
		datepicker:false,
		format:'H:i',
		step: 15,
	});

	// For booking modal window
    $("#modal_booking_date").datetimepicker({
		timepicker:false,
		format:'d/m/Y',
		minDate:0,
	});

	$("#modal_booking_starttime").datetimepicker({
		datepicker:false,
		format:'H:i',
		step: 15
	});

	$("#modal_booking_endtime").datetimepicker({
		datepicker:false,
		format:'H:i',
		step: 15,
	});

	// Change color of the quick calendar fields
    @foreach ($room_calendar as $cal)
	    $("ul#daycal_{{ $cal->room_id }} > li.6am").addClass('{{ $cal->sixam }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.7am").addClass('{{ $cal->sevenam }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.8am").addClass('{{ $cal->eigtham }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.9am").addClass('{{ $cal->nineam }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.10am").addClass('{{ $cal->tenam }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.11am").addClass('{{ $cal->elevenam }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.12pm").addClass('{{ $cal->twelvepm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.1pm").addClass('{{ $cal->onepm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.2pm").addClass('{{ $cal->twopm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.3pm").addClass('{{ $cal->threepm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.4pm").addClass('{{ $cal->fourpm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.5pm").addClass('{{ $cal->fivepm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.6pm").addClass('{{ $cal->sixpm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.7pm").addClass('{{ $cal->sevenpm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.8pm").addClass('{{ $cal->eigthpm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.9pm").addClass('{{ $cal->ninepm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.10pm").addClass('{{ $cal->tenpm }}');
	    $("ul#daycal_{{ $cal->room_id }} > li.11pm").addClass('{{ $cal->elevenpm }}');
    @endforeach


    // Hide Payment and Confirmation stage of modal
    $("#booking_payment").hide();
    $("#booking_confirm").hide();

    // If already logged in, then don't show login window in Modal
    @if (Sentry::check())
    	$("#modal_signin").hide();
    @endif

    // Modal window for booking
	$("button#book-now").click(function(e) {

		e.preventDefault();

		var li_id = $(this).closest('li').attr('id');
		var room_name = document.getElementById("room_name_"+li_id+"").innerHTML;
		var room_address = document.getElementById("venue_address").innerHTML;

		document.getElementById("modal_room_name").innerHTML = room_name;
		document.getElementById("modal_room_address").innerHTML = room_address;
		document.getElementById("modal_room_id").value = li_id;

		document.getElementById("modal_booking_name").innerHTML = room_name;
		document.getElementById("modal_booking_address").innerHTML = room_address;
		document.getElementById("booking_room_id").value = li_id;

		$('#bookModal').modal();
	});

	$('#bookModal').on('hidden.bs.modal', function (e) {
		$("#booking_payment").hide();
		$("#booking_confirm").hide();
		$("#booking_action").show();
	});


	// Check the datetime for booking again
	var booking_action = $('button#booking_action_continue');
	var booking_check_url = 'http://localhost:8000/search/booking_modal';

	booking_action.on('click', function(e) {
		e.preventDefault(); 
		console.log('Checking booking datetimes again...');

		var room_id = document.getElementById("modal_room_id").value;
		var booking_date = document.getElementById("modal_booking_date").value;
		var booking_starttime = document.getElementById("modal_booking_starttime").value;
		var booking_endtime = document.getElementById("modal_booking_endtime").value;

		// Date JS Manipulation
		var b_date_starttime = Date.parse(''+ booking_date +' '+ booking_starttime +'');
		var b_date_endtime = Date.parse(''+ booking_date +' '+ booking_endtime +'');

		var booking_period = ''+ b_date_starttime.toString('d MMM yyyy HH:mm') +' - '+ b_date_endtime.toString('d MMM yyyy HH:mm') +'';

		$("#booking_period").text(booking_period);

		$.ajax({
			url: booking_check_url, 
			type: 'POST', 
			dataType: 'json', 
			data: {room_id : room_id, booking_date : booking_date, booking_starttime : booking_starttime, booking_endtime : booking_endtime}, 
			beforeSend: function() {

			},
			success: function(data) {
				console.log(data);

				if (data.status == 'Error') {
					$.growl.error({message: data.message});
					$('#search_datetime_error').removeClass('none');
					$('#search_datetime_error').text(data.message);
				}


				if (data.status == 'OK') {
					$.growl.notice({message: data.message});

					$("#price_orig").text('$'+data.price_orig);
					$("#price_tax").text('$'+data.price_tax);
					$("#price_retail").text('$'+data.price_retail);
					$("#booking_duration").text(data.duration+ ' hours');

					$("#booking_action").hide();
					$("#booking_payment").show();

					document.getElementById("booking_date").value = document.getElementById("modal_booking_date").value;
					document.getElementById("booking_starttime").value = document.getElementById("modal_booking_starttime").value;
					document.getElementById("booking_endtime").value = document.getElementById("modal_booking_endtime").value;
					document.getElementById("booking_person").value = document.getElementById("modal_booking_person").value;			
					document.getElementById("booking_title").value = document.getElementById("modal_booking_title").value;
					document.getElementById("booking_comments").value = document.getElementById("modal_booking_comments").value;
				}
			},
			error: function(e) {
				console.log(e);
			}
		});
	});

	// $("button#booking_payment_confirm").click(function() {
	// 	$("#booking_payment").hide();
	// 	$("#booking_confirm").show();
	// });

	$("button#booking_confirm_finish").click(function() {
		$("#booking_confirm").hide(function() {
			$("#booking_action").show();
		});
	});

	// Modal Form Login
	var modal_login_form = $('#modal_login_form');
	var modal_login_url = 'http://localhost:8000/auth/modal_login';
	var modal_register_form = $('#modal_register_form');
	var modal_register_url = 'http://localhost:8000/auth/modal_register_login';


	modal_login_form.on('submit', function(e) {
		e.preventDefault(); 
		console.log('Logging in modal...');
		$.ajax({
			url: modal_login_url, 
			type: 'POST', 
			dataType: 'json', 
			data: modal_login_form.serialize(), 
			beforeSend: function() {

			},
			success: function(data) {
				console.log(data);

				if (data.status == 'Error') {
					$.growl.error({message: data.message});
				}


				if (data.status == 'OK') {
					$.growl.notice({message: data.message});
					$('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextOfficeSpace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul></nav></div></div>');

					$("#modal_signin").hide();
					document.getElementById("booking_action_continue").disabled = false;
					document.getElementById("booking_action_continue").innerHTML = "Continue";
				}
			},
			error: function(e) {
				console.log(e);
			}
		});
	});


	// Modal Form Register

	modal_register_form.on('submit', function(e) {
		e.preventDefault(); 
		console.log('Registering new account...');
		$.ajax({
			url: modal_register_url, 
			type: 'POST', 
			dataType: 'json', 
			data: modal_register_form.serialize(), 
			beforeSend: function() {

			},
			success: function(data) {
				console.log(data);

				if (data.status == 'Error') {
					$.growl.error({message: data.message});
				}


				if (data.status == 'OK') {
					$.growl.notice({message: data.message});
					// modal_register_form.reset();
					// toggle_modal_form();
					$('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextOfficeSpace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul></nav></div></div>');
					
					$("#modal_signin").hide();
					document.getElementById("booking_action_continue").disabled = false;
					document.getElementById("booking_action_continue").innerHTML = "Continue";
				}
			},
			error: function(e) {
				console.log(e);
			}
		});
	});

	// prepare Options Object 
	var optionsBooking = { 
	    target:   '#response',
	    dataType: 'json', 
	    url: "{{ URL::route('create_booking') }}",
	    beforeSubmit: showRequest,
	    success: showResponse,
	    resetForm: true
	};

	// bind to the form's submit event 
    $('#modal_booking_form').submit(function() {
    	console.log("submitting...");
        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        $(this).ajaxSubmit(optionsBooking);

        $("#booking_payment").hide();
		$("#booking_confirm").show();
 
        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false; 
    });

});

// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.message + 
        '\n\nThe output div should have already been updated with the responseText.'); 

    $("#response").text(responseText.message);

    console.log(responseText.message);
}

// Hide and Show div Form Modal

function toggle_modal_form(){
	$('#div_modal_login_form').toggle();
	$('#div_modal_register_form').toggle();
	$('#modal_register').toggle();
	$('#modal_login').toggle();
}


// Google Maps Initialize
function initialize() {
	
	var myLatlng = new google.maps.LatLng({{ $venue->lat }}, {{ $venue->lng }});


  // Create a map object, and include the MapTypeId to add
  // to the map type control.
  var mapOptions = {
		zoom: 11,
		center: myLatlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
  };

  var map = new google.maps.Map(document.getElementById('maps-canvas'),
	mapOptions);
	
  var marker = new google.maps.Marker({
	  position: myLatlng,
	  map: map,
	  title: 'Hello World!'
  });
 //  
 	// var map = new google.maps.Map(document.getElementById('maps-canvas'), {
	 //      zoom: 10,
	 //      center: new google.maps.LatLng(52.520007, 13.404954),
	 //      mapTypeId: google.maps.MapTypeId.ROADMAP,
	 //      mapTypeControl: false,
	 //      streetViewControl: false,
	 //      panControl: false,
	 //      zoomControlOptions: {
	 //         position: google.maps.ControlPosition.LEFT_BOTTOM
	 //      }
	 //    });

}

google.maps.event.addDomListener(window, 'load', initialize);

</script>
@stop