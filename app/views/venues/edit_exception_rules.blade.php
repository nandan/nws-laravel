<div class="col-md-12">
	<div class="row white-bg padding-20">
		<div class="wizard-item">
			<hr class="line"/>
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="clearfix wizard-element">
					<div class="col-md-3">
						<strong>Your time Zone</strong>
					</div>
					<div class="col-md-9">
						<h5 id="timezone" style="padding:0">{{$venue->zone_name}}</h5>
					</div>
				</div>
				<div class="clearfix wizard-element mt-30">
					<div class="col-md-12">
					    <table class="table table-striped table-bordered table-hover exception-table">
					    	<tr>
					    		<th>Title</th>
					    		<th colspan="2">Date</th>
					    		<th colspan="2">Time</th>
					    		<th>Recurrence</th>
					    		<th></th>
					    	</tr>
					    	<tr>
					    		<th></th>
					    		<th>From</th>
					    		<th>To</th>
					    		<th>Start</th>
					    		<th>End</th>
					    		<th></th>
					    		<th></th>
					    	</tr>
					    	@foreach ($exception_rules as $rule)
					    		<?php $start = date_create($rule->dtstart) ?>
					    		<?php $end = date_create($rule->dtend) ?>
					    		<tr>
					    			<td>{{ $rule->title }}</td>
						    		<td>{{ date_format($start, 'd/m/Y') }}</td>
						    		<td>{{ date_format($end, 'd/m/Y') }}</td>
						    		<td>{{ date_format($start, 'H:i') }}</td>
						    		<td>{{ date_format($end, 'H:i') }}</td>
						    		<td>{{ $rule->freq }}</td>
						    		<td><a href="{{ URL::route('controlcenter_view_venue_single_exception_rule', array($rule->id)) }}"><button type="button" class="btn btn-success btn-xs">view</button></a> <a href="{{ URL::route('controlcenter_edit_venue_single_exception_rule', array($rule->id)) }}"><button type="button" class="btn btn-info btn-xs">edit</button></a> <a href="{{ URL::route('controlcenter_remove_venue_single_exception_rule', array($rule->id)) }}"><button type="button" class="btn btn-danger btn-xs">remove</button></a></td>
						    	</tr>
					    	@endforeach
					    </table>
					    <button type="button" id="add_new_rules" class="btn btn-info btn-md pull-right">Add New Rules</button>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>