@extends('layouts.main')

@section('page_css')
<style>
	/* Wizard Menu at the top */
	.wizard-menu {
		list-style: none;
		font-size: 15px;
		font-weight: 700;
		line-height: 1;
		padding: 0px 20px 13px 20px;
	}
	.wizard-menu > li {
		position: relative;
		text-shadow: none;
		float: left;
	}

	.wizard-menu > li.waiting {
		color: #bdc3c7;
	}

	.wizard-menu > li:after {
	  color: #bdc3c7;
	  content: "\f054";
	  font-family: FontAwesome;
	  display: inline-block;
	  font-size: 9.75px;
	  margin: -4px 9px 0 13px;
	  vertical-align: middle;
	  -webkit-font-smoothing: antialiased;
	}
	
	.wizard-menu > li:last-child:after {
		display: none;
	}
	
	.wizard-menu .active {
	  color: #bdc3c7;
	  cursor: default;
	}
	.wizard-menu .active:after {
	  display: none;
	}
	.wizard-menu > li + li:before {
	  content: "";
	  padding: 0;
	}
	
	
	/* Wizard items and elements */
	.wizard-item {
		padding: 10px;
	}
	.wizard-element {
		margin-bottom: 10px;
	}
	.wizard-element.address-field {
		height: 34px;
	}
	.wizard-button {
		margin-right: -15px;
	}
	.wizard-button button {
		margin-right: 35px;
		width: 100px;
	}
	
	/* Google Maps Places */
	#maps-canvas {
		height: 500px;
		width: 500px;
	}
	.controls {
		margin-top: 16px;
		border: 1px solid transparent;
		border-radius: 2px 0 0 2px;
		box-sizing: border-box;
		-moz-box-sizing: border-box;
		height: 32px;
		outline: none;
		box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
	}
	
	#pac-input {
		background-color: #fff;
		padding: 0 11px 0 13px;
		width: 300px;
		font-family: Roboto;
		font-size: 15px;
		font-weight: 300;
		text-overflow: ellipsis;
	}
	
	#pac-input:focus {
		border-color: #4d90fe;
		margin-left: -1px;
		padding-left: 14px;  /* Regular padding-left + 1. */
		width: 401px;
	}
	
	.pac-container {
		font-family: Roboto;
	}
	
	table.time-table tr  td.table-days{
		padding-top: 15px;
	}
</style>
@stop


@section('content')
<!-- Main Content -->
<div id="content_wrapper">
	<div class="container">
		<div class="row clearfix pv-30 mt-20">
			@include('wizard.wizard_sidebar')

			<div class="col-xs-12 col-sm-8 col-md-9">
				<div class="white-bg padding-20">
					<ul class="wizard-menu">
					  <li>Add Company</li>
					  <li class="waiting">Add Venue</li>
					  <li class="waiting">Add Office</li>
					  <li class="waiting">Finish</li>
					</ul>
				</div>
				
				<div class="clear mb-30"></div>

				<form id="wizard_form">
					<div id="add_company" class="step">
						@include('companies.create')
						<div class="clear"></div>
					</div>
					<div id="add_venue" class="step">
						@include('venues.create')
						<div class="clear"></div>
					</div>
					<div id="add_office" class="submit step">
						@include('rooms.create')
						<div class="clear"></div>
					</div>
					<div id="finish_wizard">
						<div class="col-md-12">
							<div class="row white-bg padding-20">
								<div class="wizard-item">
									<h2>You have successfully created your first entry.</h2>
									<h3 id="response"></h3>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="row white-bg padding-20 wizard-button">
							<button type="submit" name="process" class="btn btn-primary pull-right submit">Submit</button>
							<button type="button" name="forward" class="btn btn-primary pull-right forward">Next</button>
							<button type="button" name="backward" class="btn btn-primary pull-right backward">Back</button>
						</div>
					</div>
					<div class="clear"></div>
				</form>
			</div>
		</div>
	</div>
</div><!-- end content_wrapper -->
<!-- Main Content End -->
<!-- Footer -->
@include('layouts.footer')

@stop

@section('page_js')
<script src="{{asset('custom_js/jquery.form.js')}}"></script>
<script src="{{asset('custom_js/jquery.wizard.js')}}"></script>
<script>
	
	// Google Maps Places API Autocompletion
	var componentForm = {
	  street_number: 'short_name',
	  route: 'long_name',
	  locality: 'long_name',
	  administrative_area_level_1: 'short_name',
	  country: 'long_name',
	  postal_code: 'short_name'
	};
	
	function initialize() {
			var mapOptions = {
			    center: new google.maps.LatLng(-33.8688, 151.2195),
			    zoom: 13
			};
			
			var map = new google.maps.Map(document.getElementById('maps-canvas'),
			mapOptions);
			
			var input = (document.getElementById('pac-input')); 
			  				
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
			
			// Create the autocomplete object, restricting the search
			// to geographical location types.
			var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.bindTo('bounds', map);
			
		
			var infowindow = new google.maps.InfoWindow();
			var marker = new google.maps.Marker({
				map: map
			});
		
			// When the user selects an address from the dropdown,
			// populate the address fields in the form.
			google.maps.event.addListener(autocomplete, 'place_changed', function() {
			    infowindow.close();
			    marker.setVisible(false);
			    
			    var place = autocomplete.getPlace();
			    
				for (var component in componentForm) {
					document.getElementById(component).value = '';
					document.getElementById(component).disabled = false;
				}
				
				// Get each component of the address from the place details
				// and fill the corresponding field on the form.
				for (var i = 0; i < place.address_components.length; i++) {
					var addressType = place.address_components[i].types[0];
					if (componentForm[addressType]) {
						var val = place.address_components[i][componentForm[addressType]];
						document.getElementById(addressType).value = val;
						// $("#"+addressType).text(val);
					}
				}
			    
			    if (!place.geometry) {
			      return;
			    }

			    // Get Latitutde and Longitude of the place
			    var lat = place.geometry.location.lat();
				var lng = place.geometry.location.lng();
				
				document.getElementById("venue_lat").value = lat;
				document.getElementById("venue_lng").value = lng;
				
				console.log(lat);
				console.log(lng);

				// Get Timezone Information of the place
				$.ajax({
	                url: 'https://maps.googleapis.com/maps/api/timezone/json?location='+ lat +','+ lng +'&timestamp=1331161200&sensor=false',
	                type: 'GET', // form submit method get/post
	                dataType: 'json', // request type html/json/xml
	                beforeSend: function() {
	
	                },
	                success: function(data) {
	                  console.log(data.timeZoneId);
	                  document.getElementById("venue_timeZoneId").value = '';
	                  document.getElementById("venue_timeZoneId").disabled = false;
	                  document.getElementById("venue_timeZoneId").value = data.timeZoneId;
	                  document.getElementById("venue_timeZoneName").value = data.timeZoneName;
	                  $("#timezone").text(data.timeZoneId);
	                },
	                error: function(e) {
	                    console.log(e)
	                }
	            });
			
			    // If the place has a geometry, then present it on a map.
			    if (place.geometry.viewport) {
			      map.fitBounds(place.geometry.viewport);
			    } else {
			      map.setCenter(place.geometry.location);
			      map.setZoom(17);  // Why 17? Because it looks good.
			    }
			    marker.setIcon(/** @type {google.maps.Icon} */({
			      url: place.icon,
			      size: new google.maps.Size(71, 71),
			      origin: new google.maps.Point(0, 0),
			      anchor: new google.maps.Point(17, 34),
			      scaledSize: new google.maps.Size(35, 35)
			    }));
			    marker.setPosition(place.geometry.location);
			    marker.setVisible(true);
			
			    var address = '';
			    if (place.address_components) {
			      address = [
			        (place.address_components[0] && place.address_components[0].short_name || ''),
			        (place.address_components[1] && place.address_components[1].short_name || ''),
			        (place.address_components[2] && place.address_components[2].short_name || '')
			      ].join(' ');
			    }
			
			    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			    infowindow.open(map, marker);
			
			});
	}	
	
	google.maps.event.addDomListener(window, 'load', initialize);

	$(function() {
		console.log("off goes the form...");

		$("#finish_wizard").hide();

		// prepare Options Object 
		var options = { 
		    target:     '#response',
		    dataType: 'json', 
		    url:        '{{ URL::route("store_wizard") }}',
		    beforeSubmit: showRequest,
		    success: showResponse,
		    resetForm: true
		};

		$(".container").wizard({
            stepsWrapper: "#wizard_form",
            submit: ".submit",
            animations: {
		        show: {
		            options: {
		                duration: 600
		            },
		            properties: {
		                opacity: "show"
		            }
		        },
		        hide: {
		            options: {
		                duration: 400
		            },
		            properties: {
		                opacity: "hide"
		            }
		        }
		    },
        }).wizard('form').submit(function( event ) {
                event.preventDefault();
                console.log('ajax submitting');

                $("#add_company").hide();
                $("#add_venue").hide();
                $("#add_office").hide();
                $(".wizard-button").hide();
                $("#finish_wizard").show();

                $(this).ajaxSubmit(options);

                return false;
        });
	});

	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
	    // formData is an array; here we use $.param to convert it to a string to display it 
	    // but the form plugin does this for you automatically when it submits the data 
	    var queryString = $.param(formData); 
	 
	    // jqForm is a jQuery object encapsulating the form element.  To access the 
	    // DOM element for the form do this: 
	    // var formElement = jqForm[0]; 
	 
	    alert('About to submit: \n\n' + queryString); 
	 
	    // here we could return false to prevent the form from being submitted; 
	    // returning anything other than false will allow the form submit to continue 
	    return true; 
	} 
	 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
	    // for normal html responses, the first argument to the success callback 
	    // is the XMLHttpRequest object's responseText property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'xml' then the first argument to the success callback 
	    // is the XMLHttpRequest object's responseXML property 
	 
	    // if the ajaxForm method was passed an Options Object with the dataType 
	    // property set to 'json' then the first argument to the success callback 
	    // is the json data object returned by the server 
	 
	    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText.message + 
	        '\n\nThe output div should have already been updated with the responseText.'); 

	    $("#response").text(responseText.message);

	    console.log(responseText.message);
	} 
</script>
@stop



