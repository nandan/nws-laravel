<!-- Begin sidebar -->
<div class="col-xs-12 col-sm-4 col-md-3">
	<div class="sidebar">

		<div class="sidebar-item white-bg padding-20 mb-20">
			<h4>List your offices</h4>
			<ul class="cat-list">
				<li>Give your office a large audience</li>
				<li>Instant booking</li>
				<li>24/7 Customer Service</li>
			</ul>	
		</div><!-- end categories -->


		<div class="sidebar-item white-bg padding-20 mb-20">
			<div class="lp-box">
				<i class="text-info fa-phone-square"></i>
				<h4>Need Assistance?</h4>
				<p>Our team is 24/7 at your service to help you with your booking issues or answer any related questions</p>
				<span class="text-info font24">+1900 12 213 21</span>
			</div>
		</div><!-- end assistance  -->

	</div><!-- end sidebar  -->

</div><!-- end sidebar outer  -->