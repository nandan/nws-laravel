<?php

use
    Sabre\DAV,
    Sabre\CalDAV,
    Sabre\DAVACL;

//$pdo = new \PDO('sqlite:data/db.sqlite');
/// MYSQL CONNECTION
$pdo = new \PDO('mysql:host=192.168.178.95;dbname=officebooking','officebooking','officebooking');
$pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

//Mapping PHP errors to exceptions
function exception_error_handler($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
}
set_error_handler("exception_error_handler");

// Files we need
// Already loaded by laravel default
//require_once base_path().'/vendor/sabre/dav/lib/Sabre/autoload.php';

//.'/lib/Sabre/autoload.php';

// Backends
$authBackend = new DAV\Auth\Backend\PDO($pdo);
$principalBackend = new DAVACL\PrincipalBackend\PDO($pdo);
$calendarBackend = new CalDAV\Backend\PDO($pdo);

// Directory tree
$tree = array(
        new DAVACL\PrincipalCollection($principalBackend),
        new CalDAV\CalendarRootNode($principalBackend, $calendarBackend)
);      


// The object tree needs in turn to be passed to the server class
$server = new DAV\Server($tree);

// You are highly encouraged to set your WebDAV server base url. Without it,
// SabreDAV will guess, but the guess is not always correct. Putting the
// server on the root of the domain will improve compatibility.
$server->setBaseUri('/caldav/server');

// Authentication plugin
$authPlugin = new DAV\Auth\Plugin($authBackend,'SabreDAV');
$server->addPlugin($authPlugin);

// CalDAV plugin
$caldavPlugin = new CalDAV\Plugin();
$server->addPlugin($caldavPlugin);

// ACL plugin
$aclPlugin = new DAVACL\Plugin();
$server->addPlugin($aclPlugin);

// Support for html frontend
$browser = new DAV\Browser\Plugin();
$server->addPlugin($browser);

// And off we go!
$server->exec();
