 /**
  *  Express Calendar
  */

 // Misc Variables
 var last_update = '';
 var show_calendar_url = 'http://localhost:8000/express/init_express_calendar';
 var active_calendar_index = "0"; // 0 = Venue, 1 2 3 = Room Ids..
 var bookings_array = [];
 var bookings_array_different_days = [];
 var bookings_sidebar_html = [];
 var bookings_sidebar = $('#bookings_sidebar');
 var sidebar_content = $('#sidebar_content');
 var longpolling_url = 'http://localhost:8000/express/longpolling';

 //Calendar Init Variables
 var myfullcalendar = $('#calendar');
 var init_year;
 var init_month;
 var init_day;
 var timezone;


 //Insert Events
 var insert_title;
 var insert_start_date;
 var insert_end_date;

 $(document).ready(function() {

   /**
    * [Ajax creating the calendar]
    * @param  {[type]} )        {                  } [description]
    * @param  {[type]} success: function(data) {                       if (data.status == 'Error') {            $.growl.error({message: data.message});        }        if (data.status == 'OK') {            $.growl.notice({message: data.message});            console.log('init express calendar...');                                init_calendar_after_ajax();                for (var i=0;i<data.calendardata.length;i++)                {                    bookings_array.push(data.calendardata[i]);                    console.log(data.calendardata[i]);                }                                                render_bookings_array(0 [description]
    * @return {[type]}          [description]
    */
   $.ajax({
     url: show_calendar_url,
     type: 'POST',
     beforeSend: function() {},
     success: function(data) {

       if (data.status == 'Error') {
         $.growl.error({
           message: data.message
         });
       }

       if (data.status == 'OK') {
         $.growl.notice({
           message: data.message
         });
         console.log('init express calendar...');

         //Set Clientside time == Server Side time for jquery fullcalendar
         var temp_date = Date.parse(data.last_update);
         init_year = temp_date.getFullYear();
         init_moth = temp_date.getMonth() + 1;
         init_day = temp_date.getDate();

         //Init calendar with empty events
         init_calendar_after_ajax();
         // Load Bookings Array with initial fetched objects
         for (var i = 0; i < data.calendardata.length; i++) {
           bookings_array.push(data.calendardata[i]);
           console.log(data.calendardata[i]);
         }

         //Render Bookings array entries
         render_bookings_array(0);
         render_newest_bookings_sidebar(0);
         last_update = data.last_update;


         //Show Sidebar Last Update Info
         console.log('last update:' + last_update);
         $('#last_updated_info').text('Last Update: ' + last_update);
       }
     },
     error: function(e) {
       console.log(e);
     }
   });
   //setTimeout('start_longpolling()', 4000);
   //Document Ready End 

   /**

     $("table").hover(
         function() {
             $(this).css('color', 'red')
         }, function() {
             $(this).css('color', 'green')
         }
     );
 **/


 });

 /**
  * [render_bookings_array description]
  * @param  {[type]} room_id [description]
  * @return {[type]}         [description]
  */
 function render_bookings_array(room_id) {
   //Venues = 0
   if (room_id == 0) {
     for (var i = 0; i < bookings_array.length; i++) {
       var new_event = {
         title: bookings_array[i].first_name + '' + bookings_array[i].last_name,
         start: bookings_array[i].start_time,
         end: bookings_array[i].end_time,
         room_name: bookings_array[i].room_name,
         booking_id: bookings_array[i].id,
         className: 'eventbooked',
         allDay: false
       };
       myfullcalendar.fullCalendar('renderEvent', new_event);
     }
   } else {
     // If it has other numbers only render if the room id matches.
     for (var i = 0; i < bookings_array.length; i++) {
       if (bookings_array[i].room_id == room_id) {
         var new_event = {
           title: bookings_array[i].first_name + '' + bookings_array[i].last_name,
           start: bookings_array[i].start_time,
           end: bookings_array[i].end_time,
           room_name: bookings_array[i].room_name,
           booking_id: bookings_array[i].id,
           allDay: false,
           backgroundColor: 'green'
         };
         myfullcalendar.fullCalendar('renderEvent', new_event);
       }
     }
   }

 }


 /**
  * [render_newest_bookings_sidebar description]
  * @param  {[type]} room_id [description]
  * @return {[type]}         [description]
  */
 function render_newest_bookings_sidebar(room_id) {
   //Sort Array by date
   bookings_array.sort(function(a, b) {
     a = new Date(a.start_time);
     b = new Date(b.start_time);
     return a < b ? -1 : a > b ? 1 : 0;
   });


   //Check how many different dates there are

   for (var i = 0; i < bookings_array.length; i++) {

     //Parse an Date Object from startime
     var temp_datetime = Date.parse(bookings_array[i].start_time);
     var temp_date = temp_datetime.toLocaleDateString();
     var temp_date_underscore = temp_date.replace(/\./g, "_");
     var found = $.inArray(temp_date, bookings_array_different_days);
     //If found in array append to panel_temp_date
     if (active_calendar_index == '0') {
       if (found > -1) {
         bookings_sidebar_html.push('$("#panel_' + temp_date_underscore + '").append("' + '<div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div>' + '")');

       } else {

         bookings_array_different_days.push(temp_date);
         //Create new panel with entry

         if (bookings_sidebar_html.length >= 0) {
           bookings_sidebar_html.push('$("#sidebar_content_injection").append("' + '<div class=\'panel panel-default\'><div class=\'panel-heading\'><h4 class=\'panel-title\'><a data-toggle=\'collapse\' data-parent=\'#accordion\' href=\'#' + temp_date_underscore + '\'>' + temp_date + '</a></h4></div><div id=\'' + temp_date_underscore + '\' class=\'panel-collapse collapse\'><div class=\'panel-body\' id=\'panel_' + temp_date_underscore + '\'><div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div></div></div></div>' + '")');

         } else {
           bookings_sidebar_html.push('$("#sidebar_content_injection").append("' + '<div class=\'panel panel-default\'><div class=\'panel-heading\'><h4 class=\'panel-title\'><a data-toggle=\'collapse\' data-parent=\'#accordion\' href=\'#' + temp_date_underscore + '\'>' + temp_date + '</a></h4></div><div id=\'' + temp_date_underscore + '\' class=\'panel-collapse collapse in\'><div class=\'panel-body\' id=\'panel_' + temp_date_underscore + '\'><div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div></div></div></div>' + '")');
         }
       }
     } else {

       if (bookings_array[i].room_id == active_calendar_index) {
         if (found > -1) {
           //  bookings_sidebar_html.push('$("#panel_' + temp_date_underscore + '").append("' + '<p>' + bookings_array[i].title + '</p>' + '")');
           bookings_sidebar_html.push('$("#panel_' + temp_date_underscore + '").append("' + '<div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div>' + '")');
         } else {

           bookings_array_different_days.push(temp_date);
           //Create new panel with entry

           if (bookings_sidebar_html.length >= 0) {
             bookings_sidebar_html.push('$("#sidebar_content_injection").append("' + '<div class=\'panel panel-default\'><div class=\'panel-heading\'><h4 class=\'panel-title\'><a data-toggle=\'collapse\' data-parent=\'#accordion\' href=\'#' + temp_date_underscore + '\'>' + temp_date + '</a></h4></div><div id=\'' + temp_date_underscore + '\' class=\'panel-collapse collapse\'><div class=\'panel-body\' id=\'panel_' + temp_date_underscore + '\'><div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div></div></div></div>' + '")');

           } else {
             bookings_sidebar_html.push('$("#sidebar_content_injection").append("' + '<div class=\'panel panel-default\'><div class=\'panel-heading\'><h4 class=\'panel-title\'><a data-toggle=\'collapse\' data-parent=\'#accordion\' href=\'#' + temp_date_underscore + '\'>' + temp_date + '</a></h4></div><div id=\'' + temp_date_underscore + '\' class=\'panel-collapse collapse in\'><div class=\'panel-body\' id=\'panel_' + temp_date_underscore + '\'><div class=\'row\'><table class=\'table borderless\' id=\'table_' + temp_date_underscore + '\'><tr><td>' + bookings_array[i].first_name + ' ' + bookings_array[i].last_name + '</td><td>' + Date.parse(bookings_array[i].start_time).toLocaleTimeString() + ' - ' + Date.parse(bookings_array[i].end_time).toLocaleTimeString() + '</td><td>' + bookings_array[i].room_name + '</td><td><button class=\'btn btn-primary\' onclick=\'javascript:sidebar_modal(' + bookings_array[i].id + ')\' id=\'bt_' + bookings_array[i].id + '\'>Details</button></td></tr></table></div></div></div></div>' + '")');
           }
         }
       }
     }


   }

   //Evaluate Html output
   for (var i = 0; i < bookings_sidebar_html.length; i++) {
     console.log('HTML: ' + bookings_sidebar_html[i]);
     jQuery.globalEval(bookings_sidebar_html[i]);
   }

   console.log('bookings_sidebar_array: ' + bookings_sidebar_html);


 }



 /**
  * [init_calendar_after_ajax description]
  * @return {[type]} [description]
  */
 function init_calendar_after_ajax() {

   var calendar = $('#calendar').fullCalendar({
     year: init_year,
     month: init_month,
     date: init_day,
     minTime: 7,
     maxTime: 21,
     header: {
       left: 'prev,next today',
       center: 'title',
       right: 'month,agendaWeek,agendaDay'
     },

     eventClick: function(calEvent, jsEvent, view) {
       sidebar_modal(calEvent.booking_id);
       //alert('Event: ' + calEvent.title);
       // change the border color just for fun
       //$(this).css('border-color', 'red');
     },
     selectable: true,
     selectHelper: true,
     select: function(start, end, allDay) {

       //Check if start and end is not bigger than 1 day

       var start_date = start.toLocaleDateString();
       var end_date = end.toLocaleDateString();

       var start_time = start.toLocaleTimeString();
       var end_time = end.toLocaleTimeString();

       //If the range is in the same day
       if (start_date == end_date) {
         //If Venue Calendar is selected
         if (active_calendar_index == '0') {
           $.growl.error({
             message: 'Please select a room first.'
           });
         } else {
           // Adjust the modal text
           $('#modal2_date').val(start_date);
           $('#modal2_start_time').val(start_time);
           $('#modal2_end_time').val(end_time);
           $('#modal2_room_id').val(active_calendar_index);

           //Call Modal
           $('#myModal2').modal();
         }

       } else {
         $.growl.error({
           message: 'The event has to be within a day.'
         });
         //alert('ERROR: YOUR BOOKING CANNOT EXCEED 1 DAY');
       }
       calendar.fullCalendar('unselect');
     },
     events: [

     ]
   });
 } //function end


 /**
  * [set_active_calendar_index description]
  * @param {[type]} index [description]
  */
 /**
 function set_active_calendar_index(index) {





   active_calendar_index = index;
   console.log('set active calendar index to: ' + active_calendar_index);
   bookings_sidebar_html = [];
   bookings_array_different_days = [];
   $('#sidebar_content_injection').empty();
   render_newest_bookings_sidebar(active_calendar_index);
   myfullcalendar.fullCalendar('removeEvents');
   render_bookings_array(active_calendar_index);
 }
**/

$( ".index_switch" ).click(function(event) {
  event.preventDefault();
  var button_id = $(this).attr('id');
  var id_index_array = button_id.split("_");
  var index = id_index_array[2];

   active_calendar_index = index;
   console.log('set active calendar index to: ' + active_calendar_index);
   bookings_sidebar_html = [];
   bookings_array_different_days = [];
   $('#sidebar_content_injection').empty();
   render_newest_bookings_sidebar(active_calendar_index);
   myfullcalendar.fullCalendar('removeEvents');
   render_bookings_array(active_calendar_index);
});







 /**
  * [start_longpolling description]
  * @return {[type]} [description]
  */
 function start_longpolling() {
   console.log('start longpolling function called');

   (function poll() {
     $.ajax({
       url: longpolling_url,
       type: "POST",
       data: {
         'last_updated_at': last_update
       },
       success: function(data) {
         console.log('poll ajax called');
         if (data.calendardata != null) {
           console.log('previous: ' + data.previous_last_update);
           console.log('last update ' + data.last_update);
           console.log('new events calendardata ' + data.calendardata);
           console.log(data.message);

           for (var i = 0; i < data.calendardata.length; i++) {
             bookings_array.push(data.calendardata[i]);
             console.log(data.calendardata[i]);
           }

           myfullcalendar.fullCalendar('removeEvents');
           bookings_sidebar_html = [];
           render_bookings_array(0);
           render_newest_bookings_sidebar(0);
           last_update = data.last_update;
         }
       },
       dataType: "json",
       complete: poll,
       timeout: 30000
     });
   })();
 }



 /**
  * [sidebar_modal description]
  * @param  {[type]} booking_id [description]
  * @return {[type]}            [description]
  */
 function sidebar_modal(booking_id) {
   //Change Modal Text...
   //Call modal
   //Get Ajax Infos
   var booking_details_url = 'http://localhost:8000/express/booking_details';
   $.ajax({
     type: 'POST',
     url: booking_details_url,
     data: {
       booking_id: booking_id
     }
   }).success(function(data) {

     if (data.status == 'Error') {
       $.growl.error({
         message: data.message
       });
     }


     if (data.status == 'OK') {
       $.growl.notice({
         message: data.message
       });
       console.log('details: ' + JSON.stringify(data.booking_details));
       $('#modal_content').text(data.booking_details[0].first_name + ' ' + data.booking_details[0].last_name);
       $('#modal_content').text(data.booking_details[0].comment);
       //Change Modal Text
       $('#myModal').modal();
     }
   });
 };

 /**
  * Insert Custom Event from mymodal2
  * @return {[type]} [description]
  */
 $("#insert_event").click(function() {



   calendar.fullCalendar('renderEvent', {
       title: title,
       start: start,
       end: end,
       allDay: false
     },
     true // make the event "stick"
   );



 });