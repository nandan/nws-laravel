// Express Authentication

$(document).ready(function() {


var login_form = $('#login_form');
var login_url = 'http://localhost:8000/auth/express_login';

// Login

login_form.on('submit', function(e) {
    e.preventDefault(); 
    console.log('Logging in...');
    $.ajax({
        url: login_url, 
        type: 'POST', 
        dataType: 'json', 
        data: login_form.serialize(), 
        beforeSend: function() {

        },
        success: function(data) {
            console.log(data);

            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});
             	window.location.href = "http://localhost:8000/express/calendar";
                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
});



});