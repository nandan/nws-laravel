/*
 * Handling AJAX Authentication with NextOfficeSpace
 * @Date 10/01/2014
 */


$(document).ready(function() {

    // Initialize Variables

    var login_form = $('#login_form');
    var register_form = $('#register_form');
    var login_url = 'http://localhost:8000/auth/login';
    var register_url = 'http://localhost:8000/auth/register';


    // Login

    login_form.on('submit', function(e) {
        e.preventDefault();
        console.log('Logging in...');
        $.ajax({
            url: login_url,
            type: 'POST',
            dataType: 'json',
            data: login_form.serialize(),
            beforeSend: function() {

            },
            success: function(data) {
                console.log(data);

                if (data.status == 'Error') {
                    $.growl.error({
                        message: data.message
                    });
                }


                if (data.status == 'OK') {
                    $.growl.notice({
                        message: data.message
                    });
                    $('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextWorkspace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul><</nav></div></div>');
                    //window.location.href = "http://localhost:8000/";
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    // Register

    register_form.on('submit', function(e) {
        e.preventDefault();
        console.log('Registering new account...');
        $.ajax({
            url: register_url,
            type: 'POST',
            dataType: 'json',
            data: register_form.serialize(),
            beforeSend: function() {

            },
            success: function(data) {
                console.log(data);

                if (data.status == 'Error') {
                    $.growl.error({
                        message: data.message
                    });
                }


                if (data.status == 'OK') {
                    $.growl.notice({
                        message: data.message
                    });
                    register_form.reset();


                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

});