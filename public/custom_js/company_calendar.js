$(document).ready(function() {

var last_poll_datetime = "";
var show_calendar_url = 'http://localhost:8000/controlcenter/show_company_calendar_ajax';
var active_calendar_index= "0"; // All

//Ajax Call init Calendar
 $.ajax({
        url: show_calendar_url, 
        type: 'POST',
        beforeSend: function() {
        },
        success: function(data) {

            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});
                console.log('init calendar...');
                //Init calendar with empty events
                init_calendar_after_ajax();
                //Fetch Initial Events
                //Start Polling
                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });









function init_calendar_after_ajax(){
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();

	var calendar = $('#calendar').fullCalendar({
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		selectable: true,
		selectHelper: true,
		select: function(start, end, allDay) {
			var title = prompt('Event Title:');
			if (title) {
				calendar.fullCalendar('renderEvent',
				{
					title: title,
					start: start,
					end: end,
					allDay: false
				},
						true // make the event "stick"
						);
			}
			calendar.fullCalendar('unselect');
		},
		events: [
		
		]
	});
} //function end


// Sets the global active calendar index from button click
function set_active_calendar_index(index){
	active_calendar_index = index;
}


// Example Events on click

// Delete Button Trigger
$( "#examples" ).click(function(e) {
  e.preventDefault(); 
  add_calendar_events_from_server();
});



// Add Calendar events from json ajax 
function add_calendar_events_from_server(){
var example_events = 'http://localhost:8000/controlcenter/example_calendar_events';
var myfullcalendar = $('#calendar');
 $.ajax({
        url: example_events, 
        type: 'POST',
        beforeSend: function() {
        },
        success: function(data) {
        	console.log(data);
            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});
                console.log(data.calendardata);
          	 	for (var i=0;i<data.calendardata.length;i++)
          	 	{
          	 		var new_event = {
          	 			title: data.calendardata[i].title,
          	 			start: data.calendardata[i].start_time,
          	 			end: data.calendardata[i].end_time,
          	 			allDay: false
          	 		};
          	 		myfullcalendar.fullCalendar( 'renderEvent', new_event);
          	 	}

                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });

}

// Longpolling for Venue Calendar


// Longpolling for Room 



// Fetch Calendar Data for Room ID

function run_calendar_events_room(id){
var run_calendar_events_room_url = 'http://localhost:8000/controlenter/run_calendar_events_room';
var myfullcalendar = $('#calendar');

$.ajax({
        url: example_events, 
        type: 'POST',
        beforeSend: function() {
        },
        success: function(data) {
        	console.log(data);
            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});
               
                //Clear Calendar Events

                // Re-Render new Events


                //Start Longpolling for room
                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });


}





});