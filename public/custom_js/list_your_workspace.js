// Button Start Listing your Workspace

function list_your_workspace(){
	var list_your_workspace_url = 'http://localhost:8000/run_list_your_workspace';
	$.ajax({
		url: list_your_workspace_url, 
		type: 'POST', 
		dataType: 'json', 
		beforeSend: function() {

		},
		success: function(data) {
			console.log(data);


			if (data.status == 'OK') {
			$.growl.notice({message: data.message});
			window.location.href = "http://localhost:8000/wizard/create";
		}

			if (data.status == 'Error') {
			//	$.growl.warning({message: data.message});
			$('#myModal').modal();
		}

	},
	error: function(e) {
		console.log(e);
	}
});
}

// Hide and Show div Form Modal

function toggle_modal_form(){
$('#div_modal_login_form').toggle();
$('#div_modal_register_form').toggle();
$('#modal_footer_register').toggle();
$('#modal_footer_login').toggle();
}



$(document).ready(function() {

// Modal Form Login

var modal_login_form = $('#modal_login_form');
var modal_login_url = 'http://localhost:8000/auth/modal_login';
var modal_register_form = $('#modal_register_form');
var modal_register_url = 'http://localhost:8000/auth/modal_register';


modal_login_form.on('submit', function(e) {
	e.preventDefault(); 
	console.log('Logging in modal...');
	$.ajax({
		url: modal_login_url, 
		type: 'POST', 
		dataType: 'json', 
		data: modal_login_form.serialize(), 
		beforeSend: function() {

		},
		success: function(data) {
			console.log(data);

			if (data.status == 'Error') {
				$.growl.error({message: data.message});
			}


			if (data.status == 'OK' && data.group_id == '1') {
				$.growl.notice({message: data.message});
				$('#header').html('<div class="top-header"><div class="clearfix"><div class="top-header-inner clearfix"><div class="header-currency mr-20"><a href="#"><span><i class="fa-dollar"></i>US Dollar</span></a><div class="currency-show"><ul><li><a href="#"><span><i class="fa-gbp"></i>UK Pound</span></a></li><li><a href="#"><span><i class="fa-euro"></i>EU Euro</span></a></li></ul></div><!-- end other --></div><div class="header-language ml-10"><a href="#" class="english"><span>English</span></a><div class="other_languages"><ul class="clearfix"><li><a href="#" class="deutsch"><span>Deutsch</span></a></li><li><a href="#" class="espanol"><span>Español</span></a></li><li><a href="#" class="italiano"><span>Italiano</span></a></li></ul></div><!-- end other --></div><div class="header-login mr-15"><a href="http://localhost:8000/auth/logout" class="br"><i class="fa-sign-in mi"></i>Logout</a></div><div class="clear xss-mb-10"></div></div><!-- end top-header-inner --></div><!-- end container --></div><!-- end top-header --><div class="clear"></div><div class="large-header my_sticky"><div class="clearfix"><div class="logo"><a href="index.html" title="Logo"><h1>NextOfficeSpace</h1></a></div><!-- end logo --><nav><ul class="sf-menu"><li><a href="http://localhost:8000/controlcenter">Control Center</a></li><li><a href="#">How it works</a></li><li><a href="#">Help Center</a></li><li><a href="#">List your Workspace</a></li></ul></nav></div></div>');
				$('#myModal').modal('hide');
				window.location.href = "http://localhost:8000/wizard/create";
			}
		},
		error: function(e) {
			console.log(e);
		}
	});
});


// Modal Form Register

modal_register_form.on('submit', function(e) {
	e.preventDefault(); 
	console.log('Registering new account...');
	$.ajax({
		url: modal_register_url, 
		type: 'POST', 
		dataType: 'json', 
		data: modal_register_form.serialize(), 
		beforeSend: function() {

		},
		success: function(data) {
			console.log(data);

			if (data.status == 'Error') {
				$.growl.error({message: data.message});
			}


			if (data.status == 'OK') {
				$.growl.notice({message: data.message});
				modal_register_form.reset();
				toggle_modal_form();
				$('#myModal').modal('hide');


			}
		},
		error: function(e) {
			console.log(e);
		}
	});
});
});
