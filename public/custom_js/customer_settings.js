/*
* Customer Settings
*/

$(document).ready(function() {


    var personal_information_form = $('#personal_information_form');
    var personal_information_url = 'http://localhost:8000/controlcenter/run_personal_information';


// Personal Information

personal_information_form.on('submit', function(e) {
    e.preventDefault(); 
    console.log('Updating Personal Information');
    $.ajax({
        url: personal_information_url, 
        type: 'POST', 
        dataType: 'json', 
        data: personal_information_form.serialize(), 
        beforeSend: function() {

        },
        success: function(data) {
            console.log(data);

            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});

                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
});

// Profile Picture Delete

function run_delete_profile_picture(){

    var run_delete_profile_picture = 'http://localhost:8000/controlcenter/run_delete_profile_picture';
    console.log('Deleting current profile picture...');
    $.ajax({
        url: run_delete_profile_picture, 
        type: 'POST', 
        beforeSend: function() {

        },
        success: function(data) {
            console.log(data);

            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});
                $('#profile_picture_div').html("<img data-src='holder.js/200x200'>");
                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });

}

// Delete Button Trigger
$( "#delete_bt" ).click(function(e) {
  e.preventDefault(); 
  run_delete_profile_picture();
});



// Localization Form

    var localization_form = $('#localization_form');
    var localization_url = 'http://localhost:8000/controlcenter/run_localization';

localization_form.on('submit', function(e) {
    e.preventDefault(); 
    console.log('Updating Localization Form');
    $.ajax({
        url: localization_url, 
        type: 'POST', 
        dataType: 'json', 
        data: localization_form.serialize(), 
        beforeSend: function() {

        },
        success: function(data) {
            console.log(data);

            if (data.status == 'Error') {
                $.growl.error({message: data.message});
            }


            if (data.status == 'OK') {
                $.growl.notice({message: data.message});

                
            }
        },
        error: function(e) {
            console.log(e);
        }
    });
});



});